<?php

namespace MikadoCore\CPT\Shortcodes\SectionTitle;

use MikadoCore\Lib;

class SectionTitle implements Lib\ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'mkdf_section_title';
		
		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map(
				array(
					'name'                      => esc_html__( 'Mikado Section Title', 'mkdf-core' ),
					'base'                      => $this->base,
					'category'                  => esc_html__( 'by MIKADO', 'mkdf-core' ),
					'icon'                      => 'icon-wpb-section-title extended-custom-icon',
					'allowed_container_element' => 'vc_row',
					'params'                    => array(
						array(
							'type'        => 'textfield',
							'param_name'  => 'custom_class',
							'heading'     => esc_html__( 'Custom CSS Class', 'mkdf-core' ),
							'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS', 'mkdf-core' )
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'holder_padding',
							'heading'    => esc_html__( 'Holder Side Padding (px or %)', 'mkdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'text_alignment',
							'heading'     => esc_html__( 'Text Alignment', 'mkdf-core' ),
							'value'       => array(
								esc_html__( 'Default', 'mkdf-core' ) => '',
								esc_html__( 'Left', 'mkdf-core' )    => 'left',
								esc_html__( 'Center', 'mkdf-core' )  => 'center'
							),
							'save_always' => true
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Title', 'mkdf-core' ),
							'admin_label' => true
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'title_tag',
							'heading'     => esc_html__( 'Title Tag', 'mkdf-core' ),
							'value'       => array_flip( evently_mikado_get_title_tag( true ) ),
							'save_always' => true,
							'dependency'  => array( 'element' => 'title', 'not_empty' => true ),
							'group'      => esc_html__( 'Title Style', 'mkdf-core' )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'title_color',
							'heading'    => esc_html__( 'Title Color', 'mkdf-core' ),
							'dependency' => array( 'element' => 'title', 'not_empty' => true ),
							'group'      => esc_html__( 'Title Style', 'mkdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'title_mark',
							'heading'     => esc_html__( 'Title Prefix Mark', 'mkdf-core' ),
							'value'       => array(
								esc_html__( 'None', 'mkdf-core' )        => '',
								esc_html__( 'Dot', 'mkdf-core' )         => 'dot',
								esc_html__( 'Three Dots', 'mkdf-core' )  => 'three-dots',
								esc_html__( 'Question', 'mkdf-core' )    => 'question',
								esc_html__( 'Exclamation', 'mkdf-core' ) => 'exclamation'
							),
							'save_always' => true,
							'dependency'  => array( 'element' => 'title', 'not_empty' => true ),
							'group'      => esc_html__( 'Title Style', 'mkdf-core' )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'title_mark_color',
							'heading'    => esc_html__( 'Title Prefix Mark Color', 'mkdf-core' ),
							'dependency' => array( 'element' => 'title_mark', 'not_empty' => true ),
							'group'      => esc_html__( 'Title Style', 'mkdf-core' )
						),
						array(
							'type'       => 'dropdown',
							'param_name' => 'title_separator',
							'heading'    => esc_html__( 'Enable Title Separator', 'mkdf-core' ),
							'value'      => array_flip( evently_mikado_get_yes_no_select_array( false ) ),
							'dependency' => array( 'element' => 'title', 'not_empty' => true ),
							'group'      => esc_html__( 'Title Style', 'mkdf-core' )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'title_separator_color',
							'heading'    => esc_html__( 'Title Separator Color', 'mkdf-core' ),
							'dependency' => array( 'element' => 'title_separator', 'value' => array( 'yes' ) ),
							'group'      => esc_html__( 'Title Style', 'mkdf-core' )
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'title_separator_margin',
							'heading'    => esc_html__( 'Title Separator Top Margin (px)', 'mkdf-core' ),
							'dependency' => array( 'element' => 'title_separator', 'value' => array( 'yes' ) ),
							'group'      => esc_html__( 'Title Style', 'mkdf-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'tagline',
							'heading'     => esc_html__( 'Title Tagline', 'mkdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'tagline_tag',
							'heading'     => esc_html__( 'Tagline Tag', 'mkdf-core' ),
							'value'       => array_flip( evently_mikado_get_title_tag( true ) ),
							'save_always' => true,
							'dependency'  => array( 'element' => 'tagline', 'not_empty' => true ),
							'group'      => esc_html__( 'Tagline Style', 'mkdf-core' )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'tagline_color',
							'heading'    => esc_html__( 'Tagline Color', 'mkdf-core' ),
							'dependency' => array( 'element' => 'tagline', 'not_empty' => true ),
							'group'      => esc_html__( 'Tagline Style', 'mkdf-core' )
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'tagline_margin',
							'heading'    => esc_html__( 'Tagline Bottom Margin (px)', 'mkdf-core' ),
							'dependency' => array( 'element' => 'tagline', 'not_empty' => true ),
							'group'      => esc_html__( 'Tagline Style', 'mkdf-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'subtitle',
							'heading'     => esc_html__( 'Subtitle', 'mkdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'subtitle_tag',
							'heading'     => esc_html__( 'Subtitle Tag', 'mkdf-core' ),
							'value'       => array_flip( evently_mikado_get_title_tag( true ) ),
							'save_always' => true,
							'dependency'  => array( 'element' => 'subtitle', 'not_empty' => true ),
							'group'      => esc_html__( 'Subtitle Style', 'mkdf-core' )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'subtitle_color',
							'heading'    => esc_html__( 'Subtitle Color', 'mkdf-core' ),
							'dependency' => array( 'element' => 'subtitle', 'not_empty' => true ),
							'group'      => esc_html__( 'Subtitle Style', 'mkdf-core' )
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'subtitle_margin',
							'heading'    => esc_html__( 'Subtitle Top Margin (px)', 'mkdf-core' ),
							'dependency' => array( 'element' => 'subtitle', 'not_empty' => true ),
							'group'      => esc_html__( 'Subtitle Style', 'mkdf-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'subtitle_break_words',
							'heading'     => esc_html__( 'Position of Subtitle Line Break', 'mkdf-core' ),
							'description' => esc_html__( 'Enter the position of the word after which you would like to create a line break (e.g. if you would like the line break after the 3rd word, you would enter "3")', 'mkdf-core' ),
							'dependency'  => array( 'element' => 'subtitle', 'not_empty' => true ),
							'group'       => esc_html__( 'Subtitle Style', 'mkdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'disable_break_words',
							'heading'     => esc_html__( 'Disable Line Break for Smaller Screens', 'mkdf-core' ),
							'value'       => array_flip( evently_mikado_get_yes_no_select_array( false ) ),
							'save_always' => true,
							'dependency'  => array( 'element' => 'subtitle_break_words', 'not_empty' => true ),
							'group'       => esc_html__( 'Subtitle Style', 'mkdf-core' )
						),
						array(
							'type'       => 'textarea',
							'param_name' => 'text',
							'heading'    => esc_html__( 'Text', 'mkdf-core' )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'text_color',
							'heading'    => esc_html__( 'Text Color', 'mkdf-core' ),
							'dependency' => array( 'element' => 'text', 'not_empty' => true ),
							'group'      => esc_html__( 'Text Style', 'mkdf-core' )
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'text_margin',
							'heading'    => esc_html__( 'Text Top Margin (px)', 'mkdf-core' ),
							'dependency' => array( 'element' => 'text', 'not_empty' => true ),
							'group'      => esc_html__( 'Text Style', 'mkdf-core' )
						)
					)
				)
			);
		}
	}
	
	public function render( $atts, $content = null ) {
		$args   = array(
			'custom_class'           => '',
			'holder_padding'         => '',
			'text_alignment'         => '',
			'title'                  => '',
			'title_tag'              => 'h2',
			'title_color'            => '',
			'title_mark'             => '',
			'title_mark_color'       => '',
			'title_separator'        => 'no',
			'title_separator_color'  => '',
			'title_separator_margin' => '',
			'tagline'                => '',
			'tagline_tag'            => 'h6',
			'tagline_color'          => '',
			'tagline_margin'         => '',
			'subtitle'               => '',
			'subtitle_tag'           => 'h5',
			'subtitle_color'         => '',
			'subtitle_margin'        => '',
			'subtitle_break_words'   => '',
			'disable_break_words'    => 'no',
			'text'                   => '',
			'text_color'             => '',
			'text_margin'            => ''
		);
		$params = shortcode_atts( $args, $atts );
		
		$params['holder_classes']    = $this->getHolderClasses( $params );
		$params['holder_styles']     = $this->getHolderStyles( $params );
		$params['title_styles']      = $this->getTitleStyles( $params );
		$params['title_mark']        = $this->getTitleMark( $params );
		$params['title_mark_styles'] = $this->getTitleMarkStyles( $params );
		$params['separator_styles']  = $this->getSeparatorStyles( $params );
		$params['tagline_styles']    = $this->getTaglineStyles( $params );
		$params['subtitle']          = $this->getModifiedSubtitle( $params );
		$params['subtitle_styles']   = $this->getSubtitleStyles( $params );
		$params['text_styles']       = $this->getTextStyles( $params );
		
		$params['title_tag']    = ! empty( $params['title_tag'] ) ? $params['title_tag'] : $args['title_tag'];
		$params['tagline_tag']  = ! empty( $params['tagline_tag'] ) ? $params['tagline_tag'] : $args['tagline_tag'];
		$params['subtitle_tag'] = ! empty( $params['subtitle_tag'] ) ? $params['subtitle_tag'] : $args['subtitle_tag'];
		
		$html = mkdf_core_get_shortcode_module_template_part( 'templates/section-title', 'section-title', '', $params );
		
		return $html;
	}
	
	private function getHolderClasses( $params ) {
		$holderClasses = array();
		
		$holderClasses[] = ! empty( $params['custom_class'] ) ? esc_attr( $params['custom_class'] ) : '';
		$holderClasses[] = $params['disable_break_words'] === 'yes' ? 'mkdf-st-disable-subtitle-break' : '';
		
		return implode( ' ', $holderClasses );
	}
	
	private function getHolderStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['holder_padding'] ) ) {
			$styles[] = 'padding: 0 ' . $params['holder_padding'];
		}
		
		if ( ! empty( $params['text_alignment'] ) ) {
			$styles[] = 'text-align: ' . $params['text_alignment'];
		}
		
		return implode( ';', $styles );
	}
	
	private function getTitleStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['title_color'] ) ) {
			$styles[] = 'color: ' . $params['title_color'];
		}
		
		return implode( ';', $styles );
	}
	
	private function getTitleMark( $params ) {
		$mark = '';
		
		if ( ! empty( $params['title_mark'] ) ) {
			switch($params['title_mark']) {
				case 'dot':
					$mark = '.';
					break;
				case 'three-dots':
					$mark = '...';
					break;
				case 'question':
					$mark = '?';
					break;
				case 'exclamation':
					$mark = '!';
					break;
			}
		}
		
		return $mark;
	}
	
	private function getTitleMarkStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['title_mark_color'] ) ) {
			$styles[] = 'color: ' . $params['title_mark_color'];
		}
		
		return implode( ';', $styles );
	}
	
	private function getSeparatorStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['title_separator_color'] ) ) {
			$styles[] = 'background-color: ' . $params['title_separator_color'];
		}
		
		if ( $params['title_separator_margin'] !== '' ) {
			$styles[] = 'margin-top: ' . evently_mikado_filter_px( $params['title_separator_margin'] ) . 'px';
		}
		
		return implode( ';', $styles );
	}
	
	private function getTaglineStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['tagline_color'] ) ) {
			$styles[] = 'color: ' . $params['tagline_color'];
		}
		
		if ( $params['tagline_margin'] !== '' ) {
			$styles[] = 'margin-bottom: ' . evently_mikado_filter_px( $params['tagline_margin'] ) . 'px';
		}
		
		return implode( ';', $styles );
	}
	
	private function getModifiedSubtitle( $params ) {
		$subtitle             = $params['subtitle'];
		$subtitle_break_words = str_replace( ' ', '', $params['subtitle_break_words'] );
		
		if ( ! empty( $subtitle ) ) {
			$split_subtitle = explode( ' ', $subtitle );
			
			if ( ! empty( $subtitle_break_words ) ) {
				if ( ! empty( $split_subtitle[ $subtitle_break_words - 1 ] ) ) {
					$split_subtitle[ $subtitle_break_words - 1 ] = $split_subtitle[ $subtitle_break_words - 1 ] . '<br />';
				}
			}
			
			$subtitle = implode( ' ', $split_subtitle );
		}
		
		return $subtitle;
	}
	
	private function getSubtitleStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['subtitle_color'] ) ) {
			$styles[] = 'color: ' . $params['subtitle_color'];
		}
		
		if ( $params['subtitle_margin'] !== '' ) {
			$styles[] = 'margin-top: ' . evently_mikado_filter_px( $params['subtitle_margin'] ) . 'px';
		}
		
		return implode( ';', $styles );
	}
	
	private function getTextStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['text_color'] ) ) {
			$styles[] = 'color: ' . $params['text_color'];
		}
		
		if ( $params['text_margin'] !== '' ) {
			$styles[] = 'margin-top: ' . evently_mikado_filter_px( $params['text_margin'] ) . 'px';
		}
		
		return implode( ';', $styles );
	}
}