<div class="mkdf-section-title-holder <?php echo esc_attr( $holder_classes ); ?>" <?php echo evently_mikado_get_inline_style( $holder_styles ); ?>>
	<div class="mkdf-st-inner">
		<?php if ( ! empty( $tagline ) ) { ?>
			<<?php echo esc_attr( $tagline_tag ); ?> class="mkdf-st-tagline" <?php echo evently_mikado_get_inline_style( $tagline_styles ); ?>>
				<?php echo wp_kses( $tagline, array( 'br' => true ) ); ?>
			</<?php echo esc_attr( $tagline_tag ); ?>>
		<?php } ?>
		<?php if ( ! empty( $title ) ) { ?>
			<<?php echo esc_attr( $title_tag ); ?> class="mkdf-st-title" <?php echo evently_mikado_get_inline_style( $title_styles ); ?>>
				<?php echo wp_kses( $title, array(
						'br' => true,
						'span' => array(
							'class' => true,
							'style' => true
						)
				) ); ?>
				<span class="mkdf-st-title-mark" <?php echo evently_mikado_get_inline_style( $title_mark_styles ); ?>><?php echo esc_html( $title_mark ); ?></span>
			</<?php echo esc_attr( $title_tag ); ?>>
		<?php } ?>
		<?php if ( $title_separator == 'yes' ) { ?>
			<div class="mkdf-st-separator" <?php echo evently_mikado_get_inline_style( $separator_styles ); ?>></div>
		<?php } ?>
		<?php if ( ! empty( $subtitle ) ) { ?>
			<<?php echo esc_attr( $subtitle_tag ); ?> class="mkdf-st-subtitle" <?php echo evently_mikado_get_inline_style( $subtitle_styles ); ?>>
				<?php echo wp_kses( $subtitle, array( 'br' => true ) ); ?>
			</<?php echo esc_attr( $subtitle_tag ); ?>>
		<?php } ?>
		<?php if ( ! empty( $text ) ) { ?>
			<p class="mkdf-st-text" <?php echo evently_mikado_get_inline_style( $text_styles ); ?>>
				<?php echo wp_kses( $text, array( 'br' => true ) ); ?>
			</p>
		<?php } ?>
	</div>
</div>