<div class="mkdf-ss-holder <?php echo esc_attr( $holder_classes ); ?>">
	<?php if ( ! empty( $image ) ) { ?>
		<div class="mkdf-ss-image" <?php echo evently_mikado_get_inline_style( $image_styles ); ?>>
			<?php echo wp_get_attachment_image( $image, 'full' ); ?>
		</div>
	<?php } ?>
	<div class="mkdf-ss-content" <?php echo evently_mikado_get_inline_style( $content_style ); ?>>
		<?php if ( ! empty( $title ) ) { ?>
			<<?php echo esc_attr( $title_tag ); ?> class="mkdf-ss-title" <?php echo evently_mikado_get_inline_style( $title_styles ); ?>>
				<?php echo esc_html( $title ); ?>
			</<?php echo esc_attr( $title_tag ); ?>>
		<?php } ?>
		<?php if ( ! empty( $text ) ) { ?>
			<p class="mkdf-ss-text"><?php echo do_shortcode( $content ); ?></p>
		<?php } ?>
	</div>
</div>