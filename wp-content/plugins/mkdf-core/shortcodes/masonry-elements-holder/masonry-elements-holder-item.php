<?php
namespace MikadoCore\CPT\Shortcodes\MasonryElementsHolderItem;

use MikadoCore\Lib;

class MasonryElementsHolderItem implements Lib\ShortcodeInterface {
	private $base;

	function __construct() {
		$this->base = 'mkdf_masonry_elements_holder_item';
		add_action('vc_before_init', array($this, 'vcMap'));
	}

	public function getBase() {
		return $this->base;
	}

	public function vcMap() {
		if (function_exists('vc_map')) {
			vc_map(
				array(
					'name'                    => esc_html__('Mikado Masonry Elements Holder Item', 'mkdf-core'),
					'base'                    => $this->base,
					'as_child'                => array('only' => 'mkdf_masonry_elements_holder'),
					'as_parent'               => array('except' => 'vc_row, vc_accordion, no_cover_boxes, no_portfolio_list, no_portfolio_slider'),
					'content_element'         => true,
					'category'                => 'by MIKADO',
					'icon'                    => 'icon-wpb-masonry-elements-holder-item extended-custom-icon',
					'show_settings_on_create' => true,
					'js_view'                 => 'VcColumnView',
					'params'                  => array(
						array(
							'type'        => 'dropdown',
							'class'       => '',
							'heading'     => esc_html__('Size', 'mkdf-core'),
							'param_name'  => 'size',
							'value'       => array(
								esc_html__('Square', 'mkdf-core')                 => 'square',
								esc_html__('Large Width', 'mkdf-core')            => 'large-width',
								esc_html__('Large Height', 'mkdf-core')           => 'large-height',
								esc_html__('Large Width and Height', 'mkdf-core') => 'large-width-height'
							),
							'save_always' => true
						),
						array(
							'type'        => 'dropdown',
							'class'       => '',
							'heading'     => esc_html__('Vertical Alignment', 'mkdf-core'),
							'param_name'  => 'vertical_align',
							'value'       => array(
								esc_html__('Middle', 'mkdf-core') => 'middle',
								esc_html__('Top', 'mkdf-core')    => 'top',
								esc_html__('Bottom', 'mkdf-core') => 'bottom'
							),
							'save_always' => true
						),
						array(
							'type'        => 'textfield',
							'class'       => '',
							'heading'     => esc_html__('Padding', 'mkdf-core'),
							'param_name'  => 'item_padding',
							'value'       => '',
							'description' => esc_html__('Please insert padding in format 0px 10px 0px 10px', 'mkdf-core')
						),
						array(
							'type'       => 'colorpicker',
							'class'      => '',
							'heading'    => esc_html__('Background Color', 'mkdf-core'),
							'param_name' => 'background_color',
							'value'      => ''
						),
						array(
							'type'       => 'attach_image',
							'class'      => '',
							'heading'    => esc_html__('Background Image', 'mkdf-core'),
							'param_name' => 'background_image',
							'value'      => ''
						),
						array(
							'type'        => 'dropdown',
							'class'       => '',
							'heading'     => esc_html__('Shadow', 'mkdf-core'),
							'param_name'  => 'shadow',
							'value'       => array(
								esc_html__('No', 'mkdf-core')  => 'no',
								esc_html__('Yes', 'mkdf-core') => 'yes'
							),
							'save_always' => true
						),
						array(
							'type'        => 'textfield',
							'class'       => '',
							'group'       => esc_html__('Width & Responsiveness', 'mkdf-core'),
							'heading'     => esc_html__('Padding on screen size between 1280px-1440px', 'mkdf-core'),
							'param_name'  => 'item_padding_1280_1440',
							'value'       => '',
							'description' => esc_html__('Please insert padding in format 0px 10px 0px 10px', 'mkdf-core')
						),
						array(
							'type'        => 'textfield',
							'class'       => '',
							'group'       => esc_html__('Width & Responsiveness', 'mkdf-core'),
							'heading'     => esc_html__('Padding on screen size between 1024px-1280px', 'mkdf-core'),
							'param_name'  => 'item_padding_1024_1280',
							'value'       => '',
							'description' => esc_html__('Please insert padding in format 0px 10px 0px 10px', 'mkdf-core')
						),
						array(
							'type'        => 'textfield',
							'class'       => '',
							'group'       => esc_html__('Width & Responsiveness', 'mkdf-core'),
							'heading'     => esc_html__('Padding on screen size between 768px-1024px', 'mkdf-core'),
							'param_name'  => 'item_padding_768_1024',
							'value'       => '',
							'description' => esc_html__('Please insert padding in format 0px 10px 0px 10px', 'mkdf-core')
						),
						array(
							'type'        => 'textfield',
							'class'       => '',
							'group'       => esc_html__('Width & Responsiveness', 'mkdf-core'),
							'heading'     => esc_html__('Padding on screen size between 600px-768px', 'mkdf-core'),
							'param_name'  => 'item_padding_600_768',
							'value'       => '',
							'description' => esc_html__('Please insert padding in format 0px 10px 0px 10px', 'mkdf-core')
						),
						array(
							'type'        => 'textfield',
							'class'       => '',
							'group'       => esc_html__('Width & Responsiveness', 'mkdf-core'),
							'heading'     => esc_html__('Padding on screen size between 480px-600px', 'mkdf-core'),
							'param_name'  => 'item_padding_480_600',
							'value'       => '',
							'description' => esc_html__('Please insert padding in format 0px 10px 0px 10px', 'mkdf-core')
						),
						array(
							'type'        => 'textfield',
							'class'       => '',
							'group'       => esc_html__('Width & Responsiveness', 'mkdf-core'),
							'heading'     => esc_html__('Padding on Screen Size Bellow 480px', 'mkdf-core'),
							'param_name'  => 'item_padding_480',
							'value'       => '',
							'description' => esc_html__('Please insert padding in format 0px 10px 0px 10px', 'mkdf-core')
						)
					)
				)
			);
		}
	}

	public function render($atts, $content = null) {
		$args = array(
			'size'                   => '',
			'vertical_align'         => '',
			'item_padding'           => '',
			'background_color'       => '',
			'background_image'       => '',
			'shadow'                 => '',
			'item_padding_1280_1440' => '',
			'item_padding_1024_1280' => '',
			'item_padding_768_1024'  => '',
			'item_padding_600_768'   => '',
			'item_padding_480_600'   => '',
			'item_padding_480'       => ''
		);

		$params = shortcode_atts($args, $atts);
		extract($params);

		$params['content'] = $content;

		$rand_class = 'mkdf-masonry-elements-item-custom-' . mt_rand(100000, 1000000);

		$params['holder_classes']    = $this->getClasses($params);
		$params['holder_style']      = $this->getStyle($params);
		$params['holder_background'] = $this->getBackground($params);
		$params['inner_style']       = $this->getInnerStyle($params);

		$params['item_class'] = $rand_class;
		$params['item_data']  = $this->getData($params);

		$html = mkdf_core_get_shortcode_module_template_part('templates/masonry-elements-holder-item-template', 'masonry-elements-holder', '', $params);

		return $html;
	}

	/**
	 * Generates holder classes
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getClasses($params) {
		$classes = array(
			'mkdf-masonry-elements-item'
		);

		$classes[] = 'mkdf-' . $params['size'];

		if (isset($params['shadow']) && $params['shadow'] == 'yes') {
			$classes[] = 'mkdf-masonry-elements-holder-shadow';
		}

		return $classes;
	}

	/**
	 * Return Masonry Elements Holder Item style
	 *
	 * @param $params
	 *
	 * @return array
	 */
	private function getStyle($params) {

		$style = array();

		if (isset($params['item_padding']) && $params['item_padding'] !== '') {
			$style[] = 'padding: ' . $params['item_padding'];
		}

		return $style;
	}

	private function getBackground($params) { 
		$style = array();

		if (isset($params['background_image']) && $params['background_image'] !== '') {
			$style[] = 'background-image: url(' . wp_get_attachment_url($params['background_image']) . ')';
		}

		if (isset($params['background_color']) && $params['background_color'] !== '') {
			$style[] = 'background-color: ' . $params['background_color'];
		}

		return $style;
	}

	private function getInnerStyle($params) {
		$style = array();

		if (isset($params['vertical_align']) && $params['vertical_align'] !== '') {
			$style[] = 'vertical-align: ' . $params['vertical_align'];
		}

		return $style;
	}

	private function getData($params) {
		$data = array();

		$data['data-item-class'] = $params['item_class'];

		if ($params['item_padding_1280_1440'] !== '') {
			$data['data-1280-1440'] = $params['item_padding_1280_1440'];
		}

		if ($params['item_padding_1024_1280'] !== '') {
			$data['data-1024-1280'] = $params['item_padding_1024_1280'];
		}

		if ($params['item_padding_768_1024'] !== '') {
			$data['data-768-1024'] = $params['item_padding_768_1024'];
		}

		if ($params['item_padding_600_768'] !== '') {
			$data['data-600-768'] = $params['item_padding_600_768'];
		}

		if ($params['item_padding_480_600'] !== '') {
			$data['data-480-600'] = $params['item_padding_480_600'];
		}

		if ($params['item_padding_480'] !== '') {
			$data['data-480'] = $params['item_padding_480'];
		}

		return $data;
	}
}
