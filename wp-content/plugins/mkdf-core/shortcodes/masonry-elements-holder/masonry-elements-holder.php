<?php
namespace MikadoCore\CPT\Shortcodes\MasonryElementsHolder;

use MikadoCore\Lib;

class MasonryElementsHolder implements Lib\ShortcodeInterface {
	private $base;

	function __construct() {
		$this->base = 'mkdf_masonry_elements_holder';
		add_action('vc_before_init', array($this, 'vcMap'));
	}

	public function getBase() {
		return $this->base;
	}

	public function vcMap() {
		vc_map(array(
			'name'      => esc_html__('Mikado Masonry Elements Holder', 'mkdf-core'),
			'base'      => $this->base,
			'icon'      => 'icon-wpb-masonry-elements-holder extended-custom-icon',
			'category'  => 'by MIKADO',
			'as_parent' => array('only' => 'mkdf_masonry_elements_holder_item'),
			'js_view'   => 'VcColumnView',
			'params'    => array(
				array(
					'type'        => 'dropdown',
					'class'       => '',
					'heading'     => esc_html__('Columns', 'mkdf-core'),
					'param_name'  => 'columns',
					'value'       => array(
						esc_html__('Two', 'mkdf-core')   => 'two',
						esc_html__('Three', 'mkdf-core') => 'three',
						esc_html__('Four', 'mkdf-core')  => 'four'
					),
					'save_always' => true
				)
			)
		));
	}

	public function render($atts, $content = null) {

		$args = array(
			'columns' => ''
		);

		$params = shortcode_atts($args, $atts);
		extract($params);

		$params['content'] = $content;

		$params['holder_classes'] = $this->getClasses($params);

		$html = mkdf_core_get_shortcode_module_template_part('templates/masonry-elements-holder-template', 'masonry-elements-holder', '', $params);

		return $html;
	}

	/**
	 * Generates holder classes
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getClasses($params) {
		$classes = array(
			'mkdf-masonry-elements-holder'
		);

		if ($params['columns'] !== '') {
			$classes[] = 'mkdf-masonry-' . esc_attr($params['columns']) . '-columns';
		}

		return $classes;
	}
}