<div <?php evently_mikado_class_attribute($holder_classes); ?>>
	<div class="mkdf-masonry-elements-grid-sizer"></div>
	<?php echo do_shortcode($content); ?>
</div>