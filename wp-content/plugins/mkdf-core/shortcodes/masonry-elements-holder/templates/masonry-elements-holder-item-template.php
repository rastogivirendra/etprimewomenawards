<div <?php evently_mikado_class_attribute($holder_classes); ?>>
	<div class="mkdf-masonry-elements-item-inner <?php echo esc_attr($item_class); ?>" <?php evently_mikado_inline_style($holder_style); ?> <?php echo evently_mikado_get_inline_attrs($item_data); ?>>
		<span class="mkdf-masonry-elements-item-background" <?php evently_mikado_inline_style($holder_background); ?>></span>
		<div class="mkdf-masonry-elements-item-inner-helper">
			<div class="mkdf-masonry-elements-item-inner-tb">
				<div class="mkdf-masonry-elements-item-inner-tc" <?php evently_mikado_inline_style($inner_style); ?>>
					<?php echo do_shortcode($content); ?>
				</div>
			</div>
		</div>
	</div>
</div>