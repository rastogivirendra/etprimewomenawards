<div class="mkdf-vss-ms-section" <?php echo evently_mikado_get_inline_attrs($content_data); ?> <?php evently_mikado_inline_style($content_style);?>>
	<?php echo do_shortcode($content); ?>
</div>