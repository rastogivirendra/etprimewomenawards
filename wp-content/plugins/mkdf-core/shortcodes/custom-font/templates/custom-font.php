<<?php echo esc_attr($title_tag); ?> class="mkdf-custom-font-holder <?php echo esc_attr($holder_classes); ?>" <?php evently_mikado_inline_style($holder_styles); ?> <?php echo evently_mikado_get_inline_attrs($holder_data); ?>>
	<span class="mkdf-st-title-label">
		<?php echo esc_html($title); ?>
		<?php if ( ! empty( $title_mark ) ) { ?>
			<span class="mkdf-st-title-mark" <?php echo evently_mikado_get_inline_style( $title_mark_styles ); ?>><?php echo esc_html( $title_mark ); ?></span>
		<?php } ?>
	</span>
</<?php echo esc_attr($title_tag); ?>>