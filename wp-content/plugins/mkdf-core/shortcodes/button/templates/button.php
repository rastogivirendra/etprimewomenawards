<button type="submit" <?php evently_mikado_inline_style($button_styles); ?> <?php evently_mikado_class_attribute($button_classes); ?> <?php echo evently_mikado_get_inline_attrs($button_data); ?> <?php echo evently_mikado_get_inline_attrs($button_custom_attrs); ?>>
	<?php if( $icon_position === 'left' ): ?>
		<?php echo evently_mikado_icon_collections()->renderIcon($icon, $icon_pack); ?>
	<?php endif; ?>
	<span class="mkdf-btn-text"><?php echo esc_html($text); ?></span>
	<?php if( $icon_position === 'right' ): ?>
		<?php echo evently_mikado_icon_collections()->renderIcon($icon, $icon_pack); ?>
	<?php endif; ?>
</button>