<div class="mkdf-image-with-text-holder <?php echo esc_attr($holder_classes); ?>">
	<div class="mkdf-iwt-image">
		<?php if ($image_behavior === 'lightbox') { ?>
		<a itemprop="image" href="<?php echo esc_url($image['url']); ?>" data-rel="prettyPhoto[iwt_pretty_photo]" title="<?php echo esc_attr($image['alt']); ?>">
			<?php } else if ($image_behavior === 'custom-link' && !empty($custom_link)) { ?>
			<a itemprop="url" href="<?php echo esc_url($custom_link); ?>" target="<?php echo esc_attr($custom_link_target); ?>">
				<?php } ?>
				<?php if(is_array($image_size) && count($image_size)) : ?>
					<?php echo evently_mikado_generate_thumbnail($image['image_id'], null, $image_size[0], $image_size[1]); ?>
				<?php else: ?>
					<?php echo wp_get_attachment_image($image['image_id'], $image_size); ?>
				<?php endif; ?>
				<?php if ($image_behavior === 'lightbox' || $image_behavior === 'custom-link') { ?>
			</a>
		<?php } ?>
	</div>
	<div class="mkdf-iwt-content-holder">
		<<?php echo esc_attr($title_tag); ?> class="mkdf-iwt-text-holder">
			<?php echo esc_html($title); ?>
			<?php if ( ! empty( $title_mark ) ) { ?>
				<span class="mkdf-iwt-title-mark" <?php echo evently_mikado_get_inline_style( $title_mark_styles ); ?>><?php echo esc_html( $title_mark ); ?></span>
			<?php } ?>
		</<?php echo esc_attr($title_tag); ?>>

		<?php if ( ! empty( $custom_link ) ): ?>
			<?php echo evently_mikado_get_button_html( array(
				'custom_class'           => 'mkdf-iwt-button',
				'text'                   => esc_html__('Read More', 'mkdf-core'),
				'type'                   => 'simple',
				'link'                   => ! empty( $custom_link ) ? esc_url( $custom_link ) : '#',
				'target'                 => ! empty( $custom_link_target ) ? esc_attr( $custom_link_target ) : '_self',
				'margin'                 => '29px 0 0 3px'
			) ); ?>
		<?php endif; ?>
	</div>
</div>