(function($) {
	'use strict';
	
	var parallaxHolder = {};
	mkdf.modules.parallaxHolder = parallaxHolder;

	parallaxHolder.mkdfInitElementsHolderResponsiveStyle = parallaxHolder;


	parallaxHolder.mkdfOnWindowLoad = mkdfOnWindowLoad;
	
	$(window).load(mkdfOnWindowLoad);
	
	/*
	 All functions to be called on $(document).ready() should be in this function
	 */
	function mkdfOnWindowLoad() {
		mkdfParallaxHolder();
	}

	function mkdfParallaxHolder() {
		var parallaxElements = $('.mkdf-parallax-holder, .mkdf-parallax-item');

		if (parallaxElements.length && !mkdf.htmlEl.hasClass('touch')) {
			    parallaxElements.each(function(){
			        var parallaxElement = $(this),
                    	randCoeff = (Math.floor(Math.random() * 3) + 1 ) * 0.1,
                    	delta = -Math.round(parallaxElement.height() * randCoeff),
						dataParallax = '{"y":'+delta+', "smoothness":20}';

		            parallaxElement.attr('data-parallax', dataParallax);
			    });

		    setTimeout(function(){
		        ParallaxScroll.init(); //initialzation removed from plugin js file to have it run only on non-touch devices
		    }, 100); //wait for calcs
		}
	}
	
})(jQuery);