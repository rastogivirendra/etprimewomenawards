<?php

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
	class WPBakeryShortCode_Mkdf_Parallax_Holder extends WPBakeryShortCodesContainer {}
}

if ( ! function_exists( 'mkdf_core_enqueue_scripts_for_parallax_holder_shortcodes' ) ) {
	/**
	 * Function that includes all necessary 3rd party scripts for this shortcode
	 */
	function mkdf_core_enqueue_scripts_for_parallax_holder_shortcodes() {
		wp_enqueue_script( 'parallax-scroll', MIKADO_CORE_SHORTCODES_URL_PATH . '/parallax-holder/assets/js/plugins/jquery.parallax-scroll.js', array( 'jquery' ), false, true );
	}
	
	add_action( 'evently_mikado_action_enqueue_third_party_scripts', 'mkdf_core_enqueue_scripts_for_parallax_holder_shortcodes' );
}

if ( ! function_exists( 'mkdf_core_add_parallax_holder_shortcodes' ) ) {
	function mkdf_core_add_parallax_holder_shortcodes( $shortcodes_class_name ) {
		$shortcodes = array(
			'MikadoCore\CPT\Shortcodes\ParallaxHolder\ParallaxHolder'
		);
		
		$shortcodes_class_name = array_merge( $shortcodes_class_name, $shortcodes );
		
		return $shortcodes_class_name;
	}
	
	add_filter( 'mkdf_core_filter_add_vc_shortcode', 'mkdf_core_add_parallax_holder_shortcodes' );
}

if ( ! function_exists( 'mkdf_core_set_parallax_holder_icon_class_name_for_vc_shortcodes' ) ) {
	/**
	 * Function that set custom icon class name for pie chart shortcode to set our icon for Visual Composer shortcodes panel
	 */
	function mkdf_core_set_parallax_holder_icon_class_name_for_vc_shortcodes( $shortcodes_icon_class_array ) {
		$shortcodes_icon_class_array[] = '.icon-wpb-parallax-holder';
		
		return $shortcodes_icon_class_array;
	}
	
	add_filter( 'mkdf_core_filter_add_vc_shortcodes_custom_icon_class', 'mkdf_core_set_parallax_holder_icon_class_name_for_vc_shortcodes' );
}