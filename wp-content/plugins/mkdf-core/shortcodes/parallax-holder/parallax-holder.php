<?php
namespace MikadoCore\CPT\Shortcodes\ParallaxHolder;

use MikadoCore\Lib;

class ParallaxHolder implements Lib\ShortcodeInterface{

	private $base;

	function __construct() {
		$this->base = 'mkdf_parallax_holder';
		add_action('vc_before_init', array($this, 'vcMap'));
	}

	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Mikado Parallax Holder', 'mkdf-core'),
			'base' => $this->base,
			'icon' => 'icon-wpb-parallax-holder extended-custom-icon',
			'category' => esc_html__('by MIKADO', 'mkdf-core'),
			'as_parent' => array('except' => ''),
			'js_view' => 'VcColumnView',
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Y Axis Translation', 'mkdf-core'),
					'admin_label' => true,
					'param_name' => 'y_axis_translation',
					'value' => '-200',
					'description' => esc_html__('Enter the value in pixels. Negative value changes parallax direction.', 'mkdf-core')
				),
			)
		));
	}

	public function render($atts, $content = null) {
	
		$args = array(
			'y_axis_translation' => '-200',
		);
		$params = shortcode_atts($args, $atts);

		$params['holder_data'] = $this->getHolderData($params);

		$params['content']     = $content;

		$html = mkdf_core_get_shortcode_module_template_part( 'templates/parallax-holder', 'parallax-holder', '', $params );

		return $html;

	}

	public function getHolderData($params) {
		$data = array();

		$y_absolute = evently_mikado_filter_px($params['y_axis_translation']);
		$smoothness = 20; //1 is for linear, non-animated parallax

		$data['data-parallax'] = '{&quot;y&quot;: '.$y_absolute.', &quot;smoothness&quot;: '.$smoothness.'}';

		return $data;
	}

}
