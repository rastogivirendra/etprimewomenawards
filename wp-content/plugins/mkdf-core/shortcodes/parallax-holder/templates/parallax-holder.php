<div class="mkdf-parallax-holder" <?php echo evently_mikado_get_inline_attrs($holder_data); ?>>
	<?php echo do_shortcode($content); ?>
</div>