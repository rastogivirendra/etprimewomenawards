<?php
namespace MikadoCore\CPT\Shortcodes\Banner;

use MikadoCore\Lib;

class Banner implements Lib\ShortcodeInterface {
	private $base;
	
	public function __construct() {
		$this->base = 'mkdf_banner';
		
		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map(
				array(
					'name'                      => esc_html__( 'Mikado Banner', 'mkdf-core' ),
					'base'                      => $this->getBase(),
					'category'                  => esc_html__( 'by MIKADO', 'mkdf-core' ),
					'icon'                      => 'icon-wpb-banner extended-custom-icon',
					'allowed_container_element' => 'vc_row',
					'params'                    => array(
						array(
							'type'        => 'textfield',
							'param_name'  => 'custom_class',
							'heading'     => esc_html__( 'Custom CSS Class', 'mkdf-core' ),
							'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS', 'mkdf-core' )
						),
						array(
							'type'        => 'attach_image',
							'param_name'  => 'image',
							'heading'     => esc_html__( 'Image', 'mkdf-core' ),
							'description' => esc_html__( 'Select image from media library', 'mkdf-core' )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'overlay_color',
							'heading'    => esc_html__( 'Image Overlay Color', 'mkdf-core' )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'overlay_hover_color',
							'heading'    => esc_html__( 'Image Overlay Hover Color', 'mkdf-core' )
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'title',
							'heading'    => esc_html__( 'Title', 'mkdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'title_tag',
							'heading'     => esc_html__( 'Title Tag', 'mkdf-core' ),
							'value'       => array_flip( evently_mikado_get_title_tag( true, array( 'p' => 'p' ) ) ),
							'save_always' => true,
							'dependency'  => array( 'element' => 'title', 'not_empty' => true )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'title_size',
							'heading'     => esc_html__( 'Title Font Size (px or em)', 'mkdf-core' ),
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'title_color',
							'heading'    => esc_html__( 'Title Color', 'mkdf-core' ),
							'dependency' => array( 'element' => 'title', 'not_empty' => true )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'title_mark',
							'heading'     => esc_html__( 'Title Prefix Mark', 'mkdf-core' ),
							'value'       => array(
								esc_html__( 'None', 'mkdf-core' )        => '',
								esc_html__( 'Dot', 'mkdf-core' )         => 'dot',
								esc_html__( 'Three Dots', 'mkdf-core' )  => 'three-dots',
								esc_html__( 'Question', 'mkdf-core' )    => 'question',
								esc_html__( 'Exclamation', 'mkdf-core' ) => 'exclamation'
							),
							'save_always' => true,
							'dependency'  => array( 'element' => 'title', 'not_empty' => true )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'title_mark_color',
							'heading'    => esc_html__( 'Title Prefix Mark Color', 'mkdf-core' ),
							'dependency' => array( 'element' => 'title_mark', 'not_empty' => true )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'subtitle',
							'heading'     => esc_html__( 'Hover Title', 'mkdf-core' ),
							'description' => esc_html__( 'Visible On hover', 'mkdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'subtitle_tag',
							'heading'     => esc_html__( 'Hover Title Tag', 'mkdf-core' ),
							'value'       => array_flip( evently_mikado_get_title_tag( true, array( 'p' => 'p' ) ) ),
							'save_always' => true,
							'dependency'  => array( 'element' => 'subtitle', 'not_empty' => true )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'subtitle_color',
							'heading'    => esc_html__( 'Hover Title Color', 'mkdf-core' ),
							'dependency' => array( 'element' => 'subtitle', 'not_empty' => true )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'subtitle_size',
							'heading'     => esc_html__( 'Hover Title Font Size (px or em)', 'mkdf-core' ),
							'description' => esc_html__( 'Visible On hover', 'mkdf-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'text',
							'heading'     => esc_html__( 'Text', 'mkdf-core' ),
							'description' => esc_html__( 'Visible On hover', 'mkdf-core' )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'text_color',
							'heading'    => esc_html__( 'Text Color', 'mkdf-core' ),
							'dependency' => array( 'element' => 'text', 'not_empty' => true )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'button_text',
							'heading'     => esc_html__( 'Button Text', 'mkdf-core' ),
							'description' => esc_html__( 'Visible On hover', 'mkdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'button_style',
							'heading'     => esc_html__( 'Button Hover Style', 'mkdf-core' ),
							'value'       => array(
								esc_html__( 'Default', 'mkdf-core' )     => '',
								esc_html__( 'Dark Blue', 'mkdf-core' )  => 'mkdf-btn-main',
							),
							'save_always' => true,
							'dependency' => array( 'element' => 'button_text', 'not_empty' => true )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'link',
							'heading'     => esc_html__( 'Link', 'mkdf-core' ),
							'dependency' => array( 'element' => 'button_text', 'not_empty' => true )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'target',
							'heading'     => esc_html__( 'Link Target', 'mkdf-core' ),
							'value'       => array_flip( evently_mikado_get_link_target_array() ),
							'dependency' => array( 'element' => 'link', 'not_empty' => true )
						)
					)
				)
			);
		}
	}
	
	public function render( $atts, $content = null ) {
		$args   = array(
			'custom_class'         => '',
			'image'                => '',
			'overlay_color'        => '',
			'overlay_hover_color'  => '',
			'subtitle'             => '',
			'subtitle_tag'         => 'h3',
			'subtitle_size'        => '',
			'subtitle_color'       => '',
			'title'                => '',
			'title_tag'            => 'h3',
			'title_size'           => '',
			'title_color'          => '',
			'title_mark'           => '',
			'title_mark_color'     => '',
			'text'                 => '',
			'text_color'           => '',
			'button_text'          => '',
			'button_style'         => '',
			'link'                 => '',
			'target'               => '_self',
		);
		$params = shortcode_atts( $args, $atts );
		
		$params['holder_classes']       = $this->getHolderClasses( $params, $args );
		$params['overlay_styles']       = $this->getOverlayStyles( $params );
		$params['overlay_hover_styles'] = $this->getOverlayHoverStyles( $params );
		$params['subtitle_tag']         = ! empty( $subtitle_tag ) ? $params['subtitle_tag'] : $args['subtitle_tag'];
		$params['subtitle_styles']      = $this->getSubitleStyles( $params );
		$params['title_tag']            = ! empty( $params['title_tag'] ) ? $params['title_tag'] : $args['title_tag'];
		$params['title_styles']         = $this->getTitleStyles( $params );
		$params['title_mark']           = $this->getTitleMark( $params );
		$params['title_mark_styles']    = $this->getTitleMarkStyles( $params );
		$params['text_styles']          = $this->getTextStyles( $params );
		$params['target']               = !empty( $params['button_target'] ) ? $params['button_target'] : '_self';
		
		$html = mkdf_core_get_shortcode_module_template_part( 'templates/banner', 'banner', '', $params );
		
		return $html;
	}
	
	private function getHolderClasses( $params, $args ) {
		$holderClasses = array();
		
		$holderClasses[] = ! empty( $params['custom_class'] ) ? esc_attr( $params['custom_class'] ) : '';
		$holderClasses[] = ! empty( $params['button_style'] ) ? esc_attr( $params['button_style'] ) : '';
		
		return implode( ' ', $holderClasses );
	}
	
	private function getOverlayStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['overlay_color'] ) ) {
			$styles[] = 'background-color: ' . $params['overlay_color'];
		}
		
		return implode( ';', $styles );
	}


	private function getOverlayHoverStyles( $params ) {
		$styles = array();

		if ( ! empty( $params['overlay_hover_color'] ) ) {
			$styles[] = 'background-color: ' . $params['overlay_hover_color'];
		}

		return implode( ';', $styles );
	}
	
	private function getSubitleStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['subtitle_color'] ) ) {
			$styles[] = 'color: ' . $params['subtitle_color'];
		}

		if ( ! empty( $params['subtitle_size'] ) ) {
			$styles[] = 'font-size: ' . $params['subtitle_size'];
		}

		return $styles;
	}
	
	private function getTitleStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['title_color'] ) ) {
			$styles[] = 'color: ' . $params['title_color'];
		}

		if ( ! empty( $params['title_size'] ) ) {
			$styles[] = 'font-size: ' . $params['title_size'];
		}
		
		if ( ! empty( $params['title_top_margin'] ) ) {
			$styles[] = 'margin-top: ' . evently_mikado_filter_px( $params['title_top_margin'] ) . 'px';
		}
		
		return implode( ';', $styles );
	}

	private function getTitleMark( $params ) {
		$mark = '';

		if ( ! empty( $params['title_mark'] ) ) {
			switch($params['title_mark']) {
				case 'dot':
					$mark = '.';
					break;
				case 'three-dots':
					$mark = '...';
					break;
				case 'question':
					$mark = '?';
					break;
				case 'exclamation':
					$mark = '!';
					break;
			}
		}

		return $mark;
	}

	private function getTitleMarkStyles( $params ) {
		$styles = array();

		if ( ! empty( $params['title_mark_color'] ) ) {
			$styles[] = 'color: ' . $params['title_mark_color'];
		}

		return implode( ';', $styles );
	}

	private function getTextStyles( $params ) {
		$styles = array();

		if ( ! empty( $params['text_color'] ) ) {
			$styles[] = 'color: ' . $params['text_color'];
		}

		return implode( ';', $styles );
	}
}