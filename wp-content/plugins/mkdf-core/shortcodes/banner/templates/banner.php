<div class="mkdf-banner-holder <?php echo esc_attr($holder_classes); ?>">
    <div class="mkdf-banner-image">
        <?php echo wp_get_attachment_image($image, 'full'); ?>
    </div>
    <div class="mkdf-banner-text-holder" <?php echo evently_mikado_get_inline_style($overlay_styles); ?>>
	    <div class="mkdf-banner-text-outer">
		    <div class="mkdf-banner-text-inner">
		        <?php if(!empty($title)) { ?>
		            <<?php echo esc_attr($title_tag); ?> class="mkdf-banner-title" <?php echo evently_mikado_get_inline_style($title_styles); ?>>
			            <span class="mkdf-banner-title-label"><?php echo esc_html($title); ?>
						    <?php if ( ! empty( $title_mark ) ) { ?>
							    <span class="mkdf-banner-title-mark" <?php echo evently_mikado_get_inline_style( $title_mark_styles ); ?>><?php echo esc_html( $title_mark ); ?></span>
						    <?php } ?>
				        </span>
	                </<?php echo esc_attr($title_tag); ?>>
		        <?php } ?>
			</div>
			<div class="mkdf-banner-hover-layout" <?php echo evently_mikado_get_inline_style($overlay_hover_styles); ?>>
				<div class="mkdf-banner-hover-wrap">
					<div class="mkdf-banner-hover-inner">
						<?php if(!empty($subtitle)) { ?>
							<<?php echo esc_attr($subtitle_tag); ?> class="mkdf-banner-hover-title" <?php echo evently_mikado_get_inline_style($subtitle_styles); ?>>
								<?php echo esc_html($subtitle); ?>
							</<?php echo esc_attr($subtitle_tag); ?>>
					    <?php } ?>
					    <?php if(!empty($text)) { ?>
						    <p class="mkdf-banner-hover-text" <?php echo evently_mikado_get_inline_style($text_styles); ?>><?php echo esc_html($text); ?></p>
					    <?php } ?>
						<?php if(!empty($link) && !empty($button_text)) { ?>
							<div class="mkdf-banner-button">
								<a itemprop="url" href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>" class="mkdf-btn mkdf-btn-solid mkdf-btn-medium mkdf-btn-custom-hover-bg mkdf-btn-custom-border-hover">
									<span class="mkdf-btn-text"><?php echo esc_html($button_text); ?></span>
								</a>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>