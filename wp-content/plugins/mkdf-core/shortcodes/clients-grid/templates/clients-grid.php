<div class="mkdf-clients-wc-holder <?php echo esc_attr($holder_classes); ?>">
	<div class="mkdf-ch-inner">
		<?php echo do_shortcode($content); ?>
	</div>
</div>