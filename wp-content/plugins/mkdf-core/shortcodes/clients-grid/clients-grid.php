<?php
namespace MikadoCore\CPT\Shortcodes\ClientsHolder;

use MikadoCore\Lib;

class ClientsHolder implements Lib\ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'mkdf_clients_holder';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if(function_exists('vc_map')) {
			vc_map(
				array(
					'name'      => esc_html__( 'Mikado Clients Grid', 'mkdf-core' ),
					'base'      => $this->base,
					'icon'      => 'icon-wpb-clients-wc-holder extended-custom-icon',
					'category'  => esc_html__( 'by MIKADO', 'mkdf-core' ),
					'as_parent' => array( 'only' => 'mkdf_clients_carousel_item' ),
					'js_view'   => 'VcColumnView',
					'params'    => array(
						array(
							'type'        => 'dropdown',
							'param_name'  => 'number_of_columns',
							'heading'     => esc_html__( 'Number of Columns', 'mkdf-core' ),
							'value'       => array(
								esc_html__( 'One', 'mkdf-core' )   => '1',
								esc_html__( 'Two', 'mkdf-core' )   => '2',
								esc_html__( 'Three', 'mkdf-core' ) => '3',
								esc_html__( 'Four', 'mkdf-core' )  => '4',
								esc_html__( 'Five', 'mkdf-core' )  => '5',
								esc_html__( 'Six', 'mkdf-core' )   => '6'
							),
							'save_always' => true
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'space_between_columns',
							'heading'     => esc_html__( 'Space Between Columns', 'mkdf-core' ),
							'value'       => array(
								esc_html__( 'Normal', 'mkdf-core' )   => 'normal',
								esc_html__( 'Small', 'mkdf-core' )    => 'small',
								esc_html__( 'Tiny', 'mkdf-core' )     => 'tiny',
								esc_html__( 'No Space', 'mkdf-core' ) => 'no'
							),
							'save_always' => true
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'image_alignment',
							'heading'     => esc_html__( 'Item Horizontal Alignment', 'mkdf-core' ),
							'value'       => array(
								esc_html__( 'Default', 'mkdf-core' ) => '',
								esc_html__( 'Left', 'mkdf-core' )    => 'left',
								esc_html__( 'Center', 'mkdf-core' )  => 'center',
								esc_html__( 'Right', 'mkdf-core' )   => 'right'
							),
							'save_always' => true
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'items_hover_animation',
							'heading'     => esc_html__( 'Items Hover Animation', 'mkdf-core' ),
							'value'       => array(
								esc_html__( 'Switch Images', 'mkdf-core' ) => 'switch-images',
								esc_html__( 'Roll Over', 'mkdf-core' )     => 'roll-over',
								esc_html__( 'Show Shadow', 'mkdf-core' )   => 'shadow',
								esc_html__( 'Zoom Image', 'mkdf-core' )    => 'zoom'
							),
							'save_always' => true
						)
					)
				)
			);
		}
	}

	public function render($atts, $content = null) {
		$args = array(
			'number_of_columns' 	=> '3',
			'space_between_columns'	=> 'normal',
			'image_alignment' 	    => '',
			'items_hover_animation' => 'switch-images'
		);
		$params = shortcode_atts($args, $atts);
		
		$params['holder_classes'] = $this->getHolderClasses($params, $args);
		$params['content'] = $content;

		$html = mkdf_core_get_shortcode_module_template_part( 'templates/clients-grid', 'clients-grid', '', $params );

		return $html;
	}

	/**
	 * Generates holder classes
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getHolderClasses($params, $args) {
		$holderClasses = '';
		
		$holderClasses .= !empty($params['number_of_columns']) ? ' mkdf-ch-columns-' . $params['number_of_columns'] : ' mkdf-ch-columns-' . $args['number_of_columns'];
		$holderClasses .= !empty($params['space_between_columns']) ? ' mkdf-ch-' . $params['space_between_columns'] . '-space' : ' mkdf-ch-' . $args['space_between_items'] . '-space';
		$holderClasses .= !empty($params['image_alignment']) ? ' mkdf-ch-alignment-' . $params['image_alignment'] : '';
		$holderClasses .= !empty($params['items_hover_animation']) ? ' mkdf-cc-hover-'.$params['items_hover_animation'] : ' mkdf-cc-hover-switch-images';
		
		return $holderClasses;
	}
}
