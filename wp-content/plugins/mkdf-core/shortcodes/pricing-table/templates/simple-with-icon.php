<div class="mkdf-price-table <?php echo esc_attr($holder_classes); ?>">
	<div class="mkdf-pt-inner">
		<?php if ($active_item === 'yes') { ?>
			<div class="mkdf-pt-active-mark"></div>
		<?php } ?>
		<?php if(!empty($custom_icon)) { ?>
			<div class="mkdf-pt-icon">
				<?php echo wp_get_attachment_image($custom_icon, 'full'); ?>
			</div>
		<?php } ?>
		<h4 class="mkdf-pt-title"><?php echo esc_html($title); ?></h4>
		<div class="mkdf-pt-content">
			<?php echo do_shortcode($content); ?>
		</div>
		<div class="mkdf-pt-button-price">
			<?php
			if(!empty($button_text) && !empty($button_link)) { ?>
				<div class="mkdf-pt-button">
					<?php echo evently_mikado_get_button_html(array(
						'link' => $button_link,
						'text' => $button_text,
						'type' => 'simple'
					)); ?>
				</div>
			<?php } ?>
			<div class="mkdf-pt-prices">
				<span class="mkdf-pt-value"><?php echo esc_html($currency); ?></span>
				<span class="mkdf-pt-price"><?php echo esc_html($price); ?></span>
			</div>
		</div>
	</div>
</div>