<?php
namespace MikadoCore\CPT\Shortcodes\PricingTable;

use MikadoCore\Lib;

class PricingTableItem implements Lib\ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'mkdf_pricing_table_item';
		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map(
				array(
					'name'                      => esc_html__( 'Mikado Pricing Table Item', 'mkdf-core' ),
					'base'                      => $this->base,
					'icon'                      => 'icon-wpb-pricing-table-item extended-custom-icon',
					'category'                  => esc_html__( 'by MIKADO', 'mkdf-core' ),
					'allowed_container_element' => 'vc_row',
					'as_child'                  => array( 'only' => 'mkdf_pricing_table' ),
					'params'                    => array(
						array(
							'type'        => 'textfield',
							'param_name'  => 'custom_class',
							'heading'     => esc_html__( 'Custom CSS Class', 'mkdf-core' ),
							'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS', 'mkdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'type',
							'heading'     => esc_html__( 'Type', 'mkdf-core' ),
							'value'       => array(
								esc_html__( 'Standard', 'mkdf-core' )   => 'standard',
								esc_html__( 'Simple With Icon', 'mkdf-core' )   => 'simple-with-icon'
							),
							'save_always' => true
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'active_item',
							'heading'     => esc_html__( 'Set Item as Active', 'mkdf-core' ),
							'value'       => array_flip( evently_mikado_get_yes_no_select_array( false ) ),
							'save_always' => true
						),
						array(
							'type'       => 'attach_image',
							'param_name' => 'custom_icon',
							'heading'    => esc_html__( 'Custom Icon', 'mkdf-core' ),
							'dependency' => array( 'element' => 'type', 'value' => 'simple-with-icon' )
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'title',
							'heading'    => esc_html__( 'Title', 'mkdf-core' )
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'title_tagline',
							'heading'    => esc_html__( 'Title Tagline', 'mkdf-core' ),
							'dependency' => array( 'element' => 'type', 'value' => 'standard' )
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'price',
							'heading'    => esc_html__( 'Price', 'mkdf-core' )
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'currency',
							'heading'    => esc_html__( 'Currency', 'mkdf-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'button_text',
							'heading'     => esc_html__( 'Button Text', 'mkdf-core' ),
							'value'       => esc_html__( 'VIEW FEATURES', 'mkdf-core' ),
							'save_always' => true
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'button_link',
							'heading'    => esc_html__( 'Button Link', 'mkdf-core' ),
							'dependency' => array( 'element' => 'button_text', 'not_empty' => true )
						),
						array(
							'type'       => 'textarea_html',
							'param_name' => 'content',
							'heading'    => esc_html__( 'Content', 'mkdf-core' ),
							'value'      => '<li>content content content</li><li>content content content</li><li>content content content</li>'
						)
					)
				)
			);
		}
	}
	
	public function render( $atts, $content = null ) {
		$args   = array(
			'custom_class'   => '',
			'type'           => 'standard',
			'active_item'    => 'no',
			'custom_icon'    => '',
			'title'          => '',
			'title_tagline'  => '',
			'price'          => '100',
			'currency'       => '$',
			'currency_color' => '',
			'button_text'    => '',
			'button_link'    => ''
		);
		$params = shortcode_atts( $args, $atts );
		
		$params['type']           = ! empty( $params['type'] ) ? $params['type'] : $args['type'];
		$params['holder_classes'] = $this->getHolderClasses( $params );
		$params['content']        = preg_replace( '#^<\/p>|<p>$#', '', $content ); // delete p tag before and after content
		
		$html = mkdf_core_get_shortcode_module_template_part( 'templates/'. $params['type'], 'pricing-table', '', $params );
		
		return $html;
	}
	
	private function getHolderClasses( $params ) {
		$holderClasses = array();
		
		$holderClasses[] = ! empty( $params['custom_class'] ) ? esc_attr( $params['custom_class'] ) : '';
		$holderClasses[] = ! empty( $params['type'] ) ? 'mkdf-pt-'. $params['type'] : '';
		$holderClasses[] = $params['active_item'] === 'yes' ? 'mkdf-pt-active' : '';
		
		return implode( ' ', $holderClasses );
	}
}