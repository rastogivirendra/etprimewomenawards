<div class="mkdf-cc-item">
		<a itemprop="url" class="mkdf-cc-link mkdf-block-drag-link <?php echo empty( $link ) || '#' === $link ? 'mkdf-no-link' : ''; ?>" href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>">
			<?php if(!empty($image)) { ?>
				<img itemprop="image" class="mkdf-cc-image" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
			<?php } ?>
			<?php if(!empty($hover_image)) { ?>
				<img itemprop="image" class="mkdf-cc-hover-image" src="<?php echo esc_url($hover_image['url']); ?>" alt="<?php echo esc_attr($hover_image['alt']); ?>" />
			<?php } ?>
		</a>
</div>