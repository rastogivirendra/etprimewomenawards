(function($) {
	'use strict';
	
	var tabs = {};
	mkdf.modules.tabs = tabs;
	
	tabs.mkdfInitTabs = mkdfInitTabs;
	
	
	tabs.mkdfOnDocumentReady = mkdfOnDocumentReady;
	
	$(document).ready(mkdfOnDocumentReady);
	$(window).load(mkdfOnWindowLoad);
	
	/*
	 All functions to be called on $(document).ready() should be in this function
	 */
	function mkdfOnDocumentReady() {
		mkdfInitTabs();
	}

	/*
	 All functions to be called on $(window).load() should be in this function
	 */
	function mkdfOnWindowLoad() {
		mkdfTabsNavUnderline();
	}
	
	/*
	 **	Init tabs shortcode
	 */
	function mkdfInitTabs(){
		var tabs = $('.mkdf-tabs');
		
		if(tabs.length){
			tabs.each(function(){
				var thisTabs = $(this);
				
				thisTabs.children('.mkdf-tab-container').each(function(index){
					index = index + 1;
					var that = $(this),
						link = that.attr('id'),
						navItem = that.parent().find('.mkdf-tabs-nav li:nth-child('+index+') a'),
						navLink = navItem.attr('href');
					
					link = '#'+link;

					if(link.indexOf(navLink) > -1) {
						navItem.attr('href',link);
					}
				});
				
				thisTabs.tabs();

                $('.mkdf-tabs a.mkdf-external-link').unbind('click');
			});
		}
	}

	/*
	 * Tabs nav underline animation
	 */
	function mkdfTabsNavUnderline() {

		//first level menu
		var tabs = $('.mkdf-tabs'),
			simpleTabs = 'mkdf-tabs-simple',
			verticalTabs = 'mkdf-tabs-vertical';


		if (tabs.length && tabs.hasClass(simpleTabs)) {
			$('.' + simpleTabs).each(function () {
				var tabNav = $(this);
				var tabNavs = tabNav.find('.mkdf-tabs-nav'),
					navItemActive = tabNavs.find('.ui-state-active'),
					navLine = tabNav.find('.mkdf-tabs-nav-line'),
					navItems = tabNavs.find('> li');

				var navLineParams = function () {
					var navItemActive = tabNavs.find('.ui-state-active');
					navLine.css('width', navItemActive.outerWidth());
					navLine.css('left', navItemActive.offset().left - tabNavs.offset().left);
					navLine.css('opacity', 1);
				};

				if (navItemActive.length) {
					navLineParams();
				} else {
					navLine.css('left', navItems.first().offset().left - tabNavs.offset().left);
				}

				navItems.each(function () {
					var navItem = $(this),
						navItemWidth = navItem.outerWidth(),
						navMenuOffset = tabNavs.offset().left,
						navItemOffset = navItem.offset().left - navMenuOffset;

					navItem.mouseenter(function () {
						navLine.css('width', navItemWidth);
						navLine.css('left', navItemOffset);
					});
				});

				tabNavs.mouseleave(function () {
					navLineParams();
				});


			});
		}

		//vertical tabs
		if(tabs.length && tabs.hasClass(verticalTabs)) {
			$('.'+verticalTabs).each(function(){
				var tabNav = $(this);
				var tabNavs = tabNav.find('.mkdf-tabs-nav'),
					navItemActive = tabNavs.find('.ui-state-active'),
					navLine = tabNav.find('.mkdf-tabs-nav-line'),
					navItems = tabNavs.find('> li'),
					setIdleState = false;

				var navLineParams = function() {
					var navItemActive = tabNavs.find('.ui-state-active');
					navLine.css('height', navItemActive.outerHeight());
					navLine.css('top', navItemActive.offset().top - tabNavs.offset().top);
					navLine.css('opacity', 1);
				};

				if( navItemActive.length ) {
					navLineParams();
				}

				navItems.each(function(){
					var navItem = $(this),
						navItemHeight = navItem.outerHeight(),
						verticalNavOffset = tabNavs.offset().top,
						navItemOffset = navItem.offset().top  - verticalNavOffset;

					if (!setIdleState) {
						navLine.css('top', navItemOffset);
						setIdleState = true;
					}

					navItem.mouseenter(function(){
						navLine.css('height', navItemHeight);
						navLine.css('top', navItemOffset);
					});
				});

				tabNavs.mouseleave(function(){
					navLineParams();
				});

			});
		}
	}
	
})(jQuery);