<?php

if(!function_exists('mkdf_core_team_meta_box_functions')) {
	function mkdf_core_team_meta_box_functions($post_types) {
		$post_types[] = 'team-member';
		
		return $post_types;
	}
	
	add_filter('evently_mikado_filter_meta_box_post_types_save', 'mkdf_core_team_meta_box_functions');
	add_filter('evently_mikado_filter_meta_box_post_types_remove', 'mkdf_core_team_meta_box_functions');
}

if(!function_exists('mkdf_core_team_scope_meta_box_functions')) {
	function mkdf_core_team_scope_meta_box_functions($post_types) {
		$post_types[] = 'team-member';
		
		return $post_types;
	}
	
	add_filter('evently_mikado_filter_set_scope_for_meta_boxes', 'mkdf_core_team_scope_meta_box_functions');
}

if(!function_exists('mkdf_core_team_enqueue_meta_box_styles')) {
	function mkdf_core_team_enqueue_meta_box_styles() {
		global $post;
		
		if($post->post_type == 'team-member'){
			wp_enqueue_style('mkdf-jquery-ui', get_template_directory_uri().'/framework/admin/assets/css/jquery-ui/jquery-ui.css');
		}
	}
	
	add_action('evently_mikado_action_enqueue_meta_box_styles', 'mkdf_core_team_enqueue_meta_box_styles');
}

if(!function_exists('mkdf_core_register_team_cpt')) {
	function mkdf_core_register_team_cpt($cpt_class_name) {
		$cpt_class = array(
			'MikadoCore\CPT\Team\TeamRegister'
		);
		
		$cpt_class_name = array_merge($cpt_class_name, $cpt_class);
		
		return $cpt_class_name;
	}
	
	add_filter('mkdf_core_filter_register_custom_post_types', 'mkdf_core_register_team_cpt');
}

if(!function_exists('mkdf_core_get_single_team')) {
	/**
	 * Loads holder template for doctor single
	 */
	function mkdf_core_get_single_team() {
		$team_member_id = get_the_ID();
		
		$params = array(
			'sidebar_layout' => evently_mikado_sidebar_layout(),
			'position'       => get_post_meta($team_member_id, 'mkdf_team_member_position', true),
			'website'        => get_post_meta($team_member_id, 'mkdf_team_member_website', true),
			'social_icons'   => mkdf_core_single_team_social_icons($team_member_id),
		);
		
		mkdf_core_get_cpt_single_module_template_part('templates/single/holder', 'team', '', $params);
	}
}

if(!function_exists('mkdf_core_single_team_social_icons')) {
	function mkdf_core_single_team_social_icons($id){
		$social_icons = array();
		
		for ($i = 1; $i < 6; $i++) {
			$team_icon_pack = get_post_meta($id, 'mkdf_team_member_social_icon_pack_' . $i, true);
			if($team_icon_pack !== '') {
				$team_icon_collection = evently_mikado_icon_collections()->getIconCollection(get_post_meta($id, 'mkdf_team_member_social_icon_pack_' . $i, true));
				$team_social_icon     = get_post_meta($id, 'mkdf_team_member_social_icon_pack_' . $i . '_' . $team_icon_collection->param, true);
				$team_social_link     = get_post_meta($id, 'mkdf_team_member_social_icon_' . $i . '_link', true);
				$team_social_target   = get_post_meta($id, 'mkdf_team_member_social_icon_' . $i . '_target', true);
				
				if ($team_social_icon !== '') {
					$team_icon_params = array();
					$team_icon_params['icon_pack']                  = $team_icon_pack;
					$team_icon_params[$team_icon_collection->param] = $team_social_icon;
					$team_icon_params['link']                       = !empty($team_social_link) ? $team_social_link : '';
					$team_icon_params['target']                     = !empty($team_social_target) ? $team_social_target : '_self';
					
					$social_icons[] = evently_mikado_execute_shortcode('mkdf_icon', $team_icon_params);
				}
			}
		}
		
		return $social_icons;
	}
}

if(!function_exists('mkdf_core_get_team_category_list')) {
	function mkdf_core_get_team_category_list($category = '') {
		$number_of_columns = 3;
		
		$params = array(
			'number_of_columns'   => $number_of_columns
		);
		
		if(!empty($category)) {
			$params['category'] = $category;
		}
		
		$html = evently_mikado_execute_shortcode('mkdf_team_list', $params);
		
		print $html;
	}
}

if(!function_exists('mkdf_core_add_team_to_search_types')) {
    function mkdf_core_add_team_to_search_types($post_types) {

        $post_types['team-member'] = 'Team Member';

        return $post_types;
    }

    add_filter('evently_mikado_filter_search_post_type_widget_params_post_type', 'mkdf_core_add_team_to_search_types');
}

if(!function_exists('mkdf_core_team_list_info_opening')) {
	function mkdf_core_team_list_info_opening() {

		$data = array();
		$html = '';

		if(isset($_POST) && isset($_POST['member_id']) ) {

			$member_id = $_POST['member_id'];

			if(!empty($member_id)) {

				$data['member_id']  = $member_id;
				$data['title']      = get_the_title($member_id);
				$data['list_image'] = get_post_meta($member_id, 'mkdf_team_member_list_image', true);
				$data['position']   = get_post_meta($member_id, 'mkdf_team_member_position', true);
				$data['website']    = get_post_meta($member_id, 'mkdf_team_member_website', true);
				$data['titles']     = get_post_meta($member_id, 'mkdf_team_member_title', true);
				$data['dates']      = get_post_meta($member_id, 'mkdf_team_member_date', true);
				$data['hours']      = get_post_meta($member_id, 'mkdf_team_member_hours', true);
				$data['places']     = get_post_meta($member_id, 'mkdf_team_member_place', true);
				$data['boxes']      = count($data['titles']);
				$data['content']    = get_post($member_id)->post_content;
				$data['team_main_bckg_color'] = 'background-color: '.get_post_meta($member_id, 'mkdf_team_member_background_color_meta', true);

				$social_icons = array();

				for($i = 1; $i < 6; $i++) {
					$team_icon_pack = get_post_meta($member_id, 'mkdf_team_member_social_icon_pack_'.$i, true);
					if($team_icon_pack) {
						$team_icon_collection = evently_mikado_icon_collections()->getIconCollection(get_post_meta($member_id, 'mkdf_team_member_social_icon_pack_' . $i, true));
						$team_social_icon = get_post_meta($member_id, 'mkdf_team_member_social_icon_pack_' . $i . '_' . $team_icon_collection->param, true);
						$team_social_link = get_post_meta($member_id, 'mkdf_team_member_social_icon_' . $i . '_link', true);
						$team_social_target = get_post_meta($member_id, 'mkdf_team_member_social_icon_' . $i . '_target', true);

						if ($team_social_icon !== '') {

							$team_icon_params = array();
							$team_icon_params['icon_pack'] = $team_icon_pack;
							$team_icon_params[$team_icon_collection->param] = $team_social_icon;
							$team_icon_params['link'] = ($team_social_link !== '') ? $team_social_link : '';
							$team_icon_params['target'] = ($team_social_target !== '') ? $team_social_target : '';

							$social_icons[] = evently_mikado_execute_shortcode('mkdf_icon', $team_icon_params);
						}
					}
				}

				$data['team_social_icons'] = $social_icons;

				$html = mkdf_core_get_cpt_shortcode_module_template_part('team', 'team-popup', '', $data);
			}

		} else {
			$status = 'error';
		}

		$response = array (
			'html' => $html
		);

		$output = json_encode($response);

		exit($output);

		wp_die();
	}

	add_action( 'wp_ajax_nopriv_mkdf_core_team_list_info_opening', 'mkdf_core_team_list_info_opening' );
	add_action( 'wp_ajax_mkdf_core_team_list_info_opening', 'mkdf_core_team_list_info_opening' );
}

if(!function_exists('mkdf_core_team_list_render_box_html')){
	function mkdf_core_team_list_render_box_html(){
		$html = '<div class="mkdf-team-modal-holder"></div>';
		
		print $html;
	}

	add_action( 'evently_mikado_action_after_header_area', 'mkdf_core_team_list_render_box_html', 20);

}