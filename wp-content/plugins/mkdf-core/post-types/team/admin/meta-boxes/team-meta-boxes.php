<?php

if ( ! function_exists( 'mkdf_core_map_team_single_meta' ) ) {
	function mkdf_core_map_team_single_meta() {
		
		$meta_box = evently_mikado_add_meta_box(
			array(
				'scope' => 'team-member',
				'title' => esc_html__( 'Team Member Info', 'mkdf-core' ),
				'name'  => 'team_meta'
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_team_member_position',
				'type'        => 'text',
				'label'       => esc_html__( 'Position', 'mkdf-core' ),
				'description' => esc_html__( 'The members\'s role within the team', 'mkdf-core' ),
				'parent'      => $meta_box
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_team_member_website',
				'type'        => 'text',
				'label'       => esc_html__( 'Website', 'mkdf-core' ),
				'description' => esc_html__( 'The members\'s website', 'mkdf-core' ),
				'parent'      => $meta_box
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_team_member_list_image',
				'type'        => 'image',
				'label'       => esc_html__( 'List Image', 'mkdf-core' ),
				'description' => esc_html__( 'Upload members\'s image that will be used in team list / or team member shortcode', 'mkdf-core' ),
				'parent'      => $meta_box
			)
		);
		
		evently_mikado_add_repeater_field(
			array(
				'parent' => $meta_box,
				'name'   => 'mkdf_team_member_meta_group',
				'label'  => esc_html__( 'Conference Data', 'mkdf-core' ),
				'fields' => array(
					array(
						'type'  => 'text',
						'name'  => 'mkdf_team_member_title',
						'label' => esc_html__( 'Title', 'mkdf-core' )
					),
					array(
						'type'  => 'text',
						'name'  => 'mkdf_team_member_date',
						'label' => esc_html__( 'Date', 'mkdf-core' )
					),
					array(
						'type'  => 'text',
						'name'  => 'mkdf_team_member_hours',
						'label' => esc_html__( 'Time', 'mkdf-core' )
					),
					array(
						'type'  => 'text',
						'name'  => 'mkdf_team_member_place',
						'label' => esc_html__( 'Place', 'mkdf-core' )
					)
				)
			)
		);
		for ( $x = 1; $x < 6; $x ++ ) {
			
			$social_icon_group = evently_mikado_add_admin_group(
				array(
					'name'   => 'mkdf_team_member_social_icon_group' . $x,
					'title'  => esc_html__( 'Social Link ', 'mkdf-core' ) . $x,
					'parent' => $meta_box
				)
			);
			
			$social_row1 = evently_mikado_add_admin_row(
				array(
					'name'   => 'mkdf_team_member_social_icon_row1' . $x,
					'parent' => $social_icon_group
				)
			);
			
			EventlyMikadoClassIconCollections::get_instance()->getSocialIconsMetaBoxOrOption(
				array(
					'type'             => 'meta-box',
					'field_type'       => 'simple',
					'name'             => 'mkdf_team_member_social_icon_pack_' . $x,
					'label'            => esc_html__( 'Icon ', 'mkdf-core' ) . $x,
					'parent'           => $social_row1,
					'defaul_icon_pack' => '',
				)
			);
			
			$social_row2 = evently_mikado_add_admin_row(
				array(
					'name'   => 'mkdf_team_member_social_icon_row2' . $x,
					'parent' => $social_icon_group
				)
			);
			
			evently_mikado_add_meta_box_field(
				array(
					'type'   => 'textsimple',
					'name'   => 'mkdf_team_member_social_icon_' . $x . '_link',
					'label'  => esc_html__( 'Link', 'mkdf-core' ),
					'parent' => $social_row2
				)
			);
			
			evently_mikado_add_meta_box_field(
				array(
					'type'    => 'selectsimple',
					'name'    => 'mkdf_team_member_social_icon_' . $x . '_target',
					'label'   => esc_html__( 'Target', 'mkdf-core' ),
					'options' => evently_mikado_get_link_target_array(),
					'parent'  => $social_row2
				)
			);
		}
		
	}
	
	add_action( 'evently_mikado_action_meta_boxes_map', 'mkdf_core_map_team_single_meta', 5 );
}