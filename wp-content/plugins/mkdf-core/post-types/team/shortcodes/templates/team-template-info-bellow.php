<div class="mkdf-team <?php echo esc_attr($team_member_layout) ?>" data-member-id="<?php echo esc_attr($member_id);?>">
	<div class="mkdf-team-inner">
		<?php if (get_the_post_thumbnail($member_id) !== '') { ?>
			<div class="mkdf-team-image">
                <a itemprop="url" href="<?php echo esc_url(get_the_permalink($member_id)) ?>">
	                <?php if( ! empty($list_image) ): ?>
		                <img src="<?php echo esc_url($list_image); ?>" alt="<?php echo esc_attr($title) ?>" />
	                <?php else: ?>
		                <?php echo get_the_post_thumbnail($member_id, 'full'); ?>
	                <?php endif; ?>
                </a>
			</div>
		<?php } ?>
		<div class="mkdf-team-info">
            <div class="mkdf-team-title-holder">
                <h4 itemprop="name" class="mkdf-team-name entry-title">
                    <a itemprop="url" href="<?php echo esc_url(get_the_permalink($member_id)) ?>"><?php echo esc_html($title) ?></a>
                </h4>

                <?php if (!empty($position)) { ?>
                    <span class="mkdf-team-position"><?php echo esc_html($position); ?></span>
                <?php } ?>
            </div>
			<?php if (!empty($excerpt) && $show_excerpt !== 'no') { ?>
				<div class="mkdf-team-text">
					<div class="mkdf-team-text-inner">
						<div class="mkdf-team-description">
							<p itemprop="description" class="mkdf-team-excerpt"><?php echo esc_html($excerpt); ?></p>
						</div>
					</div>
				</div>
			<?php } ?>
			<div class="mkdf-team-bottom-holder">
				<?php
					echo evently_mikado_get_button_html(array(
							'type'  => 'simple',
							'text'  => esc_html__('Read More', 'mkdf-core'),
							'link'  => esc_url(get_the_permalink($member_id))
					));
				?>
				<?php if( !empty($team_social_icons) ): ?>
					<div class="mkdf-social-share-wrap">
						<div class="mkdf-social-share-holder mkdf-dropdown">
							<a href="javascript:void(0)" target="_self" class="mkdf-social-share-dropdown-opener">
								<i class="social_share"></i>
							</a>

							<div class="mkdf-social-share-dropdown">
								<ul>
									<?php foreach ($team_social_icons as $team_social_icon) { ?>
										<li class="mkdf-team-icon">
											<?php echo wp_kses_post($team_social_icon); ?>
										</li>
									<?php } ?>
								</ul>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>