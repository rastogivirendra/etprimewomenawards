<div class="mkdf-team <?php echo esc_attr($team_member_layout) ?>" data-member-id="<?php echo esc_attr($member_id);?>">
    <div class="mkdf-team-inner">
        <?php if (get_the_post_thumbnail($member_id) !== '') { ?>
            <div class="mkdf-team-image">
	            <?php if( ! empty($list_image) ): ?>
		            <img src="<?php echo esc_url($list_image); ?>" alt="<?php echo esc_attr($title) ?>" />
	            <?php else: ?>
		            <?php echo get_the_post_thumbnail($member_id, 'full'); ?>
	            <?php endif; ?>
            </div>
        <?php } ?>
	    <a itemprop="url" class="mkdf-team-overlay-link" href="<?php echo esc_url(get_the_permalink($member_id)) ?>"></a>
    </div>
</div>