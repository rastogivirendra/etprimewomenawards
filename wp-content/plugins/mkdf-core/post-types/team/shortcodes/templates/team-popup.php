<div class="mkdf-team-popup">
    <div class="mkdf-team-popup-inner">
	    <div class="mkdf-team-popup-content">
		    <?php if( ! empty($list_image) ): ?>
			    <?php
			        $image_id = evently_mikado_get_attachment_id_from_url($list_image);
			    ?>
			    <div class="mkdf-team-popup-image mkdf-team-popup-image-bgrnd" style="background-image:url(<?php echo esc_url($list_image); ?>)">
				    <!-- <?php echo wp_get_attachment_image($image_id, 'evently_mikado_landscape'); ?> -->
				    <i class="fa fa-times mkdf-close" aria-hidden="true" <?php evently_mikado_inline_style($team_main_bckg_color); ?>></i>
			    </div>
		    <?php else: ?>
			    <?php if (get_the_post_thumbnail($member_id) !== '') { ?>
				    <div class="mkdf-team-popup-image">
					    <?php echo get_the_post_thumbnail($member_id, 'evently_mikado_landscape'); ?>
					    <i class="fa fa-times mkdf-close" aria-hidden="true" <?php evently_mikado_inline_style($team_main_bckg_color); ?>></i>
				    </div>
			    <?php } ?>
		    <?php endif; ?>
            <div class="mkdf-team-popup-info-holder">
            	<div class="mkdf-team-title-holder">
                    <h3 itemprop="name" class="mkdf-team-name entry-title">
                        <?php echo esc_html($title) ?>
                    </h3>
                    <?php if (is_array($team_social_icons) && count($team_social_icons)) { ?>
	                    <div class="mkdf-team-popup-social">
							<?php foreach ($team_social_icons as $team_social_icon) {
								echo wp_kses_post($team_social_icon);
							} ?>
						</div>
					<?php } ?>
                </div>
                <?php if (get_the_excerpt($member_id) !== '') { ?>
                <div class="mkdf-team-info-section mkdf-excerpt-section">
                    <h4 class="mkdf-team-info-section-title"><?php esc_html_e('About','mkdf-core')?></h4>
                    <p><?php echo wp_kses_post( get_the_excerpt($member_id) ); ?></p>
                </div>
                <?php } ?>
                <?php if ($website !== '') { ?>
                <div class="mkdf-team-info-section">
                    <h4 class="mkdf-team-info-section-title"><?php esc_html_e('Website','mkdf-core')?></h4>
	                <a href="<?php echo esc_url($website); ?>" class="mkdf-team-website"><?php echo esc_html($website); ?></a>
                </div>
                <?php } ?>
	            <?php if( $boxes > 0 && ( !empty( $titles[0] ) || !empty( $dates[0] ) || !empty( $hours[0] ) || !empty( $places[0] ) ) ): ?>
	            <div class="mkdf-team-info-section">
		            <h4 class="mkdf-team-info-section-title"><?php esc_html_e('Sessions', 'mkdf-core'); ?></h4>
		            <div class="mkdf-team-info-sessions">
			            <?php for( $n = 0; $n < $boxes; $n++ ): ?>
				            <div class="mkdf-team-session-box">
					            <div class="mkdf-team-session-box-inner">
						            <?php if( isset($titles[$n]) ): ?>
							            <span class="mkdf-team-session-title"><?php echo esc_html($titles[$n]); ?></span>
						            <?php endif; ?>
						            <?php if( isset($dates[$n]) || isset($hours[$n]) ): ?>
							            <div class="mkdf-team-date-wrap">
								            <?php if( isset($dates[$n]) ): ?>
									            <span class="mkdf-team-session-date"><?php echo esc_html($dates[$n]); ?></span>
								            <?php endif; ?>
								            <?php if( isset($hours[$n]) ): ?>
									            <span class="mkdf-team-session-time"><?php echo esc_html($hours[$n]); ?></span>
								            <?php endif; ?>
							            </div>
						            <?php endif; ?>
						            <?php if( isset($places[$n]) ): ?>
							            <p class="mkdf-team-session-place"><?php echo esc_html($places[$n]); ?></p>
						            <?php endif; ?>
					            </div>
				            </div>
			            <?php endfor; ?>
		            </div>
	            </div>
	            <?php endif; ?>
            </div>
	    </div>
    </div>
</div>