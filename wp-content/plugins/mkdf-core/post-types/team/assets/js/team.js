(function($) {
    'use strict';

    var team = {};
    mkdf.modules.team = team;

    team.mkdfOnDocumentReady = mkdfOnDocumentReady;

    $(document).ready(mkdfOnDocumentReady);

    /*
     All functions to be called on $(document).ready() should be in this function
     */
    function mkdfOnDocumentReady() {
	    ajaxLoadTeamContent();
    }

	function ajaxLoadTeamContent() {

		var teamInfomembers = $('.mkdf-team-list-holder.mkdf-team-popup .mkdf-team');
		var modal = $('.mkdf-team-modal-holder');
		var spinner = $('<div class="mkdf-team-spinner"><div class="mkdf-team-pulse"></div><div class="mkdf-team-pulse"></div></div>');

		if(teamInfomembers.length) {
			teamInfomembers.each(function () {
				var teamInfoMember = $(this);

				teamInfoMember.find('a').on('click', function(e){
					e.preventDefault();
					modal.fadeIn(400, 'easeInOutQuint');
					modal.append(spinner);
					spinner.fadeIn(200);

					var ajaxData = {
						action: 'mkdf_core_team_list_info_opening',
						member_id : teamInfoMember.data('member-id')
					};

					$.ajax({
						type: 'POST',
						data: ajaxData,
						url: mkdfGlobalVars.vars.mkdfAjaxUrl,
						success: function (data) {
							var response = JSON.parse(data);
							var responseHtml = response.html;
							modal.empty();
							modal.append(responseHtml);
							mkdf.body.addClass('mkdf-team-info-opened');
							mkdf.html.addClass('mkdf-scroll-disabled');
							spinner.fadeOut(200).remove();
							setTimeout(function(){
								modal.addClass('mkdf-modal-opened');
							},300);

							modal.find('.mkdf-close').on('click', function(){
								e.preventDefault();
								mkdf.body.removeClass('mkdf-team-info-opened');
								mkdf.html.removeClass('mkdf-scroll-disabled');
								modal.removeClass('mkdf-modal-opened');
								setTimeout(function(){
									modal.empty();
									modal.fadeOut(100);
								}, 400);
							});

							//Close on click away
							$(document).mouseup(function (e) {
								if (modal.hasClass('mkdf-modal-opened')){
									var container = $(".mkdf-team-popup-content");
									if (!container.is(e.target) && container.has(e.target).length === 0)  {
										e.preventDefault();
										mkdf.body.removeClass('mkdf-team-info-opened');
										mkdf.html.removeClass('mkdf-scroll-disabled');
										modal.removeClass('mkdf-modal-opened');
										setTimeout(function(){
											modal.empty();
											modal.fadeOut(100);
										}, 400);
									}
								}
							});
						}
					});
				});
			});
		}
	}

})(jQuery);