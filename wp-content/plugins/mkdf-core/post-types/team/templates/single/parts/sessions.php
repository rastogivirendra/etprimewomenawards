<?php
	$id     = get_the_ID();
	$titles = get_post_meta($id, 'mkdf_team_member_title', true);
	$dates  = get_post_meta($id, 'mkdf_team_member_date', true);
	$hours  = get_post_meta($id, 'mkdf_team_member_hours', true);
	$places = get_post_meta($id, 'mkdf_team_member_place', true);
	$boxes = count($titles);
?>
<?php if( $boxes > 0 && ( !empty( $titles[0] ) || !empty( $dates[0] ) || !empty( $hours[0] ) || !empty( $places[0] ) ) ): ?>
	<h4 class="mkdf-sesions-heading"><?php printf('%s %s', esc_html__('All Sessions By', 'mkdf-core'), get_the_title($id)); ?></h4>

	<div class="mkdf-team-single-sessions clearfix mkdf-grid-row mkdf-grid-normal-gutter">
		<?php for( $n = 0; $n < $boxes; $n++ ): ?>
			<div class="mkdf-team-session-box mkdf-grid-col-<?php echo 12 / $boxes ?>">
				<div class="mkdf-team-session-box-inner">
					<?php if( isset($titles[$n]) ): ?>
						<span class="mkdf-team-session-title"><?php echo esc_html($titles[$n]); ?></span>
					<?php endif; ?>
					<?php if( isset($dates[$n]) || isset($hours[$n]) ): ?>
						<div class="mkdf-team-date-wrap">
							<?php if( isset($dates[$n]) ): ?>
								<span class="mkdf-team-session-date"><?php echo esc_html($dates[$n]); ?></span>
							<?php endif; ?>
							<?php if( isset($hours[$n]) ): ?>
								<span class="mkdf-team-session-time"><?php echo esc_html($hours[$n]); ?></span>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					<?php if( isset($places[$n]) ): ?>
						<h5 class="mkdf-team-session-place"><?php echo esc_html($places[$n]); ?></h5>
					<?php endif; ?>
				</div>
			</div>
		<?php endfor; ?>
	</div>
<?php endif; ?>