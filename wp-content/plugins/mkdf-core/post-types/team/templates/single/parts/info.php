<div class="mkdf-team-single-info-holder">
	<div class="mkdf-grid-row mkdf-grid-large-gutter">
		<div class="mkdf-ts-image-holder mkdf-grid-col-6">
			<?php the_post_thumbnail(); ?>
		</div>
		<div class="mkdf-ts-details-holder mkdf-grid-col-6">
			<h2 itemprop="name" class="mkdf-name entry-title"><?php the_title(); ?></h2>
			<?php echo do_shortcode( '[mkdf_separator width="124px" thickness="2" top_margin="21px" bottom_margin="33px" position="left" custom_class="mkdf-team-name-separator"]' ); ?>
			<p class="mkdf-team-social-icons">
				<?php foreach ($social_icons as $social_icon) {
					echo wp_kses_post($social_icon);
				} ?>
			</p>
			<div class="mkdf-ts-bio-holder">
				<?php
					//load content
					mkdf_core_get_cpt_single_module_template_part('templates/single/parts/content', 'team', '', $params);
				?>
			</div>
			<div class="mkdf-ts-website">
				<h3 class="mkdf-team-info-section-title"><?php esc_html_e('Website','mkdf-core')?></h3>
				<a href="<?php echo esc_url($website); ?>" class="mkdf-team-website"><?php echo esc_html($website); ?></a>
			</div>
		</div>
	</div>
</div>