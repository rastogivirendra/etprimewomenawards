<?php if(evently_mikado_options()->getOptionValue('portfolio_single_hide_date') === 'yes') : ?>
    <div class="mkdf-ps-info-item mkdf-ps-date">
        <h5 class="mkdf-ps-info-title"><?php esc_html_e('Date:', 'mkdf-core'); ?></h5>
        <p itemprop="dateCreated" class="mkdf-ps-info-date entry-date updated"><?php the_time(get_option('date_format')); ?></p>
        <meta itemprop="interactionCount" content="UserComments: <?php echo get_comments_number(evently_mikado_get_page_id()); ?>"/>
    </div>
<?php endif; ?>