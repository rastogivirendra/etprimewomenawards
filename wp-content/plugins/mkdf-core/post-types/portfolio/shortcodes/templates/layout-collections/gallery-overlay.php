<?php echo mkdf_core_get_cpt_shortcode_module_template_part('portfolio', 'parts/image', $item_style, $params); ?>

<div class="mkdf-pli-text-holder">
	<div class="mkdf-pli-text-wrapper">
		<div class="mkdf-pli-text">
			<?php echo mkdf_core_get_cpt_shortcode_module_template_part('portfolio', 'parts/title', $item_style, $params); ?>

			<?php echo mkdf_core_get_cpt_shortcode_module_template_part('portfolio', 'parts/category', $item_style, $params); ?>
			
			<?php echo mkdf_core_get_cpt_shortcode_module_template_part('portfolio', 'parts/excerpt', $item_style, $params); ?>
		</div>
	</div>
</div>