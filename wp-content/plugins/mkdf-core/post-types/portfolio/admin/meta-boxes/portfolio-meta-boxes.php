<?php

if (!function_exists('mkdf_core_map_portfolio_meta')) {
    function mkdf_core_map_portfolio_meta() {
        global $evently_mikado_global_Framework;

        $mkd_pages = array();
        $pages = get_pages();
        foreach ($pages as $page) {
            $mkd_pages[$page->ID] = $page->post_title;
        }

        //Portfolio Images

        $mkdPortfolioImages = new EventlyMikadoClassMetaBox('portfolio-item', esc_html__('Portfolio Images (multiple upload)', 'mkdf-core'), '', '', 'portfolio_images');
        $evently_mikado_global_Framework->mkdMetaBoxes->addMetaBox('portfolio_images', $mkdPortfolioImages);

        $mkdf_portfolio_image_gallery = new EventlyMikadoClassMultipleImages('mkdf-portfolio-image-gallery', esc_html__('Portfolio Images', 'mkdf-core'), esc_html__('Choose your portfolio images', 'mkdf-core'));
        $mkdPortfolioImages->addChild('mkdf-portfolio-image-gallery', $mkdf_portfolio_image_gallery);

        //Portfolio Images/Videos 2

        $mkdPortfolioImagesVideos2 = new EventlyMikadoClassMetaBox('portfolio-item', esc_html__('Portfolio Images/Videos (single upload)', 'mkdf-core'));
        $evently_mikado_global_Framework->mkdMetaBoxes->addMetaBox('portfolio_images_videos2', $mkdPortfolioImagesVideos2);

        $mkdf_portfolio_images_videos2 = new EventlyMikadoClassImagesVideosFramework('', '');
        $mkdPortfolioImagesVideos2->addChild('mkd_portfolio_images_videos2', $mkdf_portfolio_images_videos2);

        //Portfolio Additional Sidebar Items

        $mkdAdditionalSidebarItems = evently_mikado_add_meta_box(
            array(
                'scope' => array('portfolio-item'),
                'title' => esc_html__('Additional Portfolio Sidebar Items', 'mkdf-core'),
                'name' => 'portfolio_properties'
            )
        );

        $mkd_portfolio_properties = evently_mikado_add_options_framework(
            array(
                'label' => esc_html__('Portfolio Properties', 'mkdf-core'),
                'name' => 'mkd_portfolio_properties',
                'parent' => $mkdAdditionalSidebarItems
            )
        );
    }

    add_action('evently_mikado_action_meta_boxes_map', 'mkdf_core_map_portfolio_meta', 40);
}