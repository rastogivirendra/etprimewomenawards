<?php

if(!function_exists('mkdf_core_map_portfolio_settings_meta')) {
    function mkdf_core_map_portfolio_settings_meta() {
        $meta_box = evently_mikado_add_meta_box(array(
            'scope' => 'portfolio-item',
            'title' => esc_html__('Portfolio Settings', 'mkdf-core'),
            'name'  => 'portfolio_settings_meta_box'
        ));

        evently_mikado_add_meta_box_field(array(
            'name'        => 'mkdf_portfolio_single_template_meta',
            'type'        => 'select',
            'label'       => esc_html__('Portfolio Type', 'mkdf-core'),
            'description' => esc_html__('Choose a default type for Single Project pages', 'mkdf-core'),
            'parent'      => $meta_box,
            'options'     => array(
                ''                  => esc_html__('Default', 'mkdf-core'),
                'images'            => esc_html__('Portfolio Images', 'mkdf-core'),
                'small-images'      => esc_html__('Portfolio Small Images', 'mkdf-core'),
                'slider'            => esc_html__('Portfolio Slider', 'mkdf-core'),
                'small-slider'      => esc_html__('Portfolio Small Slider', 'mkdf-core'),
                'gallery'           => esc_html__('Portfolio Gallery', 'mkdf-core'),
                'small-gallery'     => esc_html__('Portfolio Small Gallery', 'mkdf-core'),
                'masonry'           => esc_html__('Portfolio Masonry', 'mkdf-core'),
                'small-masonry'     => esc_html__('Portfolio Small Masonry', 'mkdf-core'),
                'custom'            => esc_html__('Portfolio Custom', 'mkdf-core'),
                'full-width-custom' => esc_html__('Portfolio Full Width Custom', 'mkdf-core')
            ),
            'args' => array(
	            'dependence' => true,
	            'show' => array(
		            ''                  => '',
		            'images'            => '',
		            'small-images'      => '',
		            'slider'            => '',
		            'small-slider'      => '',
		            'gallery'           => '#mkdf_mkdf_gallery_type_meta_container',
		            'small-gallery'     => '#mkdf_mkdf_gallery_type_meta_container',
		            'masonry'           => '#mkdf_mkdf_masonry_type_meta_container',
		            'small-masonry'     => '#mkdf_mkdf_masonry_type_meta_container',
		            'custom'            => '',
		            'full-width-custom' => ''
	            ),
	            'hide' => array(
		            ''                  => '#mkdf_mkdf_gallery_type_meta_container, #mkdf_mkdf_masonry_type_meta_container',
		            'images'            => '#mkdf_mkdf_gallery_type_meta_container, #mkdf_mkdf_masonry_type_meta_container',
		            'small-images'      => '#mkdf_mkdf_gallery_type_meta_container, #mkdf_mkdf_masonry_type_meta_container',
		            'slider'            => '#mkdf_mkdf_gallery_type_meta_container, #mkdf_mkdf_masonry_type_meta_container',
		            'small-slider'      => '#mkdf_mkdf_gallery_type_meta_container, #mkdf_mkdf_masonry_type_meta_container',
		            'gallery'           => '#mkdf_mkdf_masonry_type_meta_container',
		            'small-gallery'     => '#mkdf_mkdf_masonry_type_meta_container',
		            'masonry'           => '#mkdf_mkdf_gallery_type_meta_container',
		            'small-masonry'     => '#mkdf_mkdf_gallery_type_meta_container',
		            'custom'            => '#mkdf_mkdf_gallery_type_meta_container, #mkdf_mkdf_masonry_type_meta_container',
		            'full-width-custom' => '#mkdf_mkdf_gallery_type_meta_container, #mkdf_mkdf_masonry_type_meta_container'
	            )
            )
        ));
	
	    /***************** Gallery Layout *****************/
	
	    $gallery_type_meta_container = evently_mikado_add_admin_container(
		    array(
			    'parent' => $meta_box,
			    'name' => 'mkdf_gallery_type_meta_container',
			    'hidden_property' => 'mkdf_portfolio_single_template_meta',
			    'hidden_values' => array(
				    'images',
				    'small-images',
				    'slider',
				    'small-slider',
				    'masonry',
				    'small-masonry',
				    'custom',
				    'full-width-custom'
			    )
		    )
	    );
	
	        evently_mikado_add_meta_box_field(array(
			    'name'        => 'mkdf_portfolio_single_gallery_columns_number_meta',
			    'type'        => 'select',
			    'label'       => esc_html__('Number of Columns', 'mkdf-core'),
			    'default_value' => '',
			    'description' => esc_html__('Set number of columns for portfolio gallery type', 'mkdf-core'),
			    'parent'      => $gallery_type_meta_container,
			    'options'     => array(
				    ''      => esc_html__('Default', 'mkdf-core'),
				    'two'   => esc_html__('2 Columns', 'mkdf-core'),
				    'three' => esc_html__('3 Columns', 'mkdf-core'),
				    'four'  => esc_html__('4 Columns', 'mkdf-core')
			    )
		    ));
	
	        evently_mikado_add_meta_box_field(array(
			    'name'        => 'mkdf_portfolio_single_gallery_space_between_items_meta',
			    'type'        => 'select',
			    'label'       => esc_html__('Space Between Items', 'mkdf-core'),
			    'default_value' => '',
			    'description' => esc_html__('Set space size between columns for portfolio gallery type', 'mkdf-core'),
			    'parent'      => $gallery_type_meta_container,
			    'options'     => array(
				    ''          => esc_html__('Default', 'mkdf-core'),
				    'normal'    => esc_html__('Normal', 'mkdf-core'),
				    'small'     => esc_html__('Small', 'mkdf-core'),
				    'tiny'      => esc_html__('Tiny', 'mkdf-core'),
				    'no'        => esc_html__('No Space', 'mkdf-core')
			    )
		    ));
	
	    /***************** Gallery Layout *****************/
	
	    /***************** Masonry Layout *****************/
	
	    $masonry_type_meta_container = evently_mikado_add_admin_container(
		    array(
			    'parent' => $meta_box,
			    'name' => 'mkdf_masonry_type_meta_container',
			    'hidden_property' => 'mkdf_portfolio_single_template_meta',
			    'hidden_values' => array(
				    'images',
				    'small-images',
				    'slider',
				    'small-slider',
				    'gallery',
				    'small-gallery',
				    'custom',
				    'full-width-custom'
			    )
		    )
	    );
	
		    evently_mikado_add_meta_box_field(array(
			    'name'        => 'mkdf_portfolio_single_masonry_columns_number_meta',
			    'type'        => 'select',
			    'label'       => esc_html__('Number of Columns', 'mkdf-core'),
			    'default_value' => '',
			    'description' => esc_html__('Set number of columns for portfolio masonry type', 'mkdf-core'),
			    'parent'      => $masonry_type_meta_container,
			    'options'     => array(
				    ''      => esc_html__('Default', 'mkdf-core'),
				    'two'   => esc_html__('2 Columns', 'mkdf-core'),
				    'three' => esc_html__('3 Columns', 'mkdf-core'),
				    'four'  => esc_html__('4 Columns', 'mkdf-core')
			    )
		    ));
		
		    evently_mikado_add_meta_box_field(array(
			    'name'        => 'mkdf_portfolio_single_masonry_space_between_items_meta',
			    'type'        => 'select',
			    'label'       => esc_html__('Space Between Items', 'mkdf-core'),
			    'default_value' => '',
			    'description' => esc_html__('Set space size between columns for portfolio masonry type', 'mkdf-core'),
			    'parent'      => $masonry_type_meta_container,
			    'options'     => array(
				    ''          => esc_html__('Default', 'mkdf-core'),
				    'normal'    => esc_html__('Normal', 'mkdf-core'),
				    'small'     => esc_html__('Small', 'mkdf-core'),
				    'tiny'      => esc_html__('Tiny', 'mkdf-core'),
				    'no'        => esc_html__('No Space', 'mkdf-core')
			    )
		    ));
	
	    /***************** Masonry Layout *****************/

        evently_mikado_add_meta_box_field(
            array(
                'name' => 'mkdf_show_title_area_portfolio_single_meta',
                'type' => 'select',
                'default_value' => '',
                'label'       => esc_html__('Show Title Area', 'mkdf-core'),
                'description' => esc_html__('Enabling this option will show title area on your single portfolio page', 'mkdf-core'),
                'parent'      => $meta_box,
                'options' => array(
                    '' => esc_html__('Default', 'mkdf-core'),
                    'yes' => esc_html__('Yes', 'mkdf-core'),
                    'no' => esc_html__('No', 'mkdf-core')
                )
            )
        );

	    evently_mikado_add_meta_box_field(array(
		    'name'        => 'portfolio_info_top_padding',
		    'type'        => 'text',
		    'label'       => esc_html__('Portfolio Info Top Padding', 'mkdf-core'),
		    'description' => esc_html__('Set top padding for portfolio info elements holder. This option works only for Portfolio Images, Slider, Gallery and Masonry portfolio types', 'mkdf-core'),
		    'parent'      => $meta_box,
		    'args'        => array(
			    'col_width' => 3,
			    'suffix' => 'px'
		    )
	    ));

        $all_pages = array();
        $pages     = get_pages();
        foreach($pages as $page) {
            $all_pages[$page->ID] = $page->post_title;
        }

        evently_mikado_add_meta_box_field(array(
            'name'        => 'portfolio_single_back_to_link',
            'type'        => 'select',
            'label'       => esc_html__('"Back To" Link', 'mkdf-core'),
            'description' => esc_html__('Choose "Back To" page to link from portfolio Single Project page', 'mkdf-core'),
            'parent'      => $meta_box,
            'options'     => $all_pages,
			'args' => array(
				'select2' => true
			)
        ));

        evently_mikado_add_meta_box_field(array(
            'name'        => 'portfolio_external_link',
            'type'        => 'text',
            'label'       => esc_html__('Portfolio External Link', 'mkdf-core'),
            'description' => esc_html__('Enter URL to link from Portfolio List page', 'mkdf-core'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));
	
	    evently_mikado_add_meta_box_field(
		    array(
			    'name' => 'mkdf_portfolio_featured_image_meta',
			    'type' => 'image',
			    'label' => esc_html__('Featured Image', 'mkdf-core'),
			    'description' => esc_html__('Choose an image for Portfolio Lists shortcode where Hover Type option is Switch Featured Images', 'mkdf-core'),
			    'parent' => $meta_box
		    )
	    );
	
	    evently_mikado_add_meta_box_field(array(
		    'name' => 'mkdf_portfolio_masonry_fixed_dimensions_meta',
		    'type' => 'select',
		    'label' => esc_html__('Dimensions for Masonry - Image Fixed Proportion', 'mkdf-core'),
		    'description' => esc_html__('Choose image layout when it appears in Masonry type portfolio lists where image proportion is fixed', 'mkdf-core'),
		    'default_value' => 'default',
		    'parent' => $meta_box,
		    'options' => array(
			    'default' => esc_html__('Default', 'mkdf-core'),
			    'large-width' => esc_html__('Large Width', 'mkdf-core'),
			    'large-height' => esc_html__('Large Height', 'mkdf-core'),
			    'large-width-height' => esc_html__('Large Width/Height', 'mkdf-core')
		    )
	    ));
	
	    evently_mikado_add_meta_box_field(array(
		    'name' => 'mkdf_portfolio_masonry_original_dimensions_meta',
		    'type' => 'select',
		    'label' => esc_html__('Dimensions for Masonry - Image Original Proportion', 'mkdf-core'),
		    'description' => esc_html__('Choose image layout when it appears in Masonry type portfolio lists where image proportion is original', 'mkdf-core'),
		    'default_value' => 'default',
		    'parent' => $meta_box,
		    'options' => array(
			    'default' => esc_html__('Default', 'mkdf-core'),
			    'large-width' => esc_html__('Large Width', 'mkdf-core')
		    )
	    ));
    }

    add_action('evently_mikado_action_meta_boxes_map', 'mkdf_core_map_portfolio_settings_meta', 41);
}