<?php

if ( ! function_exists('evently_mikado_portfolio_options_map') ) {
	function evently_mikado_portfolio_options_map() {

		evently_mikado_add_admin_page(array(
			'slug'  => '_portfolio',
			'title' => esc_html__('Portfolio', 'mkdf-core'),
			'icon'  => 'fa fa-camera-retro'
		));

		$panel_archive = evently_mikado_add_admin_panel(array(
			'title' => esc_html__('Portfolio Archive', 'mkdf-core'),
			'name'  => 'panel_portfolio_archive',
			'page'  => '_portfolio'
		));

		evently_mikado_add_admin_field(array(
			'name'        => 'portfolio_archive_number_of_items',
			'type'        => 'text',
			'label'       => esc_html__('Number of Items', 'mkdf-core'),
			'description' => esc_html__('Set number of items for your portfolio list on archive pages. Default value is 12', 'mkdf-core'),
			'parent'      => $panel_archive,
			'args'        => array(
				'col_width' => 3
			)
		));

		evently_mikado_add_admin_field(array(
			'name'        => 'portfolio_archive_number_of_columns',
			'type'        => 'select',
			'label'       => esc_html__('Number of Columns', 'mkdf-core'),
			'default_value' => '4',
			'description' => esc_html__('Set number of columns for your portfolio list on archive pages. Default value is 4 columns', 'mkdf-core'),
			'parent'      => $panel_archive,
			'options'     => array(
				'2' => esc_html__('2 Columns', 'mkdf-core'),
				'3' => esc_html__('3 Columns', 'mkdf-core'),
				'4' => esc_html__('4 Columns', 'mkdf-core'),
				'5' => esc_html__('5 Columns', 'mkdf-core')
			)
		));

		evently_mikado_add_admin_field(array(
			'name'        => 'portfolio_archive_space_between_items',
			'type'        => 'select',
			'label'       => esc_html__('Space Between Items', 'mkdf-core'),
			'default_value' => 'normal',
			'description' => esc_html__('Set space size between portfolio items for your portfolio list on archive pages. Default value is normal', 'mkdf-core'),
			'parent'      => $panel_archive,
			'options'     => array(
				'normal'    => esc_html__('Normal', 'mkdf-core'),
				'small'     => esc_html__('Small', 'mkdf-core'),
				'tiny'      => esc_html__('Tiny', 'mkdf-core'),
				'no'        => esc_html__('No Space', 'mkdf-core')
			)
		));

		evently_mikado_add_admin_field(array(
			'name'        => 'portfolio_archive_image_size',
			'type'        => 'select',
			'label'       => esc_html__('Image Proportions', 'mkdf-core'),
			'default_value' => 'landscape',
			'description' => esc_html__('Set image proportions for your portfolio list on archive pages. Default value is landscape', 'mkdf-core'),
			'parent'      => $panel_archive,
			'options'     => array(
				'full'      => esc_html__('Original', 'mkdf-core'),
				'landscape' => esc_html__('Landscape', 'mkdf-core'),
				'portrait'  => esc_html__('Portrait', 'mkdf-core'),
				'square'    => esc_html__('Square', 'mkdf-core')
			)
		));
		
		evently_mikado_add_admin_field(array(
			'name'        => 'portfolio_archive_item_layout',
			'type'        => 'select',
			'label'       => esc_html__('Item Style', 'mkdf-core'),
			'default_value' => 'standard-shader',
			'description' => esc_html__('Set item style for your portfolio list on archive pages. Default value is Standard - Shader', 'mkdf-core'),
			'parent'      => $panel_archive,
			'options'     => array(
				'standard-shader' => esc_html__('Standard - Shader', 'mkdf-core'),
				'gallery-overlay' => esc_html__('Gallery - Overlay', 'mkdf-core')
			)
		));

		$panel = evently_mikado_add_admin_panel(array(
			'title' => esc_html__('Portfolio Single', 'mkdf-core'),
			'name'  => 'panel_portfolio_single',
			'page'  => '_portfolio'
		));

		evently_mikado_add_admin_field(array(
			'name'          => 'portfolio_single_template',
			'type'          => 'select',
			'label'         => esc_html__('Portfolio Type', 'mkdf-core'),
			'default_value'	=> 'small-images',
			'description'   => esc_html__('Choose a default type for Single Project pages', 'mkdf-core'),
			'parent'        => $panel,
			'options'       => array(
				'images'            => esc_html__('Portfolio Images', 'mkdf-core'),
				'small-images'      => esc_html__('Portfolio Small Images', 'mkdf-core'),
				'slider'            => esc_html__('Portfolio Slider', 'mkdf-core'),
				'small-slider'      => esc_html__('Portfolio Small Slider', 'mkdf-core'),
				'gallery'           => esc_html__('Portfolio Gallery', 'mkdf-core'),
				'small-gallery'     => esc_html__('Portfolio Small Gallery', 'mkdf-core'),
				'masonry'           => esc_html__('Portfolio Masonry', 'mkdf-core'),
				'small-masonry'     => esc_html__('Portfolio Small Masonry', 'mkdf-core'),
				'custom'            => esc_html__('Portfolio Custom', 'mkdf-core'),
				'full-width-custom' => esc_html__('Portfolio Full Width Custom', 'mkdf-core')
			),
			'args' => array(
				'dependence' => true,
				'show' => array(
					'images'            => '',
					'small-images'      => '',
					'slider'            => '',
					'small-slider'      => '',
					'gallery'           => '#mkdf_portfolio_gallery_container',
					'small-gallery'     => '#mkdf_portfolio_gallery_container',
					'masonry'           => '#mkdf_portfolio_masonry_container',
					'small-masonry'     => '#mkdf_portfolio_masonry_container',
					'custom'            => '',
					'full-width-custom' => ''
				),
				'hide' => array(
					'images'            => '#mkdf_portfolio_gallery_container, #mkdf_portfolio_masonry_container',
					'small-images'      => '#mkdf_portfolio_gallery_container, #mkdf_portfolio_masonry_container',
					'slider'            => '#mkdf_portfolio_gallery_container, #mkdf_portfolio_masonry_container',
					'small-slider'      => '#mkdf_portfolio_gallery_container, #mkdf_portfolio_masonry_container',
					'gallery'           => '#mkdf_portfolio_masonry_container',
					'small-gallery'     => '#mkdf_portfolio_masonry_container',
					'masonry'           => '#mkdf_portfolio_gallery_container',
					'small-masonry'     => '#mkdf_portfolio_gallery_container',
					'custom'            => '#mkdf_portfolio_gallery_container, #mkdf_portfolio_masonry_container',
					'full-width-custom' => '#mkdf_portfolio_gallery_container, #mkdf_portfolio_masonry_container'
				)
			)
		));
		
		/***************** Gallery Layout *****************/
		
		$portfolio_gallery_container = evently_mikado_add_admin_container(array(
			'parent'          => $panel,
			'name'            => 'portfolio_gallery_container',
			'hidden_property' => 'portfolio_single_template',
			'hidden_values' => array(
				'images',
				'small-images',
				'slider',
				'small-slider',
				'masonry',
				'small-masonry',
				'custom',
				'full-width-custom'
			)
		));
		
			evently_mikado_add_admin_field(array(
				'name'        => 'portfolio_single_gallery_columns_number',
				'type'        => 'select',
				'label'       => esc_html__('Number of Columns', 'mkdf-core'),
				'default_value' => 'three',
				'description' => esc_html__('Set number of columns for portfolio gallery type', 'mkdf-core'),
				'parent'      => $portfolio_gallery_container,
				'options'     => array(
					'two'   => esc_html__('2 Columns', 'mkdf-core'),
					'three' => esc_html__('3 Columns', 'mkdf-core'),
					'four'  => esc_html__('4 Columns', 'mkdf-core')
				)
			));
		
			evently_mikado_add_admin_field(array(
				'name'        => 'portfolio_single_gallery_space_between_items',
				'type'        => 'select',
				'label'       => esc_html__('Space Between Items', 'mkdf-core'),
				'default_value' => 'normal',
				'description' => esc_html__('Set space size between columns for portfolio gallery type', 'mkdf-core'),
				'parent'      => $portfolio_gallery_container,
				'options'     => array(
					'normal'    => esc_html__('Normal', 'mkdf-core'),
					'small'     => esc_html__('Small', 'mkdf-core'),
					'tiny'      => esc_html__('Tiny', 'mkdf-core'),
					'no'        => esc_html__('No Space', 'mkdf-core')
				)
			));
		
		/***************** Gallery Layout *****************/
		
		/***************** Masonry Layout *****************/
		
		$portfolio_masonry_container = evently_mikado_add_admin_container(array(
			'parent'          => $panel,
			'name'            => 'portfolio_masonry_container',
			'hidden_property' => 'portfolio_single_template',
			'hidden_values' => array(
				'images',
				'small-images',
				'slider',
				'small-slider',
				'gallery',
				'small-gallery',
				'custom',
				'full-width-custom'
			)
		));
		
			evently_mikado_add_admin_field(array(
				'name'        => 'portfolio_single_masonry_columns_number',
				'type'        => 'select',
				'label'       => esc_html__('Number of Columns', 'mkdf-core'),
				'default_value' => 'three',
				'description' => esc_html__('Set number of columns for portfolio masonry type', 'mkdf-core'),
				'parent'      => $portfolio_masonry_container,
				'options'     => array(
					'two'   => esc_html__('2 Columns', 'mkdf-core'),
					'three' => esc_html__('3 Columns', 'mkdf-core'),
					'four'  => esc_html__('4 Columns', 'mkdf-core')
				)
			));
			
			evently_mikado_add_admin_field(array(
				'name'        => 'portfolio_single_masonry_space_between_items',
				'type'        => 'select',
				'label'       => esc_html__('Space Between Items', 'mkdf-core'),
				'default_value' => 'normal',
				'description' => esc_html__('Set space size between columns for portfolio masonry type', 'mkdf-core'),
				'parent'      => $portfolio_masonry_container,
				'options'     => array(
					'normal'    => esc_html__('Normal', 'mkdf-core'),
					'small'     => esc_html__('Small', 'mkdf-core'),
					'tiny'      => esc_html__('Tiny', 'mkdf-core'),
					'no'        => esc_html__('No Space', 'mkdf-core')
				)
			));
		
		/***************** Masonry Layout *****************/
		
		evently_mikado_add_admin_field(
			array(
				'type' => 'select',
				'name' => 'show_title_area_portfolio_single',
				'default_value' => '',
				'label'       => esc_html__('Show Title Area', 'mkdf-core'),
				'description' => esc_html__('Enabling this option will show title area on single projects', 'mkdf-core'),
				'parent'      => $panel,
                'options' => array(
                    '' => esc_html__('Default', 'mkdf-core'),
                    'yes' => esc_html__('Yes', 'mkdf-core'),
                    'no' => esc_html__('No', 'mkdf-core')
                ),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		evently_mikado_add_admin_field(array(
			'name'          => 'portfolio_single_lightbox_images',
			'type'          => 'yesno',
			'label'         => esc_html__('Enable Lightbox for Images', 'mkdf-core'),
			'description'   => esc_html__('Enabling this option will turn on lightbox functionality for projects with images', 'mkdf-core'),
			'parent'        => $panel,
			'default_value' => 'yes'
		));

		evently_mikado_add_admin_field(array(
			'name'          => 'portfolio_single_lightbox_videos',
			'type'          => 'yesno',
			'label'         => esc_html__('Enable Lightbox for Videos', 'mkdf-core'),
			'description'   => esc_html__('Enabling this option will turn on lightbox functionality for YouTube/Vimeo projects', 'mkdf-core'),
			'parent'        => $panel,
			'default_value' => 'no'
		));

		evently_mikado_add_admin_field(array(
			'name'          => 'portfolio_single_enable_categories',
			'type'          => 'yesno',
			'label'         => esc_html__('Enable Categories', 'mkdf-core'),
			'description'   => esc_html__('Enabling this option will enable category meta description on single projects', 'mkdf-core'),
			'parent'        => $panel,
			'default_value' => 'yes'
		));

		evently_mikado_add_admin_field(array(
			'name'          => 'portfolio_single_hide_date',
			'type'          => 'yesno',
			'label'         => esc_html__('Enable Date', 'mkdf-core'),
			'description'   => esc_html__('Enabling this option will enable date meta on single projects', 'mkdf-core'),
			'parent'        => $panel,
			'default_value' => 'yes'
		));
		
		evently_mikado_add_admin_field(array(
			'name'          => 'portfolio_single_sticky_sidebar',
			'type'          => 'yesno',
			'label'         => esc_html__('Enable Sticky Side Text', 'mkdf-core'),
			'description'   => esc_html__('Enabling this option will make side text sticky on Single Project pages. This option works only for Full Width Images, Small Images, Small Gallery and Small Masonry portfolio types', 'mkdf-core'),
			'parent'        => $panel,
			'default_value' => 'yes'
		));

		evently_mikado_add_admin_field(array(
			'name'          => 'portfolio_single_comments',
			'type'          => 'yesno',
			'label'         => esc_html__('Show Comments', 'mkdf-core'),
			'description'   => esc_html__('Enabling this option will show comments on your page', 'mkdf-core'),
			'parent'        => $panel,
			'default_value' => 'no'
		));

		evently_mikado_add_admin_field(array(
			'name'          => 'portfolio_single_hide_pagination',
			'type'          => 'yesno',
			'label'         => esc_html__('Hide Pagination', 'mkdf-core'),
			'description'   => esc_html__('Enabling this option will turn off portfolio pagination functionality', 'mkdf-core'),
			'parent'        => $panel,
			'default_value' => 'no',
			'args' => array(
				'dependence' => true,
				'dependence_hide_on_yes' => '#mkdf_navigate_same_category_container'
			)
		));

			$container_navigate_category = evently_mikado_add_admin_container(array(
				'name'            => 'navigate_same_category_container',
				'parent'          => $panel,
				'hidden_property' => 'portfolio_single_hide_pagination',
				'hidden_value'    => 'yes'
			));
	
				evently_mikado_add_admin_field(array(
					'name'            => 'portfolio_single_nav_same_category',
					'type'            => 'yesno',
					'label'           => esc_html__('Enable Pagination Through Same Category', 'mkdf-core'),
					'description'     => esc_html__('Enabling this option will make portfolio pagination sort through current category', 'mkdf-core'),
					'parent'          => $container_navigate_category,
					'default_value'   => 'no'
				));

		evently_mikado_add_admin_field(array(
			'name'        => 'portfolio_single_slug',
			'type'        => 'text',
			'label'       => esc_html__('Portfolio Single Slug', 'mkdf-core'),
			'description' => esc_html__('Enter if you wish to use a different Single Project slug (Note: After entering slug, navigate to Settings -> Permalinks and click "Save" in order for changes to take effect)', 'mkdf-core'),
			'parent'      => $panel,
			'args'        => array(
				'col_width' => 3
			)
		));
	}

	add_action( 'evently_mikado_action_options_map', 'evently_mikado_portfolio_options_map', 10);
}