<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <?php
    /**
     * evently_mikado_action_header_meta hook
     *
     * @see evently_mikado_header_meta() - hooked with 10
     * @see evently_mikado_user_scalable_meta - hooked with 10
     */
    do_action('evently_mikado_action_header_meta');

    wp_head(); ?>
</head>
<body <?php body_class();?> itemscope itemtype="http://schema.org/WebPage">
	<?php
	/**
	 * evently_mikado_action_after_body_tag hook
	 *
	 * @see evently_mikado_get_side_area() - hooked with 10
	 * @see evently_mikado_smooth_page_transitions() - hooked with 10
	 */
	do_action('evently_mikado_action_after_body_tag'); ?>
	
	<div class="mkdf-wrapper mkdf-404-page">
	    <div class="mkdf-wrapper-inner">
		    <?php evently_mikado_get_header(); ?>
		    
			<div class="mkdf-content" <?php evently_mikado_content_elem_style_attr(); ?>>
	            <div class="mkdf-content-inner">
					<div class="mkdf-page-not-found">
						<?php
							$mkdf_title_image_404 = evently_mikado_options()->getOptionValue('404_page_title_image');
							$mkdf_title_404       = evently_mikado_options()->getOptionValue('404_title');
							$mkdf_subtitle_404    = evently_mikado_options()->getOptionValue('404_subtitle');
							$mkdf_text_404        = evently_mikado_options()->getOptionValue('404_text');
							$mkdf_button_label    = evently_mikado_options()->getOptionValue('404_back_to_home');
							$mkdf_button_style    = evently_mikado_options()->getOptionValue('404_button_style');
						?>

						<?php if (!empty($mkdf_title_image_404)) { ?>
							<div class="mkdf-404-title-image">
								<img src="<?php echo esc_url($mkdf_title_image_404); ?>" alt="<?php esc_html_e('404 Title Image', 'evently'); ?>" />
							</div>
						<?php } else { ?>
							<div class="mkdf-404-title-image">
								<img src="<?php echo MIKADO_ASSETS_ROOT . '/img/404.png' ?>" alt="<?php esc_html_e('404 Title Image', 'evently'); ?>" />
							</div>
						<?php } ?>

						<h1 class="mkdf-404-title">
							<?php if(!empty($mkdf_subtitle_404)){
								echo esc_html($mkdf_subtitle_404);
							} else {
								esc_html_e('Page you are looking is not found ', 'evently');
							} ?>
						</h1>

						<p class="mkdf-404-text">
							<?php if(!empty($mkdf_text_404)){
								echo esc_html($mkdf_text_404);
							} else {
								esc_html_e('The page you are looking for does not exist. It may have been moved, or removed altogether. Go back to the site\'s homepage and see if you can find what you are looking for.', 'evently');
							} ?>
						</p>

						<?php
							$mkdf_params = array();
							$mkdf_params['text'] = !empty($mkdf_button_label) ? $mkdf_button_label : esc_html__('BACK TO HOME', 'evently');
							$mkdf_params['link'] = esc_url(home_url('/'));
							$mkdf_params['target'] = '_self';
							$mkdf_params['size'] = 'large';
						
							if ($mkdf_button_style == 'light-style'){
								$mkdf_params['custom_class'] = 'mkdf-btn-light-style';
							}
	
							echo evently_mikado_execute_shortcode('mkdf_button',$mkdf_params); ?>
					</div>
				</div>	
			</div>
		</div>
	</div>		
	<?php wp_footer(); ?>
</body>
</html>