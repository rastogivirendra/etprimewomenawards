<?php

if(!function_exists('evently_mikado_design_styles')) {
    /**
     * Generates general custom styles
     */
    function evently_mikado_design_styles() {
	    $font_family = evently_mikado_options()->getOptionValue( 'google_fonts' );
	    if ( ! empty( $font_family ) && evently_mikado_is_font_option_valid( $font_family ) ) {
		    $font_family_selector = array(
			    'body',
			    'h1',
			    'h2',
			    'h3',
			    'h4',
			    'h5',
			    'h6',
			    'blockquote',
			    '.mkdf-comment-holder .mkdf-comment-text .mkdf-comment-dat',
			    '#respond textarea',
			    '#respond input[type="text"]',
				'.post-password-form input[type="password"]',
			    '.mkdf-page-footer .mkdf-footer-bottom-holder .widget.widget_text',
			    '.mkdf-blog-holder article .mkdf-post-info-top .mkdf-post-info-date a',
			    '.mkdf-blog-holder.mkdf-blog-metro article.format-quote .mkdf-quote-author',
			    '.mkdf-blog-holder.mkdf-blog-standard article .mkdf-post-info-bottom > div',
			    '.mkdf-blog-holder.mkdf-blog-standard article.format-quote .mkdf-quote-author',
			    '.mkdf-blog-single-navigation .mkdf-blog-single-prev-holder .mkdf-blog-single-nav-wrapper .mkdf-blog-single-nav-date',
			    '.mkdf-blog-single-navigation .mkdf-blog-single-next-holder .mkdf-blog-single-nav-wrapper .mkdf-blog-single-nav-date',
			    '.mkdf-blog-list-holder .mkdf-bli-info-top .mkdf-post-info-date a',
			    '.mkdf-blog-list-holder .mkdf-bli-info-bottom .mkdf-bli-info-bottom-left > div',
			    '.mkdf-blog-list-holder.mkdf-bl-masonry .mkdf-post-quote-holder .mkdf-quote-author',
			    '.mkdf-blog-holder.mkdf-blog-single article .mkdf-post-info-bottom > div',
			    '.mkdf-blog-holder.mkdf-blog-single article .mkdf-post-info .mkdf-post-info-right .mkdf-blog-share .mkdf-share-label',
			    '.mkdf-blog-holder.mkdf-blog-single article.format-quote .mkdf-quote-author',
			    '.mkdf-ttevents-single .tt_event_items_list li.type_info label',
			    '.tt_tabs .tt_tabs_navigation li a',
			    'table.tt_timetable thead th',
			    'table.tt_timetable .tt_tooltip .tt_tooltip_content',
			    '.tt_responsive .tt_timetable.small .box_header',
			    '.tt_responsive .tt_timetable.small .tt_items_list a',
			    '.mkdf-timetable-list .mkdf-tl-heading .mkdf-tl-subtitle',
			    '.mkdf-timetable-list .mkdf-timetable-list-item .mkdf-tli-title',
			    '.mkdf-title-holder.mkdf-centered-type .mkdf-page-tagline',
			    '.mkdf-title-holder.mkdf-standard-type .mkdf-page-tagline',
			    '.mkdf-team-modal-holder .mkdf-team-website',
			    '.mkdf-team-modal-holder .mkdf-team-info-sessions .mkdf-team-date-wrap span',
			    '.mkdf-team.info-aside .mkdf-team-position',
			    '.mkdf-team.info-bellow .mkdf-team-position',
			    '.mkdf-team-single-holder .mkdf-ts-website .mkdf-team-website',
			    '.mkdf-team-single-holder .mkdf-team-single-sessions .mkdf-team-session-box-inner .mkdf-team-date-wrap span',
			    '.mkdf-testimonials-holder.mkdf-testimonials-standard .mkdf-testimonial-author .mkdf-testimonials-author-job',
			    '.mkdf-iwt.mkdf-iwt-icon-top .mkdf-iwt-content .mkdf-iwt-text',
			    '.woocommerce .mkdf-onsale',
			    '.woocommerce .mkdf-out-of-stock',
			    '.mkdf-woocommerce-page .mkdf-content .variations td .select2-selection__rendered',
			    '.mkdf-plc-holder .mkdf-plc-item .mkdf-plc-image-outer .mkdf-plc-image .mkdf-plc-onsale',
			    '.mkdf-plc-holder .mkdf-plc-item .mkdf-plc-image-outer .mkdf-plc-image .mkdf-plc-out-of-stock',
			    '.mkdf-pl-holder .mkdf-pli-inner .mkdf-pli-image .mkdf-pli-onsale',
			    '.mkdf-pl-holder .mkdf-pli-inner .mkdf-pli-image .mkdf-pli-out-of-stock'
		    );
		    echo evently_mikado_dynamic_css( $font_family_selector, array( 'font-family' => evently_mikado_get_font_option_val( $font_family ) ) );
	    }

		$first_main_color = evently_mikado_options()->getOptionValue('first_color');
        if(!empty($first_main_color)) {
            $color_selector = array(
                'h1 a:hover',
                'h2 a:hover',
                'h3 a:hover',
                'h4 a:hover',
                'h5 a:hover',
                'h6 a:hover',
                'a:hover',
                'p a:hover',
	            'a:hover',
	            'p a:hover',
	            '.mkdf-owl-slider .owl-nav .owl-prev:hover',
	            '.mkdf-owl-slider .owl-nav .owl-next:hover',
	            '.widget.mkdf-blog-list-widget .mkdf-post-info-date a:hover',
	            '.widget.widget_rss .mkdf-widget-title .rsswidget:hover',
	            '.widget.widget_tag_cloud a:hover',
	            '.mkdf-side-menu .widget a:hover',
	            '.mkdf-side-menu .widget.widget_rss .mkdf-footer-widget-title .rsswidget:hover',
	            '.mkdf-side-menu .widget.widget_search button:hover',
	            '.mkdf-side-menu .widget.widget_tag_cloud a:hover',
	            '.mkdf-page-footer .widget.widget_rss .mkdf-footer-widget-title .rsswidget:hover',
	            '.mkdf-page-footer .widget.widget_search button:hover',
	            '.mkdf-page-footer .widget.widget_tag_cloud a:hover',
	            '.mkdf-page-footer .mkdf-footer-bottom-holder a:hover',
	            '.mkdf-top-bar a:hover',
	            '.mkdf-icon-widget-holder',
	            '.mkdf-icon-widget-holder.mkdf-link-with-href:hover .mkdf-icon-text',
	            '.widget.widget_mkdf_twitter_widget .mkdf-twitter-widget.mkdf-twitter-standard li .mkdf-twitter-icon',
	            '.widget.widget_mkdf_twitter_widget .mkdf-twitter-widget.mkdf-twitter-standard li .mkdf-tweet-text a:hover',
	            '.widget.widget_mkdf_twitter_widget .mkdf-twitter-widget.mkdf-twitter-slider li .mkdf-twitter-icon i',
	            '.widget.widget_mkdf_twitter_widget .mkdf-twitter-widget.mkdf-twitter-slider li .mkdf-tweet-text a',
	            '.widget.widget_mkdf_twitter_widget .mkdf-twitter-widget.mkdf-twitter-slider li .mkdf-tweet-text span',
	            '.mkdf-blog-holder article.sticky .mkdf-post-title a',
	            '.mkdf-blog-holder.mkdf-blog-standard article .mkdf-post-text .mkdf-post-info-top .mkdf-post-info-date a:hover',
	            '.mkdf-blog-holder.mkdf-blog-standard article .mkdf-post-text .mkdf-post-info-bottom .mkdf-post-info-bottom-left > div a:hover',
	            '.mkdf-blog-holder.mkdf-blog-standard article .mkdf-post-text .mkdf-post-info-bottom .mkdf-post-info-author a:hover',
	            '.mkdf-blog-holder.mkdf-blog-standard article .mkdf-post-text .mkdf-post-info-bottom .mkdf-post-info-comments-holder a:hover',
	            '.mkdf-author-description .mkdf-author-description-text-holder .mkdf-author-name a:hover',
	            '.mkdf-author-description .mkdf-author-description-text-holder .mkdf-author-social-icons a:hover',
	            '.mkdf-blog-pagination ul li a:hover',
	            '.mkdf-blog-pagination ul li a.mkdf-pag-active',
	            '.mkdf-bl-standard-pagination ul li.mkdf-bl-pag-active a',
	            '.mkdf-blog-list-holder .mkdf-bli-info-top .mkdf-post-info-date a:hover',
	            '.mkdf-blog-list-holder .mkdf-bli-info-bottom .mkdf-post-info-author a:hover',
	            '.mkdf-blog-list-holder .mkdf-bli-info-bottom .mkdf-post-info-comments-holder a:hover',
	            '.mkdf-blog-list-holder.mkdf-bl-standard .mkdf-bli-info-bottom-left > div a:hover',
	            '.mkdf-blog-slider-holder .mkdf-blog-slider-item .mkdf-item-info-section > div a:hover',
	            '.mkdf-blog-holder.mkdf-blog-single article .mkdf-post-info-top .mkdf-post-info-date a:hover',
	            '.mkdf-blog-holder.mkdf-blog-single article .mkdf-post-info-bottom > div a:hover',
	            '.mkdf-blog-holder.mkdf-blog-single article .mkdf-post-info-bottom .mkdf-post-info-bottom-left > div a:hover',
	            '.mkdf-blog-holder.mkdf-blog-single article .mkdf-post-info-bottom .mkdf-post-info-comments-holder a:hover',
	            '.mkdf-blog-holder.mkdf-blog-single article .mkdf-post-info .mkdf-tags a:hover',
	            '.mkdf-footer-light-skin .mkdf-page-footer .mkdf-footer-bottom-holder a',
	            '.mkdf-main-menu ul li a:hover',
	            '.mkdf-main-menu > ul > li.mkdf-active-item > a',
	            '.mkdf-drop-down .second .inner ul li a:hover',
	            '.mkdf-drop-down .second .inner ul li.current-menu-ancestor > a',
	            '.mkdf-drop-down .second .inner ul li.current-menu-item > a',
	            '.mkdf-drop-down .wide .second .inner > ul > li.current-menu-ancestor > a',
	            '.mkdf-drop-down .wide .second .inner > ul > li.current-menu-item > a',
	            '.mkdf-fullscreen-menu-opener.mkdf-fm-opened',
	            'nav.mkdf-fullscreen-menu ul li a:hover',
	            'nav.mkdf-fullscreen-menu ul li.open_sub > a',
	            'nav.mkdf-fullscreen-menu ul li ul li a:hover',
	            '.mkdf-mobile-header .mkdf-mobile-nav ul li a:hover',
	            '.mkdf-mobile-header .mkdf-mobile-nav ul li h5:hover',
	            '.mkdf-mobile-header .mkdf-mobile-nav ul ul li.current-menu-ancestor > a',
	            '.mkdf-mobile-header .mkdf-mobile-nav ul ul li.current-menu-item > a',
	            '.mkdf-mobile-header .mkdf-mobile-nav .mkdf-grid > ul > li.mkdf-active-item > a',
	            '.mkdf-search-page-holder article.sticky .mkdf-post-title a',
	            '.mkdf-fullscreen-search-holder .mkdf-search-submit',
	            '.mkdf-ttevents-single .mkdf-ttevents-single-subtitle',
	            '.mkdf-ttevents-single .tt_event_hours li h4:nth-of-type(2)',
	            '.mkdf-ttevents-single .tt_event_items_list li.type_info .tt_event_text',
	            '.mkdf-ttevents-single .tt_event_items_list li:not(.type_info):before',
	            '.mkdf-pl-filter-holder ul li.mkdf-pl-current span',
	            '.mkdf-pl-filter-holder ul li:hover span',
	            '.mkdf-pl-standard-pagination ul li.mkdf-pl-pag-active a',
	            '.mkdf-portfolio-list-holder.mkdf-pl-gallery-overlay article .mkdf-pli-text .mkdf-pli-category-holder a:hover',
	            '.mkdf-team-modal-holder .mkdf-team-website:hover',
	            '.mkdf-team-single-holder .mkdf-ts-website .mkdf-team-website:hover',
	            '.mkdf-banner-holder .mkdf-banner-link-text .mkdf-banner-link-hover span',
	            '.mkdf-btn.mkdf-btn-outline',
	            '.mkdf-social-share-holder.mkdf-dropdown .mkdf-social-share-dropdown-opener:hover',
	            '.mkdf-tabs.mkdf-tabs-standard .mkdf-tabs-nav li.ui-state-active a',
	            '.mkdf-tabs.mkdf-tabs-standard .mkdf-tabs-nav li.ui-state-hover a',
	            '.woocommerce-pagination .page-numbers li a:hover',
	            '.woocommerce-pagination .page-numbers li a.current',
	            '.woocommerce-pagination .page-numbers li span:hover',
	            '.woocommerce-pagination .page-numbers li span.current',
	            '#mkdf-back-to-top>span',
	            '.mkdf-comment-holder .mkdf-comment-text .comment-edit-link:hover',
	            '.mkdf-comment-holder .mkdf-comment-text .comment-reply-link:hover',
	            '.mkdf-comment-holder .mkdf-comment-text .replay:hover',
	            '.mkdf-team-single-holder .mkdf-team-single-sessions .mkdf-team-session-box-inner .mkdf-team-session-title'
            );

            $woo_color_selector = array();
            if(evently_mikado_is_woocommerce_installed()) {
                $woo_color_selector = array(
	                '.woocommerce-page .mkdf-content .mkdf-quantity-buttons .mkdf-quantity-minus:hover',
	                '.woocommerce-page .mkdf-content .mkdf-quantity-buttons .mkdf-quantity-plus:hover',
	                'div.woocommerce .mkdf-quantity-buttons .mkdf-quantity-minus:hover',
	                'div.woocommerce .mkdf-quantity-buttons .mkdf-quantity-plus:hover',
	                'ul.products > .product .mkdf-pl-category a:hover',
	                'ul.products > .product .price ins',
	                '.mkdf-woo-pl-info-on-image-hover ul.products > .product .mkdf-pl-text .mkdf-pl-text-inner .button:hover',
	                '.mkdf-woo-pl-info-on-image-hover ul.products > .product .mkdf-pl-text .mkdf-pl-text-inner .added_to_cart:hover',
	                'div.woocommerce > .single-product .product_meta > span a:hover',
	                'div.woocommerce > .single-product .mkdf-woo-social-share-holder .social_share',
	                'div.woocommerce > .single-product .woocommerce-tabs ul.tabs > li.active a, div.woocommerce > .single-product .woocommerce-tabs ul.tabs > li:hover a',
	                '.mkdf-woo-single-page .mkdf-single-product-summary .mkdf-woo-social-share-holder .social_share',
	                '.mkdf-woo-single-page .mkdf-single-product-summary .product_meta > span a:hover',
	                '.mkdf-woo-single-page .woocommerce-tabs ul.tabs > li.active a, .mkdf-woo-single-page .woocommerce-tabs ul.tabs > li:hover a',
	                '.widget.woocommerce.widget_layered_nav ul li.chosen a',
	                '.widget.woocommerce.widget_products ul li ins .amount',
	                '.widget.woocommerce.widget_recently_viewed_products ul li ins .amount',
	                '.widget.woocommerce.widget_recent_reviews ul li ins .amount',
	                '.widget.woocommerce.widget_top_rated_products ul li ins .amount',
	                '.widget.woocommerce.widget_product_tag_cloud .tagcloud a:hover',
	                '.mkdf-plc-holder .mkdf-plc-item p.mkdf-plc-title a:hover',
	                '.mkdf-plc-holder .mkdf-plc-item .mkdf-plc-price ins',
	                '.mkdf-plc-holder .mkdf-plc-item .button:hover',
	                '.mkdf-plc-holder .mkdf-plc-item .added_to_cart:hover',
	                '.mkdf-pls-holder .mkdf-pls-text p.mkdf-pls-title a:hover',
	                '.mkdf-pl-holder .mkdf-pli .mkdf-pli-price ins',
	                '.mkdf-pl-holder.mkdf-info-on-image .mkdf-pli-text .mkdf-pli-text-inner .mkdf-pli-category a:hover',
	                '.mkdf-pl-holder.mkdf-info-on-image .mkdf-pli-text .mkdf-pli-text-inner .button:hover',
	                '.mkdf-pl-holder.mkdf-info-on-image .mkdf-pli-text .mkdf-pli-text-inner .added_to_cart:hover'
                );
            }

            $color_selector = array_merge($color_selector, $woo_color_selector);

	        $color_important_selector = array(
		        '.mkdf-blog-slider-holder .mkdf-blog-slider-item .mkdf-section-button-holder a:hover',
		        '.tt_tabs .tt_tabs_navigation .ui-tabs-active a',
		        '.tt_tabs .tt_tabs_navigation li a:hover',
		        'table.tt_timetable .event a'
	        );

            $background_color_selector = array(
                '.mkdf-st-loader .pulse',
                '.mkdf-st-loader .double_pulse .double-bounce1', 
                '.mkdf-st-loader .double_pulse .double-bounce2',
                '.mkdf-st-loader .cube',
                '.mkdf-st-loader .rotating_cubes .cube1',
                '.mkdf-st-loader .rotating_cubes .cube2',
                '.mkdf-st-loader .stripes > div',
                '.mkdf-st-loader .wave > div',
                '.mkdf-st-loader .two_rotating_circles .dot1',
                '.mkdf-st-loader .two_rotating_circles .dot2',
                '.mkdf-st-loader .five_rotating_circles .container1 > div',
                '.mkdf-st-loader .five_rotating_circles .container2 > div',
                '.mkdf-st-loader .five_rotating_circles .container3 > div',
                '.mkdf-st-loader .atom .ball-1:before',
                '.mkdf-st-loader .atom .ball-2:before',
                '.mkdf-st-loader .atom .ball-3:before',
                '.mkdf-st-loader .atom .ball-4:before',
                '.mkdf-st-loader .clock .ball:before',
                '.mkdf-st-loader .mitosis .ball',
                '.mkdf-st-loader .lines .line1',
                '.mkdf-st-loader .lines .line2',
                '.mkdf-st-loader .lines .line3',
                '.mkdf-st-loader .lines .line4',
                '.mkdf-st-loader .fussion .ball',
                '.mkdf-st-loader .fussion .ball-1',
                '.mkdf-st-loader .fussion .ball-2',
                '.mkdf-st-loader .fussion .ball-3',
                '.mkdf-st-loader .fussion .ball-4',
                '.mkdf-st-loader .wave_circles .ball',
                '.mkdf-st-loader .pulse_circles .ball',
	            '#submit_comment:hover',
	            '.post-password-form input[type="submit"]:hover',
	            'input.wpcf7-form-control.wpcf7-submit',
	            '.widget #wp-calendar td#today',
	            '.mkdf-blog-holder article.format-audio .mkdf-blog-audio-holder .mejs-container .mejs-controls > .mejs-time-rail .mejs-time-total .mejs-time-current',
	            '.mkdf-blog-holder article.format-audio .mkdf-blog-audio-holder .mejs-container .mejs-controls > a.mejs-horizontal-volume-slider .mejs-horizontal-volume-current',
	            '.mkdf-blog-holder.mkdf-blog-masonry article.format-link .mkdf-post-content .mkdf-post-link-holder',
	            '.mkdf-blog-holder.mkdf-blog-metro article.format-link .mkdf-post-content .mkdf-post-link-holder',
	            '.mkdf-blog-holder.mkdf-blog-standard article.format-link .mkdf-post-content .mkdf-post-link-holder',
	            '.mkdf-blog-list-holder.mkdf-bl-masonry .mkdf-post-link-holder',
	            '.mkdf-blog-holder.mkdf-blog-single article.format-link .mkdf-post-content .mkdf-post-link-holder',
	            '.mkdf-search-fade .mkdf-fullscreen-search-holder .mkdf-fullscreen-search-table',
	            'table.tt_timetable .tt_tooltip .tt_tooltip_content',
	            '.widget.upcoming_events_widget .tt_upcoming_event_controls a:hover',
	            '.mkdf-team-modal-holder .mkdf-close',
	            '.mkdf-team-modal-holder .mkdf-team-title-holder .mkdf-team-popup-social .mkdf-icon-shortcode',
	            '.mkdf-team-single-holder .mkdf-team-social-icons .mkdf-icon-shortcode',
	            '.mkdf-accordion-holder.mkdf-ac-boxed .mkdf-accordion-title.ui-state-active',
	            '.mkdf-accordion-holder.mkdf-ac-boxed .mkdf-accordion-title.ui-state-hover',
	            '.mkdf-icon-shortcode.mkdf-circle',
	            '.mkdf-icon-shortcode.mkdf-square',
	            '.mkdf-icon-shortcode.mkdf-dropcaps.mkdf-circle',
	            '.mkdf-process-holder .mkdf-process-circle',
	            '.mkdf-process-holder .mkdf-process-line',
	            '.mkdf-progress-bar .mkdf-pb-content-holder .mkdf-pb-content',
	            '.mkdf-section-title-holder .mkdf-st-separator',
	            '.mkdf-tabs.mkdf-tabs-boxed .mkdf-tabs-nav li.ui-state-active a',
	            '.mkdf-tabs.mkdf-tabs-boxed .mkdf-tabs-nav li.ui-state-hover a',
	            '.mkdf-image-gallery .mkdf-ig-slider .owl-dots .owl-dot.active span',
	            '.mkdf-image-gallery .mkdf-ig-slider .owl-dots .owl-dot:hover span',
	            '.mkdf-banner-holder .mkdf-banner-hover-layout .mkdf-banner-button .mkdf-btn:hover'
            );

            $woo_background_color_selector = array();
            if(evently_mikado_is_woocommerce_installed()) {
                $woo_background_color_selector = array(
	                '.woocommerce-page .mkdf-content a.button:hover',
	                '.woocommerce-page .mkdf-content a.added_to_cart:hover',
	                '.woocommerce-page .mkdf-content input[type="submit"]:hover',
	                '.woocommerce-page .mkdf-content button[type="submit"]:not(.mkdf-woo-search-widget-button):hover',
	                '.woocommerce-page .mkdf-content .wc-forward:not(.added_to_cart):not(.checkout-button):hover',
	                'div.woocommerce a.button:hover',
	                'div.woocommerce a.added_to_cart:hover',
	                'div.woocommerce input[type="submit"]:hover',
	                'div.woocommerce button[type="submit"]:not(.mkdf-woo-search-widget-button):hover',
	                'div.woocommerce .wc-forward:not(.added_to_cart):not(.checkout-button):hover',
	                '.mkdf-shopping-cart-dropdown .mkdf-cart-bottom .mkdf-view-cart',
	                '.mkdf-pl-holder.mkdf-info-below-image .mkdf-pli-add-to-cart.mkdf-default-skin .button:hover',
	                '.mkdf-pl-holder.mkdf-info-below-image .mkdf-pli-add-to-cart.mkdf-default-skin .added_to_cart:hover',
	                '.mkdf-pl-holder.mkdf-info-below-image .mkdf-pli-add-to-cart.mkdf-light-skin .button:hover',
	                '.mkdf-pl-holder.mkdf-info-below-image .mkdf-pli-add-to-cart.mkdf-light-skin .added_to_cart:hover',
	                '.mkdf-pl-holder.mkdf-info-below-image .mkdf-pli-add-to-cart.mkdf-dark-skin .button:hover',
	                '.mkdf-pl-holder.mkdf-info-below-image .mkdf-pli-add-to-cart.mkdf-dark-skin .added_to_cart:hover'
                );
            }

            $background_color_selector = array_merge($background_color_selector, $woo_background_color_selector);

	        $background_color_important_selector = array();
	        if(evently_mikado_is_woocommerce_installed()) {
		        $background_color_important_selector = array(
			        '.mkdf-dark-header .mkdf-page-header > div:not(.mkdf-sticky-header) .mkdf-fullscreen-menu-opener:not(.opened):hover .mkdf-line',
			        '.mkdf-dark-header .mkdf-page-header > div:not(.mkdf-sticky-header) .mkdf-fullscreen-menu-opener:not(.opened):hover .mkdf-line:before',
			        '.mkdf-dark-header .mkdf-page-header > div:not(.mkdf-sticky-header) .mkdf-fullscreen-menu-opener:not(.opened):hover .mkdf-line:after',
			        '.tt_tabs .tt_tabs_navigation .ui-tabs-active a',
			        '.tt_tabs .tt_tabs_navigation li a:hover',
			        '.mkdf-btn.mkdf-btn-outline:not(.mkdf-btn-custom-hover-bg):hover'
		        );
	        }

            $border_color_selector = array(
                '.mkdf-st-loader .pulse_circles .ball',
	            '.mkdf-btn.mkdf-btn-solid',
	            '.mkdf-sidebar .mkdf-widget-title:after',
	            '.mkdf-blog-pagination ul li a:hover',
	            '.mkdf-blog-pagination ul li a.mkdf-pag-active',
	            '.mkdf-blog-holder.mkdf-blog-single article .mkdf-post-info .mkdf-tags a:hover',
	            'nav.mkdf-fullscreen-menu ul > li > a > span:after',
	            'nav.mkdf-fullscreen-menu ul > li > ul > li > a > span:after',
	            'table.tt_timetable .tt_tooltip .tt_tooltip_arrow',
	            '.widget.upcoming_events_widget .tt_upcoming_event_controls a:hover',
	            '.mkdf-team-modal-holder .mkdf-team-info-section .mkdf-team-info-section-title:after',
	            '.mkdf-team-single-holder .mkdf-team-name-separator .mkdf-separator',
	            '.mkdf-team-single-holder .mkdf-ts-website .mkdf-team-info-section-title:after',
	            '.mkdf-testimonials-holder.mkdf-testimonials-standard .mkdf-testimonial-title:after',
	            '.mkdf-btn.mkdf-btn-outline',
	            '.woocommerce-pagination .page-numbers li a:hover',
	            '.woocommerce-pagination .page-numbers li a.current',
	            '.woocommerce-pagination .page-numbers li span:hover',
	            '.woocommerce-pagination .page-numbers li span.current',
	            '.mkdf-content .woocommerce.add_to_cart_inline',
	            'div.woocommerce > .single-product .woocommerce-tabs ul.tabs > li:after',
	            '.mkdf-woo-single-page .woocommerce-tabs ul.tabs > li:after',
	            '.mkdf-image-gallery .mkdf-ig-slider .owl-dots .owl-dot.active span',
	            '.mkdf-image-gallery .mkdf-ig-slider .owl-dots .owl-dot:hover span',
	            '.mkdf-banner-holder .mkdf-banner-hover-layout .mkdf-banner-button .mkdf-btn:hover'
            );

	        $border_color_important_selector = array(
	        	'.mkdf-btn.mkdf-btn-outline:not(.mkdf-btn-custom-border-hover):hover'
	        );

            echo evently_mikado_dynamic_css($color_selector, array('color' => $first_main_color));
	        echo evently_mikado_dynamic_css($color_important_selector, array('color' => $first_main_color.'!important'));
	        echo evently_mikado_dynamic_css($background_color_selector, array('background-color' => $first_main_color));
	        echo evently_mikado_dynamic_css($background_color_important_selector, array('background-color' => $first_main_color.'!important'));
	        echo evently_mikado_dynamic_css($border_color_selector, array('border-color' => $first_main_color));
	        echo evently_mikado_dynamic_css($border_color_important_selector, array('border-color' => $first_main_color.'!important'));
        }
	
	    $page_background_color = evently_mikado_options()->getOptionValue( 'page_background_color' );
	    if ( ! empty( $page_background_color ) ) {
		    $background_color_selector = array(
			    'body',
			    '.mkdf-content',
			    '.mkdf-container'
		    );
		    echo evently_mikado_dynamic_css( $background_color_selector, array( 'background-color' => $page_background_color ) );
	    }
	
	    $selection_color = evently_mikado_options()->getOptionValue( 'selection_color' );
	    if ( ! empty( $selection_color ) ) {
		    echo evently_mikado_dynamic_css( '::selection', array( 'background' => $selection_color ) );
		    echo evently_mikado_dynamic_css( '::-moz-selection', array( 'background' => $selection_color ) );
	    }
	
	    $preload_background_styles = array();
	
	    if ( evently_mikado_options()->getOptionValue( 'preload_pattern_image' ) !== "" ) {
		    $preload_background_styles['background-image'] = 'url(' . evently_mikado_options()->getOptionValue( 'preload_pattern_image' ) . ') !important';
	    }
	
	    echo evently_mikado_dynamic_css( '.mkdf-preload-background', $preload_background_styles );
    }

    add_action('evently_mikado_action_style_dynamic', 'evently_mikado_design_styles');
}

if ( ! function_exists( 'evently_mikado_content_styles' ) ) {
	function evently_mikado_content_styles() {
		$content_style = array();
		
		$padding_top = evently_mikado_options()->getOptionValue( 'content_top_padding' );
		if ( $padding_top !== '' ) {
			$content_style['padding-top'] = evently_mikado_filter_px( $padding_top ) . 'px';
		}
		
		$content_selector = array(
			'.mkdf-content .mkdf-content-inner > .mkdf-full-width > .mkdf-full-width-inner',
		);
		
		echo evently_mikado_dynamic_css( $content_selector, $content_style );
		
		$content_style_in_grid = array();
		
		$padding_top_in_grid = evently_mikado_options()->getOptionValue( 'content_top_padding_in_grid' );
		if ( $padding_top_in_grid !== '' ) {
			$content_style_in_grid['padding-top'] = evently_mikado_filter_px( $padding_top_in_grid ) . 'px';
		}
		
		$content_selector_in_grid = array(
			'.mkdf-content .mkdf-content-inner > .mkdf-container > .mkdf-container-inner',
		);
		
		echo evently_mikado_dynamic_css( $content_selector_in_grid, $content_style_in_grid );
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_content_styles' );
}

if ( ! function_exists( 'evently_mikado_h1_styles' ) ) {
	function evently_mikado_h1_styles() {
		$margin_top    = evently_mikado_options()->getOptionValue( 'h1_margin_top' );
		$margin_bottom = evently_mikado_options()->getOptionValue( 'h1_margin_bottom' );
		
		$item_styles = evently_mikado_get_typography_styles( 'h1' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = evently_mikado_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = evently_mikado_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h1'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo evently_mikado_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_h1_styles' );
}

if ( ! function_exists( 'evently_mikado_h2_styles' ) ) {
	function evently_mikado_h2_styles() {
		$margin_top    = evently_mikado_options()->getOptionValue( 'h2_margin_top' );
		$margin_bottom = evently_mikado_options()->getOptionValue( 'h2_margin_bottom' );
		
		$item_styles = evently_mikado_get_typography_styles( 'h2' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = evently_mikado_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = evently_mikado_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h2'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo evently_mikado_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_h2_styles' );
}

if ( ! function_exists( 'evently_mikado_h3_styles' ) ) {
	function evently_mikado_h3_styles() {
		$margin_top    = evently_mikado_options()->getOptionValue( 'h3_margin_top' );
		$margin_bottom = evently_mikado_options()->getOptionValue( 'h3_margin_bottom' );
		
		$item_styles = evently_mikado_get_typography_styles( 'h3' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = evently_mikado_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = evently_mikado_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h3'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo evently_mikado_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_h3_styles' );
}

if ( ! function_exists( 'evently_mikado_h4_styles' ) ) {
	function evently_mikado_h4_styles() {
		$margin_top    = evently_mikado_options()->getOptionValue( 'h4_margin_top' );
		$margin_bottom = evently_mikado_options()->getOptionValue( 'h4_margin_bottom' );
		
		$item_styles = evently_mikado_get_typography_styles( 'h4' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = evently_mikado_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = evently_mikado_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h4'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo evently_mikado_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_h4_styles' );
}

if ( ! function_exists( 'evently_mikado_h5_styles' ) ) {
	function evently_mikado_h5_styles() {
		$margin_top    = evently_mikado_options()->getOptionValue( 'h5_margin_top' );
		$margin_bottom = evently_mikado_options()->getOptionValue( 'h5_margin_bottom' );
		
		$item_styles = evently_mikado_get_typography_styles( 'h5' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = evently_mikado_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = evently_mikado_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h5'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo evently_mikado_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_h5_styles' );
}

if ( ! function_exists( 'evently_mikado_h6_styles' ) ) {
	function evently_mikado_h6_styles() {
		$margin_top    = evently_mikado_options()->getOptionValue( 'h6_margin_top' );
		$margin_bottom = evently_mikado_options()->getOptionValue( 'h6_margin_bottom' );
		
		$item_styles = evently_mikado_get_typography_styles( 'h6' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = evently_mikado_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = evently_mikado_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h6'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo evently_mikado_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_h6_styles' );
}

if ( ! function_exists( 'evently_mikado_text_styles' ) ) {
	function evently_mikado_text_styles() {
		$item_styles = evently_mikado_get_typography_styles( 'text' );
		
		$item_selector = array(
			'p'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo evently_mikado_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_text_styles' );
}

if ( ! function_exists( 'evently_mikado_link_styles' ) ) {
	function evently_mikado_link_styles() {
		$link_styles      = array();
		$link_color       = evently_mikado_options()->getOptionValue( 'link_color' );
		$link_font_style  = evently_mikado_options()->getOptionValue( 'link_fontstyle' );
		$link_font_weight = evently_mikado_options()->getOptionValue( 'link_fontweight' );
		$link_decoration  = evently_mikado_options()->getOptionValue( 'link_fontdecoration' );
		
		if ( ! empty( $link_color ) ) {
			$link_styles['color'] = $link_color;
		}
		if ( ! empty( $link_font_style ) ) {
			$link_styles['font-style'] = $link_font_style;
		}
		if ( ! empty( $link_font_weight ) ) {
			$link_styles['font-weight'] = $link_font_weight;
		}
		if ( ! empty( $link_decoration ) ) {
			$link_styles['text-decoration'] = $link_decoration;
		}
		
		$link_selector = array(
			'a',
			'p a'
		);
		
		if ( ! empty( $link_styles ) ) {
			echo evently_mikado_dynamic_css( $link_selector, $link_styles );
		}
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_link_styles' );
}

if ( ! function_exists( 'evently_mikado_link_hover_styles' ) ) {
	function evently_mikado_link_hover_styles() {
		$link_hover_styles     = array();
		$link_hover_color      = evently_mikado_options()->getOptionValue( 'link_hovercolor' );
		$link_hover_decoration = evently_mikado_options()->getOptionValue( 'link_hover_fontdecoration' );
		
		if ( ! empty( $link_hover_color ) ) {
			$link_hover_styles['color'] = $link_hover_color;
		}
		if ( ! empty( $link_hover_decoration ) ) {
			$link_hover_styles['text-decoration'] = $link_hover_decoration;
		}
		
		$link_hover_selector = array(
			'a:hover',
			'p a:hover'
		);
		
		if ( ! empty( $link_hover_styles ) ) {
			echo evently_mikado_dynamic_css( $link_hover_selector, $link_hover_styles );
		}
		
		$link_heading_hover_styles = array();
		
		if ( ! empty( $link_hover_color ) ) {
			$link_heading_hover_styles['color'] = $link_hover_color;
		}
		
		$link_heading_hover_selector = array(
			'h1 a:hover',
			'h2 a:hover',
			'h3 a:hover',
			'h4 a:hover',
			'h5 a:hover',
			'h6 a:hover'
		);
		
		if ( ! empty( $link_heading_hover_styles ) ) {
			echo evently_mikado_dynamic_css( $link_heading_hover_selector, $link_heading_hover_styles );
		}
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_link_hover_styles' );
}

if ( ! function_exists( 'evently_mikado_smooth_page_transition_styles' ) ) {
	function evently_mikado_smooth_page_transition_styles( $style ) {
		$id            = evently_mikado_get_page_id();
		$loader_style  = array();
		$current_style = '';
		
		$background_color = evently_mikado_get_meta_field_intersect( 'smooth_pt_bgnd_color', $id );
		if ( ! empty( $background_color ) ) {
			$loader_style['background-color'] = $background_color;
		}
		
		$loader_selector = array(
			'.mkdf-smooth-transition-loader'
		);
		
		if ( ! empty( $loader_style ) ) {
			$current_style .= evently_mikado_dynamic_css( $loader_selector, $loader_style );
		}
		
		$spinner_style = array();
		$spinner_color = evently_mikado_get_meta_field_intersect( 'smooth_pt_spinner_color', $id );
		if ( ! empty( $spinner_color ) ) {
			$spinner_style['background-color'] = $spinner_color;
		}
		
		$spinner_selectors = array(
			'.mkdf-st-loader .mkdf-rotate-circles > div',
			'.mkdf-st-loader .pulse',
			'.mkdf-st-loader .double_pulse .double-bounce1',
			'.mkdf-st-loader .double_pulse .double-bounce2',
			'.mkdf-st-loader .cube',
			'.mkdf-st-loader .rotating_cubes .cube1',
			'.mkdf-st-loader .rotating_cubes .cube2',
			'.mkdf-st-loader .stripes > div',
			'.mkdf-st-loader .wave > div',
			'.mkdf-st-loader .two_rotating_circles .dot1',
			'.mkdf-st-loader .two_rotating_circles .dot2',
			'.mkdf-st-loader .five_rotating_circles .container1 > div',
			'.mkdf-st-loader .five_rotating_circles .container2 > div',
			'.mkdf-st-loader .five_rotating_circles .container3 > div',
			'.mkdf-st-loader .atom .ball-1:before',
			'.mkdf-st-loader .atom .ball-2:before',
			'.mkdf-st-loader .atom .ball-3:before',
			'.mkdf-st-loader .atom .ball-4:before',
			'.mkdf-st-loader .clock .ball:before',
			'.mkdf-st-loader .mitosis .ball',
			'.mkdf-st-loader .lines .line1',
			'.mkdf-st-loader .lines .line2',
			'.mkdf-st-loader .lines .line3',
			'.mkdf-st-loader .lines .line4',
			'.mkdf-st-loader .fussion .ball',
			'.mkdf-st-loader .fussion .ball-1',
			'.mkdf-st-loader .fussion .ball-2',
			'.mkdf-st-loader .fussion .ball-3',
			'.mkdf-st-loader .fussion .ball-4',
			'.mkdf-st-loader .wave_circles .ball',
			'.mkdf-st-loader .pulse_circles .ball'
		);
		
		if ( ! empty( $spinner_style ) ) {
			$current_style .= evently_mikado_dynamic_css( $spinner_selectors, $spinner_style );
		}
		
		$current_style = $current_style . $style;
		
		return $current_style;
	}
	
	add_filter( 'evently_mikado_filter_add_page_custom_style', 'evently_mikado_smooth_page_transition_styles' );
}