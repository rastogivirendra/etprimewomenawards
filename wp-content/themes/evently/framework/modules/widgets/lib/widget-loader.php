<?php

if (!function_exists('evently_mikado_register_widgets')) {
	function evently_mikado_register_widgets() {
		$widgets = apply_filters('evently_mikado_filter_register_widgets', $widgets = array());

		foreach ($widgets as $widget) {
			register_widget($widget);
		}
	}
	
	add_action('widgets_init', 'evently_mikado_register_widgets');
}