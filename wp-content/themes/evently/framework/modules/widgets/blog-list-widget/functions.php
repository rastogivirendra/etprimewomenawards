<?php

if(!function_exists('evently_mikado_register_blog_list_widget')) {
	/**
	 * Function that register blog list widget
	 */
	function evently_mikado_register_blog_list_widget($widgets) {
		$widgets[] = 'EventlyMikadoClassBlogListWidget';
		
		return $widgets;
	}
	
	add_filter('evently_mikado_filter_register_widgets', 'evently_mikado_register_blog_list_widget');
}