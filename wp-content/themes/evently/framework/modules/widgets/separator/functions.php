<?php

if(!function_exists('evently_mikado_register_separator_widget')) {
	/**
	 * Function that register separator widget
	 */
	function evently_mikado_register_separator_widget($widgets) {
		$widgets[] = 'EventlyMikadoClassSeparatorWidget';
		
		return $widgets;
	}
	
	add_filter('evently_mikado_filter_register_widgets', 'evently_mikado_register_separator_widget');
}