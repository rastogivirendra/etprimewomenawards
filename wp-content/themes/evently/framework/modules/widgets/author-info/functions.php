<?php

if(!function_exists('evently_mikado_register_author_info_widget')) {
	/**
	 * Function that register button widget
	 */
	function evently_mikado_register_author_info_widget($widgets) {
		$widgets[] = 'EventlyMikadoClassAuthorInfoWidget';
		
		return $widgets;
	}
	
	add_filter('evently_mikado_filter_register_widgets', 'evently_mikado_register_author_info_widget');
}