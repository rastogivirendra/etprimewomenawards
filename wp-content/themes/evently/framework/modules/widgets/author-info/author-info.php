<?php
class EventlyMikadoClassAuthorInfoWidget extends EventlyMikadoClassWidget {
    /**
     * Set basic widget options and call parent class construct
     */
    public function __construct() {
        parent::__construct(
            'mkdf_author_info_widget',
            esc_html__('Mikado Author Info Widget', 'evently'),
            array( 'description' => esc_html__( 'Add author info element to widget areas', 'evently'))
        );

        $this->setParams();
    }

    /**
     * Sets widget options
     */
    protected function setParams() {
        $this->params = array(
            array(
                'type' => 'textfield',
                'name' => 'extra_class',
                'title' => esc_html__('Custom CSS Class', 'evently')
            ),
	        array(
		        'type'  => 'textfield',
		        'name'  => 'widget_title',
		        'title' => esc_html__( 'Widget Title', 'evently' )
	        ),
            array(
                'type' => 'textfield',
                'name' => 'author_username',
                'title' => esc_html__('Author Username', 'evently')
            )
        );
    }

    /**
     * Generates widget's HTML
     *
     * @param array $args args from widget area
     * @param array $instance widget's options
     */
    public function widget($args, $instance) {
        extract($args);

        $extra_class = '';
        if (!empty($instance['extra_class'])) {
            $extra_class = $instance['extra_class'];
        }

        $authorID = 1;
	    if(!empty($instance['author_username'])) {
		    $author = get_user_by( 'login', $instance['author_username']);

		    if ($author) $authorID = $author->ID;
	    }

	    $author_info      = get_the_author_meta('description', $authorID);
        ?>
        <div class="widget mkdf-author-info-widget <?php echo esc_html($extra_class); ?>">
	        <?php
		        if ( ! empty( $instance['widget_title'] ) ) {
			        echo wp_kses_post( $args['before_title'] ) . esc_html( $instance['widget_title'] ) . wp_kses_post( $args['after_title'] );
		        }
	        ?>
            <div class="mkdf-aiw-inner">
	            <div class="mkdf-aiw-image-wrap">
		            <a itemprop="url" class="mkdf-aiw-image" href="<?php echo esc_url(get_author_posts_url($authorID)); ?>" target="_self">
			            <?php echo evently_mikado_kses_img(get_avatar($authorID, 300)); ?>
		            </a>
		            <div class="mkdf-aiw-name-wrap">
			            <div class="mkdf-aiw-name vcard author">
				            <a itemprop="url" href="<?php echo esc_attr(get_author_posts_url($authorID)); ?>" target="_self">
						<span class="fn">
							<?php
							if(esc_attr(get_the_author_meta('first_name', $authorID)) != "" || esc_attr(get_the_author_meta('last_name', $authorID) != "")) {
								echo esc_attr(get_the_author_meta('first_name', $authorID)) . " " . esc_attr(get_the_author_meta('last_name', $authorID));
							} else {
								echo esc_attr(get_the_author_meta('display_name', $authorID));
							}
							?>
						</span>
				            </a>
			            </div>
		            </div>
	            </div>
		        <?php if($author_info !== "") { ?>
			        <p itemprop="description" class="mkdf-aiw-text"><?php echo esc_attr($author_info); ?></p>
		        <?php } ?>
            </div>
        </div>
    <?php 
    }
}