<?php

if(!function_exists('evently_mikado_register_icon_widget')) {
	/**
	 * Function that register icon widget
	 */
	function evently_mikado_register_icon_widget($widgets) {
		$widgets[] = 'EventlyMikadoClassIconWidget';
		
		return $widgets;
	}
	
	add_filter('evently_mikado_filter_register_widgets', 'evently_mikado_register_icon_widget');
}