<?php

class EventlyMikadoClassIconWidget extends EventlyMikadoClassWidget {
	public function __construct() {
		parent::__construct(
			'mkdf_icon_widget',
			esc_html__( 'Mikado Icon Widget', 'evently' ),
			array( 'description' => esc_html__( 'Add icons to widget areas', 'evently' ) )
		);

		$this->setParams();
	}

	/**
	 * Sets widget options
	 */
	protected function setParams() {
		$this->params = array_merge(
			evently_mikado_icon_collections()->getIconWidgetParamsArray(),
			array(
				array(
					'type'  => 'textfield',
					'name'  => 'icon_text',
					'title' => esc_html__( 'Icon Text', 'evently' )
				),
				array(
					'type'  => 'textfield',
					'name'  => 'text_color',
					'title' => esc_html__( 'Text Color', 'evently' )
				),
				array(
					'type'        => 'textfield',
					'name'        => 'text_break_words',
					'title'       => esc_html__( 'Position of Line Break', 'evently' ),
					'description' => esc_html__( 'Enter the position of the word after which you would like to create a line break of the Icon Text (e.g. if you would like the line break after the 3rd word, you would enter "3")', 'evently' )
				),
				array(
					'type'  => 'textfield',
					'name'  => 'text_size',
					'title' => esc_html__( 'Text Size (px)', 'evently' )
				),
				array(
					'type'  => 'textfield',
					'name'  => 'line_height',
					'title' => esc_html__( 'Icon Text Line Height (px)', 'evently' )
				),
				array(
					'type'  => 'textfield',
					'name'  => 'link',
					'title' => esc_html__( 'Link', 'evently' )
				),
				array(
					'type'    => 'dropdown',
					'name'    => 'target',
					'title'   => esc_html__( 'Target', 'evently' ),
					'options' => array(
						'_self'  => esc_html__( 'Same Window', 'evently' ),
						'_blank' => esc_html__( 'New Window', 'evently' )
					)
				),
				array(
					'type'  => 'textfield',
					'name'  => 'icon_size',
					'title' => esc_html__( 'Icon Size (px)', 'evently' )
				),
				array(
					'type'  => 'textfield',
					'name'  => 'icon_color',
					'title' => esc_html__( 'Icon Color', 'evently' )
				),
				array(
					'type'  => 'textfield',
					'name'  => 'icon_hover_color',
					'title' => esc_html__( 'Icon Hover Color', 'evently' )
				),
				array(
					'type'        => 'textfield',
					'name'        => 'icon_margin',
					'title'       => esc_html__( 'Icon Margin', 'evently' ),
					'description' => esc_html__( 'Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'evently' )
				)
			)
		);
	}

	/**
	 * Generates widget's HTML
	 *
	 * @param array $args args from widget area
	 * @param array $instance widget's options
	 */
	public function widget( $args, $instance ) {
		$holder_classes = array( 'mkdf-icon-widget-holder' );
		if ( ! empty( $instance['icon_hover_color'] ) ) {
			$holder_classes[] = 'mkdf-icon-has-hover';
		}

		$data   = array();
		$data[] = ! empty( $instance['icon_hover_color'] ) ? evently_mikado_get_inline_attr( $instance['icon_hover_color'], 'data-hover-color' ) : '';
		$data   = implode( ' ', $data );

		$holder_styles = array();
		if ( ! empty( $instance['icon_color'] ) ) {
			$holder_styles[] = 'color: ' . $instance['icon_color'];
		}

		if ( ! empty( $instance['icon_size'] ) ) {
			$holder_styles[] = 'font-size: ' . evently_mikado_filter_px( $instance['icon_size'] ) . 'px';
		}

		if ( ! empty( $instance['line_height'] ) ) {
			$holder_styles[] = 'line-height: ' . evently_mikado_filter_px( $instance['line_height'] ) . 'px';
		}

		if ( $instance['icon_margin'] !== '' ) {
			$holder_styles[] = 'margin: ' . $instance['icon_margin'];
		}

		$link   = ! empty( $instance['link'] ) ? $instance['link'] : '#';
		$target = ! empty( $instance['target'] ) ? $instance['target'] : '_self';

		$icon_holder_html = '';
		if ( ! empty( $instance['icon_pack'] ) ) {
			$icon_class   = array( 'mkdf-icon-widget' );
			$icon_class[] = ! empty( $instance['fa_icon'] ) && $instance['icon_pack'] === 'font_awesome' ? 'fa ' . $instance['fa_icon'] : '';
			$icon_class[] = ! empty( $instance['fe_icon'] ) && $instance['icon_pack'] === 'font_elegant' ? $instance['fe_icon'] : '';
			$icon_class[] = ! empty( $instance['ion_icon'] ) && $instance['icon_pack'] === 'ion_icons' ? $instance['ion_icon'] : '';
			$icon_class[] = ! empty( $instance['linea_icon'] ) && $instance['icon_pack'] === 'linea_icons' ? $instance['linea_icon'] : '';
			$icon_class[] = ! empty( $instance['linear_icon'] ) && $instance['icon_pack'] === 'linear_icons' ? 'lnr ' . $instance['linear_icon'] : '';
			$icon_class[] = ! empty( $instance['simple_line_icon'] ) && $instance['icon_pack'] === 'simple_line_icons' ? $instance['simple_line_icon'] : '';

			$icon_class = implode( ' ', $icon_class );

			$icon_holder_html = '<span class="' . esc_attr($icon_class) . '"></span>';
		}

		$text_styles = array();
		if ( ! empty( $instance['text_size'] ) ) {
			$text_styles[] = 'font-size: ' . evently_mikado_filter_px( $instance['text_size'] ) . 'px;';
		}

		if ( ! empty( $instance['text_color'] ) ) {
			$text_styles[] = 'color: ' . $instance['text_color'] .';';
		}

		$icon_text_html = '';
		if ( ! empty( $instance['icon_text'] ) ) {

			if( ! empty( $text_styles ) ) {
				$text_styles = join( ' ', $text_styles );
			}

			$icon_text      = $this->getModifiedText( $instance );
			$icon_text_html = '<span class="mkdf-icon-text" style="' . esc_attr($text_styles) . '">' . $icon_text . '</span>';
		}
		?>

		<a <?php evently_mikado_class_attribute( $holder_classes ); ?> <?php echo wp_kses_post( $data ); ?>
				href="<?php echo esc_url( $link ); ?>"
				target="<?php echo esc_attr( $target ); ?>" <?php echo evently_mikado_get_inline_style( $holder_styles ); ?>>
			<?php echo wp_kses( $icon_holder_html, array(
				'span' => array(
					'class' => true
				)
			) ); ?>
			<?php echo wp_kses( $icon_text_html, array(
				'span' => array(
					'class' => true,
					'style' => true
				),
				'br'   => true
			) ); ?>
		</a>
		<?php
	}

	private function getModifiedText( $instance ) {
		$text             = $instance['icon_text'];
		$text_break_words = str_replace( ' ', '', $instance['text_break_words'] );

		if ( ! empty( $text ) ) {
			$split_text = explode( ' ', $text );

			if ( ! empty( $text_break_words ) ) {
				if ( ! empty( $split_text[ $text_break_words - 1 ] ) ) {
					$split_text[ $text_break_words - 1 ] = $split_text[ $text_break_words - 1 ] . '<br />';
				}
			}

			$text = implode( ' ', $split_text );
		}

		return $text;
	}
}
