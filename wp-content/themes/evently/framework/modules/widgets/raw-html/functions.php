<?php

if(!function_exists('evently_mikado_register_raw_html_widget')) {
	/**
	 * Function that register raw html widget
	 */
	function evently_mikado_register_raw_html_widget($widgets) {
		$widgets[] = 'EventlyMikadoClassRawHTMLWidget';
		
		return $widgets;
	}
	
	add_filter('evently_mikado_filter_register_widgets', 'evently_mikado_register_raw_html_widget');
}