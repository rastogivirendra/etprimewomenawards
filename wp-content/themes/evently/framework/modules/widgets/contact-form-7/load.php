<?php

if(evently_mikado_contact_form_7_installed()) {
	include_once MIKADO_FRAMEWORK_MODULES_ROOT_DIR . '/widgets/contact-form-7/contact-form-7.php';
	add_filter('evently_mikado_filter_register_widgets', 'evently_mikado_register_cf7_widget');
}

if(!function_exists('evently_mikado_register_cf7_widget')) {
	/**
	 * Function that register cf7 widget
	 */
	function evently_mikado_register_cf7_widget($widgets) {
		$widgets[] = 'EventlyMikadoClassContactForm7Widget';
		
		return $widgets;
	}
}