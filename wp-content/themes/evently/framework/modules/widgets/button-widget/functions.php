<?php

if(!function_exists('evently_mikado_register_button_widget')) {
	/**
	 * Function that register button widget
	 */
	function evently_mikado_register_button_widget($widgets) {
		$widgets[] = 'EventlyMikadoClassButtonWidget';
		
		return $widgets;
	}
	
	add_filter('evently_mikado_filter_register_widgets', 'evently_mikado_register_button_widget');
}