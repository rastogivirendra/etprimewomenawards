<?php

class EventlyMikadoClassButtonWidget extends EventlyMikadoClassWidget {
	public function __construct() {
		parent::__construct(
			'mkdf_button_widget',
			esc_html__('Mikado Button Widget', 'evently'),
			array( 'description' => esc_html__( 'Add button element to widget areas', 'evently'))
		);

		$this->setParams();
	}

	/**
	 * Sets widget options
	 */
	protected function setParams() {
		$this->params = array(
			array(
				'type'    => 'dropdown',
				'name'    => 'type',
				'title'   => esc_html__( 'Type', 'evently' ),
				'options' => array(
					'solid'   => esc_html__( 'Solid', 'evently' ),
					'outline' => esc_html__( 'Outline', 'evently' ),
					'simple'  => esc_html__( 'Simple', 'evently' )
				)
			),
			array(
				'type'        => 'dropdown',
				'name'        => 'size',
				'title'       => esc_html__( 'Size', 'evently' ),
				'options'     => array(
					'small'  => esc_html__( 'Small', 'evently' ),
					'medium' => esc_html__( 'Medium', 'evently' ),
					'large'  => esc_html__( 'Large', 'evently' ),
					'huge'   => esc_html__( 'Huge', 'evently' )
				),
				'description' => esc_html__( 'This option is only available for solid and outline button type', 'evently' )
			),
			array(
				'type'    => 'textfield',
				'name'    => 'text',
				'title'   => esc_html__( 'Text', 'evently' ),
				'default' => esc_html__( 'Button Text', 'evently' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'link',
				'title' => esc_html__( 'Link', 'evently' )
			),
			array(
				'type'    => 'dropdown',
				'name'    => 'target',
				'title'   => esc_html__( 'Link Target', 'evently' ),
				'options' => evently_mikado_get_link_target_array()
			),
			array(
				'type'  => 'textfield',
				'name'  => 'color',
				'title' => esc_html__( 'Color', 'evently' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'hover_color',
				'title' => esc_html__( 'Hover Color', 'evently' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'background_color',
				'title'       => esc_html__( 'Background Color', 'evently' ),
				'description' => esc_html__( 'This option is only available for solid button type', 'evently' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'hover_background_color',
				'title'       => esc_html__( 'Hover Background Color', 'evently' ),
				'description' => esc_html__( 'This option is only available for solid button type', 'evently' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'border_color',
				'title'       => esc_html__( 'Border Color', 'evently' ),
				'description' => esc_html__( 'This option is only available for solid and outline button type', 'evently' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'hover_border_color',
				'title'       => esc_html__( 'Hover Border Color', 'evently' ),
				'description' => esc_html__( 'This option is only available for solid and outline button type', 'evently' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'margin',
				'title'       => esc_html__( 'Margin', 'evently' ),
				'description' => esc_html__( 'Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'evently' )
			)
		);
	}

	/**
	 * Generates widget's HTML
	 *
	 * @param array $args args from widget area
	 * @param array $instance widget's options
	 */
	public function widget($args, $instance) {
		$params = '';

		if (!is_array($instance)) { $instance = array(); }

		// Filter out all empty params
		$instance = array_filter($instance, function($array_value) { return trim($array_value) != ''; });

		// Default values
		if (!isset($instance['text'])) { $instance['text'] = 'Button Text'; }

		// Generate shortcode params
		foreach($instance as $key => $value) {
			$params .= " $key='$value' ";
		}

		echo '<div class="widget mkdf-button-widget">';
			echo do_shortcode("[mkdf_button $params]"); // XSS OK
		echo '</div>';
	}
}