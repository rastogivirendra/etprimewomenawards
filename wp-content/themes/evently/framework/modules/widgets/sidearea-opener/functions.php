<?php

if(!function_exists('evently_mikado_register_sidearea_opener_widget')) {
	/**
	 * Function that register sidearea opener widget
	 */
	function evently_mikado_register_sidearea_opener_widget($widgets) {
		$widgets[] = 'EventlyMikadoClassSideAreaOpener';
		
		return $widgets;
	}
	
	add_filter('evently_mikado_filter_register_widgets', 'evently_mikado_register_sidearea_opener_widget');
}