<?php

if(!function_exists('evently_mikado_register_image_widget')) {
	/**
	 * Function that register image widget
	 */
	function evently_mikado_register_image_widget($widgets) {
		$widgets[] = 'EventlyMikadoClassImageWidget';
		
		return $widgets;
	}
	
	add_filter('evently_mikado_filter_register_widgets', 'evently_mikado_register_image_widget');
}