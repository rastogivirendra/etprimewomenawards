<?php

class EventlyMikadoClassSearchOpener extends EventlyMikadoClassWidget {
    public function __construct() {
        parent::__construct(
            'mkdf_search_opener',
	        esc_html__('Mikado Search Opener', 'evently'),
	        array( 'description' => esc_html__( 'Display a "search" icon that opens the search form', 'evently'))
        );

        $this->setParams();
    }

    /**
     * Sets widget options
     */
	protected function setParams() {
		$this->params = array(
			array(
				'type'        => 'textfield',
				'name'        => 'search_icon_size',
				'title'       => esc_html__( 'Icon Size (px)', 'evently' ),
				'description' => esc_html__( 'Define size for search icon', 'evently' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'search_icon_color',
				'title'       => esc_html__( 'Icon Color', 'evently' ),
				'description' => esc_html__( 'Define color for search icon', 'evently' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'search_icon_hover_color',
				'title'       => esc_html__( 'Icon Hover Color', 'evently' ),
				'description' => esc_html__( 'Define hover color for search icon', 'evently' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'search_icon_margin',
				'title'       => esc_html__( 'Icon Margin', 'evently' ),
				'description' => esc_html__( 'Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'evently' )
			),
			array(
				'type'        => 'dropdown',
				'name'        => 'show_label',
				'title'       => esc_html__( 'Enable Search Icon Text', 'evently' ),
				'description' => esc_html__( 'Enable this option to show search text next to search icon in header', 'evently' ),
				'options'     => evently_mikado_get_yes_no_select_array()
			)
		);
	}

    /**
     * Generates widget's HTML
     *
     * @param array $args args from widget area
     * @param array $instance widget's options
     */
    public function widget($args, $instance) {
        global $evently_mikado_global_options, $evently_mikado_global_IconCollections;

	    $search_type_class    = 'mkdf-search-opener mkdf-icon-has-hover';
	    $styles = array();
	    $show_search_text     = $instance['show_label'] == 'yes' || $evently_mikado_global_options['enable_search_icon_text'] == 'yes' ? true : false;

	    if(!empty($instance['search_icon_size'])) {
		    $styles[] = 'font-size: '.intval($instance['search_icon_size']).'px';
	    }

	    if(!empty($instance['search_icon_color'])) {
		    $styles[] = 'color: '.$instance['search_icon_color'].';';
	    }

	    if (!empty($instance['search_icon_margin'])) {
		    $styles[] = 'margin: ' . $instance['search_icon_margin'].';';
	    }
	    ?>

	    <a <?php evently_mikado_inline_attr($instance['search_icon_hover_color'], 'data-hover-color'); ?> <?php evently_mikado_inline_style($styles); ?>
		    <?php evently_mikado_class_attribute($search_type_class); ?> href="javascript:void(0)">
            <span class="mkdf-search-opener-wrapper">
                <?php if(isset($evently_mikado_global_options['search_icon_pack'])) {
	                $evently_mikado_global_IconCollections->getSearchIcon($evently_mikado_global_options['search_icon_pack'], false);
                } ?>
	            <?php if($show_search_text) { ?>
		            <span class="mkdf-search-icon-text"><?php esc_html_e('Search', 'evently'); ?></span>
	            <?php } ?>
            </span>
	    </a>
    <?php }
}