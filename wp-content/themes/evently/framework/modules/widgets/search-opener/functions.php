<?php

if(!function_exists('evently_mikado_register_search_opener_widget')) {
	/**
	 * Function that register search opener widget
	 */
	function evently_mikado_register_search_opener_widget($widgets) {
		$widgets[] = 'EventlyMikadoClassSearchOpener';
		
		return $widgets;
	}
	
	add_filter('evently_mikado_filter_register_widgets', 'evently_mikado_register_search_opener_widget');
}