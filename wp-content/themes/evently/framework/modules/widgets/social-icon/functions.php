<?php

if(!function_exists('evently_mikado_register_social_icon_widget')) {
	/**
	 * Function that register social icon widget
	 */
	function evently_mikado_register_social_icon_widget($widgets) {
		$widgets[] = 'EventlyMikadoClassSocialIconWidget';
		
		return $widgets;
	}
	
	add_filter('evently_mikado_filter_register_widgets', 'evently_mikado_register_social_icon_widget');
}