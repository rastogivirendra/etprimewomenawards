<?php

class EventlyMikadoClassSocialIconWidget extends EventlyMikadoClassWidget {
    public function __construct() {
        parent::__construct(
            'mkdf_social_icon_widget',
            esc_html__('Mikado Social Icon Widget', 'evently'),
            array( 'description' => esc_html__( 'Add social network icons to widget areas', 'evently'))
        );

        $this->setParams();
    }

    /**
     * Sets widget options
     */
	protected function setParams() {
		$this->params = array_merge(
			evently_mikado_icon_collections()->getSocialIconWidgetParamsArray(),
			array(
				array(
					'type'  => 'textfield',
					'name'  => 'link',
					'title' => esc_html__( 'Link', 'evently' )
				),
				array(
					'type'    => 'dropdown',
					'name'    => 'target',
					'title'   => esc_html__( 'Target', 'evently' ),
					'options' => evently_mikado_get_link_target_array()
				),
				array(
					'type'  => 'textfield',
					'name'  => 'icon_size',
					'title' => esc_html__( 'Icon Size (px)', 'evently' )
				),
				array(
					'type'  => 'textfield',
					'name'  => 'color',
					'title' => esc_html__( 'Color', 'evently' )
				),
				array(
					'type'  => 'textfield',
					'name'  => 'hover_color',
					'title' => esc_html__( 'Hover Color', 'evently' )
				),
				array(
					'type'        => 'textfield',
					'name'        => 'margin',
					'title'       => esc_html__( 'Margin', 'evently' ),
					'description' => esc_html__( 'Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'evently' )
				),
				array(
					'type'        => 'dropdown',
					'name'        => 'style',
					'title'       => esc_html__( 'Use Our Predefined style', 'evently' ),
					'options' => array(
						'yes'    => esc_html__( 'Yes', 'evently' ),
						'no'     => esc_html__( 'No', 'evently' )
					),
					'description' => esc_html__( 'If you select yes icons will have pre-defined size within a circle.', 'evently' )
				)
			)
		);
	}

    /**
     * Generates widget's HTML
     *
     * @param array $args args from widget area
     * @param array $instance widget's options
     */
    public function widget($args, $instance) {
	    $icon_styles = array();
	    $circle_styles = array();

	    if ( ! empty( $instance['circle_size'] ) ) {
		    $circle_styles[] = 'font-size: ' . evently_mikado_filter_px( $instance['circle_size'] ) . 'px';
	    }

	    if ( ! empty( $instance['circle_color'] ) ) {
		    $icon_styles[] = 'color: ' . $instance['circle_color'] . ';';
	    }
	
	    if ( ! empty( $instance['color'] ) ) {
		    $icon_styles[] = 'color: ' . $instance['color'] . ';';
	    }
	
	    if ( ! empty( $instance['icon_size'] ) ) {
		    $icon_styles[] = 'font-size: ' . evently_mikado_filter_px( $instance['icon_size'] ) . 'px';
	    }
	
	    if ( ! empty( $instance['margin'] ) ) {
		    $icon_styles[] = 'margin: ' . $instance['margin'] . ';';
	    }

	    $holder_class = '';
	    if ( isset( $instance['style'] ) && $instance['style'] === 'yes' ) {
		    $holder_class = 'mkdf-icon-predefined-style';
	    }
	
	    $link        = ! empty( $instance['link'] ) ? $instance['link'] : '#';
	    $target      = ! empty( $instance['target'] ) ? $instance['target'] : '_self';
	    $hover_color = ! empty( $instance['hover_color'] ) ? $instance['hover_color'] : '';

	    $icon_holder_html = '';
	    if ( ! empty( $instance['icon_pack'] ) ) {
		    $icon_class   = array( 'mkdf-social-icon-widget' );
		    $icon_class[] = ! empty( $instance['fa_icon'] ) && $instance['icon_pack'] === 'font_awesome' ? 'fa ' . $instance['fa_icon'] : '';
		    $icon_class[] = ! empty( $instance['fe_icon'] ) && $instance['icon_pack'] === 'font_elegant' ? $instance['fe_icon'] : '';
		    $icon_class[] = ! empty( $instance['ion_icon'] ) && $instance['icon_pack'] === 'ion_icons' ? $instance['ion_icon'] : '';
		    $icon_class[] = ! empty( $instance['linea_icon'] ) && $instance['icon_pack'] === 'linea_icons' ? $instance['linea_icon'] : '';
		    $icon_class[] = ! empty( $instance['linear_icon'] ) && $instance['icon_pack'] === 'linear_icons' ? 'lnr ' . $instance['linear_icon'] : '';
		    $icon_class[] = ! empty( $instance['simple_line_icon'] ) && $instance['icon_pack'] === 'simple_line_icons' ? $instance['simple_line_icon'] : '';

		    $icon_class = implode( ' ', $icon_class );

		    $icon_holder_html = '<span class="' . $icon_class . '"></span>';
	    }
        ?>

        <a class="mkdf-social-icon-widget-holder mkdf-icon-has-hover <?php echo esc_attr( $holder_class ); ?>" <?php echo evently_mikado_get_inline_attr($hover_color, 'data-hover-color'); ?> <?php evently_mikado_inline_style($icon_styles) ?> href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>">
            <?php echo wp_kses_post($icon_holder_html); ?>
        </a>
    <?php
    }
}