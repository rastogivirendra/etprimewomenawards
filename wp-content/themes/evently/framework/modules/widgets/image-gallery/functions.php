<?php

if(!function_exists('evently_mikado_register_image_gallery_widget')) {
	/**
	 * Function that register image gallery widget
	 */
	function evently_mikado_register_image_gallery_widget($widgets) {
		$widgets[] = 'EventlyMikadoClassImageGalleryWidget';
		
		return $widgets;
	}
	
	add_filter('evently_mikado_filter_register_widgets', 'evently_mikado_register_image_gallery_widget');
}