<div <?php evently_mikado_class_attribute($holder_classes); ?>>
	<div class="mkdf-tl-heading">
		<?php if( ! empty($subtitle) ): ?>
			<span class="mkdf-tl-subtitle"><?php echo esc_html($subtitle); ?></span>
		<?php endif; ?>
		<?php if( ! empty($title) ): ?>
			<h4 class="mkdf-tl-title"><?php echo esc_html($title); ?></h4>
		<?php endif; ?>
	</div>
	<?php echo do_shortcode($content); ?>
</div>