<?php
namespace MikadoCore\CPT\Shortcodes\TimetableListItem;

use MikadoCore\Lib;

class TimetableListItem implements Lib\ShortcodeInterface {
	private $base;
	
	public function __construct() {
		$this->base = 'mkdf_timetable_list_item';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 */
	public function vcMap() {
		if(function_exists('vc_map')) {
			vc_map(
				array(
					'name'                      => esc_html__( 'Mikado Timetable List Item', 'evently' ),
					'base'                      => $this->getBase(),
					'category'                  => esc_html__( 'by MIKADO', 'evently' ),
					'icon'                      => 'icon-wpb-a-timetable-list-item extended-custom-icon',
					'allowed_container_element' => 'vc_row',
					'as_child'                  => array( 'only' => 'mkdf_timetable_list' ),
					'params'                    => array(
						array(
							'type'        => 'textfield',
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Title', 'evently' )
						),
						array(
							'type'        => 'textarea_html',
							'param_name'  => 'content',
							'heading'     => esc_html__( 'Text', 'evently' )
						)
					)
				)
			);
		}
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 * @return string
	 */
	public function render( $atts, $content = null ) {
		$args   = array(
			'title'  => ''
		);
		$params = shortcode_atts( $args, $atts );

		$params['content'] = $content;

		$html = evently_mikado_get_timetable_shortcode_module_template_part( 'templates/item', 'timetable-items-list', '', $params );

		return $html;
	}
}