<?php

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
	class WPBakeryShortCode_Mkdf_Timetable_List extends WPBakeryShortCodesContainer {}
}

if(!function_exists('evently_mikado_add_timetable_items_list_shortcode')) {
	function evently_mikado_add_timetable_items_list_shortcode($shortcodes_class_name) {
		$shortcodes = array(
			'MikadoCore\CPT\Shortcodes\TimetableList\TimetableList',
			'MikadoCore\CPT\Shortcodes\TimetableListItem\TimetableListItem',
		);

		$shortcodes_class_name = array_merge($shortcodes_class_name, $shortcodes);

		return $shortcodes_class_name;
	}

	if(evently_mikado_core_plugin_installed()) {
		add_filter('mkdf_core_filter_add_vc_shortcode', 'evently_mikado_add_timetable_items_list_shortcode');
	}
}