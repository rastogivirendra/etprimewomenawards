<?php
namespace MikadoCore\CPT\Shortcodes\TimetableList;

use MikadoCore\Lib;

class TimetableList implements Lib\ShortcodeInterface {
	private $base;
	
	public function __construct() {
		$this->base = 'mkdf_timetable_list';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 */
	public function vcMap() {
		if(function_exists('vc_map')) {
			vc_map(
				array(
					'name'                      => esc_html__( 'Mikado Timetable List', 'evently' ),
					'base'                      => $this->getBase(),
					'category'                  => esc_html__( 'by MIKADO', 'evently' ),
					'icon'                      => 'icon-wpb-a-timetable-list extended-custom-icon',
					'content_element'           => true,
					'as_parent'                 => array( 'only' => 'mkdf_timetable_list_item' ),
					'js_view'                   => 'VcColumnView',
					'show_settings_on_create'   => true,
					'params'                    => array(
						array(
							'type'        => 'textfield',
							'param_name'  => 'subtitle',
							'heading'     => esc_html__( 'Subtitle', 'evently' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Title', 'evently' )
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__('Enable Shadow Around Holder', 'evently'),
							'param_name'  => 'shadow',
							'value'       => array_flip(evently_mikado_get_yes_no_select_array(false, true)),
							'save_always' => true
						),
					)
				)
			);
		}
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 * @return string
	 */
	public function render( $atts, $content = null ) {
		$args   = array(
			'subtitle'  => '',
			'title'     => '',
			'shadow'    => 'yes'
		);
		$params = shortcode_atts( $args, $atts );

		$params['holder_classes'] = $this->getHolderClass( $params );
		$params['content']        = $content;

		$html = evently_mikado_get_timetable_shortcode_module_template_part( 'templates/items-list', 'timetable-items-list', '', $params );

		return $html;
	}

	private function getHolderClass( $params ) {
		$classes      = array('mkdf-timetable-list');

		if( $params['shadow'] === 'yes' ) {
			$classes[] = 'mkdf-tl-shadow';
		}

		return $classes;
	}
}