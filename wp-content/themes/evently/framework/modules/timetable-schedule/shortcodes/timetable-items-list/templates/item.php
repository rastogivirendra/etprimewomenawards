<div class="mkdf-timetable-list-item">
	<?php if( ! empty($title) ): ?>
		<span class="mkdf-tli-title"><?php echo esc_html($title); ?></span>
	<?php endif; ?>
	<?php if( ! empty($content) ): ?>
		<p class="mkdf-tli-content"><?php echo do_shortcode($content); ?></p>
	<?php endif; ?>
</div>