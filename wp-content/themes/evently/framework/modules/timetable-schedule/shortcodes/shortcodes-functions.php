<?php

if(!function_exists('evently_mikado_include_timetable_shortcodes')) {
	function evently_mikado_include_timetable_shortcodes() {
		foreach(glob(MIKADO_FRAMEWORK_MODULES_ROOT_DIR.'/timetable-schedule/shortcodes/*/load.php') as $shortcode_load) {
			include_once $shortcode_load;
		}
	}

	if(evently_mikado_core_plugin_installed()) {
		add_action('mkdf_core_action_include_shortcodes_file', 'evently_mikado_include_timetable_shortcodes');
	}
}

if ( ! function_exists( 'mkdf_core_set_timetable_event_icon_class_name_for_vc_shortcodes' ) ) {
	/**
	 * Function that set timetable events class name for shortcodes to set our icon for Visual Composer shortcodes panel
	 */
	function mkdf_core_set_timetable_event_icon_class_name_for_vc_shortcodes( $shortcodes_icon_class_array ) {
		$shortcodes_icon_class_array[] = '.icon-wpb-a-timetable-events-hours';
		$shortcodes_icon_class_array[] = '.icon-wpb-a-timetable-event-info-holder';
		$shortcodes_icon_class_array[] = '.icon-wpb-a-timetable-list';
		$shortcodes_icon_class_array[] = '.icon-wpb-a-timetable-list-item';

		return $shortcodes_icon_class_array;
	}

	if ( evently_mikado_core_plugin_installed() ) {
		add_filter( 'mkdf_core_filter_add_vc_shortcodes_custom_icon_class', 'mkdf_core_set_timetable_event_icon_class_name_for_vc_shortcodes' );
	}
}