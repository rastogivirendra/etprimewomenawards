<?php

if (!function_exists('evently_mikado_register_timetable_sidebars')) {
	/**
	 * Function that registers theme's sidebars
	 */
	function evently_mikado_register_timetable_sidebars() {
		
		register_sidebar(array(
			'name' => esc_html__('Sidebar Event', 'evently'),
			'id' => 'sidebar-event',
			'description' => esc_html__('Default Sidebar for Timetable pages', 'evently'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<div class="mkdf-widget-title-holder"><h4 class="mkdf-widget-title">',
			'after_title' => '</h4></div>'
		));
	}
	
	add_action('widgets_init', 'evently_mikado_register_timetable_sidebars', 1);
}

if ( ! function_exists( 'evently_mikado_get_tt_event_single_content' ) ) {
	/**
	 * Loads timetable single event page
	 */
	function evently_mikado_get_tt_event_single_content() {
		$subtitle = get_post_meta( get_the_ID(), 'timetable_subtitle', true );
		
		$params = array(
			'subtitle' => $subtitle
		);
		
		evently_mikado_get_module_template_part( 'templates/events-single', 'timetable-schedule', '', $params );
	}
}

if ( ! function_exists( 'evently_mikado_tt_events_single_default_sidebar' ) ) {
	/**
	 * Sets default sidebar for timetable single event event
	 *
	 * @param $sidebar
	 *
	 * @return string
	 */
	function evently_mikado_tt_events_single_default_sidebar( $sidebar ) {
		$page_id = evently_mikado_get_page_id();

		if ( get_post_type( $page_id ) === 'events' ) {
			$custom_sidebar_area = get_post_meta( $page_id, 'mkdf_custom_sidebar_area_meta', true );
			$sidebar             = ! empty( $custom_sidebar_area ) ? $custom_sidebar_area : 'sidebar-event';
		}
		
		return $sidebar;
	}
	
	add_filter( 'evently_mikado_filter_sidebar_name', 'evently_mikado_tt_events_single_default_sidebar' );
}

if(!function_exists('evently_mikado_events_scope_meta_box_functions')) {
	function evently_mikado_events_scope_meta_box_functions($post_types) {
		$post_types[] = 'events';
		$post_types[] = 'tt-events';

		return $post_types;
	}

	add_filter('evently_mikado_filter_meta_box_post_types_save', 'evently_mikado_events_scope_meta_box_functions');
	add_filter('evently_mikado_filter_meta_box_post_types_remove', 'evently_mikado_events_scope_meta_box_functions');
	add_filter('evently_mikado_filter_set_scope_for_meta_boxes', 'evently_mikado_events_scope_meta_box_functions');
}


if ( ! function_exists( 'evently_mikado_get_timetable_shortcode_module_template_part' ) ) {
	/**
	 * Loads module template part.
	 *
	 * @param string $template name of the template to load
	 * @param string $module name of the module folder
	 * @param string $slug
	 * @param array $params array of parameters to pass to template
	 *
	 * @return html
	 * @see evently_mikado_get_template_part()
	 */
	function evently_mikado_get_timetable_shortcode_module_template_part( $template, $module, $slug = '', $params = array() ) {

		//HTML Content from template
		$html          = '';
		$template_path = 'framework/modules/timetable-schedule/shortcodes/' . $module;

		$temp = $template_path . '/' . $template;

		if ( is_array( $params ) && count( $params ) ) {
			extract( $params );
		}

		$templates = array();

		if ( $temp !== '' ) {
			if ( $slug !== '' ) {
				$templates[] = "{$temp}-{$slug}.php";
			}

			$templates[] = $temp . '.php';
		}
		$located = evently_mikado_find_template_path( $templates );
		if ( $located ) {
			ob_start();
			include( $located );
			$html = ob_get_clean();
		}

		return $html;
	}
}