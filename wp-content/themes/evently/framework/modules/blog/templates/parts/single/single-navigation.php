<?php
$blog_single_navigation = evently_mikado_options()->getOptionValue('blog_single_navigation') === 'no' ? false : true;
$blog_navigation_through_same_category = evently_mikado_options()->getOptionValue('blog_navigation_through_same_category') === 'no' ? false : true;
?>
<?php if($blog_single_navigation){ ?>
	<div class="mkdf-blog-single-navigation">
		<div class="mkdf-blog-single-navigation-inner clearfix">
			<?php
			/* Single navigation section - SETTING PARAMS */
			$post_navigation = array(
				'prev' => array(),
				'next' => array()
			);

			if(get_previous_post() !== ""){
				if($blog_navigation_through_same_category){
					if(get_previous_post(true) !== ""){
						$post = get_previous_post(true);
						$post_navigation['prev']['post']  = $post;
						$post_navigation['prev']['label'] = get_the_title($post->ID);
					}
				} else {
					if(get_previous_post() != ""){
						$post = get_previous_post();
						$post_navigation['prev']['post']  = $post;
						$post_navigation['prev']['label'] = get_the_title($post->ID);
					}
				}
			}
			if(get_next_post() != ""){
				if($blog_navigation_through_same_category){
					if(get_next_post(true) !== ""){
						$post = get_next_post(true);
						$post_navigation['next']['post']  = $post;
						$post_navigation['next']['label'] = get_the_title($post->ID);
					}
				} else {
					if(get_next_post() !== ""){
						$post = get_next_post();
						$post_navigation['next']['post']  = $post;
						$post_navigation['next']['label'] = get_the_title($post->ID);
					}
				}
			}

			/* Single navigation section - RENDERING */
			if (isset($post_navigation['prev']['post']) || isset($post_navigation['next']['post'])) {
				foreach (array('prev', 'next') as $nav_type) {
					if (isset($post_navigation[$nav_type]['post'])) { ?>
						<?php if($nav_type === 'prev') { ?>
							<div class="mkdf-blog-single-<?php echo esc_attr($nav_type); ?>-holder clearfix">
								<?php if( has_post_thumbnail($post_navigation[$nav_type]['post']->ID) ) { ?>
									<div class="mkdf-blog-single-thumb-wrapper">
										<a itemprop="url" class="mkdf-blog-single-nav-thumb" href="<?php echo get_permalink($post_navigation[$nav_type]['post']->ID); ?>">
											<?php echo get_the_post_thumbnail($post_navigation[$nav_type]['post']->ID, 'medium'); ?>
										</a>
									</div>
								<?php } ?>
								<div class="mkdf-blog-single-nav-wrapper">
									<a itemprop="url" class="mkdf-blog-single-<?php echo esc_attr($nav_type); ?> mkdf-blog-nav-title" href="<?php echo get_permalink($post_navigation[$nav_type]['post']->ID); ?>">
										<?php echo wp_kses($post_navigation[$nav_type]['label'], array('span' => array('class' => true))); ?>
									</a>
									<span class="mkdf-blog-single-nav-date">
										<?php echo get_the_date(get_option('date_format')); ?>
									</span>
								</div>
							</div>
						<?php } else { ?>
							<div  class="mkdf-blog-single-<?php echo esc_attr($nav_type); ?>-holder clearfix">
								<div class="mkdf-blog-single-nav-wrapper">
									<a itemprop="url" class="mkdf-blog-single-<?php echo esc_attr($nav_type); ?> mkdf-blog-nav-title" href="<?php echo get_permalink($post_navigation[$nav_type]['post']->ID); ?>">
										<?php echo wp_kses($post_navigation[$nav_type]['label'], array('span' => array('class' => true))); ?>
									</a>
									<span class="mkdf-blog-single-nav-date">
										<?php echo get_the_date(get_option('date_format')); ?>
									</span>
								</div>
								<?php if( has_post_thumbnail($post_navigation[$nav_type]['post']->ID) ) { ?>
									<div class="mkdf-blog-single-thumb-wrapper">
										<a itemprop="url" class="mkdf-blog-single-nav-thumb" href="<?php echo get_permalink($post_navigation[$nav_type]['post']->ID); ?>">
											<?php echo get_the_post_thumbnail($post_navigation[$nav_type]['post']->ID, 'medium'); ?>
										</a>
									</div>
								<?php } ?>
							</div>
						<?php } ?>
					<?php }
				}
			}
			?>
		</div>
	</div>
<?php } ?>