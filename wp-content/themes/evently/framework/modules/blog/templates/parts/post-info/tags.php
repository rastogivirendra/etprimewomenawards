<?php
$tags = get_the_tags();
?>
<?php if($tags) { ?>
<div class="mkdf-tags-holder">
	<?php echo evently_mikado_icon_collections()->renderIcon('icon_ribbon_alt', 'font_elegant'); ?>
    <div class="mkdf-tags">
        <?php the_tags('', ', ', ''); ?>
    </div>
</div>
<?php } ?>