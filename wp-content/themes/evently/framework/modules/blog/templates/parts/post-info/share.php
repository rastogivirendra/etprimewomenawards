<?php
$share_type = isset($share_type) ? $share_type : 'dropdown';
?>
<?php if(evently_mikado_options()->getOptionValue('enable_social_share') === 'yes' && evently_mikado_options()->getOptionValue('enable_social_share_on_post') === 'yes') { ?>
    <div class="mkdf-blog-share">
	    <?php if( is_single() ): ?>
		    <span class="mkdf-share-label"><?php esc_html_e('Share', 'evently'); ?></span>
	    <?php endif; ?>
        <?php echo evently_mikado_get_social_share_html(array('type' => $share_type)); ?>
    </div>
<?php } ?>
