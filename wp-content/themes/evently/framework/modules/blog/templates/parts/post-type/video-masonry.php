<?php
$video_type = get_post_meta( get_the_ID(), "mkdf_video_type_meta", true );
$has_video_link = get_post_meta(get_the_ID(), "mkdf_post_video_custom_meta", true) !== '' || get_post_meta(get_the_ID(), "mkdf_post_video_link_meta", true) !== '';
$videolink = get_post_meta( get_the_ID(), "mkdf_post_video_link_meta", true );
$params['hide_category'] = true;
?>
<?php if($has_video_link) { ?>
	<div class="mkdf-post-image-video">
		<?php evently_mikado_get_module_template_part('templates/parts/image', 'blog', '', $params); ?>
		<div class="mkdf-post-video">
			<?php evently_mikado_get_module_template_part('templates/parts/title', 'blog', '', $params); ?>
			<?php evently_mikado_get_module_template_part('templates/parts/excerpt', 'blog', '', $params); ?>
			<a class="mkdf-video-post-link" href="<?php echo esc_url($videolink); ?>"
			   data-rel="prettyPhoto[<?php the_ID(); ?>]">
				<?php echo evently_mikado_icon_collections()->renderIcon('arrow_triangle-right_alt2', 'font_elegant'); ?>
				<span class="mkdf-video-link-text"><?php esc_html_e('Watch Video', 'evently'); ?></span>
			</a>
		</div>
	</div>
<?php } ?>