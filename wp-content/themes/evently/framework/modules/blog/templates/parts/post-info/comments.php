<?php if(comments_open()) { ?>
	<div class="mkdf-post-info-comments-holder">
		<a itemprop="url" class="mkdf-post-info-comments" href="<?php comments_link(); ?>" target="_self">
			<?php echo evently_mikado_icon_collections()->renderIcon('icon_comment_alt', 'font_elegant'); ?>
			<span class="mkdf-post-info-comment-text"><?php comments_number('0 ' . esc_html__('Comments','evently'), '1 '.esc_html__('Comment','evently'), '% '.esc_html__('Comments','evently') ); ?></span>
		</a>
	</div>
<?php } ?>