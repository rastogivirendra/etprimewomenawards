<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="mkdf-post-content">
        <div class="mkdf-post-heading">
            <?php evently_mikado_get_module_template_part('templates/parts/image', 'blog', '', $part_params); ?>
            <?php evently_mikado_get_module_template_part('templates/parts/post-type/audio', 'blog', '', $part_params); ?>
        </div>
        <div class="mkdf-post-text">
            <div class="mkdf-post-text-inner">
	            <div class="mkdf-post-info-top">
		            <?php evently_mikado_get_module_template_part('templates/parts/post-info/date', 'blog', '', $part_params); ?>
	            </div>
	            <?php evently_mikado_get_module_template_part('templates/parts/title', 'blog', '', $part_params); ?>
	            <div class="mkdf-post-info-bottom clearfix">
		            <?php evently_mikado_get_module_template_part('templates/parts/post-info/author', 'blog', '', $part_params); ?>
		            <?php evently_mikado_get_module_template_part('templates/parts/post-info/comments', 'blog', '', $part_params); ?>
		            <?php if ( ! has_post_thumbnail() ) {
			            evently_mikado_get_module_template_part( 'templates/parts/post-info/category', 'blog', '', $part_params );
		            } ?>
		            <?php evently_mikado_get_module_template_part('templates/parts/post-info/tags', 'blog', '', $part_params); ?>
	            </div>
	            <div class="mkdf-post-text-main">
		            <?php the_content(); ?>
		            <?php do_action('evently_mikado_action_single_link_pages'); ?>
	            </div>
	            <?php
		            $tags = get_the_tags();
		            if(!empty($tags) || ( evently_mikado_options()->getOptionValue('enable_social_share') === 'yes' && evently_mikado_options()->getOptionValue('enable_social_share_on_post') === 'yes' ) ):
	            ?>
		            <div class="mkdf-post-info">
			            <div class="mkdf-post-info-left">
				            <?php evently_mikado_get_module_template_part('templates/parts/post-info/additional-tags', 'blog', '', $part_params); ?>
			            </div>
			            <div class="mkdf-post-info-right">
				            <?php evently_mikado_get_module_template_part('templates/parts/post-info/share', 'blog', '', $part_params); ?>
			            </div>
		            </div>
	            <?php endif; ?>
            </div>
        </div>
    </div>
</article>