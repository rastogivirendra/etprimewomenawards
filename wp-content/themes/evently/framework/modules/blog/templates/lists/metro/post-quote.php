<?php
$image_proportion = evently_mikado_get_meta_field_intersect('blog_list_featured_image_proportion', evently_mikado_get_page_id());
$image_proportion_value = get_post_meta(get_the_ID(), 'mkdf_blog_masonry_gallery_' . $image_proportion .'_dimensions_meta', true);
$part_params['is_metro'] = true;

if (isset($image_proportion_value) && $image_proportion_value !== '') {
    $size = $image_proportion_value !== '' ? $image_proportion_value : 'default';
    $post_classes[] = 'mkdf-post-size-'. $size;
}
else {
    $size = 'default';
    $post_classes[] = 'mkdf-post-size-default';
}
if($image_proportion == 'original') {
    $part_params['image_size'] = 'full';
}
else {
    switch ($size):
        case 'default':
            $part_params['image_size'] = 'evently_mikado_square';
            break;
        case 'large-width':
            $part_params['image_size'] = 'evently_mikado_landscape';
            break;
        case 'large-height':
            $part_params['image_size'] = 'evently_mikado_portrait';
            break;
        case 'large-width-height':
            $part_params['image_size'] = 'evently_mikado_huge';
            break;
        default:
            $part_params['image_size'] = 'evently_mikado_square';
            break;
    endswitch;
}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class($post_classes); ?>>
	<div class="mkdf-post-content">
		<div class="mkdf-post-quote-holder">
			<div class="mkdf-post-mark">
				<?php echo evently_mikado_icon_collections()->renderIcon('icon_quotations', 'font_elegant'); ?>
			</div>
			<?php evently_mikado_get_module_template_part('templates/parts/post-type/quote', 'blog', '', $part_params); ?>
		</div>
	</div>
</article>