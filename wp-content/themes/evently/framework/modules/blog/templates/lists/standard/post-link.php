<article id="post-<?php the_ID(); ?>" <?php post_class($post_classes); ?>>
    <div class="mkdf-post-content">
        <div class="mkdf-post-text">
            <div class="mkdf-post-link-holder">
                <div class="mkdf-post-mark">
                    <?php echo evently_mikado_icon_collections()->renderIcon('icon_link', 'font_elegant'); ?>
                </div>
                <?php evently_mikado_get_module_template_part('templates/parts/post-type/link', 'blog', '', $part_params); ?>
            </div>
        </div>
    </div>
</article>