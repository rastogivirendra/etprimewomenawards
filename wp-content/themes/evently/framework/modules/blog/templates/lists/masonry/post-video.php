<article id="post-<?php the_ID(); ?>" <?php post_class($post_classes); ?>>
    <div class="mkdf-post-content">
	    <?php evently_mikado_get_module_template_part('templates/parts/post-type/video-masonry', 'blog', '', $part_params); ?>
    </div>
</article>