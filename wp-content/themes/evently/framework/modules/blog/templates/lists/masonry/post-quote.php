<article id="post-<?php the_ID(); ?>" <?php post_class($post_classes); ?>>
    <div class="mkdf-post-content">
        <div class="mkdf-post-text">
            <div class="mkdf-post-quote-holder">
                <div class="mkdf-post-text-main">
                    <div class="mkdf-post-mark">
	                    <?php echo evently_mikado_icon_collections()->renderIcon('icon_quotations', 'font_elegant'); ?>
                    </div>
                    <?php evently_mikado_get_module_template_part('templates/parts/post-type/quote', 'blog', '', $part_params); ?>
                </div>
            </div>
        </div>
    </div>
</article>