<li class="mkdf-bl-item clearfix">
	<div class="mkdf-bli-inner">
		<?php if ( $post_info_image == 'yes' ) {
				evently_mikado_get_module_template_part( 'templates/parts/image', 'blog', '', $params );
		} ?>
        <div class="mkdf-bli-content">
            <?php if ($post_info_section == 'yes') { ?>
		        <div class="mkdf-bli-info-top">
			        <?php
				        if ( $post_info_date == 'yes' ) {
					        evently_mikado_get_module_template_part( 'templates/parts/post-info/date', 'blog', '', $params );
				        }
			        ?>
		        </div>
	            <?php evently_mikado_get_module_template_part( 'templates/parts/title', 'blog', '', $params ); ?>
	            <div class="mkdf-bli-excerpt">
		            <?php evently_mikado_get_module_template_part( 'templates/parts/excerpt', 'blog', '', $params ); ?>
		            <?php evently_mikado_get_module_template_part( 'templates/parts/post-info/read-more', 'blog', '', $params ); ?>
	            </div>
	            <?php if ( $post_info_author == 'yes' || $post_info_comments == 'yes' || $post_info_share == 'yes' ){ ?>
	                <div class="mkdf-bli-info-bottom">
			            <?php if ( $post_info_author == 'yes' || $post_info_comments == 'yes' ){ ?>
			                <div class="mkdf-bli-info-bottom-left">
				                <?php
					                if ( $post_info_author == 'yes' ) {
						                evently_mikado_get_module_template_part( 'templates/parts/post-info/author', 'blog', '', $params );
					                }
					                if ( ! has_post_thumbnail() && $post_info_category === 'yes' ) {
						                evently_mikado_get_module_template_part( 'templates/parts/post-info/category', 'blog', '', $params );
					                }
					                if ( $post_info_comments == 'yes' ) {
						                evently_mikado_get_module_template_part( 'templates/parts/post-info/comments', 'blog', '', $params );
					                }
				                ?>
			                </div>
			            <?php } ?>
			            <?php if ( $post_info_share == 'yes' ){ ?>
			                <div class="mkdf-bli-info-bottom-right">
				                <?php
					                if ( $post_info_share == 'yes' ) {
						                evently_mikado_get_module_template_part( 'templates/parts/post-info/share', 'blog', '', $params );
					                }
				                ?>
			                </div>
			            <?php } ?>
	                </div>
	            <?php } ?>
            <?php } ?>

        </div>
	</div>
</li>