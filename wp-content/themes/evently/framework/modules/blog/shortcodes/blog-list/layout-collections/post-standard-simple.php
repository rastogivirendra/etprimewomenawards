<li class="mkdf-bl-item clearfix">
	<div class="mkdf-bli-inner">
        <div class="mkdf-bli-content">
            <?php if ($post_info_section == 'yes') { ?>
		        <div class="mkdf-bli-info-top">
			        <?php
				        if ( $post_info_date == 'yes' ) {
					        evently_mikado_get_module_template_part( 'templates/parts/post-info/date', 'blog', '', $params );
				        }
			        ?>
		        </div>
	            <?php evently_mikado_get_module_template_part( 'templates/parts/title', 'blog', '', $params ); ?>
	            <div class="mkdf-bli-excerpt">
		            <?php evently_mikado_get_module_template_part( 'templates/parts/excerpt', 'blog', '', $params ); ?>
	            </div>
	                <div class="mkdf-bli-info-bottom">
			            <?php  if ( $post_info_author == 'yes' ) {
				            evently_mikado_get_module_template_part( 'templates/parts/post-info/author', 'blog', '', $params );
			            } ?>
	                </div>
            <?php } ?>
        </div>
	</div>
</li>