<?php
	$params['is_simple']  = true;
	$params['image_size'] = 'medium';
?>
<li class="mkdf-bl-item clearfix">
	<div class="mkdf-bli-inner">
		<?php if ( $post_info_image == 'yes' ) {
			evently_mikado_get_module_template_part( 'templates/parts/image', 'blog', '', $params );
		} ?>
		<div class="mkdf-bli-content">
			<?php evently_mikado_get_module_template_part( 'templates/parts/title', 'blog', '', $params ); ?>
			<?php evently_mikado_get_module_template_part( 'templates/parts/post-info/date', 'blog', '', $params ); ?>
		</div>
	</div>
</li>