<?php
namespace MikadoCore\CPT\Shortcodes\BlogList;

use MikadoCore\Lib;

class BlogList implements Lib\ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'mkdf_blog_list';
		
		add_action('vc_before_init', array($this,'vcMap'));
		
		//Category filter
		add_filter( 'vc_autocomplete_mkdf_blog_list_category_callback', array( &$this, 'blogCategoryAutocompleteSuggester', ), 10, 1 ); // Get suggestion(find). Must return an array
		
		//Category render
		add_filter( 'vc_autocomplete_mkdf_blog_list_category_render', array( &$this, 'blogCategoryAutocompleteRender', ), 10, 1 ); // Get suggestion(find). Must return an array
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map(
			array(
				'name'                      => esc_html__( 'Mikado Blog List', 'evently' ),
				'base'                      => $this->base,
				'icon'                      => 'icon-wpb-blog-list extended-custom-icon',
				'category'                  => esc_html__( 'by MIKADO', 'evently' ),
				'allowed_container_element' => 'vc_row',
				'params'                    => array(
					array(
						'type'        => 'dropdown',
						'param_name'  => 'type',
						'heading'     => esc_html__( 'Type', 'evently' ),
						'value'       => array(
							esc_html__( 'Standard', 'evently' )         => 'standard',
							esc_html__( 'Standard Simple', 'evently' )  => 'standard-simple',
							esc_html__( 'Masonry', 'evently' )          => 'masonry',
							esc_html__( 'Simple', 'evently' )           => 'simple',
							esc_html__( 'Minimal', 'evently' )          => 'minimal'
						),
						'save_always' => true
					),
					array(
						'type'       => 'textfield',
						'param_name' => 'number_of_posts',
						'heading'    => esc_html__( 'Number of Posts', 'evently' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'number_of_columns',
						'heading'    => esc_html__( 'Number of Columns', 'evently' ),
						'value'      => array(
							esc_html__( 'One', 'evently' )   => '1',
							esc_html__( 'Two', 'evently' )   => '2',
							esc_html__( 'Three', 'evently' ) => '3',
							esc_html__( 'Four', 'evently' )  => '4',
							esc_html__( 'Five', 'evently' )  => '5'
						),
						'dependency' => Array( 'element' => 'type', 'value' => array( 'standard', 'standard-simple', 'masonry' ) )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'space_between_columns',
						'heading'    => esc_html__( 'Space Between Columns', 'evently' ),
						'value'      => array(
							esc_html__( 'Normal', 'evently' )   => 'normal',
							esc_html__( 'Small', 'evently' )    => 'small',
							esc_html__( 'Tiny', 'evently' )     => 'tiny',
							esc_html__( 'No Space', 'evently' ) => 'no'
						),
						'dependency' => Array( 'element' => 'type', 'value' => array( 'standard', 'standard-simple', 'masonry' ) )
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'order_by',
						'heading'     => esc_html__( 'Order By', 'evently' ),
						'value'       => array_flip( evently_mikado_get_query_order_by_array() ),
						'save_always' => true
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'order',
						'heading'     => esc_html__( 'Order', 'evently' ),
						'value'       => array_flip( evently_mikado_get_query_order_array() ),
						'save_always' => true
					),
					array(
						'type'        => 'autocomplete',
						'param_name'  => 'category',
						'heading'     => esc_html__( 'Category', 'evently' ),
						'description' => esc_html__( 'Enter one category slug (leave empty for showing all categories)', 'evently' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'image_size',
						'heading'    => esc_html__( 'Image Size', 'evently' ),
						'value'      => array(
							esc_html__( 'Original', 'evently' )  => 'full',
							esc_html__( 'Square', 'evently' )    => 'evently_mikado_square',
							esc_html__( 'Landscape', 'evently' ) => 'evently_mikado_landscape',
							esc_html__( 'Portrait', 'evently' )  => 'evently_mikado_portrait',
							esc_html__( 'Thumbnail', 'evently' ) => 'thumbnail',
							esc_html__( 'Medium', 'evently' )    => 'medium',
							esc_html__( 'Large', 'evently' )     => 'large'
						),
						'save_always' => true,
						'dependency'  => Array( 'element' => 'type', 'value' => array( 'standard', 'standard-simple', 'masonry' ) )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'content_bg',
						'heading'    => esc_html__( 'Content Background Color', 'evently' ),
						'value'      => array(
							esc_html__( 'Light', 'evently' )       => 'light',
							esc_html__( 'Transparent', 'evently' ) => 'transparent',
						),
						'save_always' => true,
						'dependency'  => Array( 'element' => 'type', 'value' => array( 'standard' ) )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'title_tag',
						'heading'    => esc_html__( 'Title Tag', 'evently' ),
						'value'      => array_flip( evently_mikado_get_title_tag( true ) ),
						'group'      => esc_html__( 'Post Info', 'evently' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'title_transform',
						'heading'    => esc_html__( 'Title Text Transform', 'evently' ),
						'value'      => array_flip( evently_mikado_get_text_transform_array( true ) ),
						'group'      => esc_html__( 'Post Info', 'evently' )
					),
					array(
						'type'        => 'textfield',
						'param_name'  => 'excerpt_length',
						'heading'     => esc_html__( 'Text Length', 'evently' ),
						'description' => esc_html__( 'Number of characters', 'evently' ),
						'dependency'  => Array( 'element' => 'type', 'value'   => array( 'standard', 'standard-simple', 'masonry', 'simple' ) ),
						'group'       => esc_html__( 'Post Info', 'evently' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'post_info_image',
						'heading'    => esc_html__( 'Enable Post Info Image', 'evently' ),
						'value'      => array_flip( evently_mikado_get_yes_no_select_array( false, true ) ),
						'dependency' => Array( 'element' => 'type', 'value'   => array( 'standard', 'masonry' ) ),
						'group'      => esc_html__( 'Post Info', 'evently' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'post_info_section',
						'heading'    => esc_html__( 'Enable Post Info Section', 'evently' ),
						'value'      => array_flip( evently_mikado_get_yes_no_select_array( false, true ) ),
						'dependency' => Array( 'element' => 'type', 'value'   => array( 'standard', 'masonry' ) ),
						'group'      => esc_html__( 'Post Info', 'evently' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'post_info_author',
						'heading'    => esc_html__( 'Enable Post Info Author', 'evently' ),
						'value'      => array_flip( evently_mikado_get_yes_no_select_array( false, true ) ),
						'dependency' => Array( 'element' => 'post_info_section', 'value' => array( 'yes' ) ),
						'group'      => esc_html__( 'Post Info', 'evently' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'post_info_date',
						'heading'    => esc_html__( 'Enable Post Info Date', 'evently' ),
						'value'      => array_flip( evently_mikado_get_yes_no_select_array( false, true ) ),
						'dependency' => Array( 'element' => 'post_info_section', 'value' => array( 'yes' ) ),
						'group'      => esc_html__( 'Post Info', 'evently' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'post_info_category',
						'heading'    => esc_html__( 'Enable Post Info Category', 'evently' ),
						'value'      => array_flip( evently_mikado_get_yes_no_select_array( false, true ) ),
						'dependency' => Array( 'element' => 'post_info_section', 'value' => array( 'yes' ) ),
						'group'      => esc_html__( 'Post Info', 'evently' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'post_info_comments',
						'heading'    => esc_html__( 'Enable Post Info Comments', 'evently' ),
						'value'      => array_flip( evently_mikado_get_yes_no_select_array( false ) ),
						'dependency' => Array( 'element' => 'post_info_section', 'value' => array( 'yes' ) ),
						'group'      => esc_html__( 'Post Info', 'evently' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'post_info_share',
						'heading'    => esc_html__( 'Enable Post Info Share', 'evently' ),
						'value'      => array_flip( evently_mikado_get_yes_no_select_array( false ) ),
						'dependency' => Array( 'element' => 'post_info_section', 'value' => array( 'yes' ) ),
						'group'      => esc_html__( 'Post Info', 'evently' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'disable_shadow',
						'heading'    => esc_html__( 'Disable Post Shadow', 'evently' ),
						'value'      => array_flip( evently_mikado_get_yes_no_select_array( false ) ),
						'dependency' => Array( 'element' => 'type', 'value'   => 'standard' ),
						'group'      => esc_html__( 'Additional Features', 'evently' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'pagination_type',
						'heading'    => esc_html__( 'Pagination Type', 'evently' ),
						'value'      => array(
							esc_html__( 'None', 'evently' )            => 'no-pagination',
							esc_html__( 'Standard', 'evently' )        => 'standard-blog-list',
							esc_html__( 'Load More', 'evently' )       => 'load-more',
							esc_html__( 'Infinite Scroll', 'evently' ) => 'infinite-scroll'
						),
						'group'      => esc_html__( 'Additional Features', 'evently' )
					)
				)
			)
		);
	}

	public function render($atts, $content = null) {
		$default_atts = array(
			'type'                  => 'standard',
            'number_of_posts'       => '-1',
            'number_of_columns'     => '3',
            'space_between_columns' => 'normal',
			'category'              => '',
			'order_by'              => 'title',
			'order'                 => 'ASC',
			'image_size'            => 'full',
            'content_bg'            => 'light',
            'title_tag'             => 'h4',
			'title_transform'       => '',
			'excerpt_length'        => '40',
            'post_info_section'     => 'yes',
			'post_info_image'       => 'yes',
			'post_info_author'      => 'yes',
			'post_info_date'        => 'yes',
			'post_info_category'    => 'yes',
			'post_info_comments'    => 'no',
			'post_info_share'       => 'no',
			'disable_shadow'        => 'no',
            'pagination_type'       => 'no-pagination'
        );
		$params = shortcode_atts($default_atts, $atts);
		
		$queryArray = $this->generateQueryArray($params);
		$query_result = new \WP_Query($queryArray);
		$params['query_result'] = $query_result;

		$params['holder_data'] = $this->getHolderData($params);
		$params['holder_classes'] = $this->getHolderClasses($params);
		$params['module'] = 'list';
		
		$params['max_num_pages'] = $query_result->max_num_pages;
		$params['paged'] = isset($query_result->query['paged']) ? $query_result->query['paged'] : 1;

		if( $params['post_info_category'] === 'no' ) {
			$params['hide_category'] = true;
		}

		$params['this_object'] = $this;
		
		ob_start();
		
		evently_mikado_get_module_template_part('shortcodes/blog-list/holder', 'blog', $params['type'], $params);
		
		$html = ob_get_contents();
		
		ob_end_clean();
		
		return $html;	
	}
	
	public function generateQueryArray( $params ) {
		$queryArray = array(
			'post_status'    => 'publish',
			'post_type'      => 'post',
			'orderby'        => $params['order_by'],
			'order'          => $params['order'],
			'posts_per_page' => $params['number_of_posts'],
			'post__not_in'   => get_option( 'sticky_posts' )
		);
		
		if ( ! empty( $params['category'] ) ) {
			$queryArray['category_name'] = $params['category'];
		}
		
		if ( ! empty( $params['next_page'] ) ) {
			$queryArray['paged'] = $params['next_page'];
		} else {
			$query_array['paged'] = 1;
		}
		
		return $queryArray;
	}
	
	public function getHolderClasses( $params ) {
		$holderClasses = array();
		
		$holderClasses[] = ! empty( $params['content_bg'] ) ? 'mkdf-bl-' . $params['type'] : 'mkdf-bl-standard';
		$holderClasses[] = $this->getColumnNumberClass( $params['number_of_columns'] );
		$holderClasses[] = ! empty( $params['space_between_columns'] ) ? 'mkdf-bl-' . $params['space_between_columns'] . '-space' : 'mkdf-bl-normal-space';
		$holderClasses[] = ! empty( $params['content_bg'] ) ? 'mkdf-bl-' . $params['content_bg'] : 'mkdf-bl-light';
		$holderClasses[] = ! empty( $params['pagination_type'] ) ? 'mkdf-bl-pag-' . $params['pagination_type'] : 'mkdf-bl-pag-no-pagination';
		$holderClasses[] = isset( $params['disable_shadow'] ) && $params['disable_shadow'] === 'yes' ? 'mkdf-bl-no-shadow' : '';
		
		return implode( ' ', $holderClasses );
	}
	
	public function getColumnNumberClass( $params ) {
		switch ( $params ) {
			case 1:
				$classes = 'mkdf-bl-one-column';
				break;
			case 2:
				$classes = 'mkdf-bl-two-columns';
				break;
			case 3:
				$classes = 'mkdf-bl-three-columns';
				break;
			case 4:
				$classes = 'mkdf-bl-four-columns';
				break;
			case 5:
				$classes = 'mkdf-bl-five-columns';
				break;
			default:
				$classes = 'mkdf-bl-three-columns';
				break;
		}
		
		return $classes;
	}
	
	public function getHolderData( $params ) {
		$dataString = '';
		
		if ( get_query_var( 'paged' ) ) {
			$paged = get_query_var( 'paged' );
		} elseif ( get_query_var( 'page' ) ) {
			$paged = get_query_var( 'page' );
		} else {
			$paged = 1;
		}
		
		$query_result = $params['query_result'];
		
		$params['max_num_pages'] = $query_result->max_num_pages;
		
		if ( ! empty( $paged ) ) {
			$params['next-page'] = $paged + 1;
		}
		
		foreach ( $params as $key => $value ) {
			if ( $key !== 'query_result' && $value !== '' ) {
				$new_key = str_replace( '_', '-', $key );
				
				$dataString .= ' data-' . $new_key . '=' . esc_attr( $value );
			}
		}
		
		return $dataString;
	}
	
	public function getTitleStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['title_transform'] ) ) {
			$styles[] = 'text-transform: ' . $params['title_transform'];
		}
		
		return implode( ';', $styles );
	}

	/**
	 * Filter blog categories
	 *
	 * @param $query
	 *
	 * @return array
	 */
	public function blogCategoryAutocompleteSuggester( $query ) {
		global $wpdb;
		$post_meta_infos       = $wpdb->get_results( $wpdb->prepare( "SELECT a.slug AS slug, a.name AS category_title
					FROM {$wpdb->terms} AS a
					LEFT JOIN ( SELECT term_id, taxonomy  FROM {$wpdb->term_taxonomy} ) AS b ON b.term_id = a.term_id
					WHERE b.taxonomy = 'category' AND a.name LIKE '%%%s%%'", stripslashes( $query ) ), ARRAY_A );
		
		$results = array();
		if ( is_array( $post_meta_infos ) && ! empty( $post_meta_infos ) ) {
			foreach ( $post_meta_infos as $value ) {
				$data          = array();
				$data['value'] = $value['slug'];
				$data['label'] = ( ( strlen( $value['category_title'] ) > 0 ) ? esc_html__( 'Category', 'evently' ) . ': ' . $value['category_title'] : '' );
				$results[]     = $data;
			}
		}
		
		return $results;
	}
	
	/**
	 * Find blog category by slug
	 * @since 4.4
	 *
	 * @param $query
	 *
	 * @return bool|array
	 */
	public function blogCategoryAutocompleteRender( $query ) {
		$query = trim( $query['value'] ); // get value from requested
		if ( ! empty( $query ) ) {
			// get portfolio category
			$category = get_term_by( 'slug', $query, 'category' );
			if ( is_object( $category ) ) {
				
				$category_slug = $category->slug;
				$category_title = $category->name;
				
				$category_title_display = '';
				if ( ! empty( $category_title ) ) {
					$category_title_display = esc_html__( 'Category', 'evently' ) . ': ' . $category_title;
				}
				
				$data          = array();
				$data['value'] = $category_slug;
				$data['label'] = $category_title_display;
				
				return ! empty( $data ) ? $data : false;
			}
			
			return false;
		}
		
		return false;
	}
}