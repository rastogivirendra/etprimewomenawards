<li class="mkdf-bl-item clearfix">
	<div class="mkdf-bli-inner">
		<div class="mkdf-bli-content">
			<?php evently_mikado_get_module_template_part( 'templates/parts/title', 'blog', '', $params ); ?>
			<?php evently_mikado_get_module_template_part( 'templates/parts/post-info/date', 'blog', '', $params ); ?>
		</div>
	</div>
</li>