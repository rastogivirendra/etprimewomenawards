<?php
	$format = get_post_format();
?>
<li class="mkdf-bl-item clearfix">
	<div class="mkdf-bli-inner">
        <?php if( in_array( $format, array('quote', 'link') ) ){ ?>
		<div class="mkdf-post-<?php echo esc_attr($format); ?>-holder">
			<div class="mkdf-post-text-main">
				<div class="mkdf-post-mark">
					<?php
						$icon = 'icon_quotations';
						if( $format === 'link' ) {
							$icon = 'icon_link';
						}
					?>
					<?php echo evently_mikado_icon_collections()->renderIcon($icon, 'font_elegant'); ?>
				</div>
	        <?php evently_mikado_get_module_template_part('templates/parts/post-type/' . esc_html($format), 'blog', '', $params); ?>
			</div>
		</div>
	    <?php } elseif( $format === 'video' ) { ?>
			<?php if ( $post_info_image == 'yes' ) {
		        evently_mikado_get_module_template_part('templates/parts/post-type/video-masonry', 'blog', '', $params);
	        } ?>
        <?php } else { ?>
			<?php if ( $post_info_image == 'yes' ) {
					evently_mikado_get_module_template_part( 'templates/parts/image', 'blog', '', $params );
			} ?>
			<div class="mkdf-bli-content">
	            <?php if ($post_info_section == 'yes') { ?>
			        <div class="mkdf-bli-info-top">
				        <?php
					        if ( $post_info_date == 'yes' ) {
						        evently_mikado_get_module_template_part( 'templates/parts/post-info/date', 'blog', '', $params );
					        }
				        ?>
			        </div>
		            <?php evently_mikado_get_module_template_part( 'templates/parts/title', 'blog', '', $params ); ?>
		            <div class="mkdf-bli-excerpt">
			            <?php evently_mikado_get_module_template_part( 'templates/parts/excerpt', 'blog', '', $params ); ?>
		            </div>
	                <div class="mkdf-bli-info-bottom">
		                <div class="mkdf-bli-info-bottom-left">
			                <?php evently_mikado_get_module_template_part( 'templates/parts/post-info/read-more', 'blog', '', $params ); ?>
		                </div>
		                <div class="mkdf-bli-info-bottom-right">
			                <?php
				                if ( $post_info_share == 'yes' ) {
					                evently_mikado_get_module_template_part( 'templates/parts/post-info/share', 'blog', '', $params );
				                }
			                ?>
		                </div>
	                </div>
	            <?php } ?>
			</div>
        <?php } ?>
	</div>
</li>