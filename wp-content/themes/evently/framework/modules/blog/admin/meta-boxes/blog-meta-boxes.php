<?php

foreach ( glob( MIKADO_FRAMEWORK_MODULES_ROOT_DIR . '/blog/admin/meta-boxes/*/*.php' ) as $meta_box_load ) {
	include_once $meta_box_load;
}

if ( ! function_exists( 'evently_mikado_map_blog_meta' ) ) {
	function evently_mikado_map_blog_meta() {
		$mkd_blog_categories = array();
		$categories           = get_categories();
		foreach ( $categories as $category ) {
			$mkd_blog_categories[ $category->slug ] = $category->name;
		}
		
		$blog_meta_box = evently_mikado_add_meta_box(
			array(
				'scope' => array( 'page' ),
				'title' => esc_html__( 'Blog', 'evently' ),
				'name'  => 'blog_meta'
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_blog_category_meta',
				'type'        => 'selectblank',
				'label'       => esc_html__( 'Blog Category', 'evently' ),
				'description' => esc_html__( 'Choose category of posts to display (leave empty to display all categories)', 'evently' ),
				'parent'      => $blog_meta_box,
				'options'     => $mkd_blog_categories
			)
		);

		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_blog_order_meta',
				'type'        => 'select',
				'label'       => esc_html__( 'Change Order By', 'evently' ),
				'description' => esc_html__( 'Set masonry, or metro layout. Default is in grid.', 'evently' ),
				'parent'      => $blog_meta_box,
				'options'     => array(
					''           => esc_html__( 'Default', 'evently' ),
					'ASC'    => esc_html__( 'Ascending', 'evently' ),
					'DESC' => esc_html__( 'Descending', 'evently' )
				)
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_show_posts_per_page_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Number of Posts', 'evently' ),
				'description' => esc_html__( 'Enter the number of posts to display', 'evently' ),
				'parent'      => $blog_meta_box,
				'options'     => $mkd_blog_categories,
				'args'        => array( "col_width" => 3 )
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_blog_masonry_layout_meta',
				'type'        => 'select',
				'label'       => esc_html__( 'Masonry / Metro - Layout', 'evently' ),
				'description' => esc_html__( 'Set masonry, or metro layout. Default is in grid.', 'evently' ),
				'parent'      => $blog_meta_box,
				'options'     => array(
					''           => esc_html__( 'Default', 'evently' ),
					'in-grid'    => esc_html__( 'In Grid', 'evently' ),
					'full-width' => esc_html__( 'Full Width', 'evently' )
				)
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_blog_masonry_number_of_columns_meta',
				'type'        => 'select',
				'label'       => esc_html__( 'Masonry - Number of Columns', 'evently' ),
				'description' => esc_html__( 'Set number of columns for your masonry blog lists', 'evently' ),
				'parent'      => $blog_meta_box,
				'options'     => array(
					''      => esc_html__( 'Default', 'evently' ),
					'two'   => esc_html__( '2 Columns', 'evently' ),
					'three' => esc_html__( '3 Columns', 'evently' ),
					'four'  => esc_html__( '4 Columns', 'evently' ),
					'five'  => esc_html__( '5 Columns', 'evently' )
				)
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_blog_masonry_space_between_items_meta',
				'type'        => 'select',
				'label'       => esc_html__( 'Masonry - Space Between Items', 'evently' ),
				'description' => esc_html__( 'Set space size between posts for your masonry blog lists', 'evently' ),
				'parent'      => $blog_meta_box,
				'options'     => array(
					''       => esc_html__( 'Default', 'evently' ),
					'normal' => esc_html__( 'Normal', 'evently' ),
					'small'  => esc_html__( 'Small', 'evently' ),
					'tiny'   => esc_html__( 'Tiny', 'evently' ),
					'no'     => esc_html__( 'No Space', 'evently' )
				)
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_blog_list_featured_image_proportion_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Masonry - Featured Image Proportion', 'evently' ),
				'description'   => esc_html__( 'Choose type of proportions you want to use for featured images on masonry blog lists', 'evently' ),
				'parent'        => $blog_meta_box,
				'default_value' => '',
				'options'       => array(
					''         => esc_html__( 'Default', 'evently' ),
					'fixed'    => esc_html__( 'Fixed', 'evently' ),
					'original' => esc_html__( 'Original', 'evently' )
				)
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_blog_pagination_type_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Pagination Type', 'evently' ),
				'description'   => esc_html__( 'Choose a pagination layout for Blog Lists', 'evently' ),
				'parent'        => $blog_meta_box,
				'default_value' => '',
				'options'       => array(
					''                => esc_html__( 'Default', 'evently' ),
					'standard'        => esc_html__( 'Standard', 'evently' ),
					'load-more'       => esc_html__( 'Load More', 'evently' ),
					'infinite-scroll' => esc_html__( 'Infinite Scroll', 'evently' ),
					'no-pagination'   => esc_html__( 'No Pagination', 'evently' )
				)
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'type'          => 'text',
				'name'          => 'mkdf_number_of_chars_meta',
				'default_value' => '',
				'label'         => esc_html__( 'Number of Words in Excerpt', 'evently' ),
				'description'   => esc_html__( 'Enter a number of words in excerpt (article summary). Default value is 40', 'evently' ),
				'parent'        => $blog_meta_box,
				'args'          => array(
					'col_width' => 3
				)
			)
		);
	}
	
	add_action( 'evently_mikado_action_meta_boxes_map', 'evently_mikado_map_blog_meta', 30 );
}