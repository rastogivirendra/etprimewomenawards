<?php

if ( ! function_exists( 'evently_mikado_map_post_quote_meta' ) ) {
	function evently_mikado_map_post_quote_meta() {
		$quote_post_format_meta_box = evently_mikado_add_meta_box(
			array(
				'scope' => array( 'post' ),
				'title' => esc_html__( 'Quote Post Format', 'evently' ),
				'name'  => 'post_format_quote_meta'
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_post_quote_text_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Quote Text', 'evently' ),
				'description' => esc_html__( 'Enter Quote text', 'evently' ),
				'parent'      => $quote_post_format_meta_box,
			
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_post_quote_author_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Quote Author', 'evently' ),
				'description' => esc_html__( 'Enter Quote author', 'evently' ),
				'parent'      => $quote_post_format_meta_box,
			)
		);
	}
	
	add_action( 'evently_mikado_action_meta_boxes_map', 'evently_mikado_map_post_quote_meta', 25 );
}