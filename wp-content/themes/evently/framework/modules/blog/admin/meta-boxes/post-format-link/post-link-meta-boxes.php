<?php

if ( ! function_exists( 'evently_mikado_map_post_link_meta' ) ) {
	function evently_mikado_map_post_link_meta() {
		$link_post_format_meta_box = evently_mikado_add_meta_box(
			array(
				'scope' => array( 'post' ),
				'title' => esc_html__( 'Link Post Format', 'evently' ),
				'name'  => 'post_format_link_meta'
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_post_link_link_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Link', 'evently' ),
				'description' => esc_html__( 'Enter link', 'evently' ),
				'parent'      => $link_post_format_meta_box,
			
			)
		);
	}
	
	add_action( 'evently_mikado_action_meta_boxes_map', 'evently_mikado_map_post_link_meta', 24 );
}