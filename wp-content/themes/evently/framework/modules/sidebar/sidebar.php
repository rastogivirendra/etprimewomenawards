<?php

if ( ! function_exists( 'evently_mikado_register_sidebars' ) ) {
	/**
	 * Function that registers theme's sidebars
	 */
	function evently_mikado_register_sidebars() {
		
		register_sidebar(
			array(
				'id'            => 'sidebar',
				'name'          => esc_html__( 'Sidebar', 'evently' ),
				'description'   => esc_html__( 'Default Sidebar', 'evently' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<div class="mkdf-widget-title-holder"><h5 class="mkdf-widget-title">',
				'after_title'   => '</h5></div>'
			)
		);
	}
	
	add_action( 'widgets_init', 'evently_mikado_register_sidebars', 1 );
}

if ( ! function_exists( 'evently_mikado_add_support_custom_sidebar' ) ) {
	/**
	 * Function that adds theme support for custom sidebars. It also creates EventlyMikadoClassSidebar object
	 */
	function evently_mikado_add_support_custom_sidebar() {
		add_theme_support( 'EventlyMikadoClassSidebar' );
		
		if ( get_theme_support( 'EventlyMikadoClassSidebar' ) ) {
			new EventlyMikadoClassSidebar();
		}
	}
	
	add_action( 'after_setup_theme', 'evently_mikado_add_support_custom_sidebar' );
}