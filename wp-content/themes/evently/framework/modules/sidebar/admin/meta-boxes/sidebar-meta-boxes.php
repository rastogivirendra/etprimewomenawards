<?php

if ( ! function_exists( 'evently_mikado_map_sidebar_meta' ) ) {
	function evently_mikado_map_sidebar_meta() {
		$mkdf_sidebar_meta_box = evently_mikado_add_meta_box(
			array(
				'scope' => apply_filters( 'evently_mikado_filter_set_scope_for_meta_boxes', array( 'page' ) ),
				'title' => esc_html__( 'Sidebar', 'evently' ),
				'name'  => 'sidebar_meta'
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_sidebar_layout_meta',
				'type'        => 'select',
				'label'       => esc_html__( 'Layout', 'evently' ),
				'description' => esc_html__( 'Choose the sidebar layout', 'evently' ),
				'parent'      => $mkdf_sidebar_meta_box,
				'options'     => array(
					''                 => esc_html__( 'Default', 'evently' ),
					'no-sidebar'       => esc_html__( 'No Sidebar', 'evently' ),
					'sidebar-33-right' => esc_html__( 'Sidebar 1/3 Right', 'evently' ),
					'sidebar-25-right' => esc_html__( 'Sidebar 1/4 Right', 'evently' ),
					'sidebar-33-left'  => esc_html__( 'Sidebar 1/3 Left', 'evently' ),
					'sidebar-25-left'  => esc_html__( 'Sidebar 1/4 Left', 'evently' )
				)
			)
		);
		
		$mkdf_custom_sidebars = evently_mikado_get_custom_sidebars();
		if ( count( $mkdf_custom_sidebars ) > 0 ) {
			evently_mikado_add_meta_box_field(
				array(
					'name'        => 'mkdf_custom_sidebar_area_meta',
					'type'        => 'selectblank',
					'label'       => esc_html__( 'Choose Widget Area in Sidebar', 'evently' ),
					'description' => esc_html__( 'Choose Custom Widget area to display in Sidebar"', 'evently' ),
					'parent'      => $mkdf_sidebar_meta_box,
					'options'     => $mkdf_custom_sidebars,
					'args'        => array(
						'select2'	=> true
					)
				)
			);
		}
	}
	
	add_action( 'evently_mikado_action_meta_boxes_map', 'evently_mikado_map_sidebar_meta', 31 );
}