<?php

if ( ! function_exists('evently_mikado_sidebar_options_map') ) {

	function evently_mikado_sidebar_options_map() {

		evently_mikado_add_admin_page(
			array(
				'slug' => '_sidebar_page',
				'title' => esc_html__('Sidebar Area', 'evently'),
				'icon' => 'fa fa-indent'
			)
		);

		$sidebar_panel = evently_mikado_add_admin_panel(
			array(
				'title' => esc_html__('Sidebar Area', 'evently'),
				'name' => 'sidebar',
				'page' => '_sidebar_page'
			)
		);
		
		evently_mikado_add_admin_field(array(
			'name'          => 'sidebar_layout',
			'type'          => 'select',
			'label'         => esc_html__('Sidebar Layout', 'evently'),
			'description'   => esc_html__('Choose a sidebar layout for pages', 'evently'),
			'parent'        => $sidebar_panel,
			'default_value' => 'no-sidebar',
			'options'       => array(
				'no-sidebar'        => esc_html__('No Sidebar', 'evently'),
				'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right', 'evently'),
				'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right', 'evently'),
				'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left', 'evently'),
				'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left', 'evently')
			)
		));
		
		$evently_custom_sidebars = evently_mikado_get_custom_sidebars();
		if(count($evently_custom_sidebars) > 0) {
			evently_mikado_add_admin_field(array(
				'name' => 'custom_sidebar_area',
				'type' => 'selectblank',
				'label' => esc_html__('Sidebar to Display', 'evently'),
				'description' => esc_html__('Choose a sidebar to display on pages. Default sidebar is "Sidebar"', 'evently'),
				'parent' => $sidebar_panel,
				'options' => $evently_custom_sidebars,
				'args'        => array(
					'select2'	=> true
				)
			));
		}
	}

	add_action('evently_mikado_action_additional_page_options_map', 'evently_mikado_sidebar_options_map');
}