<?php

if ( ! function_exists( 'evently_mikado_logo_options_map' ) ) {
	function evently_mikado_logo_options_map() {
		
		$panel_logo = evently_mikado_add_admin_panel(
			array(
				'name'  => 'panel_logo',
				'title' => esc_html__( 'Branding', 'evently' )
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'parent'        => $panel_logo,
				'type'          => 'yesno',
				'name'          => 'hide_logo',
				'default_value' => 'no',
				'label'         => esc_html__( 'Hide Logo', 'evently' ),
				'description'   => esc_html__( 'Enabling this option will hide logo image', 'evently' ),
				'args'          => array(
					"dependence"             => true,
					"dependence_hide_on_yes" => "#mkdf_hide_logo_container",
					"dependence_show_on_yes" => ""
				)
			)
		);
		
		$hide_logo_container = evently_mikado_add_admin_container(
			array(
				'parent'          => $panel_logo,
				'name'            => 'hide_logo_container',
				'hidden_property' => 'hide_logo',
				'hidden_value'    => 'yes'
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'logo_image',
				'type'          => 'image',
				'default_value' => MIKADO_ASSETS_ROOT . "/img/logo.png",
				'label'         => esc_html__( 'Logo Image - Default', 'evently' ),
				'parent'        => $hide_logo_container
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'logo_image_dark',
				'type'          => 'image',
				'default_value' => MIKADO_ASSETS_ROOT . "/img/logo.png",
				'label'         => esc_html__( 'Logo Image - Dark', 'evently' ),
				'parent'        => $hide_logo_container
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'logo_image_light',
				'type'          => 'image',
				'default_value' => MIKADO_ASSETS_ROOT . "/img/logo_white.png",
				'label'         => esc_html__( 'Logo Image - Light', 'evently' ),
				'parent'        => $hide_logo_container
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'logo_image_sticky',
				'type'          => 'image',
				'default_value' => MIKADO_ASSETS_ROOT . "/img/logo.png",
				'label'         => esc_html__( 'Logo Image - Sticky', 'evently' ),
				'parent'        => $hide_logo_container
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'logo_image_mobile',
				'type'          => 'image',
				'default_value' => MIKADO_ASSETS_ROOT . "/img/logo.png",
				'label'         => esc_html__( 'Logo Image - Mobile', 'evently' ),
				'parent'        => $hide_logo_container
			)
		);
	}
	
	add_action( 'evently_mikado_action_additional_general_options_map', 'evently_mikado_logo_options_map' );
}