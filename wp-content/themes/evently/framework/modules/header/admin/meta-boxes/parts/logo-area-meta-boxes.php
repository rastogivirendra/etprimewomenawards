<?php

if ( ! function_exists( 'evently_mikado_get_hide_dep_for_header_logo_area_meta_boxes' ) ) {
	function evently_mikado_get_hide_dep_for_header_logo_area_meta_boxes() {
		$hide_dep_options = apply_filters( 'evently_mikado_filter_header_logo_area_hide_meta_boxes', $hide_dep_options = array() );
		
		return $hide_dep_options;
	}
}

if ( ! function_exists( 'evently_mikado_header_logo_area_meta_options_map' ) ) {
	function evently_mikado_header_logo_area_meta_options_map( $header_meta_box ) {
		$hide_dep_options = evently_mikado_get_hide_dep_for_header_logo_area_meta_boxes();
		
		$logo_area_container = evently_mikado_add_admin_container_no_style(
			array(
				'type'            => 'container',
				'name'            => 'logo_area_container',
				'parent'          => $header_meta_box,
				'hidden_property' => 'mkdf_header_type_meta',
				'hidden_values'   => $hide_dep_options
			)
		);
		
		evently_mikado_add_admin_section_title(
			array(
				'parent' => $logo_area_container,
				'name'   => 'logo_area_style',
				'title'  => esc_html__( 'Logo Area Style', 'evently' )
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_disable_header_widget_logo_area_meta',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Disable Header Logo Area Widget', 'evently' ),
				'description'   => esc_html__( 'Enabling this option will hide widget area from the logo area', 'evently' ),
				'parent'        => $logo_area_container
			)
		);
		
		$evently_custom_sidebars = evently_mikado_get_custom_sidebars();
		if ( count( $evently_custom_sidebars ) > 0 ) {
			evently_mikado_add_meta_box_field(
				array(
					'name'        => 'mkdf_custom_logo_area_sidebar_meta',
					'type'        => 'selectblank',
					'label'       => esc_html__( 'Choose Custom Widget Area for Logo Area', 'evently' ),
					'description' => esc_html__( 'Choose custom widget area to display in header logo area"', 'evently' ),
					'parent'      => $logo_area_container,
					'options'     => $evently_custom_sidebars
				)
			);
		}
		
		evently_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_logo_area_in_grid_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Logo Area In Grid', 'evently' ),
				'description'   => esc_html__( 'Set menu area content to be in grid', 'evently' ),
				'parent'        => $logo_area_container,
				'default_value' => '',
				'options'       => evently_mikado_get_yes_no_select_array(),
				'args'          => array(
					'dependence' => true,
					'hide'       => array(
						''    => '#mkdf_logo_area_in_grid_container',
						'no'  => '#mkdf_logo_area_in_grid_container',
						'yes' => ''
					),
					'show'       => array(
						''    => '',
						'no'  => '',
						'yes' => '#mkdf_logo_area_in_grid_container'
					)
				)
			)
		);
		
		$logo_area_in_grid_container = evently_mikado_add_admin_container(
			array(
				'type'            => 'container',
				'name'            => 'logo_area_in_grid_container',
				'parent'          => $logo_area_container,
				'hidden_property' => 'mkdf_logo_area_in_grid_meta',
				'hidden_value'    => 'no',
				'hidden_values'   => array( '', 'no' )
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_logo_area_grid_background_color_meta',
				'type'        => 'color',
				'label'       => esc_html__( 'Grid Background Color', 'evently' ),
				'description' => esc_html__( 'Set grid background color for logo area', 'evently' ),
				'parent'      => $logo_area_in_grid_container
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_logo_area_grid_background_transparency_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Grid Background Transparency', 'evently' ),
				'description' => esc_html__( 'Set grid background transparency for logo area (0 = fully transparent, 1 = opaque)', 'evently' ),
				'parent'      => $logo_area_in_grid_container,
				'args'        => array(
					'col_width' => 2
				)
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_logo_area_in_grid_border_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Grid Area Border', 'evently' ),
				'description'   => esc_html__( 'Set border on grid logo area', 'evently' ),
				'parent'        => $logo_area_in_grid_container,
				'default_value' => '',
				'options'       => evently_mikado_get_yes_no_select_array(),
				'args'          => array(
					'dependence' => true,
					'hide'       => array(
						''    => '#mkdf_logo_area_in_grid_border_container',
						'no'  => '#mkdf_logo_area_in_grid_border_container',
						'yes' => ''
					),
					'show'       => array(
						''    => '',
						'no'  => '',
						'yes' => '#mkdf_logo_area_in_grid_border_container'
					)
				)
			)
		);
		
		$logo_area_in_grid_border_container = evently_mikado_add_admin_container(
			array(
				'type'            => 'container',
				'name'            => 'logo_area_in_grid_border_container',
				'parent'          => $logo_area_in_grid_container,
				'hidden_property' => 'mkdf_logo_area_in_grid_border_meta',
				'hidden_value'    => 'no',
				'hidden_values'   => array( '', 'no' )
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_logo_area_in_grid_border_color_meta',
				'type'        => 'color',
				'label'       => esc_html__( 'Border Color', 'evently' ),
				'description' => esc_html__( 'Set border color for grid area', 'evently' ),
				'parent'      => $logo_area_in_grid_border_container
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_logo_area_background_color_meta',
				'type'        => 'color',
				'label'       => esc_html__( 'Background Color', 'evently' ),
				'description' => esc_html__( 'Choose a background color for logo area', 'evently' ),
				'parent'      => $logo_area_container
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_logo_area_background_transparency_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Transparency', 'evently' ),
				'description' => esc_html__( 'Choose a transparency for the logo area background color (0 = fully transparent, 1 = opaque)', 'evently' ),
				'parent'      => $logo_area_container,
				'args'        => array(
					'col_width' => 2
				)
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_logo_area_border_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Logo Area Border', 'evently' ),
				'description'   => esc_html__( 'Set border on logo area', 'evently' ),
				'parent'        => $logo_area_container,
				'default_value' => '',
				'options'       => evently_mikado_get_yes_no_select_array(),
				'args'          => array(
					'dependence' => true,
					'hide'       => array(
						''    => '#mkdf_logo_area_border_bottom_color_container',
						'no'  => '#mkdf_logo_area_border_bottom_color_container',
						'yes' => ''
					),
					'show'       => array(
						''    => '',
						'no'  => '',
						'yes' => '#mkdf_logo_area_border_bottom_color_container'
					)
				)
			)
		);
		
		$logo_area_border_bottom_color_container = evently_mikado_add_admin_container(
			array(
				'type'            => 'container',
				'name'            => 'logo_area_border_bottom_color_container',
				'parent'          => $logo_area_container,
				'hidden_property' => 'mkdf_logo_area_border_meta',
				'hidden_value'    => 'no',
				'hidden_values'   => array( '', 'no' )
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_logo_area_border_color_meta',
				'type'        => 'color',
				'label'       => esc_html__( 'Border Color', 'evently' ),
				'description' => esc_html__( 'Choose color of header bottom border', 'evently' ),
				'parent'      => $logo_area_border_bottom_color_container
			)
		);
		
		do_action( 'evently_mikado_action_header_logo_area_additional_meta_boxes_map', $logo_area_container );
	}
	
	add_action( 'evently_mikado_action_header_logo_area_meta_boxes_map', 'evently_mikado_header_logo_area_meta_options_map', 10, 1 );
}