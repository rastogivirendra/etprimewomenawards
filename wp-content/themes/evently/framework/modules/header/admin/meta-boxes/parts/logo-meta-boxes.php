<?php

if ( ! function_exists( 'evently_mikado_logo_meta_box_map' ) ) {
	function evently_mikado_logo_meta_box_map() {
		
		$logo_meta_box = evently_mikado_add_meta_box(
			array(
				'scope' => apply_filters( 'evently_mikado_filter_set_scope_for_meta_boxes', array( 'page', 'post' ) ),
				'title' => esc_html__( 'Logo', 'evently' ),
				'name'  => 'logo_meta'
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_logo_image_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Default', 'evently' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'evently' ),
				'parent'      => $logo_meta_box
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_logo_image_dark_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Dark', 'evently' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'evently' ),
				'parent'      => $logo_meta_box
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_logo_image_light_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Light', 'evently' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'evently' ),
				'parent'      => $logo_meta_box
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_logo_image_sticky_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Sticky', 'evently' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'evently' ),
				'parent'      => $logo_meta_box
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_logo_image_mobile_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Mobile', 'evently' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'evently' ),
				'parent'      => $logo_meta_box
			)
		);
	}
	
	add_action( 'evently_mikado_action_meta_boxes_map', 'evently_mikado_logo_meta_box_map', 47 );
}