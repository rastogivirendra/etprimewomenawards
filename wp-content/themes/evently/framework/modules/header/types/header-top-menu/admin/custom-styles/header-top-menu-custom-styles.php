<?php

if ( ! function_exists( 'evently_mikado_header_top_menu_logo_area_styles' ) ) {
	/**
	 * Generates styles for menu area
	 */
	function evently_mikado_header_top_menu_logo_area_styles() {
		$menu_area_height = evently_mikado_options()->getOptionValue( 'menu_area_height' );
		
		if ( ! empty( $menu_area_height ) ) {
			echo evently_mikado_dynamic_css( '.mkdf-header-top-menu .mkdf-page-header .mkdf-logo-area', array( 'margin-top' => $menu_area_height . 'px' ) );
		}
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_header_top_menu_logo_area_styles' );
}