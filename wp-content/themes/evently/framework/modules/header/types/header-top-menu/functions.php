<?php

if ( ! function_exists( 'evently_mikado_register_header_top_menu_type' ) ) {
	/**
	 * This function is used to register header type class for header factory file
	 */
	function evently_mikado_register_header_top_menu_type( $header_types ) {
		$header_type = array(
			'header-top-menu' => 'EventlyMikadoNamespace\Modules\Header\Types\HeaderTopMenu'
		);
		
		$header_types = array_merge( $header_types, $header_type );
		
		return $header_types;
	}
}

if ( ! function_exists( 'evently_mikado_init_register_header_top_menu_type' ) ) {
	/**
	 * This function is used to wait header-function.php file to init header object and then to init hook registration function above
	 */
	function evently_mikado_init_register_header_top_menu_type() {
		add_filter( 'evently_mikado_filter_register_header_type_class', 'evently_mikado_register_header_top_menu_type' );
	}
	
	add_action( 'evently_mikado_action_before_header_function_init', 'evently_mikado_init_register_header_top_menu_type' );
}

if ( ! function_exists( 'evently_mikado_disable_behaviors_for_header_top_menu' ) ) {
	/**
	 * This function is used to disable sticky header functions that perform processing variables their used in js for this header type
	 */
	function evently_mikado_disable_behaviors_for_header_top_menu( $allow_behavior ) {
		return false;
	}
	
	if ( evently_mikado_check_is_header_type_enabled( 'header-top-menu', evently_mikado_get_page_id() ) ) {
		add_filter( 'evently_mikado_filter_allow_sticky_header_behavior', 'evently_mikado_disable_behaviors_for_header_top_menu' );
		add_filter( 'evently_mikado_filter_allow_content_boxed_layout', 'evently_mikado_disable_behaviors_for_header_top_menu' );
	}
}