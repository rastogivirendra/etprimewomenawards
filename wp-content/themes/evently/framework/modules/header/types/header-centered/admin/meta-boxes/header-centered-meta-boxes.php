<?php

if ( ! function_exists( 'evently_mikado_get_hide_dep_for_header_centered_meta_boxes' ) ) {
	function evently_mikado_get_hide_dep_for_header_centered_meta_boxes() {
		$hide_dep_options = apply_filters( 'evently_mikado_filter_header_centered_hide_meta_boxes', $hide_dep_options = array() );
		
		return $hide_dep_options;
	}
}

if ( ! function_exists( 'evently_mikado_header_centered_meta_map' ) ) {
	function evently_mikado_header_centered_meta_map( $parent ) {
		$hide_dep_options = evently_mikado_get_hide_dep_for_header_centered_meta_boxes();
		
		evently_mikado_add_meta_box_field(
			array(
				'parent'          => $parent,
				'type'            => 'text',
				'name'            => 'logo_wrapper_padding_header_centered',
				'default_value'   => '',
				'label'           => esc_html__( 'Logo Padding', 'evently' ),
				'description'     => esc_html__( 'Insert padding in format: 0px 0px 1px 0px', 'evently' ),
				'args'            => array(
					'col_width' => 3
				),
				'hidden_property' => 'mkdf_header_type_meta',
				'hidden_values'   => $hide_dep_options
			)
		);
	}
	
	add_action( 'evently_mikado_action_header_logo_area_additional_meta_boxes_map', 'evently_mikado_header_centered_meta_map', 10, 1 );
}