<?php

if ( ! function_exists( 'evently_mikado_register_header_standard_extended_type' ) ) {
	/**
	 * This function is used to register header type class for header factory file
	 */
	function evently_mikado_register_header_standard_extended_type( $header_types ) {
		$header_type = array(
			'header-standard-extended' => 'EventlyMikadoNamespace\Modules\Header\Types\HeaderStandardExtended'
		);
		
		$header_types = array_merge( $header_types, $header_type );
		
		return $header_types;
	}
}

if ( ! function_exists( 'evently_mikado_init_register_header_standard_extended_type' ) ) {
	/**
	 * This function is used to wait header-function.php file to init header object and then to init hook registration function above
	 */
	function evently_mikado_init_register_header_standard_extended_type() {
		add_filter( 'evently_mikado_filter_register_header_type_class', 'evently_mikado_register_header_standard_extended_type' );
	}
	
	add_action( 'evently_mikado_action_before_header_function_init', 'evently_mikado_init_register_header_standard_extended_type' );
}