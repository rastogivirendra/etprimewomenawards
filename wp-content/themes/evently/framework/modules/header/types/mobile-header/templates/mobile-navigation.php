<?php do_action('evently_mikado_action_before_mobile_navigation'); ?>

<nav class="mkdf-mobile-nav">
	<?php echo evently_mikado_icon_collections()->renderIcon('icon_close', 'font_elegant'); ?>
    <?php wp_nav_menu(array(
        'theme_location' => 'mobile-navigation' ,
        'container'  => '',
        'container_class' => '',
        'menu_class' => '',
        'menu_id' => '',
        'fallback_cb' => 'top_navigation_fallback',
        'link_before' => '<span>',
        'link_after' => '</span>',
        'walker' => new EventlyMikadoClassMobileNavigationWalker()
    )); ?>

	<div class="mkdf-mobile-sidebar">
	    <?php dynamic_sidebar('mkdf-mobile-bottom'); ?>
	</div>
</nav>

<?php do_action('evently_mikado_action_after_mobile_navigation'); ?>