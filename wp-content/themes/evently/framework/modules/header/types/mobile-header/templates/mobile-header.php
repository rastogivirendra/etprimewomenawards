<?php do_action('evently_mikado_action_before_mobile_header'); ?>

<header class="mkdf-mobile-header">
	<?php do_action('evently_mikado_action_after_mobile_header_html_open'); ?>

	<div class="mkdf-mobile-header-inner">
		<div class="mkdf-mobile-header-holder">
			<div class="mkdf-grid">
				<div class="mkdf-vertical-align-containers">
					<div class="mkdf-vertical-align-containers">
						<div class="mkdf-position-left">
							<div class="mkdf-position-left-inner">
								<?php if(is_active_sidebar('mkdf-right-from-mobile-logo')) {
									dynamic_sidebar('mkdf-right-from-mobile-logo');
								} ?>
							</div>
						</div>
						<div class="mkdf-position-center">
							<div class="mkdf-position-center-inner">
								<?php evently_mikado_get_mobile_logo(); ?>
							</div>
						</div>
						<div class="mkdf-position-right">
							<?php if($show_navigation_opener) : ?>
								<div class="mkdf-mobile-menu-opener">
									<a href="javascript:void(0)">
									<span class="mkdf-fm-lines">
										<span class="mkdf-fm-line mkdf-line-1"></span>
										<span class="mkdf-fm-line mkdf-line-2"></span>
										<span class="mkdf-fm-line mkdf-line-3"></span>
									</span>
										<?php if(!empty($mobile_menu_title)) { ?>
											<h5 class="mkdf-mobile-menu-text"><?php echo esc_attr($mobile_menu_title); ?></h5>
										<?php } ?>
									</a>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php evently_mikado_get_mobile_nav(); ?>

	<?php do_action('evently_mikado_action_before_mobile_header_html_close'); ?>
</header>

<?php do_action('evently_mikado_action_after_mobile_header'); ?>