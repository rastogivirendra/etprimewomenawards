<?php

if ( ! function_exists( 'evently_mikado_sticky_header_meta_boxes_options_map' ) ) {
	function evently_mikado_sticky_header_meta_boxes_options_map( $header_meta_box ) {
		
		$sticky_amount_container = evently_mikado_add_admin_container(
			array(
				'parent'          => $header_meta_box,
				'name'            => 'sticky_amount_container_meta_container',
				'hidden_property' => 'mkdf_header_behaviour_meta',
				'hidden_values'   => array(
					'',
					'no-behavior',
					'fixed-on-scroll',
					'sticky-header-on-scroll-up'
				)
			)
		);

		$evently_custom_sidebars = evently_mikado_get_custom_sidebars();
		if ( count( $evently_custom_sidebars ) > 0 ) {
			evently_mikado_add_meta_box_field(
				array(
					'name'        => 'mkdf_custom_sticky_menu_area_sidebar_meta',
					'type'        => 'selectblank',
					'label'       => esc_html__( 'Choose Custom Widget Area In Sticky Menu Area', 'evently' ),
					'description' => esc_html__( 'Choose custom widget area to display in sticky header menu area"', 'evently' ),
					'parent'      => $sticky_amount_container,
					'options'     => $evently_custom_sidebars
				)
			);
		}
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_scroll_amount_for_sticky_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Scroll amount for sticky header appearance', 'evently' ),
				'description' => esc_html__( 'Define scroll amount for sticky header appearance', 'evently' ),
				'parent'      => $sticky_amount_container,
				'args'        => array(
					'col_width' => 2,
					'suffix'    => 'px'
				)
			)
		);
	}
	
	add_action( 'evently_mikado_action_additional_header_area_meta_boxes_map', 'evently_mikado_sticky_header_meta_boxes_options_map', 10, 1 );
}