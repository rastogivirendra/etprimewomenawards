<?php

if ( ! function_exists( 'evently_mikado_get_hide_dep_for_header_standard_options' ) ) {
	function evently_mikado_get_hide_dep_for_header_standard_options() {
		$hide_dep_options = apply_filters( 'evently_mikado_filter_header_standard_hide_global_option', $hide_dep_options = array() );
		
		return $hide_dep_options;
	}
}

if ( ! function_exists( 'evently_mikado_header_standard_map' ) ) {
	function evently_mikado_header_standard_map( $parent ) {
		$hide_dep_options = evently_mikado_get_hide_dep_for_header_standard_options();
		
		evently_mikado_add_admin_field(
			array(
				'parent'          => $parent,
				'type'            => 'select',
				'name'            => 'set_menu_area_position',
				'default_value'   => 'right',
				'label'           => esc_html__( 'Choose Menu Area Position', 'evently' ),
				'description'     => esc_html__( 'Select menu area position in your header', 'evently' ),
				'options'         => array(
					'right'  => esc_html__( 'Right', 'evently' ),
					'left'   => esc_html__( 'Left', 'evently' ),
					'center' => esc_html__( 'Center', 'evently' )
				),
				'hidden_property' => 'header_type',
				'hidden_values'   => $hide_dep_options
			)
		);
	}
	
	add_action( 'evently_mikado_action_additional_header_menu_area_options_map', 'evently_mikado_header_standard_map' );
}