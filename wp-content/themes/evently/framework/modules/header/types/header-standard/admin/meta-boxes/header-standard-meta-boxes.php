<?php

if ( ! function_exists( 'evently_mikado_get_hide_dep_for_header_standard_meta_boxes' ) ) {
	function evently_mikado_get_hide_dep_for_header_standard_meta_boxes() {
		$hide_dep_options = apply_filters( 'evently_mikado_filter_header_standard_hide_meta_boxes', $hide_dep_options = array() );
		
		return $hide_dep_options;
	}
}

if ( ! function_exists( 'evently_mikado_header_standard_meta_map' ) ) {
	function evently_mikado_header_standard_meta_map( $parent ) {
		$hide_dep_options = evently_mikado_get_hide_dep_for_header_standard_meta_boxes();
		
		evently_mikado_add_meta_box_field(
			array(
				'parent'          => $parent,
				'type'            => 'select',
				'name'            => 'mkdf_set_menu_area_position_meta',
				'default_value'   => '',
				'label'           => esc_html__( 'Choose Menu Area Position', 'evently' ),
				'description'     => esc_html__( 'Select menu area position in your header', 'evently' ),
				'options'         => array(
					''       => esc_html__( 'Default', 'evently' ),
					'left'   => esc_html__( 'Left', 'evently' ),
					'right'  => esc_html__( 'Right', 'evently' ),
					'center' => esc_html__( 'Center', 'evently' )
				),
				'hidden_property' => 'mkdf_header_type_meta',
				'hidden_values'   => $hide_dep_options
			)
		);
	}
	
	add_action( 'evently_mikado_action_additional_header_area_meta_boxes_map', 'evently_mikado_header_standard_meta_map' );
}