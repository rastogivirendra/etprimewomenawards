<div class="mkdf-fullscreen-menu-holder-outer">
	<div class="mkdf-fullscreen-menu-holder">
		<div class="mkdf-fullscreen-menu-holder-inner">
			<?php if ($fullscreen_menu_in_grid) : ?>
				<div class="mkdf-container-inner">
			<?php endif; ?>

			<?php if( ! empty( $tagline ) || ! empty( $title ) ): ?>
				<div class="mkdf-fullscreen-menu-top">
					<?php if( ! empty( $tagline ) ): ?>
						<h6 class="mkdf-fullscreen-tagline"><?php  echo esc_html( $tagline ); ?></h6>
					<?php endif; ?>
					<?php if( ! empty( $title ) ): ?>
						<span class="mkdf-fullscreen-title"><?php  echo esc_html( $title ); ?></span>
					<?php endif; ?>
				</div>
			<?php endif; ?>

			<?php //Navigation
				evently_mikado_get_module_template_part('templates/full-screen-menu-navigation', 'header/types/header-minimal');
			?>

			<?php if( is_active_sidebar( 'fullscreen_menu_bottom_left' ) || is_active_sidebar( 'fullscreen_menu_bottom_right' ) ) : ?>
				<div class="mkdf-fullscreen-menu-bottom <?php echo ! $fullscreen_menu_in_grid ? 'mkdf-full-width-bottom' : ''; ?>">
					<?php if ($fullscreen_menu_in_grid) : ?>
						<div class="mkdf-container-inner">
					<?php endif; ?>
					<?php
						if( is_active_sidebar( 'fullscreen_menu_bottom_left' ) ) : ?>
							<div class="mkdf-fullscreen-below-menu-widget-holder-left">
								<?php dynamic_sidebar('fullscreen_menu_bottom_left'); ?>
							</div>
						<?php endif;
					?>
					<?php
						if(is_active_sidebar('fullscreen_menu_bottom_right')) : ?>
							<div class="mkdf-fullscreen-below-menu-widget-holder-right">
								<?php dynamic_sidebar('fullscreen_menu_bottom_right'); ?>
							</div>
						<?php endif;
					?>
					<?php if ($fullscreen_menu_in_grid) : ?>
						</div>
					<?php endif; ?>
				</div>
			<?php endif; ?>

			<?php if ($fullscreen_menu_in_grid) : ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>