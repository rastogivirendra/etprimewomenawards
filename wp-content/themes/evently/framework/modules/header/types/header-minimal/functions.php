<?php

if ( ! function_exists( 'evently_mikado_register_header_minimal_type' ) ) {
	/**
	 * This function is used to register header type class for header factory file
	 */
	function evently_mikado_register_header_minimal_type( $header_types ) {
		$header_type = array(
			'header-minimal' => 'EventlyMikadoNamespace\Modules\Header\Types\HeaderMinimal'
		);
		
		$header_types = array_merge( $header_types, $header_type );
		
		return $header_types;
	}
}

if ( ! function_exists( 'evently_mikado_init_register_header_minimal_type' ) ) {
	/**
	 * This function is used to wait header-function.php file to init header object and then to init hook registration function above
	 */
	function evently_mikado_init_register_header_minimal_type() {
		add_filter( 'evently_mikado_filter_register_header_type_class', 'evently_mikado_register_header_minimal_type' );
	}
	
	add_action( 'evently_mikado_action_before_header_function_init', 'evently_mikado_init_register_header_minimal_type' );
}

if ( ! function_exists( 'evently_mikado_include_header_minimal_full_screen_menu' ) ) {
	/**
	 * Registers additional menu navigation for theme
	 */
	function evently_mikado_include_header_minimal_full_screen_menu( $menus ) {
		$menus['popup-navigation'] = esc_html__( 'Full Screen Navigation', 'evently' );
		
		return $menus;
	}
	
	if ( evently_mikado_check_is_header_type_enabled( 'header-minimal' ) ) {
		add_filter( 'evently_mikado_filter_register_headers_menu', 'evently_mikado_include_header_minimal_full_screen_menu' );
	}
}

if ( ! function_exists( 'evently_mikado_register_header_minimal_full_screen_menu_widgets' ) ) {
	/**
	 * Registers additional widget areas for this header type
	 */
	function evently_mikado_register_header_minimal_full_screen_menu_widgets() {
		register_sidebar(
			array(
				'id'            => 'fullscreen_menu_bottom_left',
				'name'          => esc_html__( 'Fullscreen Menu Bottom Left', 'evently' ),
				'description'   => esc_html__( 'This widget area is rendered bellow full screen menu on the left side', 'evently' ),
				'before_widget' => '<div class="%2$s mkdf-fullscreen-menu-above-widget">',
				'after_widget'  => '</div>',
				'before_title'  => '<h5 class="mkdf-widget-title">',
				'after_title'   => '</h5>'
			)
		);
		
		register_sidebar(
			array(
				'id'            => 'fullscreen_menu_bottom_right',
				'name'          => esc_html__( 'Fullscreen Menu Bottom Right', 'evently' ),
				'description'   => esc_html__( 'This widget area is rendered below full screen menu on the right side', 'evently' ),
				'before_widget' => '<div class="%2$s mkdf-fullscreen-menu-below-widget">',
				'after_widget'  => '</div>',
				'before_title'  => '<h5 class="mkdf-widget-title">',
				'after_title'   => '</h5>'
			)
		);
	}
	
	if ( evently_mikado_check_is_header_type_enabled( 'header-minimal' ) ) {
		add_action( 'widgets_init', 'evently_mikado_register_header_minimal_full_screen_menu_widgets' );
	}
}

if ( ! function_exists( 'evently_mikado_get_header_minimal_full_screen_menu' ) ) {
	/**
	 * Loads fullscreen menu HTML template
	 */
	function evently_mikado_get_header_minimal_full_screen_menu() {
		$tagline = evently_mikado_options()->getOptionValue( 'fullscreen_menu_tagline' );
		$title   = evently_mikado_options()->getOptionValue( 'fullscreen_menu_title' );

		$parameters = array(
			'fullscreen_menu_in_grid' => evently_mikado_options()->getOptionValue( 'fullscreen_in_grid' ) === 'yes' ? true : false,
			'tagline'                 => ! empty( $tagline ) ? $tagline : '',
			'title'                   => ! empty( $title ) ? $title : ''
		);
		
		evently_mikado_get_module_template_part( 'templates/full-screen-menu', 'header/types/header-minimal', '', $parameters );
	}
	
	if ( evently_mikado_check_is_header_type_enabled( 'header-minimal', evently_mikado_get_page_id() ) ) {
		add_action( 'evently_mikado_action_after_header_area', 'evently_mikado_get_header_minimal_full_screen_menu', 10 );
	}
}