(function($) {
    "use strict";

    var headerMinimal = {};
    mkdf.modules.headerMinimal = headerMinimal;
	
	headerMinimal.mkdfOnDocumentReady = mkdfOnDocumentReady;

    $(document).ready(mkdfOnDocumentReady);
    
    /* 
        All functions to be called on $(document).ready() should be in this function
    */
    function mkdfOnDocumentReady() {
        mkdfFullscreenMenu();
    }

    /**
     * Init Fullscreen Menu
     */
    function mkdfFullscreenMenu() {
	    var popupMenuOpener = $( 'a.mkdf-fullscreen-menu-opener');
	    
        if (popupMenuOpener.length) {
            var popupMenuHolderOuter = $(".mkdf-fullscreen-menu-holder-outer"),
	            menu = popupMenuHolderOuter.find('.mkdf-fullscreen-menu').find('> ul');
	            
            //set height of popup holder and initialize nicescroll
            popupMenuHolderOuter.height(mkdf.windowHeight).niceScroll({
                scrollspeed: 30,
                mousescrollstep: 20,
                cursorwidth: 0,
                cursorborder: 0,
                cursorborderradius: 0,
                cursorcolor: "transparent",
                autohidemode: false,
                horizrailenabled: false
            });

            //set height of popup holder on resize
            $(window).resize(function() {
                popupMenuHolderOuter.height(mkdf.windowHeight);
            });

            // Wrap items in columns
	        var m = 0;
	        menu.find('> li').each(function(){
	        	var $this = $(this);

		        if( m === 0 ) {
			        menu.append('<ul class="mkdf-popup-column" />');
			        $this.appendTo(menu.find('.mkdf-popup-column').last());
		        } else if( m === 4 || $this === menu.find('> li').last() ) {
		        	m = 0;
			        menu.append('<ul class="mkdf-popup-column" />');
			        $this.appendTo(menu.find('.mkdf-popup-column').last());
		        } else if( m > 0 && m <= 3 ) {
			        $this.appendTo(menu.find('.mkdf-popup-column').last());
		        }

		        m++;
	        });

            // Open popup menu
            popupMenuOpener.on('click',function(e){
                e.preventDefault();

                if (!popupMenuOpener.hasClass('mkdf-fm-opened')) {
                    popupMenuOpener.addClass('mkdf-fm-opened');
                    mkdf.body.removeClass('mkdf-fullscreen-fade-out').addClass('mkdf-fullscreen-menu-opened mkdf-fullscreen-fade-in');
                    mkdf.modules.common.mkdfDisableScroll();
                    
                    $(document).keyup(function(e){
                        if (e.keyCode == 27 ) {
                            popupMenuOpener.removeClass('mkdf-fm-opened');
                            mkdf.body.removeClass('mkdf-fullscreen-menu-opened mkdf-fullscreen-fade-in').addClass('mkdf-fullscreen-fade-out');
                            mkdf.modules.common.mkdfEnableScroll();
                        }
                    });
                } else {
                    popupMenuOpener.removeClass('mkdf-fm-opened');
                    mkdf.body.removeClass('mkdf-fullscreen-menu-opened mkdf-fullscreen-fade-in').addClass('mkdf-fullscreen-fade-out');
                    mkdf.modules.common.mkdfEnableScroll();
                }
            });
        }
    }

})(jQuery);