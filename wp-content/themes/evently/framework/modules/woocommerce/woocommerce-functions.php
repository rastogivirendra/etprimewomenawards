<?php
/**
 * Woocommerce helper functions
 */

if ( ! function_exists( 'evently_mikado_woocommerce_meta_box_functions' ) ) {
	function evently_mikado_woocommerce_meta_box_functions( $post_types ) {
		$post_types[] = 'product';
		
		return $post_types;
	}
	
	add_filter( 'evently_mikado_filter_meta_box_post_types_save', 'evently_mikado_woocommerce_meta_box_functions' );
}

if(!function_exists('evently_mikado_woocommerce_add_social_share_option')) {
    function evently_mikado_woocommerce_add_social_share_option($container) {
        if ( evently_mikado_is_woocommerce_installed() ) {
            evently_mikado_add_admin_field(
                array(
                    'type'          => 'yesno',
                    'name'          => 'enable_social_share_on_product',
                    'default_value' => 'no',
                    'label'         => esc_html__( 'Product', 'evently' ),
                    'description'   => esc_html__( 'Show Social Share for Product Items', 'evently' ),
                    'parent'        => $container
                )
            );
        }
    }

    add_action('evently_mikado_action_post_types_social_share', 'evently_mikado_woocommerce_add_social_share_option', 10, 1);
}

if ( ! function_exists( 'evently_mikado_get_woo_shortcode_module_template_part' ) ) {
	/**
	 * Loads module template part.
	 *
	 * @param string $template name of the template to load
	 * @param string $module name of the module folder
	 * @param string $slug
	 * @param array $params array of parameters to pass to template
	 *
	 * @return html
	 * @see evently_mikado_get_template_part()
	 */
	function evently_mikado_get_woo_shortcode_module_template_part( $template, $module, $slug = '', $params = array() ) {
		
		//HTML Content from template
		$html          = '';
		$template_path = 'framework/modules/woocommerce/shortcodes/' . $module;
		
		$temp = $template_path . '/' . $template;
		
		if ( is_array( $params ) && count( $params ) ) {
			extract( $params );
		}
		
		$templates = array();
		
		if ( $temp !== '' ) {
			if ( $slug !== '' ) {
				$templates[] = "{$temp}-{$slug}.php";
			}
			
			$templates[] = $temp . '.php';
		}
		$located = evently_mikado_find_template_path( $templates );
		if ( $located ) {
			ob_start();
			include( $located );
			$html = ob_get_clean();
		}
		
		return $html;
	}
}

if ( ! function_exists( 'evently_mikado_is_woocommerce_page' ) ) {
	/**
	 * Function that checks if current page is woocommerce shop, product or product taxonomy
	 * @return bool
	 *
	 * @see is_woocommerce()
	 */
	function evently_mikado_is_woocommerce_page() {
		if ( function_exists( 'is_woocommerce' ) && is_woocommerce() ) {
			return is_woocommerce();
		} elseif ( function_exists( 'is_cart' ) && is_cart() ) {
			return is_cart();
		} elseif ( function_exists( 'is_checkout' ) && is_checkout() ) {
			return is_checkout();
		} elseif ( function_exists( 'is_account_page' ) && is_account_page() ) {
			return is_account_page();
		}
	}
}

if ( ! function_exists( 'evently_mikado_is_woocommerce_shop' ) ) {
	/**
	 * Function that checks if current page is shop or product page
	 * @return bool
	 *
	 * @see is_shop()
	 */
	function evently_mikado_is_woocommerce_shop() {
		return function_exists( 'is_shop' ) && ( is_shop() || is_product() );
	}
}

if ( ! function_exists( 'evently_mikado_get_woo_shop_page_id' ) ) {
	/**
	 * Function that returns shop page id that is set in WooCommerce settings page
	 * @return int id of shop page
	 */
	function evently_mikado_get_woo_shop_page_id() {
		if ( evently_mikado_is_woocommerce_installed() ) {
			//get shop page id from options table
			$shop_id = get_option( 'woocommerce_shop_page_id' );
			
			if ( ! empty( $shop_id ) ) {
				$page_id = $shop_id;
			} else {
				$page_id = '-1';
			}
			
			return $page_id;
		}
	}
}

if ( ! function_exists( 'evently_mikado_is_product_category' ) ) {
	function evently_mikado_is_product_category() {
		return function_exists( 'is_product_category' ) && is_product_category();
	}
}

if ( ! function_exists( 'evently_mikado_is_product_tag' ) ) {
	function evently_mikado_is_product_tag() {
		return function_exists( 'is_product_tag' ) && is_product_tag();
	}
}

if ( ! function_exists( 'evently_mikado_load_woo_assets' ) ) {
	/**
	 * Function that checks whether WooCommerce assets needs to be loaded.
	 *
	 * @see evently_mikado_is_woocommerce_page()
	 * @see evently_mikado_has_woocommerce_shortcode()
	 * @see evently_mikado_has_woocommerce_widgets()
	 * @return bool
	 */
	
	function evently_mikado_load_woo_assets() {
		return evently_mikado_is_woocommerce_installed() && ( evently_mikado_is_woocommerce_page() || evently_mikado_has_woocommerce_shortcode() || evently_mikado_has_woocommerce_widgets() );
	}
}

if ( ! function_exists( 'evently_mikado_return_woocommerce_global_variable' ) ) {
	function evently_mikado_return_woocommerce_global_variable() {
		if ( evently_mikado_is_woocommerce_installed() ) {
			global $product;
			
			return $product;
		}
	}
}

if ( ! function_exists( 'evently_mikado_has_woocommerce_shortcode' ) ) {
	/**
	 * Function that checks if current page has at least one of WooCommerce shortcodes added
	 * @return bool
	 */
	function evently_mikado_has_woocommerce_shortcode() {
		$woocommerce_shortcodes = array(
			'mkdf_product_info',
			'mkdf_product_list',
			'mkdf_product_list_carousel',
			'mkdf_product_list_simple',
			'add_to_cart',
			'add_to_cart_url',
			'product_page',
			'product',
			'products',
			'product_categories',
			'product_category',
			'recent_products',
			'featured_products',
			'sale_products',
			'best_selling_products',
			'top_rated_products',
			'product_attribute',
			'related_products',
			'woocommerce_messages',
			'woocommerce_cart',
			'woocommerce_checkout',
			'woocommerce_order_tracking',
			'woocommerce_my_account',
			'woocommerce_edit_address',
			'woocommerce_change_password',
			'woocommerce_view_order',
			'woocommerce_pay',
			'woocommerce_thankyou'
		);
		
		foreach ( $woocommerce_shortcodes as $woocommerce_shortcode ) {
			$has_shortcode = evently_mikado_has_shortcode( $woocommerce_shortcode );
			
			if ( $has_shortcode ) {
				return true;
			}
		}
		
		return false;
	}
}

if ( ! function_exists( 'evently_mikado_has_woocommerce_widgets' ) ) {
	/**
	 * Function that checks if current page has at least one of WooCommerce shortcodes added
	 * @return bool
	 */
	function evently_mikado_has_woocommerce_widgets() {
		$widgets_array = array(
			'mkdf_woocommerce_dropdown_cart',
			'woocommerce_widget_cart',
			'woocommerce_layered_nav',
			'woocommerce_layered_nav_filters',
			'woocommerce_price_filter',
			'woocommerce_product_categories',
			'woocommerce_product_search',
			'woocommerce_product_tag_cloud',
			'woocommerce_products',
			'woocommerce_recent_reviews',
			'woocommerce_recently_viewed_products',
			'woocommerce_top_rated_products'
		);
		
		foreach ( $widgets_array as $widget ) {
			$active_widget = is_active_widget( false, false, $widget );
			
			if ( $active_widget ) {
				return true;
			}
		}
		
		return false;
	}
}

if(!function_exists('evently_mikado_woo_change_tabs_to_accordions')) {
	/**
	 * Function that add accordions elements instead of tabs for single product page meta
	 * Return html
	 */
	function evently_mikado_woo_change_tabs_to_accordions($tabs) {
		if(!empty($tabs)) { ?>
			<div class="mkdf-tabs woocommerce-tabs mkdf-tabs-simple ui-tabs ui-widget ui-widget-content ui-corner-all">
				<ul class="mkdf-tabs-nav clearfix">
					<?php foreach($tabs as $key => $tab) : ?>
						<?php $title = apply_filters('woocommerce_product_'.$key.'_tab_title', esc_html($tab['title']), $key); ?>
						<li><a href="#tab-<?php echo esc_attr($key); ?>"><?php echo esc_html($title); ?></a></li>
					<?php endforeach; ?>
					<li class="mkdf-tabs-nav-line"></li>
				</ul>
				<?php foreach($tabs as $key => $tab) : ?>
					<div class="mkdf-tab-container entry-content" id="tab-<?php echo esc_attr($key); ?>"><?php call_user_func($tab['callback'], $key, $tab) ?></div>
				<?php endforeach; ?>
			</div>
			<?php
		}
	}
}

if (!function_exists('evently_mikado_woocommerce_share')) {
	/**
	 * Function that social share for product page
	 * Return array array of WooCommerce pages
	 */
	function evently_mikado_woocommerce_share() {
		if (evently_mikado_options()->getOptionValue('enable_social_share') == 'yes' && evently_mikado_options()->getOptionValue('enable_social_share_on_product') == 'yes') :
			print '<div class="mkdf-woo-social-share-holder">';
			echo evently_mikado_get_social_share_html(array('type' => 'dropdown'));
			print '</div>';
		endif;
	}
}