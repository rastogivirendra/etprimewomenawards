<?php

if(!function_exists('evently_mikado_map_woocommerce_meta')) {
    function evently_mikado_map_woocommerce_meta() {
        $woocommerce_meta_box = evently_mikado_add_meta_box(
            array(
                'scope' => array('product'),
                'title' => esc_html__('Product Meta', 'evently'),
                'name' => 'woo_product_meta'
            )
        );

        evently_mikado_add_meta_box_field(array(
            'name'        => 'mkdf_product_featured_image_size',
            'type'        => 'select',
            'label'       => esc_html__('Dimensions for Product List Shortcode', 'evently'),
            'description' => esc_html__('Choose image layout when it appears in Mikado Product List - Masonry layout shortcode', 'evently'),
            'parent'      => $woocommerce_meta_box,
            'options'     => array(
                'mkdf-woo-image-normal-width' => esc_html__('Default', 'evently'),
                'mkdf-woo-image-large-width'  => esc_html__('Large Width', 'evently')
            )
        ));

        evently_mikado_add_meta_box_field(
            array(
                'name'          => 'mkdf_show_title_area_woo_meta',
                'type'          => 'select',
                'default_value' => '',
                'label'         => esc_html__('Show Title Area', 'evently'),
                'description'   => esc_html__('Disabling this option will turn off page title area', 'evently'),
                'parent'        => $woocommerce_meta_box,
                'options'       => evently_mikado_get_yes_no_select_array()
            )
        );

	    evently_mikado_add_meta_box_field(
		    array(
			    'name'          => 'mkdf_show_new_sign_woo_meta',
			    'type'          => 'select',
			    'default_value' => '',
			    'label'         => esc_html__('Show New Sign', 'evently'),
			    'parent'        => $woocommerce_meta_box,
			    'options'       => evently_mikado_get_yes_no_select_array(false)
		    )
	    );
    }
	
    add_action('evently_mikado_action_meta_boxes_map', 'evently_mikado_map_woocommerce_meta', 99);
}