<?php
$product = evently_mikado_return_woocommerce_global_variable();
$sale_sold = '';

if ($product->is_on_sale() && !$product->is_in_stock()) {
	$sale_sold = 'mkdf-'. esc_attr($class_name) . '-sale-sold';
}

if ($product->is_on_sale()) { ?>
	<span class="mkdf-<?php echo esc_attr($class_name); ?>-onsale"><?php esc_html_e('Sale', 'evently'); ?></span>
<?php } ?>

<?php if (!$product->is_in_stock()) { ?>
	<span class="mkdf-<?php echo esc_attr($class_name); ?>-out-of-stock <?php echo esc_attr($sale_sold); ?>"><?php esc_html_e('Sold', 'evently'); ?></span>
<?php }

$product_image_size = 'shop_catalog';
if($image_size === 'full') {
	$product_image_size = 'full';
} else if ($image_size === 'square') {
	$product_image_size = 'evently_mikado_square';
} else if ($image_size === 'landscape') {
	$product_image_size = 'evently_mikado_landscape';
} else if ($image_size === 'portrait') {
	$product_image_size = 'evently_mikado_portrait';
} else if ($image_size === 'medium') {
	$product_image_size = 'medium';
} else if ($image_size === 'large') {
	$product_image_size = 'large';
} else if ($image_size === 'shop_thumbnail') {
	$product_image_size = 'shop_thumbnail';
}

if(has_post_thumbnail()) {
	the_post_thumbnail(apply_filters( 'evently_mikado_filter_product_list_image_size', $product_image_size));
}