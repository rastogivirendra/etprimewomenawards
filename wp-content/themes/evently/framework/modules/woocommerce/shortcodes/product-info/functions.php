<?php
if(!function_exists('evently_mikado_add_product_info_shortcode')) {
	function evently_mikado_add_product_info_shortcode($shortcodes_class_name) {
		$shortcodes = array(
			'MikadoCore\CPT\Shortcodes\ProductInfo\ProductInfo',
		);

		$shortcodes_class_name = array_merge($shortcodes_class_name, $shortcodes);

		return $shortcodes_class_name;
	}

	if(evently_mikado_core_plugin_installed()) {
		add_filter('mkdf_core_filter_add_vc_shortcode', 'evently_mikado_add_product_info_shortcode');
	}
}