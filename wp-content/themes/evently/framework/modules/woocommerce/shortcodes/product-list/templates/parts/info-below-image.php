<?php
$masonry_image_size = get_post_meta(get_the_ID(), 'mkdf_product_featured_image_size', true);
if(empty($masonry_image_size)) {
	$masonry_image_size = '';
}
?>
<div class="mkdf-pli <?php echo esc_html($masonry_image_size); ?>">
	<div class="mkdf-pli-inner">
		<div class="mkdf-pli-image">
			<?php evently_mikado_get_module_template_part('templates/parts/image', 'woocommerce', '', $params); ?>
		</div>
		<div class="mkdf-pli-text" <?php echo evently_mikado_get_inline_style($shader_styles); ?>>
			<div class="mkdf-pli-text-outer">
				<div class="mkdf-pli-text-inner">
					<?php evently_mikado_get_module_template_part('templates/parts/add-to-cart', 'woocommerce', '', $params); ?>
				</div>
			</div>
		</div>
		<a class="mkdf-pli-link" itemprop="url" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"></a>
	</div>
	<div class="mkdf-pli-text-wrapper" <?php echo evently_mikado_get_inline_style($text_wrapper_styles); ?>>
		<?php evently_mikado_get_module_template_part('templates/parts/title', 'woocommerce', '', $params); ?>

		<?php evently_mikado_get_module_template_part('templates/parts/category', 'woocommerce', '', $params); ?>
		
		<?php evently_mikado_get_module_template_part('templates/parts/excerpt', 'woocommerce', '', $params); ?>

		<?php evently_mikado_get_module_template_part('templates/parts/rating', 'woocommerce', '', $params); ?>

		<?php evently_mikado_get_module_template_part('templates/parts/price', 'woocommerce', '', $params); ?>
	</div>
</div>