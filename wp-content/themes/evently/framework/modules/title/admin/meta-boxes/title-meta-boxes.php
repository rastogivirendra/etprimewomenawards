<?php

if ( ! function_exists( 'evently_mikado_get_title_types_meta_boxes' ) ) {
	function evently_mikado_get_title_types_meta_boxes() {
		$title_type_options = apply_filters( 'evently_mikado_filter_title_type_meta_boxes', $title_type_options = array( '' => esc_html__( 'Default', 'evently' ) ) );
		
		return $title_type_options;
	}
}

foreach ( glob( MIKADO_FRAMEWORK_MODULES_ROOT_DIR . '/title/types/*/admin/meta-boxes/*.php' ) as $meta_box_load ) {
	include_once $meta_box_load;
}

if ( ! function_exists( 'evently_mikado_map_title_meta' ) ) {
	function evently_mikado_map_title_meta() {
		$title_type_meta_boxes = evently_mikado_get_title_types_meta_boxes();
		
		$title_meta_box = evently_mikado_add_meta_box(
			array(
				'scope' => apply_filters( 'evently_mikado_filter_set_scope_for_meta_boxes', array( 'page', 'post' ) ),
				'title' => esc_html__( 'Title', 'evently' ),
				'name'  => 'title_meta'
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_show_title_area_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Show Title Area', 'evently' ),
				'description'   => esc_html__( 'Disabling this option will turn off page title area', 'evently' ),
				'parent'        => $title_meta_box,
				'options'       => evently_mikado_get_yes_no_select_array(),
				'args'          => array(
					'dependence' => true,
					'hide'       => array(
						''    => '',
						'no'  => '#mkdf_mkdf_show_title_area_meta_container',
						'yes' => ''
					),
					'show'       => array(
						''    => '#mkdf_mkdf_show_title_area_meta_container',
						'no'  => '',
						'yes' => '#mkdf_mkdf_show_title_area_meta_container'
					)
				)
			)
		);
		
			$show_title_area_meta_container = evently_mikado_add_admin_container(
				array(
					'parent'          => $title_meta_box,
					'name'            => 'mkdf_show_title_area_meta_container',
					'hidden_property' => 'mkdf_show_title_area_meta',
					'hidden_value'    => 'no'
				)
			);
		
				evently_mikado_add_meta_box_field(
					array(
						'name'          => 'mkdf_title_area_type_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Title Area Type', 'evently' ),
						'description'   => esc_html__( 'Choose title type', 'evently' ),
						'parent'        => $show_title_area_meta_container,
						'options'       => $title_type_meta_boxes,
						'args'          => array(
							'dependence' => true,
							'hide'       => array(
								''             => '',
								'centered'     => '',
								'standard'     => '',
								'breadcrumbs'  => '#mkdf_mkdf_title_area_title_separator_meta, #mkdf_mkdf_title_area_title_separator_color_meta'
							),
							'show'       => array(
								''            => '#mkdf_mkdf_title_area_title_separator_meta, #mkdf_mkdf_title_area_title_separator_color_meta',
								'centered'    => '#mkdf_mkdf_title_area_title_separator_meta, #mkdf_mkdf_title_area_title_separator_color_meta',
								'standard'    => '#mkdf_mkdf_title_area_title_separator_meta, #mkdf_mkdf_title_area_title_separator_color_meta',
								'breadcrumbs' => ''
							)
						)
					)
				);
		
				evently_mikado_add_meta_box_field(
					array(
						'name'        => 'mkdf_title_area_height_meta',
						'type'        => 'text',
						'label'       => esc_html__( 'Height', 'evently' ),
						'description' => esc_html__( 'Set a height for Title Area', 'evently' ),
						'parent'      => $show_title_area_meta_container,
						'args'        => array(
							'col_width' => 2,
							'suffix'    => 'px'
						)
					)
				);
				
				evently_mikado_add_meta_box_field(
					array(
						'name'        => 'mkdf_title_area_background_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Background Color', 'evently' ),
						'description' => esc_html__( 'Choose a background color for title area', 'evently' ),
						'parent'      => $show_title_area_meta_container
					)
				);
		
				evently_mikado_add_meta_box_field(
					array(
						'name'        => 'mkdf_title_area_background_image_meta',
						'type'        => 'image',
						'label'       => esc_html__( 'Background Image', 'evently' ),
						'description' => esc_html__( 'Choose an Image for title area', 'evently' ),
						'parent'      => $show_title_area_meta_container
					)
				);
		
				evently_mikado_add_meta_box_field(
					array(
						'name'          => 'mkdf_title_area_background_image_behavior_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Background Image Behavior', 'evently' ),
						'description'   => esc_html__( 'Choose title area background image behavior', 'evently' ),
						'parent'        => $show_title_area_meta_container,
						'options'       => array(
							''                    => esc_html__( 'Default', 'evently' ),
							'hide'                => esc_html__( 'Hide Image', 'evently' ),
							'responsive'          => esc_html__( 'Enable Responsive Image', 'evently' ),
							'responsive-disabled' => esc_html__( 'Disable Responsive Image', 'evently' ),
							'parallax'            => esc_html__( 'Enable Parallax Image', 'evently' ),
							'parallax-zoom-out'   => esc_html__( 'Enable Parallax With Zoom Out Image', 'evently' ),
							'parallax-disabled'   => esc_html__( 'Disable Parallax Image', 'evently' )
						)
					)
				);
				
				evently_mikado_add_meta_box_field(
					array(
						'name'          => 'mkdf_title_area_vertical_alignment_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Vertical Alignment', 'evently' ),
						'description'   => esc_html__( 'Specify title area content vertical alignment', 'evently' ),
						'parent'        => $show_title_area_meta_container,
						'options'       => array(
							''              => esc_html__( 'Default', 'evently' ),
							'header_bottom' => esc_html__( 'From Bottom of Header', 'evently' ),
							'window_top'    => esc_html__( 'From Window Top', 'evently' )
						)
					)
				);
				
				evently_mikado_add_meta_box_field(
					array(
						'name'          => 'mkdf_title_area_title_tag_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Title Tag', 'evently' ),
						'parent'        => $show_title_area_meta_container,
						'options'       => evently_mikado_get_title_tag( true )
					)
				);

				evently_mikado_add_meta_box_field(
					array(
						'name'          => 'mkdf_title_area_title_size_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Use Pre-Defined Title Size', 'evently' ),
						'parent'        => $show_title_area_meta_container,
						'options'       => evently_mikado_get_yes_no_select_array( false, true )
					)
				);
				
				evently_mikado_add_meta_box_field(
					array(
						'name'        => 'mkdf_title_text_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Title Color', 'evently' ),
						'description' => esc_html__( 'Choose a color for title text', 'evently' ),
						'parent'      => $show_title_area_meta_container
					)
				);

				evently_mikado_add_meta_box_field(
					array(
						'name'          => 'mkdf_title_area_title_separator_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Enable Title Separator', 'evently' ),
						'parent'        => $show_title_area_meta_container,
						'options'       => evently_mikado_get_yes_no_select_array( false, true )
					)
				);

				evently_mikado_add_meta_box_field(
					array(
						'name'        => 'mkdf_title_area_title_separator_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Title Spearator Color', 'evently' ),
						'description' => esc_html__( 'Choose a color for title separator', 'evently' ),
						'parent'      => $show_title_area_meta_container
					)
				);

				evently_mikado_add_meta_box_field(
					array(
						'name'          => 'mkdf_title_area_tagline_meta',
						'type'          => 'text',
						'default_value' => '',
						'label'         => esc_html__( 'Tagline Text', 'evently' ),
						'description'   => esc_html__( 'Enter your tagline text', 'evently' ),
						'parent'        => $show_title_area_meta_container,
						'args'          => array(
							'col_width' => 6
						)
					)
				);

				evently_mikado_add_meta_box_field(
					array(
						'name'        => 'mkdf_tagline_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Tagline Color', 'evently' ),
						'description' => esc_html__( 'Choose a color for tagline text', 'evently' ),
						'parent'      => $show_title_area_meta_container
					)
				);
				
				evently_mikado_add_meta_box_field(
					array(
						'name'          => 'mkdf_title_area_subtitle_meta',
						'type'          => 'text',
						'default_value' => '',
						'label'         => esc_html__( 'Subtitle Text', 'evently' ),
						'description'   => esc_html__( 'Enter your subtitle text', 'evently' ),
						'parent'        => $show_title_area_meta_container,
						'args'          => array(
							'col_width' => 6
						)
					)
				);
				
				evently_mikado_add_meta_box_field(
					array(
						'name'        => 'mkdf_subtitle_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Subtitle Color', 'evently' ),
						'description' => esc_html__( 'Choose a color for subtitle text', 'evently' ),
						'parent'      => $show_title_area_meta_container
					)
				);
		
		/***************** Additional Title Area Layout - start *****************/
		
		do_action( 'evently_mikado_action_additional_title_area_meta_boxes', $show_title_area_meta_container );
		
		/***************** Additional Title Area Layout - end *****************/
		
	}
	
	add_action( 'evently_mikado_action_meta_boxes_map', 'evently_mikado_map_title_meta', 60 );
}