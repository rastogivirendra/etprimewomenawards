<?php

if ( ! function_exists( 'evently_mikado_breadcrumbs_title_type_options_meta_boxes' ) ) {
	function evently_mikado_breadcrumbs_title_type_options_meta_boxes( $show_title_area_meta_container ) {
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_breadcrumbs_color_meta',
				'type'        => 'color',
				'label'       => esc_html__( 'Breadcrumbs Color', 'evently' ),
				'description' => esc_html__( 'Choose a color for breadcrumbs text', 'evently' ),
				'parent'      => $show_title_area_meta_container
			)
		);
	}
	
	add_action( 'evently_mikado_action_additional_title_area_meta_boxes', 'evently_mikado_breadcrumbs_title_type_options_meta_boxes' );
}