<?php

if ( ! function_exists( 'evently_mikado_set_title_centered_type_for_options' ) ) {
	/**
	 * This function set centered title type value for title options map and meta boxes
	 */
	function evently_mikado_set_title_centered_type_for_options( $type ) {
		$type['centered'] = esc_html__( 'Centered', 'evently' );
		
		return $type;
	}
	
	add_filter( 'evently_mikado_filter_title_type_global_option', 'evently_mikado_set_title_centered_type_for_options' );
	add_filter( 'evently_mikado_filter_title_type_meta_boxes', 'evently_mikado_set_title_centered_type_for_options' );
}