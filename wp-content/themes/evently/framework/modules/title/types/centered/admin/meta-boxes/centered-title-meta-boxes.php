<?php

if ( ! function_exists( 'evently_mikado_centered_title_type_options_meta_boxes' ) ) {
	function evently_mikado_centered_title_type_options_meta_boxes( $show_title_area_meta_container ) {
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_subtitle_left_padding_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Subtitle Left Padding', 'evently' ),
				'description' => esc_html__( 'Set left padding for subtitle area', 'evently' ),
				'parent'      => $show_title_area_meta_container,
				'args'        => array(
					'col_width' => 2,
					'suffix'    => 'px or %'
				)
			)
		);

		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_subtitle_right_padding_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Subtitle Right Padding', 'evently' ),
				'description' => esc_html__( 'Set right padding for subtitle area', 'evently' ),
				'parent'      => $show_title_area_meta_container,
				'args'        => array(
					'col_width' => 2,
					'suffix'    => 'px or %'
				)
			)
		);
	}
	
	add_action( 'evently_mikado_action_additional_title_area_meta_boxes', 'evently_mikado_centered_title_type_options_meta_boxes', 5 );
}