<?php

if ( ! function_exists( 'evently_mikado_contact_form7_text_styles_1' ) ) {
	/**
	 * Generates custom styles for Contact Form 7 elements
	 */
	function evently_mikado_contact_form7_text_styles_1() {
		$selector = array(
			'.cf7_custom_style_1 input.wpcf7-form-control.wpcf7-text',
			'.cf7_custom_style_1 input.wpcf7-form-control.wpcf7-number',
			'.cf7_custom_style_1 input.wpcf7-form-control.wpcf7-date',
			'.cf7_custom_style_1 textarea.wpcf7-form-control.wpcf7-textarea',
			'.cf7_custom_style_1 select.wpcf7-form-control.wpcf7-select',
			'.cf7_custom_style_1 input.wpcf7-form-control.wpcf7-quiz'
		);
		
		$styles = evently_mikado_get_typography_styles( 'cf7_style_1_text' );
		
		$background_color   = evently_mikado_options()->getOptionValue( 'cf7_style_1_background_color' );
		$background_opacity = 1;
		if ( ! empty( $background_color ) ) {
			$background_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_1_background_transparency' );
			
			if ( $background_transparency !== '' ) {
				$background_opacity = $background_transparency;
			}
			
			$styles['background-color'] = evently_mikado_rgba_color( $background_color, $background_opacity );
		}
		
		$border_color   = evently_mikado_options()->getOptionValue( 'cf7_style_1_border_color' );
		$border_opacity = 1;
		if ( $border_color !== '' ) {
			$border_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_1_border_transparency' );
			
			if ( $border_transparency !== '' ) {
				$border_opacity = $border_transparency;
			}
			
			$styles['border-color'] = evently_mikado_rgba_color( $border_color, $border_opacity );
		}
		
		$border_width = evently_mikado_options()->getOptionValue( 'cf7_style_1_border_width' );
		if ( $border_width !== '' ) {
			$styles['border-width'] = evently_mikado_filter_px( $border_width ) . 'px';
		}
		
		$border_radius = evently_mikado_options()->getOptionValue( 'cf7_style_1_border_radius' );
		if ( $border_radius !== '' ) {
			$styles['border-radius'] = evently_mikado_filter_px( $border_radius ) . 'px';
		}
		
		$padding_top = evently_mikado_options()->getOptionValue( 'cf7_style_1_padding_top' );
		if ( $padding_top !== '' ) {
			$styles['padding-top'] = evently_mikado_filter_px( $padding_top ) . 'px';
		}
		
		$padding_right = evently_mikado_options()->getOptionValue( 'cf7_style_1_padding_right' );
		if ( $padding_right !== '' ) {
			$styles['padding-right'] = evently_mikado_filter_px( $padding_right ) . 'px';
		}
		
		$padding_bottom = evently_mikado_options()->getOptionValue( 'cf7_style_1_padding_bottom' );
		if ( $padding_bottom !== '' ) {
			$styles['padding-bottom'] = evently_mikado_filter_px( $padding_bottom ) . 'px';
		}
		
		$padding_left = evently_mikado_options()->getOptionValue( 'cf7_style_1_padding_left' );
		if ( $padding_left !== '' ) {
			$styles['padding-left'] = evently_mikado_filter_px( $padding_left ) . 'px';
		}
		
		$margin_top = evently_mikado_options()->getOptionValue( 'cf7_style_1_margin_top' );
		if ( $margin_top !== '' ) {
			$styles['margin-top'] = evently_mikado_filter_px( $margin_top ) . 'px';
		}
		
		$margin_bottom = evently_mikado_options()->getOptionValue( 'cf7_style_1_margin_bottom' );
		if ( $margin_bottom !== '' ) {
			$styles['margin-bottom'] = evently_mikado_filter_px( $margin_bottom ) . 'px';
		}
		
		$textarea_height = evently_mikado_options()->getOptionValue( 'cf7_style_1_textarea_height' );
		if ( ! empty( $textarea_height ) ) {
			echo evently_mikado_dynamic_css( '.cf7_custom_style_1 textarea.wpcf7-form-control.wpcf7-textarea',
				array( 'height' => evently_mikado_filter_px( $textarea_height ) . 'px' )
			);
		}
		
		echo evently_mikado_dynamic_css( $selector, $styles );
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_contact_form7_text_styles_1' );
}

if ( ! function_exists( 'evently_mikado_contact_form7_border_styles_1' ) ) {
	/**
	 * Generates custom styles for Contact Form 7 elements
	 */
	function evently_mikado_contact_form7_border_styles_1() {
		$input_styles = array();
		$input_hover_styles = array();
		$text_styles = array();
		$text_hover_styles = array();
		$first_main = evently_mikado_options()->getOptionValue('first_color' );
		
		$input_selector = array(
			'.cf7_custom_style_1 input.wpcf7-form-control.wpcf7-text',
			'.cf7_custom_style_1 input.wpcf7-form-control.wpcf7-number',
			'.cf7_custom_style_1 input.wpcf7-form-control.wpcf7-date',
			'.cf7_custom_style_1 select.wpcf7-form-control.wpcf7-select',
			'.cf7_custom_style_1 input.wpcf7-form-control.wpcf7-quiz'
		);

		$input_focus = array(
			'.cf7_custom_style_1 input.wpcf7-form-control.wpcf7-text:focus',
			'.cf7_custom_style_1 input.wpcf7-form-control.wpcf7-number:focus',
			'.cf7_custom_style_1 input.wpcf7-form-control.wpcf7-date:focus',
			'.cf7_custom_style_1 select.wpcf7-form-control.wpcf7-select:focus',
			'.cf7_custom_style_1 input.wpcf7-form-control.wpcf7-quiz:focus'
		);

		$border_style = evently_mikado_options()->getOptionValue( 'cf7_style_1_input_border_style' );
		if ( $border_style === 'bottom-border' ) {
			$input_styles['border-width']  = '0 0 1px 0';
			$input_hover_styles['background-color'] = 'transparent';
			$input_hover_styles['border-color'] = '' !== $first_main ? $first_main : '#ee0034';
		}

		$text_selector = array(
			'.cf7_custom_style_1 textarea.wpcf7-form-control.wpcf7-textarea'
		);

		$text_focus = array(
			'.cf7_custom_style_1 textarea.wpcf7-form-control.wpcf7-textarea:focus'
		);

		$border_style = evently_mikado_options()->getOptionValue( 'cf7_style_1_textarea_border_style' );
		if ( $border_style === 'bottom-border' ) {
			$text_styles['border-width']  = '0 0 1px 0';
			$text_hover_styles['background-color'] = 'transparent';
			$text_hover_styles['border-color'] = '' !== $first_main ? $first_main : '#ee0034';
		} else {
			$text_hover_styles['border-color'] = '' !== $first_main ? $first_main : '#ee0034';
		}

		echo evently_mikado_dynamic_css( $input_selector, $input_styles );
		echo evently_mikado_dynamic_css( $text_selector, $text_styles );
		echo evently_mikado_dynamic_css( $input_focus, $input_hover_styles );
		echo evently_mikado_dynamic_css( $text_focus, $text_hover_styles );
	}

	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_contact_form7_border_styles_1' );
}

if ( ! function_exists( 'evently_mikado_contact_form7_focus_styles_1' ) ) {
	/**
	 * Generates custom styles for Contact Form 7 elements focus
	 */
	function evently_mikado_contact_form7_focus_styles_1() {
		$selector = array(
			'.cf7_custom_style_1 input.wpcf7-form-control.wpcf7-text:focus',
			'.cf7_custom_style_1 input.wpcf7-form-control.wpcf7-number:focus',
			'.cf7_custom_style_1 input.wpcf7-form-control.wpcf7-date:focus',
			'.cf7_custom_style_1 textarea.wpcf7-form-control.wpcf7-textarea:focus',
			'.cf7_custom_style_1 select.wpcf7-form-control.wpcf7-select:focus',
			'.cf7_custom_style_1 input.wpcf7-form-control.wpcf7-quiz:focus'
		);
		$styles   = array();
		
		$color = evently_mikado_options()->getOptionValue( 'cf7_style_1_focus_text_color' );
		if ( ! empty( $color ) ) {
			$styles['color'] = $color;
		}
		
		$background_color   = evently_mikado_options()->getOptionValue( 'cf7_style_1_focus_background_color' );
		$background_opacity = 1;
		if ( ! empty( $background_color ) ) {
			$background_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_1_focus_background_transparency' );
			
			if ( $background_transparency !== '' ) {
				$background_opacity = $background_transparency;
			}
			
			$styles['background-color'] = evently_mikado_rgba_color( $background_color, $background_opacity );
		}
		
		$border_color   = evently_mikado_options()->getOptionValue( 'cf7_style_1_focus_border_color' );
		$border_opacity = 1;
		if ( ! empty( $border_color ) ) {
			$border_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_1_focus_border_transparency' );
			
			if ( $border_transparency !== '' ) {
				$border_opacity = $border_transparency;
			}
			
			$styles['border-color'] = evently_mikado_rgba_color( $border_color, $border_opacity );
		}
		
		echo evently_mikado_dynamic_css( $selector, $styles );
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_contact_form7_focus_styles_1' );
}

if ( ! function_exists( 'evently_mikado_contact_form7_label_styles_1' ) ) {
	/**
	 * Generates custom styles for Contact Form 7 label
	 */
	function evently_mikado_contact_form7_label_styles_1() {
		$item_styles = evently_mikado_get_typography_styles( 'cf7_style_1_label' );
		
		$item_selector = array(
			'.cf7_custom_style_1 p'
		);
		
		echo evently_mikado_dynamic_css( $item_selector, $item_styles );
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_contact_form7_label_styles_1' );
}

if ( ! function_exists( 'evently_mikado_contact_form7_button_styles_1' ) ) {
	/**
	 * Generates custom styles for Contact Form 7 button
	 */
	function evently_mikado_contact_form7_button_styles_1() {
		$selector = array(
			'.cf7_custom_style_1 input.wpcf7-form-control.wpcf7-submit'
		);
		
		$styles = evently_mikado_get_typography_styles( 'cf7_style_1_button' );
		
		$height = evently_mikado_options()->getOptionValue( 'cf7_style_1_button_height' );
		if ( $height !== '' ) {
			$styles['padding-top']    = '0';
			$styles['padding-bottom'] = '0';
			$styles['height']         = evently_mikado_filter_px( $height ) . 'px';
			$styles['line-height']    = (int) evently_mikado_filter_px( $height ) - 3 . 'px';
		}
		
		$background_color   = evently_mikado_options()->getOptionValue( 'cf7_style_1_button_background_color' );
		$background_opacity = 1;
		if ( ! empty( $background_color ) ) {
			$background_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_1_button_background_transparency' );
			
			if ( $background_transparency !== '' ) {
				$background_opacity = $background_transparency;
			}
			
			$styles['background-color'] = evently_mikado_rgba_color( $background_color, $background_opacity );
		}
		
		$border_color   = evently_mikado_options()->getOptionValue( 'cf7_style_1_button_border_color' );
		$border_opacity = 1;
		if ( ! empty( $border_color ) ) {
			$border_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_1_button_border_transparency' );
			
			if ( $border_transparency !== '' ) {
				$border_opacity = $border_transparency;
			}
			
			$styles['border-color'] = evently_mikado_rgba_color( $border_color, $border_opacity );
		}
		
		$border_width = evently_mikado_options()->getOptionValue( 'cf7_style_1_button_border_width' );
		if ( $border_width !== '' ) {
			$styles['border-width'] = evently_mikado_filter_px( $border_width ) . 'px';
		}
		
		$border_radius = evently_mikado_options()->getOptionValue( 'cf7_style_1_button_border_radius' );
		if ( $border_radius !== '' ) {
			$styles['border-radius'] = evently_mikado_filter_px( $border_radius ) . 'px';
		}
		
		$padding_left_right = evently_mikado_options()->getOptionValue( 'cf7_style_1_button_padding' );
		if ( $padding_left_right !== '' ) {
			$styles['padding-left']  = evently_mikado_filter_px( $padding_left_right ) . 'px';
			$styles['padding-right'] = evently_mikado_filter_px( $padding_left_right ) . 'px';
		}

		$margin_top = evently_mikado_options()->getOptionValue( 'cf7_style_2_button_margin' );
		if ( $margin_top !== '' ) {
			$styles['margin-top']  = evently_mikado_filter_px( $margin_top ) . 'px';
		}

		echo evently_mikado_dynamic_css( $selector, $styles );
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_contact_form7_button_styles_1' );
}

if ( ! function_exists( 'evently_mikado_contact_form7_button_hover_styles_1' ) ) {
	/**
	 * Generates custom styles for Contact Form 7 button
	 */
	function evently_mikado_contact_form7_button_hover_styles_1() {
		$selector = array(
			'.cf7_custom_style_1 input.wpcf7-form-control.wpcf7-submit:not([disabled]):hover'
		);
		$styles   = array();
		
		$color = evently_mikado_options()->getOptionValue( 'cf7_style_1_button_hover_color' );
		if ( ! empty( $color ) ) {
			$styles['color'] = $color;
		}
		
		$background_color   = evently_mikado_options()->getOptionValue( 'cf7_style_1_button_hover_bckg_color' );
		$background_opacity = 1;
		if ( ! empty( $background_color ) ) {
			$background_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_1_button_hover_bckg_transparency' );
			
			if ( $background_transparency !== '' ) {
				$background_opacity = $background_transparency;
			}
			
			$styles['background-color'] = evently_mikado_rgba_color( $background_color, $background_opacity );
		}
		
		$border_color   = evently_mikado_options()->getOptionValue( 'cf7_style_1_button_hover_border_color' );
		$border_opacity = 1;
		if ( ! empty( $border_color ) ) {
			$border_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_1_button_border_transparency' );
			
			if ( $border_transparency !== '' ) {
				$border_opacity = $border_transparency;
			}
			
			$styles['border-color'] = evently_mikado_rgba_color( $border_color, $border_opacity );
		}
		
		echo evently_mikado_dynamic_css( $selector, $styles );
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_contact_form7_button_hover_styles_1' );
}

if ( ! function_exists( 'evently_mikado_contact_form7_text_styles_2' ) ) {
	/**
	 * Generates custom styles for Contact Form 7 elements
	 */
	function evently_mikado_contact_form7_text_styles_2() {
		$selector = array(
			'.cf7_custom_style_2 input.wpcf7-form-control.wpcf7-text',
			'.cf7_custom_style_2 input.wpcf7-form-control.wpcf7-number',
			'.cf7_custom_style_2 input.wpcf7-form-control.wpcf7-date',
			'.cf7_custom_style_2 textarea.wpcf7-form-control.wpcf7-textarea',
			'.cf7_custom_style_2 select.wpcf7-form-control.wpcf7-select',
			'.cf7_custom_style_2 input.wpcf7-form-control.wpcf7-quiz'
		);
		
		$styles = evently_mikado_get_typography_styles( 'cf7_style_2_text' );
		
		$background_color   = evently_mikado_options()->getOptionValue( 'cf7_style_2_background_color' );
		$background_opacity = 1;
		if ( ! empty( $background_color ) ) {
			$background_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_2_background_transparency' );
			
			if ( $background_transparency !== '' ) {
				$background_opacity = $background_transparency;
			}
			
			$styles['background-color'] = evently_mikado_rgba_color( $background_color, $background_opacity );
		}
		
		$border_color   = evently_mikado_options()->getOptionValue( 'cf7_style_2_border_color' );
		$border_opacity = 1;
		if ( $border_color !== '' ) {
			$border_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_2_border_transparency' );
			
			if ( $border_transparency !== '' ) {
				$border_opacity = $border_transparency;
			}
			
			$styles['border-color'] = evently_mikado_rgba_color( $border_color, $border_opacity );
		}
		
		$border_width = evently_mikado_options()->getOptionValue( 'cf7_style_2_border_width' );
		if ( $border_width !== '' ) {
			$styles['border-width'] = evently_mikado_filter_px( $border_width ) . 'px';
		}
		
		$border_radius = evently_mikado_options()->getOptionValue( 'cf7_style_2_border_radius' );
		if ( $border_radius !== '' ) {
			$styles['border-radius'] = evently_mikado_filter_px( $border_radius ) . 'px';
		}
		
		$padding_top = evently_mikado_options()->getOptionValue( 'cf7_style_2_padding_top' );
		if ( $padding_top !== '' ) {
			$styles['padding-top'] = evently_mikado_filter_px( $padding_top ) . 'px';
		}
		
		$padding_right = evently_mikado_options()->getOptionValue( 'cf7_style_2_padding_right' );
		if ( $padding_right !== '' ) {
			$styles['padding-right'] = evently_mikado_filter_px( $padding_right ) . 'px';
		}
		
		$padding_bottom = evently_mikado_options()->getOptionValue( 'cf7_style_2_padding_bottom' );
		if ( $padding_bottom !== '' ) {
			$styles['padding-bottom'] = evently_mikado_filter_px( $padding_bottom ) . 'px';
		}
		
		$padding_left = evently_mikado_options()->getOptionValue( 'cf7_style_2_padding_left' );
		if ( $padding_left !== '' ) {
			$styles['padding-left'] = evently_mikado_filter_px( $padding_left ) . 'px';
		}
		
		$margin_top = evently_mikado_options()->getOptionValue( 'cf7_style_2_margin_top' );
		if ( $margin_top !== '' ) {
			$styles['margin-top'] = evently_mikado_filter_px( $margin_top ) . 'px';
		}
		
		$margin_bottom = evently_mikado_options()->getOptionValue( 'cf7_style_2_margin_bottom' );
		if ( $margin_bottom !== '' ) {
			$styles['margin-bottom'] = evently_mikado_filter_px( $margin_bottom ) . 'px';
		}
		
		$textarea_height = evently_mikado_options()->getOptionValue( 'cf7_style_2_textarea_height' );
		if ( ! empty( $textarea_height ) ) {
			echo evently_mikado_dynamic_css( '.cf7_custom_style_2 textarea.wpcf7-form-control.wpcf7-textarea',
				array( 'height' => evently_mikado_filter_px( $textarea_height ) . 'px' )
			);
		}
		
		echo evently_mikado_dynamic_css( $selector, $styles );
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_contact_form7_text_styles_2' );
}

if ( ! function_exists( 'evently_mikado_contact_form7_border_styles_2' ) ) {
	/**
	 * Generates custom styles for Contact Form 7 elements
	 */
	function evently_mikado_contact_form7_border_styles_2() {
		$input_styles = array();
		$input_hover_styles = array();
		$input_styles = array();
		$text_styles = array();
		$text_hover_styles = array();
		$first_main = evently_mikado_options()->getOptionValue('first_color' );

		$input_selector = array(
			'.cf7_custom_style_2 input.wpcf7-form-control.wpcf7-text',
			'.cf7_custom_style_2 input.wpcf7-form-control.wpcf7-number',
			'.cf7_custom_style_2 input.wpcf7-form-control.wpcf7-date',
			'.cf7_custom_style_2 select.wpcf7-form-control.wpcf7-select',
			'.cf7_custom_style_2 input.wpcf7-form-control.wpcf7-quiz'
		);

		$input_focus = array(
			'.cf7_custom_style_2 input.wpcf7-form-control.wpcf7-text:focus',
			'.cf7_custom_style_2 input.wpcf7-form-control.wpcf7-number:focus',
			'.cf7_custom_style_2 input.wpcf7-form-control.wpcf7-date:focus',
			'.cf7_custom_style_2 select.wpcf7-form-control.wpcf7-select:focus',
			'.cf7_custom_style_2 input.wpcf7-form-control.wpcf7-quiz:focus'
		);

		$border_style = evently_mikado_options()->getOptionValue( 'cf7_style_2_input_border_style' );
		if ( $border_style === 'bottom-border' ) {
			$input_styles['border-width']  = '0 0 1px 0';
			$input_hover_styles['background-color'] = 'transparent';
			$input_hover_styles['border-color'] = '' !== $first_main ? $first_main : '#ee0034';
		}

		$text_selector = array(
			'.cf7_custom_style_2 textarea.wpcf7-form-control.wpcf7-textarea'
		);

		$text_focus = array(
			'.cf7_custom_style_2 textarea.wpcf7-form-control.wpcf7-textarea:focus'
		);

		$border_style = evently_mikado_options()->getOptionValue( 'cf7_style_2_textarea_border_style' );
		if ( $border_style === 'bottom-border' ) {
			$text_styles['border-width']  = '0 0 1px 0';
			$text_hover_styles['background-color'] = 'transparent';
			$text_hover_styles['border-color'] = '' !== $first_main ? $first_main : '#ee0034';
		} else {
			$text_hover_styles['border-color'] = '' !== $first_main ? $first_main : '#ee0034';
		}

		echo evently_mikado_dynamic_css( $input_selector, $input_styles );
		echo evently_mikado_dynamic_css( $text_selector, $text_styles );
		echo evently_mikado_dynamic_css( $input_focus, $input_hover_styles );
		echo evently_mikado_dynamic_css( $text_focus, $text_hover_styles );
	}

	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_contact_form7_border_styles_2' );
}

if ( ! function_exists( 'evently_mikado_contact_form7_focus_styles_2' ) ) {
	/**
	 * Generates custom styles for Contact Form 7 elements focus
	 */
	function evently_mikado_contact_form7_focus_styles_2() {
		$selector = array(
			'.cf7_custom_style_2 input.wpcf7-form-control.wpcf7-text:focus',
			'.cf7_custom_style_2 input.wpcf7-form-control.wpcf7-number:focus',
			'.cf7_custom_style_2 input.wpcf7-form-control.wpcf7-date:focus',
			'.cf7_custom_style_2 textarea.wpcf7-form-control.wpcf7-textarea:focus',
			'.cf7_custom_style_2 select.wpcf7-form-control.wpcf7-select:focus',
			'.cf7_custom_style_2 input.wpcf7-form-control.wpcf7-quiz:focus'
		);
		$styles   = array();
		
		$color = evently_mikado_options()->getOptionValue( 'cf7_style_2_focus_text_color' );
		if ( ! empty( $color ) ) {
			$styles['color'] = $color;
		}
		
		$background_color   = evently_mikado_options()->getOptionValue( 'cf7_style_2_focus_background_color' );
		$background_opacity = 1;
		if ( ! empty( $background_color ) ) {
			$background_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_2_focus_background_transparency' );
			
			if ( $background_transparency !== '' ) {
				$background_opacity = $background_transparency;
			}
			
			$styles['background-color'] = evently_mikado_rgba_color( $background_color, $background_opacity );
		}
		
		$border_color   = evently_mikado_options()->getOptionValue( 'cf7_style_2_focus_border_color' );
		$border_opacity = 1;
		if ( ! empty( $border_color ) ) {
			$border_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_2_focus_border_transparency' );
			
			if ( $border_transparency !== '' ) {
				$border_opacity = $border_transparency;
			}
			
			$styles['border-color'] = evently_mikado_rgba_color( $border_color, $border_opacity );
		}
		
		echo evently_mikado_dynamic_css( $selector, $styles );
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_contact_form7_focus_styles_2' );
}

if ( ! function_exists( 'evently_mikado_contact_form7_label_styles_2' ) ) {
	/**
	 * Generates custom styles for Contact Form 7 label
	 */
	function evently_mikado_contact_form7_label_styles_2() {
		$item_styles = evently_mikado_get_typography_styles( 'cf7_style_2_label' );
		
		$item_selector = array(
			'.cf7_custom_style_2 p'
		);
		
		echo evently_mikado_dynamic_css( $item_selector, $item_styles );
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_contact_form7_label_styles_2' );
}

if ( ! function_exists( 'evently_mikado_contact_form7_button_styles_2' ) ) {
	/**
	 * Generates custom styles for Contact Form 7 button
	 */
	function evently_mikado_contact_form7_button_styles_2() {
		$selector = array(
			'.cf7_custom_style_2 input.wpcf7-form-control.wpcf7-submit'
		);
		
		$styles = evently_mikado_get_typography_styles( 'cf7_style_2_button' );
		
		$height = evently_mikado_options()->getOptionValue( 'cf7_style_2_button_height' );
		if ( $height !== '' ) {
			$styles['padding-top']    = '0';
			$styles['padding-bottom'] = '0';
			$styles['height']         = evently_mikado_filter_px( $height ) . 'px';
			$styles['line-height']    = (int) evently_mikado_filter_px( $height ) - 3 . 'px';
		}
		
		$background_color   = evently_mikado_options()->getOptionValue( 'cf7_style_2_button_background_color' );
		$background_opacity = 1;
		if ( ! empty( $background_color ) ) {
			$background_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_2_button_background_transparency' );
			
			if ( $background_transparency !== '' ) {
				$background_opacity = $background_transparency;
			}
			
			$styles['background-color'] = evently_mikado_rgba_color( $background_color, $background_opacity );
		}
		
		$border_color   = evently_mikado_options()->getOptionValue( 'cf7_style_2_button_border_color' );
		$border_opacity = 1;
		if ( ! empty( $border_color ) ) {
			$border_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_2_button_border_transparency' );
			
			if ( $border_transparency !== '' ) {
				$border_opacity = $border_transparency;
			}
			
			$styles['border-color'] = evently_mikado_rgba_color( $border_color, $border_opacity );
		}
		
		$border_width = evently_mikado_options()->getOptionValue( 'cf7_style_2_button_border_width' );
		if ( $border_width !== '' ) {
			$styles['border-width'] = evently_mikado_filter_px( $border_width ) . 'px';
		}
		
		$border_radius = evently_mikado_options()->getOptionValue( 'cf7_style_2_button_border_radius' );
		if ( $border_radius !== '' ) {
			$styles['border-radius'] = evently_mikado_filter_px( $border_radius ) . 'px';
		}
		
		$padding_left_right = evently_mikado_options()->getOptionValue( 'cf7_style_2_button_padding' );
		if ( $padding_left_right !== '' ) {
			$styles['padding-left']  = evently_mikado_filter_px( $padding_left_right ) . 'px';
			$styles['padding-right'] = evently_mikado_filter_px( $padding_left_right ) . 'px';
		}

		$margin_top = evently_mikado_options()->getOptionValue( 'cf7_style_2_button_margin' );
		if ( $margin_top !== '' ) {
			$styles['margin-top']  = evently_mikado_filter_px( $margin_top ) . 'px';
		}

		echo evently_mikado_dynamic_css( $selector, $styles );
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_contact_form7_button_styles_2' );
}

if ( ! function_exists( 'evently_mikado_contact_form7_button_hover_styles_2' ) ) {
	/**
	 * Generates custom styles for Contact Form 7 button
	 */
	function evently_mikado_contact_form7_button_hover_styles_2() {
		$selector = array(
			'.cf7_custom_style_2 input.wpcf7-form-control.wpcf7-submit:not([disabled]):hover'
		);
		$styles   = array();
		
		$color = evently_mikado_options()->getOptionValue( 'cf7_style_2_button_hover_color' );
		if ( ! empty( $color ) ) {
			$styles['color'] = $color;
		}
		
		$background_color   = evently_mikado_options()->getOptionValue( 'cf7_style_2_button_hover_bckg_color' );
		$background_opacity = 1;
		if ( ! empty( $background_color ) ) {
			$background_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_2_button_hover_bckg_transparency' );
			
			if ( $background_transparency !== '' ) {
				$background_opacity = $background_transparency;
			}
			
			$styles['background-color'] = evently_mikado_rgba_color( $background_color, $background_opacity );
		}
		
		$border_color   = evently_mikado_options()->getOptionValue( 'cf7_style_2_button_hover_border_color' );
		$border_opacity = 1;
		if ( ! empty( $border_color ) ) {
			$border_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_2_button_border_transparency' );
			
			if ( $border_transparency !== '' ) {
				$border_opacity = $border_transparency;
			}
			
			$styles['border-color'] = evently_mikado_rgba_color( $border_color, $border_opacity );
		}
		
		echo evently_mikado_dynamic_css( $selector, $styles );
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_contact_form7_button_hover_styles_2' );
}

if ( ! function_exists( 'evently_mikado_contact_form7_text_styles_3' ) ) {
	/**
	 * Generates custom styles for Contact Form 7 elements
	 */
	function evently_mikado_contact_form7_text_styles_3() {
		$selector = array(
			'.cf7_custom_style_3 input.wpcf7-form-control.wpcf7-text',
			'.cf7_custom_style_3 input.wpcf7-form-control.wpcf7-number',
			'.cf7_custom_style_3 input.wpcf7-form-control.wpcf7-date',
			'.cf7_custom_style_3 textarea.wpcf7-form-control.wpcf7-textarea',
			'.cf7_custom_style_3 select.wpcf7-form-control.wpcf7-select',
			'.cf7_custom_style_3 input.wpcf7-form-control.wpcf7-quiz'
		);
		
		$styles = evently_mikado_get_typography_styles( 'cf7_style_3_text' );
		
		$background_color   = evently_mikado_options()->getOptionValue( 'cf7_style_3_background_color' );
		$background_opacity = 1;
		if ( ! empty( $background_color ) ) {
			$background_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_3_background_transparency' );
			
			if ( $background_transparency !== '' ) {
				$background_opacity = $background_transparency;
			}
			
			$styles['background-color'] = evently_mikado_rgba_color( $background_color, $background_opacity );
		}
		
		$border_color   = evently_mikado_options()->getOptionValue( 'cf7_style_3_border_color' );
		$border_opacity = 1;
		if ( $border_color !== '' ) {
			$border_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_3_border_transparency' );
			
			if ( $border_transparency !== '' ) {
				$border_opacity = $border_transparency;
			}
			
			$styles['border-color'] = evently_mikado_rgba_color( $border_color, $border_opacity );
		}
		
		$border_width = evently_mikado_options()->getOptionValue( 'cf7_style_3_border_width' );
		if ( $border_width !== '' ) {
			$styles['border-width'] = evently_mikado_filter_px( $border_width ) . 'px';
		}
		
		$border_radius = evently_mikado_options()->getOptionValue( 'cf7_style_3_border_radius' );
		if ( $border_radius !== '' ) {
			$styles['border-radius'] = evently_mikado_filter_px( $border_radius ) . 'px';
		}
		
		$padding_top = evently_mikado_options()->getOptionValue( 'cf7_style_3_padding_top' );
		if ( $padding_top !== '' ) {
			$styles['padding-top'] = evently_mikado_filter_px( $padding_top ) . 'px';
		}
		
		$padding_right = evently_mikado_options()->getOptionValue( 'cf7_style_3_padding_right' );
		if ( $padding_right !== '' ) {
			$styles['padding-right'] = evently_mikado_filter_px( $padding_right ) . 'px';
		}
		
		$padding_bottom = evently_mikado_options()->getOptionValue( 'cf7_style_3_padding_bottom' );
		if ( $padding_bottom !== '' ) {
			$styles['padding-bottom'] = evently_mikado_filter_px( $padding_bottom ) . 'px';
		}
		
		$padding_left = evently_mikado_options()->getOptionValue( 'cf7_style_3_padding_left' );
		if ( $padding_left !== '' ) {
			$styles['padding-left'] = evently_mikado_filter_px( $padding_left ) . 'px';
		}
		
		$margin_top = evently_mikado_options()->getOptionValue( 'cf7_style_3_margin_top' );
		if ( $margin_top !== '' ) {
			$styles['margin-top'] = evently_mikado_filter_px( $margin_top ) . 'px';
		}
		
		$margin_bottom = evently_mikado_options()->getOptionValue( 'cf7_style_3_margin_bottom' );
		if ( $margin_bottom !== '' ) {
			$styles['margin-bottom'] = evently_mikado_filter_px( $margin_bottom ) . 'px';
		}
		
		$textarea_height = evently_mikado_options()->getOptionValue( 'cf7_style_3_textarea_height' );
		if ( ! empty( $textarea_height ) ) {
			echo evently_mikado_dynamic_css( '.cf7_custom_style_3 textarea.wpcf7-form-control.wpcf7-textarea',
				array( 'height' => evently_mikado_filter_px( $textarea_height ) . 'px' )
			);
		}
		
		echo evently_mikado_dynamic_css( $selector, $styles );
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_contact_form7_text_styles_3' );
}

if ( ! function_exists( 'evently_mikado_contact_form7_border_styles_3' ) ) {
	/**
	 * Generates custom styles for Contact Form 7 elements
	 */
	function evently_mikado_contact_form7_border_styles_3() {
		$input_styles = array();
		$input_hover_styles = array();
		$text_styles = array();
		$text_hover_styles = array();
		$first_main = evently_mikado_options()->getOptionValue('first_color' );

		$input_selector = array(
			'.cf7_custom_style_3 input.wpcf7-form-control.wpcf7-text',
			'.cf7_custom_style_3 input.wpcf7-form-control.wpcf7-number',
			'.cf7_custom_style_3 input.wpcf7-form-control.wpcf7-date',
			'.cf7_custom_style_3 select.wpcf7-form-control.wpcf7-select',
			'.cf7_custom_style_3 input.wpcf7-form-control.wpcf7-quiz'
		);

		$input_focus = array(
			'.cf7_custom_style_3 input.wpcf7-form-control.wpcf7-text:focus',
			'.cf7_custom_style_3 input.wpcf7-form-control.wpcf7-number:focus',
			'.cf7_custom_style_3 input.wpcf7-form-control.wpcf7-date:focus',
			'.cf7_custom_style_3 select.wpcf7-form-control.wpcf7-select:focus',
			'.cf7_custom_style_3 input.wpcf7-form-control.wpcf7-quiz:focus'
		);

		$border_style = evently_mikado_options()->getOptionValue( 'cf7_style_3_input_border_style' );
		if ( $border_style === 'bottom-border' ) {
			$input_styles['border-width']  = '0 0 1px 0';
			$input_hover_styles['background-color'] = 'transparent';
			$input_hover_styles['border-color'] = '' !== $first_main ? $first_main : '#ee0034';
		}

		$text_selector = array(
			'.cf7_custom_style_3 textarea.wpcf7-form-control.wpcf7-textarea'
		);

		$text_focus = array(
			'.cf7_custom_style_3 textarea.wpcf7-form-control.wpcf7-textarea:focus'
		);

		$border_style = evently_mikado_options()->getOptionValue( 'cf7_style_3_textarea_border_style' );
		if ( $border_style === 'bottom-border' ) {
			$text_styles['border-width']  = '0 0 1px 0';
			$text_hover_styles['background-color'] = 'transparent';
			$text_hover_styles['border-color'] = '' !== $first_main ? $first_main : '#ee0034';
		} else {
			$text_hover_styles['border-color'] = '' !== $first_main ? $first_main : '#ee0034';
		}

		echo evently_mikado_dynamic_css( $input_selector, $input_styles );
		echo evently_mikado_dynamic_css( $text_selector, $text_styles );
		echo evently_mikado_dynamic_css( $input_focus, $input_hover_styles );
		echo evently_mikado_dynamic_css( $text_focus, $text_hover_styles );
	}

	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_contact_form7_border_styles_3' );
}

if ( ! function_exists( 'evently_mikado_contact_form7_focus_styles_3' ) ) {
	/**
	 * Generates custom styles for Contact Form 7 elements focus
	 */
	function evently_mikado_contact_form7_focus_styles_3() {
		$selector = array(
			'.cf7_custom_style_3 input.wpcf7-form-control.wpcf7-text:focus',
			'.cf7_custom_style_3 input.wpcf7-form-control.wpcf7-number:focus',
			'.cf7_custom_style_3 input.wpcf7-form-control.wpcf7-date:focus',
			'.cf7_custom_style_3 textarea.wpcf7-form-control.wpcf7-textarea:focus',
			'.cf7_custom_style_3 select.wpcf7-form-control.wpcf7-select:focus',
			'.cf7_custom_style_3 input.wpcf7-form-control.wpcf7-quiz:focus'
		);
		$styles   = array();
		
		$color = evently_mikado_options()->getOptionValue( 'cf7_style_3_focus_text_color' );
		if ( ! empty( $color ) ) {
			$styles['color'] = $color;
		}
		
		$background_color   = evently_mikado_options()->getOptionValue( 'cf7_style_3_focus_background_color' );
		$background_opacity = 1;
		if ( ! empty( $background_color ) ) {
			$background_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_3_focus_background_transparency' );
			
			if ( $background_transparency !== '' ) {
				$background_opacity = $background_transparency;
			}
			
			$styles['background-color'] = evently_mikado_rgba_color( $background_color, $background_opacity );
		}
		
		$border_color   = evently_mikado_options()->getOptionValue( 'cf7_style_3_focus_border_color' );
		$border_opacity = 1;
		if ( ! empty( $border_color ) ) {
			$border_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_3_focus_border_transparency' );
			
			if ( $border_transparency !== '' ) {
				$border_opacity = $border_transparency;
			}
			
			$styles['border-color'] = evently_mikado_rgba_color( $border_color, $border_opacity );
		}
		
		echo evently_mikado_dynamic_css( $selector, $styles );
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_contact_form7_focus_styles_3' );
}

if ( ! function_exists( 'evently_mikado_contact_form7_label_styles_3' ) ) {
	/**
	 * Generates custom styles for Contact Form 7 label
	 */
	function evently_mikado_contact_form7_label_styles_3() {
		$item_styles = evently_mikado_get_typography_styles( 'cf7_style_3_label' );
		
		$item_selector = array(
			'.cf7_custom_style_3 p'
		);
		
		echo evently_mikado_dynamic_css( $item_selector, $item_styles );
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_contact_form7_label_styles_3' );
}

if ( ! function_exists( 'evently_mikado_contact_form7_button_styles_3' ) ) {
	/**
	 * Generates custom styles for Contact Form 7 button
	 */
	function evently_mikado_contact_form7_button_styles_3() {
		$selector = array(
			'.cf7_custom_style_3 input.wpcf7-form-control.wpcf7-submit'
		);
		
		$styles = evently_mikado_get_typography_styles( 'cf7_style_3_button' );
		
		$height = evently_mikado_options()->getOptionValue( 'cf7_style_3_button_height' );
		if ( $height !== '' ) {
			$styles['padding-top']    = '0';
			$styles['padding-bottom'] = '0';
			$styles['height']         = evently_mikado_filter_px( $height ) . 'px';
			$styles['line-height']    = (int) evently_mikado_filter_px( $height ) - 3 . 'px';
		}
		
		$background_color   = evently_mikado_options()->getOptionValue( 'cf7_style_3_button_background_color' );
		$background_opacity = 1;
		if ( ! empty( $background_color ) ) {
			$background_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_3_button_background_transparency' );
			
			if ( $background_transparency !== '' ) {
				$background_opacity = $background_transparency;
			}
			
			$styles['background-color'] = evently_mikado_rgba_color( $background_color, $background_opacity );
		}
		
		$border_color   = evently_mikado_options()->getOptionValue( 'cf7_style_3_button_border_color' );
		$border_opacity = 1;
		if ( ! empty( $border_color ) ) {
			$border_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_3_button_border_transparency' );
			
			if ( $border_transparency !== '' ) {
				$border_opacity = $border_transparency;
			}
			
			$styles['border-color'] = evently_mikado_rgba_color( $border_color, $border_opacity );
		}
		
		$border_width = evently_mikado_options()->getOptionValue( 'cf7_style_3_button_border_width' );
		if ( $border_width !== '' ) {
			$styles['border-width'] = evently_mikado_filter_px( $border_width ) . 'px';
		}
		
		$border_radius = evently_mikado_options()->getOptionValue( 'cf7_style_3_button_border_radius' );
		if ( $border_radius !== '' ) {
			$styles['border-radius'] = evently_mikado_filter_px( $border_radius ) . 'px';
		}
		
		$padding_left_right = evently_mikado_options()->getOptionValue( 'cf7_style_3_button_padding' );
		if ( $padding_left_right !== '' ) {
			$styles['padding-left']  = evently_mikado_filter_px( $padding_left_right ) . 'px';
			$styles['padding-right'] = evently_mikado_filter_px( $padding_left_right ) . 'px';
		}

		$margin_top = evently_mikado_options()->getOptionValue( 'cf7_style_3_button_margin' );
		if ( $margin_top !== '' ) {
			$styles['margin-top']  = evently_mikado_filter_px( $margin_top ) . 'px';
		}
		
		echo evently_mikado_dynamic_css( $selector, $styles );
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_contact_form7_button_styles_3' );
}

if ( ! function_exists( 'evently_mikado_contact_form7_button_hover_styles_3' ) ) {
	/**
	 * Generates custom styles for Contact Form 7 button
	 */
	function evently_mikado_contact_form7_button_hover_styles_3() {
		$selector = array(
			'.cf7_custom_style_3 input.wpcf7-form-control.wpcf7-submit:not([disabled]):hover'
		);
		$styles   = array();
		
		$color = evently_mikado_options()->getOptionValue( 'cf7_style_3_button_hover_color' );
		if ( ! empty( $color ) ) {
			$styles['color'] = $color;
		}
		
		$background_color   = evently_mikado_options()->getOptionValue( 'cf7_style_3_button_hover_bckg_color' );
		$background_opacity = 1;
		if ( ! empty( $background_color ) ) {
			$background_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_3_button_hover_bckg_transparency' );
			
			if ( $background_transparency !== '' ) {
				$background_opacity = $background_transparency;
			}
			
			$styles['background-color'] = evently_mikado_rgba_color( $background_color, $background_opacity );
		}
		
		$border_color   = evently_mikado_options()->getOptionValue( 'cf7_style_3_button_hover_border_color' );
		$border_opacity = 1;
		if ( ! empty( $border_color ) ) {
			$border_transparency = evently_mikado_options()->getOptionValue( 'cf7_style_3_button_border_transparency' );
			
			if ( $border_transparency !== '' ) {
				$border_opacity = $border_transparency;
			}
			
			$styles['border-color'] = evently_mikado_rgba_color( $border_color, $border_opacity );
		}
		
		echo evently_mikado_dynamic_css( $selector, $styles );
	}
	
	add_action( 'evently_mikado_action_style_dynamic', 'evently_mikado_contact_form7_button_hover_styles_3' );
}