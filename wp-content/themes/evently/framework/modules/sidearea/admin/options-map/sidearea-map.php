<?php

if ( ! function_exists('evently_mikado_sidearea_options_map') ) {

	function evently_mikado_sidearea_options_map() {

		evently_mikado_add_admin_page(
			array(
				'slug' => '_side_area_page',
				'title' => esc_html__('Side Area', 'evently'),
				'icon' => 'fa fa-indent'
			)
		);

		$side_area_panel = evently_mikado_add_admin_panel(
			array(
				'title' => esc_html__('Side Area', 'evently'),
				'name' => 'side_area',
				'page' => '_side_area_page'
			)
		);

		$side_area_icon_style_group = evently_mikado_add_admin_group(
			array(
				'parent' => $side_area_panel,
				'name' => 'side_area_icon_style_group',
				'title' => esc_html__('Side Area Icon Style', 'evently'),
				'description' => esc_html__('Define styles for Side Area icon', 'evently')
			)
		);

		$side_area_icon_style_row1 = evently_mikado_add_admin_row(
			array(
				'parent'	=> $side_area_icon_style_group,
				'name'		=> 'side_area_icon_style_row1'
			)
		);

		evently_mikado_add_admin_field(
			array(
				'parent' => $side_area_icon_style_row1,
				'type' => 'colorsimple',
				'name' => 'side_area_icon_color',
				'label' => esc_html__('Color', 'evently')
			)
		);

		evently_mikado_add_admin_field(
			array(
				'parent' => $side_area_icon_style_row1,
				'type' => 'colorsimple',
				'name' => 'side_area_icon_hover_color',
				'label' => esc_html__('Hover Color', 'evently')
			)
		);

		$side_area_icon_style_row2 = evently_mikado_add_admin_row(
			array(
				'parent'	=> $side_area_icon_style_group,
				'name'		=> 'side_area_icon_style_row2',
				'next'		=> true
			)
		);

		evently_mikado_add_admin_field(
			array(
				'parent' => $side_area_icon_style_row2,
				'type' => 'colorsimple',
				'name' => 'side_area_close_icon_color',
				'label' => esc_html__('Close Icon Color', 'evently')
			)
		);

		evently_mikado_add_admin_field(
			array(
				'parent' => $side_area_icon_style_row2,
				'type' => 'colorsimple',
				'name' => 'side_area_close_icon_hover_color',
				'label' => esc_html__('Close Icon Hover Color', 'evently')
			)
		);

		evently_mikado_add_admin_field(
			array(
				'parent' => $side_area_panel,
				'type' => 'text',
				'name' => 'side_area_width',
				'default_value' => '',
				'label' => esc_html__('Side Area Width', 'evently'),
				'description' => esc_html__('Enter a width for Side Area', 'evently'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);

		evently_mikado_add_admin_field(
			array(
				'parent' => $side_area_panel,
				'type' => 'color',
				'name' => 'side_area_background_color',
				'label' => esc_html__('Background Color', 'evently'),
				'description' => esc_html__('Choose a background color for Side Area', 'evently')
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'parent' => $side_area_panel,
				'type' => 'text',
				'name' => 'side_area_padding',
				'label' => esc_html__('Padding', 'evently'),
				'description' => esc_html__('Define padding for Side Area in format top right bottom left', 'evently'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		evently_mikado_add_admin_field(
			array(
				'parent' => $side_area_panel,
				'type' => 'selectblank',
				'name' => 'side_area_aligment',
				'default_value' => '',
				'label' => esc_html__('Text Alignment', 'evently'),
				'description' => esc_html__('Choose text alignment for side area', 'evently'),
				'options' => array(
					'' => esc_html__('Default', 'evently'),
					'left' => esc_html__('Left', 'evently'),
					'center' => esc_html__('Center', 'evently'),
					'right' => esc_html__('Right', 'evently')
				)
			)
		);
	}

	add_action('evently_mikado_action_options_map', 'evently_mikado_sidearea_options_map', 4);
}