<?php

if ( ! function_exists('evently_mikado_like') ) {
	/**
	 * Returns EventlyMikadoClassLike instance
	 *
	 * @return EventlyMikadoClassLike
	 */
	function evently_mikado_like() {
		return EventlyMikadoClassLike::get_instance();
	}
}

function evently_mikado_get_like() {

	echo wp_kses(evently_mikado_like()->add_like(), array(
		'span' => array(
			'class' => true,
			'aria-hidden' => true,
			'style' => true,
			'id' => true
		),
		'i' => array(
			'class' => true,
			'style' => true,
			'id' => true
		),
		'a' => array(
			'href' => true,
			'class' => true,
			'id' => true,
			'title' => true,
			'style' => true
		)
	));
}