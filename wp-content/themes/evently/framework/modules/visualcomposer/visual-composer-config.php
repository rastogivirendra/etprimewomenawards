<?php

/**
 * Force Visual Composer to initialize as "built into the theme". This will hide certain tabs under the Settings->Visual Composer page
 */
if ( function_exists( 'vc_set_as_theme' ) ) {
	vc_set_as_theme( true );
}

/**
 * Change path for overridden templates
 */
if ( function_exists( 'vc_set_shortcodes_templates_dir' ) ) {
	$dir = MIKADO_ROOT_DIR . '/vc-templates';
	vc_set_shortcodes_templates_dir( $dir );
}

if ( ! function_exists( 'evently_mikado_configure_visual_composer_frontend_editor' ) ) {
	/**
	 * Configuration for Visual Composer FrontEnd Editor
	 * Hooks on vc_after_init action
	 */
	function evently_mikado_configure_visual_composer_frontend_editor() {
		/**
		 * Remove frontend editor
		 */
		if ( function_exists( 'vc_disable_frontend' ) ) {
			vc_disable_frontend();
		}
	}
	
	add_action( 'vc_after_init', 'evently_mikado_configure_visual_composer_frontend_editor' );
}

if ( ! function_exists( 'evently_mikado_vc_row_map' ) ) {
	/**
	 * Map VC Row shortcode
	 * Hooks on vc_after_init action
	 */
	function evently_mikado_vc_row_map() {
		
		/******* VC Row shortcode - begin *******/
		
			vc_add_param( 'vc_row',
				array(
					'type'       => 'dropdown',
					'param_name' => 'row_content_width',
					'heading'    => esc_html__( 'Mikado Row Content Width', 'evently' ),
					'value'      => array(
						esc_html__( 'Full Width', 'evently' ) => 'full-width',
						esc_html__( 'In Grid', 'evently' )    => 'grid'
					),
					'group'      => esc_html__( 'Mikado Settings', 'evently' )
				)
			);
			
			vc_add_param( 'vc_row',
				array(
					'type'        => 'textfield',
					'param_name'  => 'anchor',
					'heading'     => esc_html__( 'Mikado Anchor ID', 'evently' ),
					'description' => esc_html__( 'For example "home"', 'evently' ),
					'group'       => esc_html__( 'Mikado Settings', 'evently' )
				)
			);
			
			vc_add_param( 'vc_row',
				array(
					'type'       => 'colorpicker',
					'param_name' => 'simple_background_color',
					'heading'    => esc_html__( 'Mikado Background Color', 'evently' ),
					'group'      => esc_html__( 'Mikado Settings', 'evently' )
				)
			);
			
			vc_add_param( 'vc_row',
				array(
					'type'       => 'attach_image',
					'param_name' => 'simple_background_image',
					'heading'    => esc_html__( 'Mikado Background Image', 'evently' ),
					'group'      => esc_html__( 'Mikado Settings', 'evently' )
				)
			);
			
			vc_add_param( 'vc_row',
				array(
					'type'        => 'dropdown',
					'param_name'  => 'disable_background_image',
					'heading'     => esc_html__( 'Mikado Disable Background Image', 'evently' ),
					'value'       => array(
						esc_html__( 'Never', 'evently' )        => '',
						esc_html__( 'Below 1280px', 'evently' ) => '1280',
						esc_html__( 'Below 1024px', 'evently' ) => '1024',
						esc_html__( 'Below 768px', 'evently' )  => '768',
						esc_html__( 'Below 680px', 'evently' )  => '680',
						esc_html__( 'Below 480px', 'evently' )  => '480'
					),
					'save_always' => true,
					'description' => esc_html__( 'Choose on which stage you hide row background image', 'evently' ),
					'dependency'  => array( 'element' => 'simple_background_image', 'not_empty' => true ),
					'group'       => esc_html__( 'Mikado Settings', 'evently' )
				)
			);
			
			vc_add_param( 'vc_row',
				array(
					'type'       => 'attach_image',
					'param_name' => 'parallax_background_image',
					'heading'    => esc_html__( 'Mikado Parallax Background Image', 'evently' ),
					'group'      => esc_html__( 'Mikado Settings', 'evently' )
				)
			);
			
			vc_add_param( 'vc_row',
				array(
					'type'        => 'textfield',
					'param_name'  => 'parallax_bg_speed',
					'heading'     => esc_html__( 'Mikado Parallax Speed', 'evently' ),
					'description' => esc_html__( 'Set your parallax speed. Default value is 1.', 'evently' ),
					'dependency'  => array( 'element' => 'parallax_background_image', 'not_empty' => true ),
					'group'       => esc_html__( 'Mikado Settings', 'evently' )
				)
			);
			
			vc_add_param( 'vc_row',
				array(
					'type'       => 'textfield',
					'param_name' => 'parallax_bg_height',
					'heading'    => esc_html__( 'Mikado Parallax Section Height (px)', 'evently' ),
					'dependency' => array( 'element' => 'parallax_background_image', 'not_empty' => true ),
					'group'      => esc_html__( 'Mikado Settings', 'evently' )
				)
			);
			
			vc_add_param( 'vc_row',
				array(
					'type'       => 'dropdown',
					'param_name' => 'content_text_aligment',
					'heading'    => esc_html__( 'Mikado Content Aligment', 'evently' ),
					'value'      => array(
						esc_html__( 'Default', 'evently' ) => '',
						esc_html__( 'Left', 'evently' )    => 'left',
						esc_html__( 'Center', 'evently' )  => 'center',
						esc_html__( 'Right', 'evently' )   => 'right'
					),
					'group'      => esc_html__( 'Mikado Settings', 'evently' )
				)
			);

			vc_add_param( 'vc_row',
				array(
					'type'       => 'dropdown',
					'param_name' => 'box_shadow',
					'heading'    => esc_html__( 'Mikado Enable Box Shadow', 'evently' ),
					'value'      => array_flip(evently_mikado_get_yes_no_select_array(false)),
					'group'      => esc_html__( 'Mikado Settings', 'evently' )
				)
			);

			vc_add_param( 'vc_row',
				array(
					'type'        => 'dropdown',
					'param_name'  => 'shadow_layout',
					'heading'     => esc_html__( 'Choose Box Shadow Layout', 'evently' ),
					'value'       => array(
						esc_html__( 'Full Shadow', 'evently' )        => 'full',
						esc_html__( 'Without Bottom Shadow', 'evently' )     => 'without-bottom'
					),
					'save_always' => true,
					'description' => esc_html__( 'Choose on which stage you hide row background image', 'evently' ),
					'dependency'  => array( 'element' => 'box_shadow', 'value' => 'yes' ),
					'group'       => esc_html__( 'Mikado Settings', 'evently' )
				)
			);

			vc_add_param( 'vc_row',
				array(
					'type'       => 'dropdown',
					'param_name' => 'enable_parallax_background_elements',
					'heading'    => esc_html__( 'Enable Parallax Background Elements', 'evently' ),
					'value'      => array(
						esc_html__( 'No', 'evently' ) => 'no',
						esc_html__( 'Yes', 'evently' ) => 'yes',
					),
					'description' => esc_html__( 'Enabling this feature will let you add two additional parallax elements positioned on the sides of this row. Disabled on touch devices.', 'evently' ),
					'group'      => esc_html__( 'Mikado Settings', 'evently' )
				)
			);

			vc_add_param( 'vc_row',
				array(
					'type'       => 'attach_image',
					'param_name' => 'left_parallax_background_element',
					'heading'    => esc_html__( 'Left Parallax Background Element', 'evently' ),
					'dependency'  => array( 'element' => 'enable_parallax_background_elements', 'value' => 'yes' ),
					'group'      => esc_html__( 'Mikado Settings', 'evently' )
				)
			);

			vc_add_param( 'vc_row',
				array(
					'type'       => 'attach_image',
					'param_name' => 'right_parallax_background_element',
					'heading'    => esc_html__( 'Right Parallax Background Element', 'evently' ),
					'dependency'  => array( 'element' => 'enable_parallax_background_elements', 'value' => 'yes' ),
					'group'      => esc_html__( 'Mikado Settings', 'evently' )
				)
			);

			vc_add_param( 'vc_row',
				array(
					'type'       => 'dropdown',
					'param_name' => 'parallax_background_elements_vertical_alignment',
					'heading'    => esc_html__( 'Parallax Background Elements Vertical Alignment', 'evently' ),
					'value'      => array(
						esc_html__( 'Top', 'evently' ) => 'top',
						esc_html__( 'Middle', 'evently' ) => 'middle',
						esc_html__( 'Bottom', 'evently' ) => 'bottom',
					),
					'dependency'  => array( 'element' => 'enable_parallax_background_elements', 'value' => 'yes' ),
					'group'      => esc_html__( 'Mikado Settings', 'evently' )
				)
			);
		
		/******* VC Row shortcode - end *******/
		
		/******* VC Row Inner shortcode - begin *******/
		
			vc_add_param( 'vc_row_inner',
				array(
					'type'       => 'dropdown',
					'param_name' => 'row_content_width',
					'heading'    => esc_html__( 'Mikado Row Content Width', 'evently' ),
					'value'      => array(
						esc_html__( 'Full Width', 'evently' ) => 'full-width',
						esc_html__( 'In Grid', 'evently' )    => 'grid'
					),
					'group'      => esc_html__( 'Mikado Settings', 'evently' )
				)
			);
			
			vc_add_param( 'vc_row_inner',
				array(
					'type'       => 'colorpicker',
					'param_name' => 'simple_background_color',
					'heading'    => esc_html__( 'Mikado Background Color', 'evently' ),
					'group'      => esc_html__( 'Mikado Settings', 'evently' )
				)
			);
			
			vc_add_param( 'vc_row_inner',
				array(
					'type'       => 'attach_image',
					'param_name' => 'simple_background_image',
					'heading'    => esc_html__( 'Mikado Background Image', 'evently' ),
					'group'      => esc_html__( 'Mikado Settings', 'evently' )
				)
			);
			
			vc_add_param( 'vc_row_inner',
				array(
					'type'        => 'dropdown',
					'param_name'  => 'disable_background_image',
					'heading'     => esc_html__( 'Mikado Disable Background Image', 'evently' ),
					'value'       => array(
						esc_html__( 'Never', 'evently' )        => '',
						esc_html__( 'Below 1280px', 'evently' ) => '1280',
						esc_html__( 'Below 1024px', 'evently' ) => '1024',
						esc_html__( 'Below 768px', 'evently' )  => '768',
						esc_html__( 'Below 680px', 'evently' )  => '680',
						esc_html__( 'Below 480px', 'evently' )  => '480'
					),
					'save_always' => true,
					'description' => esc_html__( 'Choose on which stage you hide row background image', 'evently' ),
					'dependency'  => array( 'element' => 'simple_background_image', 'not_empty' => true ),
					'group'       => esc_html__( 'Mikado Settings', 'evently' )
				)
			);
			
			vc_add_param( 'vc_row_inner',
				array(
					'type'       => 'dropdown',
					'param_name' => 'content_text_aligment',
					'heading'    => esc_html__( 'Mikado Content Aligment', 'evently' ),
					'value'      => array(
						esc_html__( 'Default', 'evently' ) => '',
						esc_html__( 'Left', 'evently' )    => 'left',
						esc_html__( 'Center', 'evently' )  => 'center',
						esc_html__( 'Right', 'evently' )   => 'right'
					),
					'group'      => esc_html__( 'Mikado Settings', 'evently' )
				)
			);

			vc_add_param( 'vc_row_inner',
				array(
					'type'       => 'dropdown',
					'param_name' => 'box_shadow',
					'heading'    => esc_html__( 'Mikado Enable Box Shadow', 'evently' ),
					'value'      => array_flip(evently_mikado_get_yes_no_select_array(false)),
					'group'      => esc_html__( 'Mikado Settings', 'evently' )
				)
			);

			vc_add_param( 'vc_row_inner',
				array(
					'type'        => 'dropdown',
					'param_name'  => 'shadow_layout',
					'heading'     => esc_html__( 'Choose Box Shadow Layout', 'evently' ),
					'value'       => array(
						esc_html__( 'Full Shadow', 'evently' )        => 'full',
						esc_html__( 'Without Bottom Shadow', 'evently' )     => 'without-bottom'
					),
					'save_always' => true,
					'description' => esc_html__( 'Choose on which stage you hide row background image', 'evently' ),
					'dependency'  => array( 'element' => 'box_shadow', 'value' => 'yes' ),
					'group'       => esc_html__( 'Mikado Settings', 'evently' )
				)
			);
		
		/******* VC Row Inner shortcode - end *******/
		
		/******* VC Revolution Slider shortcode - begin *******/
		
		if ( evently_mikado_revolution_slider_installed() ) {

			vc_add_param( 'rev_slider_vc',
				array(
					'type'        => 'dropdown',
					'param_name'  => 'slider_style',
					'heading'     => esc_html__( 'Mikado Slider Style', 'evently' ),
					'value'       => array(
						esc_html__( 'Default', 'evently' )            => '',
						esc_html__( 'With Image Behind', 'evently' )  => 'image-behind'
					),
					'save_always' => true,
					'group'       => esc_html__( 'Mikado Settings', 'evently' )
				)
			);

			vc_add_param( 'rev_slider_vc',
				array(
					'type'       => 'attach_image',
					'param_name' => 'image',
					'heading'    => esc_html__( 'Background Image', 'evently' ),
					'dependency'  => array( 'element' => 'slider_style', 'value' => 'image-behind' ),
					'group'       => esc_html__( 'Mikado Settings', 'evently' )
				)
			);
			
			vc_add_param( 'rev_slider_vc',
				array(
					'type'        => 'dropdown',
					'param_name'  => 'enable_paspartu',
					'heading'     => esc_html__( 'Mikado Enable Passepartout', 'evently' ),
					'value'       => array_flip( evently_mikado_get_yes_no_select_array( false ) ),
					'save_always' => true,
					'group'       => esc_html__( 'Mikado Settings', 'evently' )
				)
			);
			
			vc_add_param( 'rev_slider_vc',
				array(
					'type'        => 'dropdown',
					'param_name'  => 'paspartu_size',
					'heading'     => esc_html__( 'Mikado Passepartout Size', 'evently' ),
					'value'       => array(
						esc_html__( 'Tiny', 'evently' )   => 'tiny',
						esc_html__( 'Small', 'evently' )  => 'small',
						esc_html__( 'Normal', 'evently' ) => 'normal',
						esc_html__( 'Large', 'evently' )  => 'large'
					),
					'save_always' => true,
					'dependency'  => array( 'element' => 'enable_paspartu', 'value' => array( 'yes' ) ),
					'group'       => esc_html__( 'Mikado Settings', 'evently' )
				)
			);
			
			vc_add_param( 'rev_slider_vc',
				array(
					'type'        => 'dropdown',
					'param_name'  => 'disable_side_paspartu',
					'heading'     => esc_html__( 'Mikado Disable Side Passepartout', 'evently' ),
					'value'       => array_flip( evently_mikado_get_yes_no_select_array( false ) ),
					'save_always' => true,
					'dependency'  => array( 'element' => 'enable_paspartu', 'value' => array( 'yes' ) ),
					'group'       => esc_html__( 'Mikado Settings', 'evently' )
				)
			);
			
			vc_add_param( 'rev_slider_vc',
				array(
					'type'        => 'dropdown',
					'param_name'  => 'disable_top_paspartu',
					'heading'     => esc_html__( 'Mikado Disable Top Passepartout', 'evently' ),
					'value'       => array_flip( evently_mikado_get_yes_no_select_array( false ) ),
					'save_always' => true,
					'dependency'  => array( 'element' => 'enable_paspartu', 'value' => array( 'yes' ) ),
					'group'       => esc_html__( 'Mikado Settings', 'evently' )
				)
			);
		}
		
		/******* VC Revolution Slider shortcode - end *******/
	}
	
	add_action( 'vc_after_init', 'evently_mikado_vc_row_map' );
}