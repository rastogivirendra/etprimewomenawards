<?php

if ( ! function_exists( 'evently_mikado_map_footer_meta' ) ) {
	function evently_mikado_map_footer_meta() {
		
		$footer_meta_box = evently_mikado_add_meta_box(
			array(
				'scope' => apply_filters( 'evently_mikado_filter_set_scope_for_meta_boxes', array( 'page', 'post' ) ),
				'title' => esc_html__( 'Footer', 'evently' ),
				'name'  => 'footer_meta'
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_disable_footer_meta',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Disable Footer for this Page', 'evently' ),
				'description'   => esc_html__( 'Enabling this option will hide footer on this page', 'evently' ),
				'parent'        => $footer_meta_box
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_show_footer_top_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Show Footer Top', 'evently' ),
				'description'   => esc_html__( 'Enabling this option will show Footer Top area', 'evently' ),
				'parent'        => $footer_meta_box,
				'options'       => evently_mikado_get_yes_no_select_array()
			)
		);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_show_footer_bottom_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Show Footer Bottom', 'evently' ),
				'description'   => esc_html__( 'Enabling this option will show Footer Bottom area', 'evently' ),
				'parent'        => $footer_meta_box,
				'options'       => evently_mikado_get_yes_no_select_array()
			)
		);

		evently_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_footer_skin_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Footer Skin', 'evently' ),
				'description'   => esc_html__( 'Choose footer skin', 'evently' ),
				'parent'        => $footer_meta_box,
				'options'     => array(
					''           => esc_html__( 'Default', 'evently' ),
					'light'    => esc_html__( 'Light', 'evently' ),
					'dark'     => esc_html__( 'Dark', 'evently' )
				)
			)
		);
	}
	
	add_action( 'evently_mikado_action_meta_boxes_map', 'evently_mikado_map_footer_meta', 70 );
}