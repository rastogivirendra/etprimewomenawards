<?php

if ( ! function_exists( 'evently_mikado_footer_options_map' ) ) {
	function evently_mikado_footer_options_map() {
		
		evently_mikado_add_admin_page(
			array(
				'slug'  => '_footer_page',
				'title' => esc_html__( 'Footer', 'evently' ),
				'icon'  => 'fa fa-sort-amount-asc'
			)
		);
		
		$footer_panel = evently_mikado_add_admin_panel(
			array(
				'title' => esc_html__( 'Footer', 'evently' ),
				'name'  => 'footer',
				'page'  => '_footer_page'
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'footer_in_grid',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Footer in Grid', 'evently' ),
				'description'   => esc_html__( 'Enabling this option will place Footer content in grid', 'evently' ),
				'parent'        => $footer_panel,
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'show_footer_top',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Show Footer Top', 'evently' ),
				'description'   => esc_html__( 'Enabling this option will show Footer Top area', 'evently' ),
				'args'          => array(
					'dependence'             => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#mkdf_show_footer_top_container'
				),
				'parent'        => $footer_panel,
			)
		);
		
		$show_footer_top_container = evently_mikado_add_admin_container(
			array(
				'name'            => 'show_footer_top_container',
				'hidden_property' => 'show_footer_top',
				'hidden_value'    => 'no',
				'parent'          => $footer_panel
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'footer_top_columns',
				'parent'        => $show_footer_top_container,
				'default_value' => '4',
				'label'         => esc_html__( 'Footer Top Columns', 'evently' ),
				'description'   => esc_html__( 'Choose number of columns for Footer Top area', 'evently' ),
				'options'       => array(
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4'
				)
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'footer_top_columns_alignment',
				'default_value' => 'left',
				'label'         => esc_html__( 'Footer Top Columns Alignment', 'evently' ),
				'description'   => esc_html__( 'Text Alignment in Footer Columns', 'evently' ),
				'options'       => array(
					''       => esc_html__( 'Default', 'evently' ),
					'left'   => esc_html__( 'Left', 'evently' ),
					'center' => esc_html__( 'Center', 'evently' ),
					'right'  => esc_html__( 'Right', 'evently' )
				),
				'parent'        => $show_footer_top_container,
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'        => 'footer_top_background_color',
				'type'        => 'color',
				'label'       => esc_html__( 'Background Color', 'evently' ),
				'description' => esc_html__( 'Set background color for top footer area', 'evently' ),
				'parent'      => $show_footer_top_container
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'show_footer_bottom',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Show Footer Bottom', 'evently' ),
				'description'   => esc_html__( 'Enabling this option will show Footer Bottom area', 'evently' ),
				'args'          => array(
					'dependence'             => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#mkdf_show_footer_bottom_container'
				),
				'parent'        => $footer_panel,
			)
		);
		
		$show_footer_bottom_container = evently_mikado_add_admin_container(
			array(
				'name'            => 'show_footer_bottom_container',
				'hidden_property' => 'show_footer_bottom',
				'hidden_value'    => 'no',
				'parent'          => $footer_panel
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'footer_bottom_columns',
				'default_value' => '3',
				'label'         => esc_html__( 'Footer Bottom Columns', 'evently' ),
				'description'   => esc_html__( 'Choose number of columns for Footer Bottom area', 'evently' ),
				'options'       => array(
					'1' => '1',
					'2' => '2',
					'3' => '3'
				),
				'parent'        => $show_footer_bottom_container,
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'        => 'footer_bottom_background_color',
				'type'        => 'color',
				'label'       => esc_html__( 'Background Color', 'evently' ),
				'description' => esc_html__( 'Set background color for bottom footer area', 'evently' ),
				'parent'      => $show_footer_bottom_container
			)
		);
	}
	
	add_action( 'evently_mikado_action_options_map', 'evently_mikado_footer_options_map', 8 );
}