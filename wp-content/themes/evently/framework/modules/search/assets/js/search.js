(function($) {
    "use strict";

    var search = {};
    mkdf.modules.search = search;

    search.mkdfOnDocumentReady = mkdfOnDocumentReady;

    $(document).ready(mkdfOnDocumentReady);
    
    /* 
        All functions to be called on $(document).ready() should be in this function
    */
    function mkdfOnDocumentReady() {
	    mkdfSearch();
    }
	
	/**
	 * Init Search Types
	 */
	function mkdfSearch() {
		var searchOpener = $('a.mkdf-search-opener'),
			searchClose;
		
		if ( searchOpener.length > 0 ) {
			//Check for type of search
			if ( mkdf.body.hasClass( 'mkdf-fullscreen-search' ) ) {
				searchClose = $( '.mkdf-fullscreen-search-close' );
				mkdfFullscreenSearch();
			}
		}
		
		/**
		 * Fullscreen search fade
		 */
		function mkdfFullscreenSearch() {
			var searchHolder = $('.mkdf-fullscreen-search-holder');
			
			searchOpener.click(function (e) {
				e.preventDefault();
				
				if (searchHolder.hasClass('mkdf-animate')) {
					mkdf.body.removeClass('mkdf-fullscreen-search-opened mkdf-search-fade-out');
					mkdf.body.removeClass('mkdf-search-fade-in');
					searchHolder.removeClass('mkdf-animate');
					
					setTimeout(function () {
						searchHolder.find('.mkdf-search-field').val('');
						searchHolder.find('.mkdf-search-field').blur();
					}, 300);
					
					mkdf.modules.common.mkdfEnableScroll();
				} else {
					mkdf.body.addClass('mkdf-fullscreen-search-opened mkdf-search-fade-in');
					mkdf.body.removeClass('mkdf-search-fade-out');
					searchHolder.addClass('mkdf-animate');
					
					setTimeout(function () {
						searchHolder.find('.mkdf-search-field').focus();
					}, 900);
					
					mkdf.modules.common.mkdfDisableScroll();
				}
				
				searchClose.click(function (e) {
					e.preventDefault();
					mkdf.body.removeClass('mkdf-fullscreen-search-opened mkdf-search-fade-in');
					mkdf.body.addClass('mkdf-search-fade-out');
					searchHolder.removeClass('mkdf-animate');
					
					setTimeout(function () {
						searchHolder.find('.mkdf-search-field').val('');
						searchHolder.find('.mkdf-search-field').blur();
					}, 300);
					
					mkdf.modules.common.mkdfEnableScroll();
				});
				
				//Close on click away
				$(document).mouseup(function (e) {
					var container = $(".mkdf-form-holder-inner");
					
					if (!container.is(e.target) && container.has(e.target).length === 0) {
						e.preventDefault();
						mkdf.body.removeClass('mkdf-fullscreen-search-opened mkdf-search-fade-in');
						mkdf.body.addClass('mkdf-search-fade-out');
						searchHolder.removeClass('mkdf-animate');
						
						setTimeout(function () {
							searchHolder.find('.mkdf-search-field').val('');
							searchHolder.find('.mkdf-search-field').blur();
						}, 300);
						
						mkdf.modules.common.mkdfEnableScroll();
					}
				});
				
				//Close on escape
				$(document).keyup(function (e) {
					if (e.keyCode == 27) { //KeyCode for ESC button is 27
						mkdf.body.removeClass('mkdf-fullscreen-search-opened mkdf-search-fade-in');
						mkdf.body.addClass('mkdf-search-fade-out');
						searchHolder.removeClass('mkdf-animate');
						
						setTimeout(function () {
							searchHolder.find('.mkdf-search-field').val('');
							searchHolder.find('.mkdf-search-field').blur();
						}, 300);
						
						mkdf.modules.common.mkdfEnableScroll();
					}
				});
			});
		}
	}

})(jQuery);
