<div class="mkdf-fullscreen-search-holder">
	<a class="mkdf-fullscreen-search-close" href="javascript:void(0)">
		<?php echo evently_mikado_icon_collections()->renderIcon( 'ion-ios-close-empty', 'ion_icons' ); ?>
	</a>
	<div class="mkdf-fullscreen-search-table">
		<div class="mkdf-fullscreen-search-cell">
			<div class="mkdf-fullscreen-search-inner">
				<h6 class="mkdf-fullscreen-search-subtitle">
					<span><?php esc_html_e('Simply enter your keyword and we will help you find what you need.', 'evently'); ?></span>
				</h6>
				<h2 class="mkdf-fullscreen-search-title">
					<span><?php esc_html_e('What are you looking for?', 'evently'); ?></span>
				</h2>

				<form action="<?php echo esc_url(home_url('/')); ?>" class="mkdf-fullscreen-search-form" method="get">
					<div class="mkdf-form-holder">
						<div class="mkdf-field-holder">
							<input type="text" name="s" placeholder="<?php esc_html_e('Enter your keyword', 'evently'); ?>" class="mkdf-search-field" autocomplete="off"/>
							<input type="submit" class="mkdf-search-submit" value="<?php esc_html_e('Search','evently'); ?>"/>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>