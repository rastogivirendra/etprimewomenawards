<?php

if ( ! function_exists( 'evently_mikado_general_options_map' ) ) {
	/**
	 * General options page
	 */
	function evently_mikado_general_options_map() {
		
		evently_mikado_add_admin_page(
			array(
				'slug'  => '',
				'title' => esc_html__( 'General', 'evently' ),
				'icon'  => 'fa fa-institution'
			)
		);
		
		/***************** Additional General Area Layout - start *****************/
		
		do_action( 'evently_mikado_action_additional_general_options_map' );
		
		/***************** Additional General Area Layout - end *****************/
		
		$panel_design_style = evently_mikado_add_admin_panel(
			array(
				'page'  => '',
				'name'  => 'panel_design_style',
				'title' => esc_html__( 'Appearance', 'evently' )
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'google_fonts',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__( 'Google Font Family', 'evently' ),
				'description'   => esc_html__( 'Choose a default Google font for your site', 'evently' ),
				'parent'        => $panel_design_style
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'additional_google_fonts',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Additional Google Fonts', 'evently' ),
				'parent'        => $panel_design_style,
				'args'          => array(
					"dependence"             => true,
					"dependence_hide_on_yes" => "",
					"dependence_show_on_yes" => "#mkdf_additional_google_fonts_container"
				)
			)
		);
		
		$additional_google_fonts_container = evently_mikado_add_admin_container(
			array(
				'parent'          => $panel_design_style,
				'name'            => 'additional_google_fonts_container',
				'hidden_property' => 'additional_google_fonts',
				'hidden_value'    => 'no'
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'additional_google_font1',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__( 'Font Family', 'evently' ),
				'description'   => esc_html__( 'Choose additional Google font for your site', 'evently' ),
				'parent'        => $additional_google_fonts_container
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'additional_google_font2',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__( 'Font Family', 'evently' ),
				'description'   => esc_html__( 'Choose additional Google font for your site', 'evently' ),
				'parent'        => $additional_google_fonts_container
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'additional_google_font3',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__( 'Font Family', 'evently' ),
				'description'   => esc_html__( 'Choose additional Google font for your site', 'evently' ),
				'parent'        => $additional_google_fonts_container
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'additional_google_font4',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__( 'Font Family', 'evently' ),
				'description'   => esc_html__( 'Choose additional Google font for your site', 'evently' ),
				'parent'        => $additional_google_fonts_container
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'additional_google_font5',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__( 'Font Family', 'evently' ),
				'description'   => esc_html__( 'Choose additional Google font for your site', 'evently' ),
				'parent'        => $additional_google_fonts_container
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'google_font_weight',
				'type'          => 'checkboxgroup',
				'default_value' => '',
				'label'         => esc_html__( 'Google Fonts Style & Weight', 'evently' ),
				'description'   => esc_html__( 'Choose a default Google font weights for your site. Impact on page load time', 'evently' ),
				'parent'        => $panel_design_style,
				'options'       => array(
					'100'  => esc_html__( '100 Thin', 'evently' ),
					'100i' => esc_html__( '100 Thin Italic', 'evently' ),
					'200'  => esc_html__( '200 Extra-Light', 'evently' ),
					'200i' => esc_html__( '200 Extra-Light Italic', 'evently' ),
					'300'  => esc_html__( '300 Light', 'evently' ),
					'300i' => esc_html__( '300 Light Italic', 'evently' ),
					'400'  => esc_html__( '400 Regular', 'evently' ),
					'400i' => esc_html__( '400 Regular Italic', 'evently' ),
					'500'  => esc_html__( '500 Medium', 'evently' ),
					'500i' => esc_html__( '500 Medium Italic', 'evently' ),
					'600'  => esc_html__( '600 Semi-Bold', 'evently' ),
					'600i' => esc_html__( '600 Semi-Bold Italic', 'evently' ),
					'700'  => esc_html__( '700 Bold', 'evently' ),
					'700i' => esc_html__( '700 Bold Italic', 'evently' ),
					'800'  => esc_html__( '800 Extra-Bold', 'evently' ),
					'800i' => esc_html__( '800 Extra-Bold Italic', 'evently' ),
					'900'  => esc_html__( '900 Ultra-Bold', 'evently' ),
					'900i' => esc_html__( '900 Ultra-Bold Italic', 'evently' )
				)
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'google_font_subset',
				'type'          => 'checkboxgroup',
				'default_value' => '',
				'label'         => esc_html__( 'Google Fonts Subset', 'evently' ),
				'description'   => esc_html__( 'Choose a default Google font subsets for your site', 'evently' ),
				'parent'        => $panel_design_style,
				'options'       => array(
					'latin'        => esc_html__( 'Latin', 'evently' ),
					'latin-ext'    => esc_html__( 'Latin Extended', 'evently' ),
					'cyrillic'     => esc_html__( 'Cyrillic', 'evently' ),
					'cyrillic-ext' => esc_html__( 'Cyrillic Extended', 'evently' ),
					'greek'        => esc_html__( 'Greek', 'evently' ),
					'greek-ext'    => esc_html__( 'Greek Extended', 'evently' ),
					'vietnamese'   => esc_html__( 'Vietnamese', 'evently' )
				)
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'        => 'first_color',
				'type'        => 'color',
				'label'       => esc_html__( 'First Main Color', 'evently' ),
				'description' => esc_html__( 'Choose the most dominant theme color. Default color is #00bbb3', 'evently' ),
				'parent'      => $panel_design_style
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'        => 'page_background_color',
				'type'        => 'color',
				'label'       => esc_html__( 'Page Background Color', 'evently' ),
				'description' => esc_html__( 'Choose the background color for page content. Default color is #ffffff', 'evently' ),
				'parent'      => $panel_design_style
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'        => 'selection_color',
				'type'        => 'color',
				'label'       => esc_html__( 'Text Selection Color', 'evently' ),
				'description' => esc_html__( 'Choose the color users see when selecting text', 'evently' ),
				'parent'      => $panel_design_style
			)
		);
		
		/***************** Passepartout Layout - begin **********************/
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'boxed',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Boxed Layout', 'evently' ),
				'parent'        => $panel_design_style,
				'args'          => array(
					"dependence"             => true,
					"dependence_hide_on_yes" => "",
					"dependence_show_on_yes" => "#mkdf_boxed_container"
				)
			)
		);
		
			$boxed_container = evently_mikado_add_admin_container(
				array(
					'parent'          => $panel_design_style,
					'name'            => 'boxed_container',
					'hidden_property' => 'boxed',
					'hidden_value'    => 'no'
				)
			);
		
				evently_mikado_add_admin_field(
					array(
						'name'        => 'page_background_color_in_box',
						'type'        => 'color',
						'label'       => esc_html__( 'Page Background Color', 'evently' ),
						'description' => esc_html__( 'Choose the page background color outside box', 'evently' ),
						'parent'      => $boxed_container
					)
				);
				
				evently_mikado_add_admin_field(
					array(
						'name'        => 'boxed_background_image',
						'type'        => 'image',
						'label'       => esc_html__( 'Background Image', 'evently' ),
						'description' => esc_html__( 'Choose an image to be displayed in background', 'evently' ),
						'parent'      => $boxed_container
					)
				);
				
				evently_mikado_add_admin_field(
					array(
						'name'        => 'boxed_pattern_background_image',
						'type'        => 'image',
						'label'       => esc_html__( 'Background Pattern', 'evently' ),
						'description' => esc_html__( 'Choose an image to be used as background pattern', 'evently' ),
						'parent'      => $boxed_container
					)
				);
				
				evently_mikado_add_admin_field(
					array(
						'name'          => 'boxed_background_image_attachment',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Background Image Attachment', 'evently' ),
						'description'   => esc_html__( 'Choose background image attachment', 'evently' ),
						'parent'        => $boxed_container,
						'options'       => array(
							''       => esc_html__( 'Default', 'evently' ),
							'fixed'  => esc_html__( 'Fixed', 'evently' ),
							'scroll' => esc_html__( 'Scroll', 'evently' )
						)
					)
				);
		
		/***************** Boxed Layout - end **********************/
		
		/***************** Passepartout Layout - begin **********************/
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'paspartu',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Passepartout', 'evently' ),
				'description'   => esc_html__( 'Enabling this option will display passepartout around site content', 'evently' ),
				'parent'        => $panel_design_style,
				'args'          => array(
					"dependence"             => true,
					"dependence_hide_on_yes" => "",
					"dependence_show_on_yes" => "#mkdf_paspartu_container"
				)
			)
		);
		
			$paspartu_container = evently_mikado_add_admin_container(
				array(
					'parent'          => $panel_design_style,
					'name'            => 'paspartu_container',
					'hidden_property' => 'paspartu',
					'hidden_value'    => 'no'
				)
			);
		
				evently_mikado_add_admin_field(
					array(
						'name'        => 'paspartu_color',
						'type'        => 'color',
						'label'       => esc_html__( 'Passepartout Color', 'evently' ),
						'description' => esc_html__( 'Choose passepartout color, default value is #ffffff', 'evently' ),
						'parent'      => $paspartu_container
					)
				);
				
				evently_mikado_add_admin_field(
					array(
						'name'        => 'paspartu_width',
						'type'        => 'text',
						'label'       => esc_html__( 'Passepartout Size', 'evently' ),
						'description' => esc_html__( 'Enter size amount for passepartout', 'evently' ),
						'parent'      => $paspartu_container,
						'args'        => array(
							'col_width' => 2,
							'suffix'    => 'px or %'
						)
					)
				);
				
				evently_mikado_add_admin_field(
					array(
						'parent'        => $paspartu_container,
						'type'          => 'yesno',
						'default_value' => 'no',
						'name'          => 'disable_top_paspartu',
						'label'         => esc_html__( 'Disable Top Passepartout', 'evently' )
					)
				);
		
		/***************** Passepartout Layout - end **********************/
		
		/***************** Content Layout - begin **********************/
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'initial_content_width',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Initial Width of Content', 'evently' ),
				'description'   => esc_html__( 'Choose the initial width of content which is in grid (Applies to pages set to "Default Template" and rows set to "In Grid")', 'evently' ),
				'parent'        => $panel_design_style,
				'options'       => array(
					'mkdf-grid-1100' => esc_html__( '1100px - default', 'evently' ),
					'mkdf-grid-1300' => esc_html__( '1300px', 'evently' ),
					'mkdf-grid-1200' => esc_html__( '1200px', 'evently' ),
					'mkdf-grid-1000' => esc_html__( '1000px', 'evently' ),
					'mkdf-grid-800'  => esc_html__( '800px', 'evently' )
				)
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'preload_pattern_image',
				'type'          => 'image',
				'label'         => esc_html__( 'Preload Pattern Image', 'evently' ),
				'description'   => esc_html__( 'Choose preload pattern image to be displayed until images are loaded', 'evently' ),
				'parent'        => $panel_design_style
			)
		);
		
		/***************** Content Layout - end **********************/
		
		$panel_settings = evently_mikado_add_admin_panel(
			array(
				'page'  => '',
				'name'  => 'panel_settings',
				'title' => esc_html__( 'Behavior', 'evently' )
			)
		);
		
		/***************** Smooth Scroll Layout - begin **********************/
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'page_smooth_scroll',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Smooth Scroll', 'evently' ),
				'description'   => esc_html__( 'Enabling this option will perform a smooth scrolling effect on every page (except on Mac and touch devices)', 'evently' ),
				'parent'        => $panel_settings
			)
		);
		
		/***************** Smooth Scroll Layout - end **********************/
		
		/***************** Smooth Page Transitions Layout - begin **********************/
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'smooth_page_transitions',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Smooth Page Transitions', 'evently' ),
				'description'   => esc_html__( 'Enabling this option will perform a smooth transition between pages when clicking on links', 'evently' ),
				'parent'        => $panel_settings,
				'args'          => array(
					"dependence"             => true,
					"dependence_hide_on_yes" => "",
					"dependence_show_on_yes" => "#mkdf_page_transitions_container"
				)
			)
		);
		
			$page_transitions_container = evently_mikado_add_admin_container(
				array(
					'parent'          => $panel_settings,
					'name'            => 'page_transitions_container',
					'hidden_property' => 'smooth_page_transitions',
					'hidden_value'    => 'no'
				)
			);
		
				evently_mikado_add_admin_field(
					array(
						'name'          => 'page_transition_preloader',
						'type'          => 'yesno',
						'default_value' => 'no',
						'label'         => esc_html__( 'Enable Preloading Animation', 'evently' ),
						'description'   => esc_html__( 'Enabling this option will display an animated preloader while the page content is loading', 'evently' ),
						'parent'        => $page_transitions_container,
						'args'          => array(
							"dependence"             => true,
							"dependence_hide_on_yes" => "",
							"dependence_show_on_yes" => "#mkdf_page_transition_preloader_container"
						)
					)
				);
				
				$page_transition_preloader_container = evently_mikado_add_admin_container(
					array(
						'parent'          => $page_transitions_container,
						'name'            => 'page_transition_preloader_container',
						'hidden_property' => 'page_transition_preloader',
						'hidden_value'    => 'no'
					)
				);
		
		
					evently_mikado_add_admin_field(
						array(
							'name'   => 'smooth_pt_bgnd_color',
							'type'   => 'color',
							'label'  => esc_html__( 'Page Loader Background Color', 'evently' ),
							'parent' => $page_transition_preloader_container
						)
					);
					
					$group_pt_spinner_animation = evently_mikado_add_admin_group(
						array(
							'name'        => 'group_pt_spinner_animation',
							'title'       => esc_html__( 'Loader Style', 'evently' ),
							'description' => esc_html__( 'Define styles for loader spinner animation', 'evently' ),
							'parent'      => $page_transition_preloader_container
						)
					);
					
					$row_pt_spinner_animation = evently_mikado_add_admin_row(
						array(
							'name'   => 'row_pt_spinner_animation',
							'parent' => $group_pt_spinner_animation
						)
					);
					
					evently_mikado_add_admin_field(
						array(
							'type'          => 'selectsimple',
							'name'          => 'smooth_pt_spinner_type',
							'default_value' => '',
							'label'         => esc_html__( 'Spinner Type', 'evently' ),
							'parent'        => $row_pt_spinner_animation,
							'options'       => array(
								'rotate_circles'        => esc_html__( 'Rotate Circles', 'evently' ),
								'pulse'                 => esc_html__( 'Pulse', 'evently' ),
								'double_pulse'          => esc_html__( 'Double Pulse', 'evently' ),
								'cube'                  => esc_html__( 'Cube', 'evently' ),
								'rotating_cubes'        => esc_html__( 'Rotating Cubes', 'evently' ),
								'stripes'               => esc_html__( 'Stripes', 'evently' ),
								'wave'                  => esc_html__( 'Wave', 'evently' ),
								'two_rotating_circles'  => esc_html__( '2 Rotating Circles', 'evently' ),
								'five_rotating_circles' => esc_html__( '5 Rotating Circles', 'evently' ),
								'atom'                  => esc_html__( 'Atom', 'evently' ),
								'clock'                 => esc_html__( 'Clock', 'evently' ),
								'mitosis'               => esc_html__( 'Mitosis', 'evently' ),
								'lines'                 => esc_html__( 'Lines', 'evently' ),
								'fussion'               => esc_html__( 'Fussion', 'evently' ),
								'wave_circles'          => esc_html__( 'Wave Circles', 'evently' ),
								'pulse_circles'         => esc_html__( 'Pulse Circles', 'evently' )
							)
						)
					);
					
					evently_mikado_add_admin_field(
						array(
							'type'          => 'colorsimple',
							'name'          => 'smooth_pt_spinner_color',
							'default_value' => '',
							'label'         => esc_html__( 'Spinner Color', 'evently' ),
							'parent'        => $row_pt_spinner_animation
						)
					);
					
					evently_mikado_add_admin_field(
						array(
							'name'          => 'page_transition_fadeout',
							'type'          => 'yesno',
							'default_value' => 'no',
							'label'         => esc_html__( 'Enable Fade Out Animation', 'evently' ),
							'description'   => esc_html__( 'Enabling this option will turn on fade out animation when leaving page', 'evently' ),
							'parent'        => $page_transitions_container
						)
					);
		
		/***************** Smooth Page Transitions Layout - end **********************/
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'show_back_button',
				'type'          => 'yesno',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Show "Back To Top Button"', 'evently' ),
				'description'   => esc_html__( 'Enabling this option will display a Back to Top button on every page', 'evently' ),
				'parent'        => $panel_settings
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'          => 'responsiveness',
				'type'          => 'yesno',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Responsiveness', 'evently' ),
				'description'   => esc_html__( 'Enabling this option will make all pages responsive', 'evently' ),
				'parent'        => $panel_settings
			)
		);
		
		$panel_custom_code = evently_mikado_add_admin_panel(
			array(
				'page'  => '',
				'name'  => 'panel_custom_code',
				'title' => esc_html__( 'Custom Code', 'evently' )
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'        => 'custom_css',
				'type'        => 'textarea',
				'label'       => esc_html__( 'Custom CSS', 'evently' ),
				'description' => esc_html__( 'Enter your custom CSS here', 'evently' ),
				'parent'      => $panel_custom_code
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'        => 'custom_js',
				'type'        => 'textarea',
				'label'       => esc_html__( 'Custom JS', 'evently' ),
				'description' => esc_html__( 'Enter your custom Javascript here', 'evently' ),
				'parent'      => $panel_custom_code
			)
		);
		
		$panel_google_api = evently_mikado_add_admin_panel(
			array(
				'page'  => '',
				'name'  => 'panel_google_api',
				'title' => esc_html__( 'Google API', 'evently' )
			)
		);
		
		evently_mikado_add_admin_field(
			array(
				'name'        => 'google_maps_api_key',
				'type'        => 'text',
				'label'       => esc_html__( 'Google Maps Api Key', 'evently' ),
				'description' => esc_html__( 'Insert your Google Maps API key here. For instructions on how to create a Google Maps API key, please refer to our to our documentation.', 'evently' ),
				'parent'      => $panel_google_api
			)
		);
	}
	
	add_action( 'evently_mikado_action_options_map', 'evently_mikado_general_options_map', 1 );
}

if ( ! function_exists( 'evently_mikado_page_general_style' ) ) {
	/**
	 * Function that prints page general inline styles
	 */
	function evently_mikado_page_general_style( $style ) {
		$current_style = '';
		$page_id       = evently_mikado_get_page_id();
		$class_prefix  = evently_mikado_get_unique_page_class( $page_id );
		
		$boxed_background_style = array();
		
		$boxed_page_background_color = evently_mikado_get_meta_field_intersect( 'page_background_color_in_box', $page_id );
		if ( ! empty( $boxed_page_background_color ) ) {
			$boxed_background_style['background-color'] = $boxed_page_background_color;
		}
		
		$boxed_page_background_image = evently_mikado_get_meta_field_intersect( 'boxed_background_image', $page_id );
		if ( ! empty( $boxed_page_background_image ) ) {
			$boxed_background_style['background-image']    = 'url(' . esc_url( $boxed_page_background_image ) . ')';
			$boxed_background_style['background-position'] = 'center 0px';
			$boxed_background_style['background-repeat']   = 'no-repeat';
		}
		
		$boxed_page_background_pattern_image = evently_mikado_get_meta_field_intersect( 'boxed_pattern_background_image', $page_id );
		if ( ! empty( $boxed_page_background_pattern_image ) ) {
			$boxed_background_style['background-image']    = 'url(' . esc_url( $boxed_page_background_pattern_image ) . ')';
			$boxed_background_style['background-position'] = '0px 0px';
			$boxed_background_style['background-repeat']   = 'repeat';
		}
		
		$boxed_page_background_attachment = evently_mikado_get_meta_field_intersect( 'boxed_background_image_attachment', $page_id );
		if ( ! empty( $boxed_page_background_attachment ) ) {
			$boxed_background_style['background-attachment'] = $boxed_page_background_attachment;
		}
		
		$boxed_background_selector = $class_prefix . '.mkdf-boxed .mkdf-wrapper';
		
		if ( ! empty( $boxed_background_style ) ) {
			$current_style .= evently_mikado_dynamic_css( $boxed_background_selector, $boxed_background_style );
		}
		
		$paspartu_style = array();
		$additional_paspartu_style_h = array();
		$additional_paspartu_style_w = array();
		$headings_style = array();
		
		$paspartu_color = evently_mikado_get_meta_field_intersect( 'paspartu_color', $page_id );
		if ( ! empty( $paspartu_color ) ) {
			$paspartu_style['background-color'] = $paspartu_color;
			$additional_paspartu_style_h['background-color'] = $paspartu_color;
			$additional_paspartu_style_w['background-color'] = $paspartu_color;
		}
		
		$paspartu_width = evently_mikado_get_meta_field_intersect( 'paspartu_width', $page_id );
		if ( $paspartu_width !== '' ) {
			if ( evently_mikado_string_ends_with( $paspartu_width, '%' ) || evently_mikado_string_ends_with( $paspartu_width, 'px' ) ) {
				$paspartu_style['padding'] = $paspartu_width;
				$additional_paspartu_style_h['height'] = $paspartu_width;
				$additional_paspartu_style_w['width']  = $paspartu_width;
				$headings_style['padding-left'] = $paspartu_width;
				$headings_style['padding-right'] = $paspartu_width;
				$headings_style['top'] = $paspartu_width;
			} else {
				$paspartu_style['padding'] = $paspartu_width . 'px';
				$additional_paspartu_style_h['height'] = $paspartu_width . 'px';
				$additional_paspartu_style_w['width']  = $paspartu_width . 'px';;
				$headings_style['padding-left'] = $paspartu_width . 'px';
				$headings_style['padding-right'] = $paspartu_width . 'px';
				$headings_style['top'] = $paspartu_width . 'px';
			}
		}
		
		$paspartu_selector = $class_prefix . '.mkdf-paspartu-enabled .mkdf-wrapper';

		$additional_paspartu_selector_h = array(
			$class_prefix . '.mkdf-paspartu-enabled .mkdf-wrapper:after',
			$class_prefix . '.mkdf-paspartu-enabled .mkdf-wrapper:before'
		);

		$additional_paspartu_selector_w = array(
			$class_prefix . '.mkdf-paspartu-enabled .mkdf-wrapper-inner:after',
			$class_prefix . '.mkdf-paspartu-enabled .mkdf-wrapper-inner:before'
		);

		$headings_selector = array(
			$class_prefix . '.mkdf-paspartu-enabled .mkdf-page-header .mkdf-fixed-wrapper.fixed',
			$class_prefix . '.mkdf-paspartu-enabled .mkdf-sticky-header',
			$class_prefix . '.mkdf-sticky-up-mobile-header.mkdf-paspartu-enabled .mobile-header-appear .mkdf-mobile-header-inner'
		);
		
		if ( ! empty( $paspartu_style ) ) {
			$current_style .= evently_mikado_dynamic_css( $paspartu_selector, $paspartu_style );
			$current_style .= evently_mikado_dynamic_css( $additional_paspartu_selector_h, $additional_paspartu_style_h );
			$current_style .= evently_mikado_dynamic_css( $additional_paspartu_selector_w, $additional_paspartu_style_w );
			$current_style .= evently_mikado_dynamic_css( $headings_selector, $headings_style );
		}
		
		$current_style = $current_style . $style;
		
		return $current_style;
	}
	
	add_filter( 'evently_mikado_filter_add_page_custom_style', 'evently_mikado_page_general_style' );
}