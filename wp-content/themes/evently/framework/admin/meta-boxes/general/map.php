<?php

if ( ! function_exists( 'evently_mikado_map_general_meta' ) ) {
	function evently_mikado_map_general_meta() {
		
		$general_meta_box = evently_mikado_add_meta_box(
			array(
				'scope' => apply_filters( 'evently_mikado_filter_set_scope_for_meta_boxes', array( 'page', 'post' ) ),
				'title' => esc_html__( 'General', 'evently' ),
				'name'  => 'general_meta'
			)
		);
		
		/***************** Slider Layout - begin **********************/
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_page_slider_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Slider Shortcode', 'evently' ),
				'description' => esc_html__( 'Paste your slider shortcode here', 'evently' ),
				'parent'      => $general_meta_box
			)
		);
		
		/***************** Slider Layout - begin **********************/
		
		/***************** Content Layout - begin **********************/
		
		evently_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_page_content_behind_header_meta',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Always put content behind header', 'evently' ),
				'description'   => esc_html__( 'Enabling this option will put page content behind page header', 'evently' ),
				'parent'        => $general_meta_box,
				'args'          => array(
					'suffix' => 'px'
				)
			)
		);
		
		$mkdf_content_padding_group = evently_mikado_add_admin_group(
			array(
				'name'        => 'content_padding_group',
				'title'       => esc_html__( 'Content Style', 'evently' ),
				'description' => esc_html__( 'Define styles for Content area', 'evently' ),
				'parent'      => $general_meta_box
			)
		);
		
			$mkdf_content_padding_row = evently_mikado_add_admin_row(
				array(
					'name'   => 'mkdf_content_padding_row',
					'next'   => true,
					'parent' => $mkdf_content_padding_group
				)
			);
		
				evently_mikado_add_meta_box_field(
					array(
						'name'   => 'mkdf_page_content_top_padding',
						'type'   => 'textsimple',
						'label'  => esc_html__( 'Content Top Padding', 'evently' ),
						'parent' => $mkdf_content_padding_row,
						'args'   => array(
							'suffix' => 'px'
						)
					)
				);
				
				evently_mikado_add_meta_box_field(
					array(
						'name'    => 'mkdf_page_content_top_padding_mobile',
						'type'    => 'selectsimple',
						'label'   => esc_html__( 'Set this top padding for mobile header', 'evently' ),
						'parent'  => $mkdf_content_padding_row,
						'options' => evently_mikado_get_yes_no_select_array( false )
					)
				);
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_page_background_color_meta',
				'type'        => 'color',
				'label'       => esc_html__( 'Page Background Color', 'evently' ),
				'description' => esc_html__( 'Choose background color for page content', 'evently' ),
				'parent'      => $general_meta_box
			)
		);
		
		/***************** Content Layout - end **********************/
		
		/***************** Boxed Layout - begin **********************/
		
		evently_mikado_add_meta_box_field(
			array(
				'name'    => 'mkdf_boxed_meta',
				'type'    => 'select',
				'label'   => esc_html__( 'Boxed Layout', 'evently' ),
				'parent'  => $general_meta_box,
				'options' => evently_mikado_get_yes_no_select_array(),
				'args'    => array(
					'dependence' => true,
					'hide'       => array(
						''    => '#mkdf_boxed_container_meta',
						'no'  => '#mkdf_boxed_container_meta',
						'yes' => ''
					),
					'show'       => array(
						''    => '',
						'no'  => '',
						'yes' => '#mkdf_boxed_container_meta'
					)
				)
			)
		);
		
			$boxed_container_meta = evently_mikado_add_admin_container(
				array(
					'parent'          => $general_meta_box,
					'name'            => 'boxed_container_meta',
					'hidden_property' => 'mkdf_boxed_meta',
					'hidden_values'   => array(
						'',
						'no'
					)
				)
			);
		
				evently_mikado_add_meta_box_field(
					array(
						'name'        => 'mkdf_page_background_color_in_box_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Page Background Color', 'evently' ),
						'description' => esc_html__( 'Choose the page background color outside box', 'evently' ),
						'parent'      => $boxed_container_meta
					)
				);
				
				evently_mikado_add_meta_box_field(
					array(
						'name'        => 'mkdf_boxed_background_image_meta',
						'type'        => 'image',
						'label'       => esc_html__( 'Background Image', 'evently' ),
						'description' => esc_html__( 'Choose an image to be displayed in background', 'evently' ),
						'parent'      => $boxed_container_meta
					)
				);
				
				evently_mikado_add_meta_box_field(
					array(
						'name'        => 'mkdf_boxed_pattern_background_image_meta',
						'type'        => 'image',
						'label'       => esc_html__( 'Background Pattern', 'evently' ),
						'description' => esc_html__( 'Choose an image to be used as background pattern', 'evently' ),
						'parent'      => $boxed_container_meta
					)
				);
				
				evently_mikado_add_meta_box_field(
					array(
						'name'          => 'mkdf_boxed_background_image_attachment_meta',
						'type'          => 'select',
						'default_value' => 'fixed',
						'label'         => esc_html__( 'Background Image Attachment', 'evently' ),
						'description'   => esc_html__( 'Choose background image attachment', 'evently' ),
						'parent'        => $boxed_container_meta,
						'options'       => array(
							''       => esc_html__( 'Default', 'evently' ),
							'fixed'  => esc_html__( 'Fixed', 'evently' ),
							'scroll' => esc_html__( 'Scroll', 'evently' )
						)
					)
				);
		
		/***************** Boxed Layout - end **********************/
		
		/***************** Passepartout Layout - begin **********************/
		
		evently_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_paspartu_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Passepartout', 'evently' ),
				'description'   => esc_html__( 'Enabling this option will display passepartout around site content', 'evently' ),
				'parent'        => $general_meta_box,
				'options'       => evently_mikado_get_yes_no_select_array(),
				'args'    => array(
					'dependence'    => true,
					'hide'          => array(
						''    => '#mkdf_mkdf_paspartu_container_meta',
						'no'  => '#mkdf_mkdf_paspartu_container_meta',
						'yes' => ''
					),
					'show'          => array(
						''    => '',
						'no'  => '',
						'yes' => '#mkdf_mkdf_paspartu_container_meta'
					)
				)
			)
		);
		
			$paspartu_container_meta = evently_mikado_add_admin_container(
				array(
					'parent'          => $general_meta_box,
					'name'            => 'mkdf_paspartu_container_meta',
					'hidden_property' => 'mkdf_paspartu_meta',
					'hidden_values'   => array(
						'',
						'no'
					)
				)
			);
		
				evently_mikado_add_meta_box_field(
					array(
						'name'        => 'mkdf_paspartu_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Passepartout Color', 'evently' ),
						'description' => esc_html__( 'Choose passepartout color, default value is #ffffff', 'evently' ),
						'parent'      => $paspartu_container_meta
					)
				);
				
				evently_mikado_add_meta_box_field(
					array(
						'name'        => 'mkdf_paspartu_width_meta',
						'type'        => 'text',
						'label'       => esc_html__( 'Passepartout Size', 'evently' ),
						'description' => esc_html__( 'Enter size amount for passepartout', 'evently' ),
						'parent'      => $paspartu_container_meta,
						'args'        => array(
							'col_width' => 2,
							'suffix'    => 'px or %'
						)
					)
				);
				
				evently_mikado_add_meta_box_field(
					array(
						'parent'        => $paspartu_container_meta,
						'type'          => 'select',
						'default_value' => '',
						'name'          => 'mkdf_disable_top_paspartu_meta',
						'label'         => esc_html__( 'Disable Top Passepartout', 'evently' ),
						'options'       => evently_mikado_get_yes_no_select_array(),
					)
				);
		
		/***************** Passepartout Layout - end **********************/
		
		/***************** Content Width Layout - begin **********************/
		
		evently_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_initial_content_width_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Initial Width of Content', 'evently' ),
				'description'   => esc_html__( 'Choose the initial width of content which is in grid (Applies to pages set to "Default Template" and rows set to "In Grid")', 'evently' ),
				'parent'        => $general_meta_box,
				'options'       => array(
					''                => esc_html__( 'Default', 'evently' ),
					'mkdf-grid-1100' => esc_html__( '1100px', 'evently' ),
					'mkdf-grid-1300' => esc_html__( '1300px', 'evently' ),
					'mkdf-grid-1200' => esc_html__( '1200px', 'evently' ),
					'mkdf-grid-1000' => esc_html__( '1000px', 'evently' ),
					'mkdf-grid-800'  => esc_html__( '800px', 'evently' )
				)
			)
		);
		
		/***************** Content Width Layout - end **********************/
		
		/***************** Smooth Page Transitions Layout - begin **********************/
		
		evently_mikado_add_meta_box_field(
			array(
				'name'          => 'mkdf_smooth_page_transitions_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Smooth Page Transitions', 'evently' ),
				'description'   => esc_html__( 'Enabling this option will perform a smooth transition between pages when clicking on links', 'evently' ),
				'parent'        => $general_meta_box,
				'options'       => evently_mikado_get_yes_no_select_array(),
				'args'          => array(
					'dependence' => true,
					'hide'       => array(
						''    => '#mkdf_page_transitions_container_meta',
						'no'  => '#mkdf_page_transitions_container_meta',
						'yes' => ''
					),
					'show'       => array(
						''    => '',
						'no'  => '',
						'yes' => '#mkdf_page_transitions_container_meta'
					)
				)
			)
		);
		
			$page_transitions_container_meta = evently_mikado_add_admin_container(
				array(
					'parent'          => $general_meta_box,
					'name'            => 'page_transitions_container_meta',
					'hidden_property' => 'mkdf_smooth_page_transitions_meta',
					'hidden_values'   => array(
						'',
						'no'
					)
				)
			);
		
				evently_mikado_add_meta_box_field(
					array(
						'name'        => 'mkdf_page_transition_preloader_meta',
						'type'        => 'select',
						'label'       => esc_html__( 'Enable Preloading Animation', 'evently' ),
						'description' => esc_html__( 'Enabling this option will display an animated preloader while the page content is loading', 'evently' ),
						'parent'      => $page_transitions_container_meta,
						'options'     => evently_mikado_get_yes_no_select_array(),
						'args'        => array(
							'dependence' => true,
							'hide'       => array(
								''    => '#mkdf_page_transition_preloader_container_meta',
								'no'  => '#mkdf_page_transition_preloader_container_meta',
								'yes' => ''
							),
							'show'       => array(
								''    => '',
								'no'  => '',
								'yes' => '#mkdf_page_transition_preloader_container_meta'
							)
						)
					)
				);
				
				$page_transition_preloader_container_meta = evently_mikado_add_admin_container(
					array(
						'parent'          => $page_transitions_container_meta,
						'name'            => 'page_transition_preloader_container_meta',
						'hidden_property' => 'mkdf_page_transition_preloader_meta',
						'hidden_values'   => array(
							'',
							'no'
						)
					)
				);
				
					evently_mikado_add_meta_box_field(
						array(
							'name'   => 'mkdf_smooth_pt_bgnd_color_meta',
							'type'   => 'color',
							'label'  => esc_html__( 'Page Loader Background Color', 'evently' ),
							'parent' => $page_transition_preloader_container_meta
						)
					);
					
					$group_pt_spinner_animation_meta = evently_mikado_add_admin_group(
						array(
							'name'        => 'group_pt_spinner_animation_meta',
							'title'       => esc_html__( 'Loader Style', 'evently' ),
							'description' => esc_html__( 'Define styles for loader spinner animation', 'evently' ),
							'parent'      => $page_transition_preloader_container_meta
						)
					);
					
					$row_pt_spinner_animation_meta = evently_mikado_add_admin_row(
						array(
							'name'   => 'row_pt_spinner_animation_meta',
							'parent' => $group_pt_spinner_animation_meta
						)
					);
					
					evently_mikado_add_meta_box_field(
						array(
							'type'    => 'selectsimple',
							'name'    => 'mkdf_smooth_pt_spinner_type_meta',
							'label'   => esc_html__( 'Spinner Type', 'evently' ),
							'parent'  => $row_pt_spinner_animation_meta,
							'options' => array(
								''                      => esc_html__( 'Default', 'evently' ),
								'rotate_circles'        => esc_html__( 'Rotate Circles', 'evently' ),
								'pulse'                 => esc_html__( 'Pulse', 'evently' ),
								'double_pulse'          => esc_html__( 'Double Pulse', 'evently' ),
								'cube'                  => esc_html__( 'Cube', 'evently' ),
								'rotating_cubes'        => esc_html__( 'Rotating Cubes', 'evently' ),
								'stripes'               => esc_html__( 'Stripes', 'evently' ),
								'wave'                  => esc_html__( 'Wave', 'evently' ),
								'two_rotating_circles'  => esc_html__( '2 Rotating Circles', 'evently' ),
								'five_rotating_circles' => esc_html__( '5 Rotating Circles', 'evently' ),
								'atom'                  => esc_html__( 'Atom', 'evently' ),
								'clock'                 => esc_html__( 'Clock', 'evently' ),
								'mitosis'               => esc_html__( 'Mitosis', 'evently' ),
								'lines'                 => esc_html__( 'Lines', 'evently' ),
								'fussion'               => esc_html__( 'Fussion', 'evently' ),
								'wave_circles'          => esc_html__( 'Wave Circles', 'evently' ),
								'pulse_circles'         => esc_html__( 'Pulse Circles', 'evently' )
							)
						)
					);
					
					evently_mikado_add_meta_box_field(
						array(
							'type'   => 'colorsimple',
							'name'   => 'mkdf_smooth_pt_spinner_color_meta',
							'label'  => esc_html__( 'Spinner Color', 'evently' ),
							'parent' => $row_pt_spinner_animation_meta
						)
					);
					
					evently_mikado_add_meta_box_field(
						array(
							'name'        => 'mkdf_page_transition_fadeout_meta',
							'type'        => 'select',
							'label'       => esc_html__( 'Enable Fade Out Animation', 'evently' ),
							'description' => esc_html__( 'Enabling this option will turn on fade out animation when leaving page', 'evently' ),
							'options'     => evently_mikado_get_yes_no_select_array(),
							'parent'      => $page_transitions_container_meta
						
						)
					);
		
		/***************** Smooth Page Transitions Layout - end **********************/
		
		/***************** Comments Layout - begin **********************/
		
		evently_mikado_add_meta_box_field(
			array(
				'name'        => 'mkdf_page_comments_meta',
				'type'        => 'select',
				'label'       => esc_html__( 'Show Comments', 'evently' ),
				'description' => esc_html__( 'Enabling this option will show comments on your page', 'evently' ),
				'parent'      => $general_meta_box,
				'options'     => evently_mikado_get_yes_no_select_array()
			)
		);
		
		/***************** Comments Layout - end **********************/
	}
	
	add_action( 'evently_mikado_action_meta_boxes_map', 'evently_mikado_map_general_meta', 10 );
}