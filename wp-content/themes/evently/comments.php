<?php
if ( post_password_required() ) {
	return;
}

if ( comments_open() || get_comments_number()) { ?>
	<div class="mkdf-comment-holder clearfix" id="comments">
		<?php if ( have_comments() ) { ?>
			<div class="mkdf-comment-holder-inner">
				<div class="mkdf-comments-title">
					<h5><?php esc_html_e('Comments', 'evently' ); ?></h5>
				</div>
				<div class="mkdf-comments">
					<ul class="mkdf-comment-list">
						<?php wp_list_comments( array_unique( array_merge( array( 'callback' => 'evently_mikado_comment' ), apply_filters( 'evently_mikado_filter_comments_callback', array() ) ) ) ); ?>
					</ul>
				</div>
			</div>
		<?php } ?>
		<?php if( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' )) { ?>
			<p><?php esc_html_e('Sorry, the comment form is closed at this time.', 'evently'); ?></p>
		<?php } ?>
	</div>
	<?php
		$mkdf_commenter = wp_get_current_commenter();
		$mkdf_req = get_option( 'require_name_email' );
		$mkdf_aria_req = ( $mkdf_req ? " aria-required='true'" : '' );

		$mkdf_args = array(
			'id_form' => 'commentform',
			'id_submit' => 'submit_comment',
			'title_reply'=> esc_html__( 'Post a Comment','evently' ),
			'title_reply_before' => '<span id="reply-title" class="comment-reply-title">',
			'title_reply_after' => '</span>',
			'title_reply_to' => esc_html__( 'Post a Reply to %s','evently' ),
			'cancel_reply_link' => esc_html__( 'cancel reply','evently' ),
			'label_submit' => esc_html__( 'Submit','evently' ),
			'comment_field' => apply_filters( 'evently_mikado_filter_comment_form_textarea_field', '<textarea id="comment" placeholder="'.esc_html__( 'Your comment','evently' ).'" name="comment" cols="45" rows="6" aria-required="true"></textarea>'),
			'comment_notes_before' => '',
			'comment_notes_after' => '',
			'fields' => apply_filters( 'evently_mikado_filter_comment_form_default_fields', array(
				'author' => '<label>'. esc_html__( 'Name*', 'evently' ) .'<input id="author" name="author" type="text" placeholder="'. esc_html__( 'Your full name', 'evently' ) .'" value="' . esc_attr( $mkdf_commenter['comment_author'] ) . '"' . $mkdf_aria_req . ' /></label>',
				'email'  => '<label>'. esc_html__( 'Email*', 'evently' ) .'<input id="email" name="email" type="text" placeholder="'. esc_html__( 'Your email address', 'evently' ) .'" value="' . esc_attr(  $mkdf_commenter['comment_author_email'] ) . '"' . $mkdf_aria_req . ' /></label>',
				'url'    => '<label>'. esc_html__( 'Website', 'evently' ) .'<input id="url" name="url" type="text" placeholder="'. esc_html__( 'Your website', 'evently' ) .'" value="' . esc_attr(  $mkdf_commenter['comment_author_url'] ) . '" size="30" maxlength="200" /></label>'
			 ) ) );
	 ?>
	<?php if(get_comment_pages_count() > 1){ ?>
		<div class="mkdf-comment-pager">
			<p><?php paginate_comments_links(); ?></p>
		</div>
	<?php } ?>

	<div class="mkdf-comment-form">
		<div class="mkdf-comment-form-inner">
			<?php comment_form($mkdf_args); ?>
		</div>
	</div>
<?php } ?>	