1.4
- Added WooCommerce 3.4.5 compatibility
- Added compatibility with Gutenberg plugin
- Updated WPBakery Page Builder to 5.5.4
- Updated Revolution Slider to 5.4.8

1.3
- Added WooCommerce 3.3.5 compatibility
- Added Envato Market plugin as required
- Updated Visual Composer to 5.4.7
- Updated Revolution Slider to 5.4.7.3
- Updated Mikado Core plugin to version 1.0.1
- Fixed Read More link on Vertical Team list
- Improved menu import functionality

1.2
- Updated WPBakery Page Builder to 5.3
- Updated Slider Revolution to 5.4.6
- Updated Timetable Responsive Schedule For WordPress to 4.0

1.1.1
- Fixed Countdown shortcode days counter

1.1
- Updated Visual Composer to 5.2.1
- Fixed potential security issue when saving theme options
- Fixed Banner hover style
- Fixed Mobile menu responsiveness